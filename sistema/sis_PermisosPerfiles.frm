VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form sis_PermisosPerfiles 
   Caption         =   "Control de acceso a menus"
   ClientHeight    =   8580
   ClientLeft      =   960
   ClientTop       =   2100
   ClientWidth     =   9840
   LinkTopic       =   "Form1"
   ScaleHeight     =   8580
   ScaleWidth      =   9840
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   825
      Top             =   8355
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   90
      OleObjectBlob   =   "sis_PermisosPerfiles.frx":0000
      Top             =   8295
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   465
      Left            =   8115
      TabIndex        =   4
      Top             =   7920
      Width           =   1185
   End
   Begin VB.Frame Frame2 
      Caption         =   "Menu"
      Height          =   6345
      Left            =   465
      TabIndex        =   2
      Top             =   1485
      Width           =   8820
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   195
         Left            =   135
         TabIndex        =   6
         ToolTipText     =   "Invertir Seleccion"
         Top             =   390
         Width           =   180
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5730
         Left            =   90
         TabIndex        =   3
         Top             =   345
         Width           =   8550
         _ExtentX        =   15081
         _ExtentY        =   10107
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T4500"
            Text            =   "Seccion"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T4000"
            Text            =   "Menu"
            Object.Width           =   7056
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Perfiles activos"
      Height          =   810
      Left            =   465
      TabIndex        =   0
      Top             =   525
      Width           =   8775
      Begin VB.CommandButton cmdAplicar 
         Caption         =   "Aplicar cambios"
         Height          =   450
         Left            =   6945
         TabIndex        =   5
         ToolTipText     =   "Habilita los menus marcados al perfil seleccionado"
         Top             =   270
         Width           =   1530
      End
      Begin VB.ComboBox CboPerfiles 
         Height          =   315
         Left            =   165
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   6600
      End
   End
End
Attribute VB_Name = "sis_PermisosPerfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CboPerfiles_Click()
    Check1.Value = 0
    Sql = "SELECT men_id " & _
          "FROM sis_accesos " & _
          "WHERE per_id=" & CboPerfiles.ItemData(CboPerfiles.ListIndex)
    Consulta RsTmp, Sql
    
    For i = 1 To LvDetalle.ListItems.Count
            LvDetalle.ListItems(i).Checked = False
        Next
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            Set itmFound = LvDetalle.FindItem(RsTmp!men_id, lvwNumeric, , lvwWhole)
            If itmFound Is Nothing Then
                'MsgBox "No se ha encontrado ninguna coincidencia"
                'Exit Sub
             Else
                itmFound.Selected = True
                LvDetalle.SelectedItem.Checked = True
            End If
            RsTmp.MoveNext
        Loop
    End If
End Sub

Private Sub Check1_Click()
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            LvDetalle.ListItems(i).Checked = False
        Else
            LvDetalle.ListItems(i).Checked = True
        End If
    Next
End Sub

Private Sub cmdAplicar_Click()
    Dim Sp_Rel As String
    
    
    If CboPerfiles.ListIndex = -1 Then Exit Sub
    
    On Error GoTo ErrorAplica
    cn.BeginTrans
        Sql = "DELETE FROM sis_accesos WHERE per_id=" & CboPerfiles.ItemData(CboPerfiles.ListIndex)
        cn.Execute Sql
        Sql = "INSERT INTO sis_accesos (per_id,men_id) VALUES "
        Sp_Rel = Empty
        For i = 1 To LvDetalle.ListItems.Count
            If LvDetalle.ListItems(i).Checked Then
                Sp_Rel = Sp_Rel & "(" & CboPerfiles.ItemData(CboPerfiles.ListIndex) & "," & LvDetalle.ListItems(i).Text & "),"
            End If
        Next
        If Sp_Rel = Empty Then Exit Sub
        Sp_Rel = Mid(Sp_Rel, 1, Len(Sp_Rel) - 1)
        cn.Execute Sql & Sp_Rel
    cn.CommitTrans
    MsgBox "Cambios Aplicados correctamente.", vbInformation
    
    Exit Sub
ErrorAplica:
    cn.RollbackTrans
    MsgBox "Actualizacion cancelada" & vbNewLine & Err.Number & vbNewLine & Err.Description
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    LLenarCombo CboPerfiles, "per_nombre", "per_id", "sis_perfiles", "per_activo='SI'", "per_id"
    
    'Carga menus
    Sql = "SELECT men_id,IFNULL((SELECT men_nombre FROM sis_menu s WHERE m.men_padre=s.men_id),'PRINCIPAL') padre,men_nombre " & _
          "FROM sis_menu m " & _
          "WHERE m.men_activo='SI' " & _
          "ORDER BY men_padre"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    
End Sub


Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

'Private Sub LVDetalle_BeforeLabelEdit(Cancel As Integer)
'    ordListView ColumnHeader, Me, LvDetalle
'End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    Me.CboPerfiles.SetFocus
    Timer1.Enabled = False
End Sub
