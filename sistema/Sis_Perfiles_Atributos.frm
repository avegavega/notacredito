VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Begin VB.Form Sis_Perfiles_Atributos 
   Caption         =   "Atributos de perfiles"
   ClientHeight    =   8160
   ClientLeft      =   3225
   ClientTop       =   1665
   ClientWidth     =   9285
   LinkTopic       =   "Form1"
   ScaleHeight     =   8160
   ScaleWidth      =   9285
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   855
      Top             =   7860
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Retornar"
      Height          =   405
      Left            =   7695
      TabIndex        =   4
      Top             =   7620
      Width           =   1320
   End
   Begin VB.Frame Frame1 
      Caption         =   "Perfiles activos"
      Height          =   810
      Left            =   210
      TabIndex        =   1
      Top             =   195
      Width           =   8775
      Begin VB.ComboBox CboPerfiles 
         Height          =   315
         Left            =   165
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   360
         Width           =   8400
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Atributos"
      Height          =   6345
      Left            =   210
      TabIndex        =   0
      Top             =   1155
      Width           =   8820
      Begin VB.Frame Frame3 
         Caption         =   "Editar Precios en la Venta"
         Height          =   1170
         Left            =   465
         TabIndex        =   5
         Top             =   375
         Width           =   7980
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Left            =   4290
            OleObjectBlob   =   "Sis_Perfiles_Atributos.frx":0000
            TabIndex        =   11
            Top             =   855
            Width           =   3105
         End
         Begin VB.TextBox TxtRecargoMax 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   6075
            MaxLength       =   2
            TabIndex        =   10
            Text            =   "0"
            Top             =   495
            Width           =   930
         End
         Begin VB.TextBox TxtDsctoMax 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   4650
            MaxLength       =   2
            TabIndex        =   8
            Text            =   "0"
            Top             =   495
            Width           =   930
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   270
            Left            =   4635
            OleObjectBlob   =   "Sis_Perfiles_Atributos.frx":00B2
            TabIndex        =   7
            Top             =   285
            Width           =   990
         End
         Begin VB.CheckBox ChkDscto 
            Caption         =   "Puede modificar Precio de Venta"
            Height          =   225
            Left            =   285
            TabIndex        =   6
            Top             =   510
            Width           =   2625
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   270
            Left            =   5985
            OleObjectBlob   =   "Sis_Perfiles_Atributos.frx":0126
            TabIndex        =   9
            Top             =   285
            Width           =   1155
         End
      End
      Begin VB.CommandButton cmdAplicar 
         Caption         =   "Guardar Atriburos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   90
         TabIndex        =   3
         ToolTipText     =   "Habilita los menus marcados al perfil seleccionado"
         Top             =   5805
         Width           =   1800
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   120
      OleObjectBlob   =   "Sis_Perfiles_Atributos.frx":019C
      Top             =   7800
   End
End
Attribute VB_Name = "Sis_Perfiles_Atributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check1_Click()

End Sub

Private Sub CboPerfiles_Click()

    Sql = "SELECT pde_permite,pde_descuento_maximo,pde_recargo_maximo " & _
            "FROM par_perfiles_atributo_descuento " & _
            "WHERE per_id=" & CboPerfiles.ItemData(CboPerfiles.ListIndex) & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!pde_permite = "SI" Then
            ChkDscto.Value = 1
            Me.TxtDsctoMax = RsTmp!pde_descuento_maximo
            Me.TxtRecargoMax = RsTmp!pde_recargo_maximo
        Else
            ChkDscto.Value = 0
            Me.TxtDsctoMax = 0
            Me.TxtRecargoMax = 0
        End If
    
    End If

End Sub

Private Sub ChkDscto_Click()
    If ChkDscto.Value = 0 Then
        TxtDsctoMax = 0
        TxtRecargoMax = 0
    End If
End Sub

Private Sub cmdAplicar_Click()
    Dim Sp_Permite_Descuento  As String
    On Error GoTo ProblemaGrabar
    
    If Me.CboPerfiles.ListIndex = -1 Then Exit Sub
    If ChkDscto.Value = 0 Then Sp_Permite_Descuento = "NO" Else Sp_Permite_Descuento = "SI"
    If Val(TxtDsctoMax) = 0 Then TxtDsctoMax = "0"
    If Val(TxtRecargoMax) = 0 Then TxtRecargoMax = "0"
    
    cn.BeginTrans
        Sql = "DELETE FROM par_perfiles_atributo_descuento " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND per_id=" & CboPerfiles.ItemData(CboPerfiles.ListIndex)
        cn.Execute Sql
        
        
        Sql = "INSERT INTO par_perfiles_atributo_descuento (rut_emp,per_id,pde_permite,pde_descuento_maximo,pde_recargo_maximo)" & _
                "VALUES('" & SP_Rut_Activo & "'," & CboPerfiles.ItemData(CboPerfiles.ListIndex) & ",'" & Sp_Permite_Descuento & "'," & Me.TxtDsctoMax & "," & Me.TxtRecargoMax & ")"

        cn.Execute Sql
    
    
    
    cn.CommitTrans
    MsgBox "Atributos aplicados correctamente...", vbInformation
    Exit Sub
ProblemaGrabar:
    MsgBox "Problema al grabar..." & vbNewLine & Err.Description, vbExclamation
    cn.RollbackTrans
    
    
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Centrar Me
    Aplicar_skin Me
    LLenarCombo CboPerfiles, "per_nombre", "per_id", "sis_perfiles", "per_activo='SI'", "per_id"
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    Me.CboPerfiles.SetFocus
    Timer1.Enabled = False
End Sub
Private Sub TxtDsctoMax_GotFocus()
    En_Foco TxtDsctoMax
End Sub

Private Sub TxtDsctoMax_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDsctoMax_Validate(Cancel As Boolean)
    If Val(TxtDsctoMax) = 0 Then TxtDsctoMax = "0"
End Sub

Private Sub TxtRecargoMax_GotFocus()
    En_Foco TxtRecargoMax
End Sub

Private Sub TxtRecargoMax_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRecargoMax_Validate(Cancel As Boolean)
    If Val(TxtRecargoMax) = 0 Then TxtRecargoMax = "0"
End Sub
