VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form BuscaEmpresa 
   Caption         =   "Buscar Empresa"
   ClientHeight    =   6075
   ClientLeft      =   1410
   ClientTop       =   2010
   ClientWidth     =   8985
   LinkTopic       =   "Form1"
   ScaleHeight     =   6075
   ScaleWidth      =   8985
   Begin VB.Frame Frame2 
      Caption         =   "Buscando empresa"
      Height          =   5340
      Left            =   255
      TabIndex        =   1
      Top             =   240
      Width           =   8445
      Begin VB.Frame Frame1 
         Caption         =   "Filtro"
         Height          =   855
         Left            =   5400
         TabIndex        =   7
         Top             =   255
         Width           =   2700
         Begin VB.OptionButton Option2 
            Caption         =   "Nombre Empresa"
            Height          =   195
            Left            =   690
            TabIndex        =   9
            Top             =   585
            Width           =   1605
         End
         Begin VB.OptionButton Option1 
            Caption         =   "RUT"
            Height          =   255
            Left            =   690
            TabIndex        =   8
            Top             =   240
            Value           =   -1  'True
            Width           =   1515
         End
      End
      Begin VB.CommandButton CmdSeleccionar 
         Caption         =   "&Seleccionar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   135
         TabIndex        =   4
         Top             =   4680
         Width           =   1815
      End
      Begin VB.TextBox TxtBusqueda 
         Height          =   375
         Left            =   135
         TabIndex        =   3
         Top             =   735
         Width           =   3855
      End
      Begin VB.CommandButton CmdTodos 
         Caption         =   "Todos"
         Height          =   390
         Left            =   4005
         TabIndex        =   2
         Top             =   735
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   375
         Left            =   120
         OleObjectBlob   =   "BuscaEmpresa.frx":0000
         TabIndex        =   5
         Top             =   345
         Width           =   2655
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3315
         Left            =   120
         TabIndex        =   6
         Top             =   1260
         Width           =   8190
         _ExtentX        =   14446
         _ExtentY        =   5847
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Text            =   "RUT"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "EMPRESA"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Control Inventario"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Id empresa"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   7635
      TabIndex        =   0
      Top             =   5625
      Width           =   1095
   End
   Begin VB.Timer Timer1 
      Interval        =   30
      Left            =   255
      Top             =   5670
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   150
      OleObjectBlob   =   "BuscaEmpresa.frx":006D
      Top             =   6630
   End
End
Attribute VB_Name = "BuscaEmpresa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RsEmpresas As Recordset
Dim Sm_FiltraUsuario As String

Private Sub CmdSalir_Click()
    SG_codigo = Empty
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem.SubItems(3)
    Unload Me
End Sub

Private Sub CmdTodos_Click()
    Sql = "SELECT rut,nombre_empresa,control_inventario,emp_id " & _
          "FROM sis_empresas " & _
          "WHERE activo='SI' "
    CargaLista
End Sub

Private Sub Form_Load()
    Sm_FiltraUsuario = ""
    Aplicar_skin Me, App.Path
    If SG_Usuario_Indivual_Empresa = "SI" Then
        If LogIdUsuario <> 7 Then ' cecilia
            Sm_FiltraUsuario = " AND rut NOT IN('12.307.912-4') "
        End If
    End If
    
    Sql = "SELECT rut,nombre_empresa,control_inventario,emp_id " & _
          "FROM sis_empresas " & _
          "WHERE activo='SI' " & Sm_FiltraUsuario
     CargaLista
    
End Sub
Private Sub CargaLista()
    Consulta RsEmpresas, Sql
    LLenar_Grilla RsEmpresas, Me, LvDetalle, False, True, True, False
End Sub
Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    SG_codigo = LvDetalle.SelectedItem.SubItems(3)
    Unload Me
End Sub

Private Sub Timer1_Timer()
    TxtBusqueda.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub TxtBusqueda_Change()

    If Option2.Value Then
        Sm_filtro = " AND nombre_empresa  LIKE '" & Me.TxtBusqueda.Text & "%'"
    Else
        Sm_filtro = " AND rut LIKE '" & Me.TxtBusqueda.Text & "%'"
    End If
    If Len(TxtBusqueda) = 0 Then Sm_filtro = Empty
    If Len(Me.TxtBusqueda.Text) = 0 Then
        'Me.AdoProducto.Recordset.Filter = 0
    Else
    Sql = "SELECT rut,nombre_empresa,control_inventario,emp_id " & _
          "FROM sis_empresas " & _
          "WHERE activo='SI'" & Sm_filtro & Sm_FiltraUsuario
        

    CargaLista
    End If
End Sub

Private Sub TxtBusqueda_GotFocus()
    En_Foco TxtBusqueda
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii))) 'Transformo a mayuscula el caracter ingresado
     If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
