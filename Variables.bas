Attribute VB_Name = "Variables"
Public DM_Comision_Boton_Especial As Boolean
Public paso As Variant
Public LogUsuario As String
Public LogPass As String
Public LogPerfil As Integer
Public RSUsuarios As Recordset
Public PosicionCombo As Integer
Public RutEmpresa As String
Public RsEmpresa As String
Public DireccionEmpresa As String
Public GiroEmpresa As String
Public AccionProducto As Integer
Public AccionProveedor As Integer
Public AccionCliente As Integer
Public Filtro As String
Public DesdeCompra As Boolean
Public Respuesta As Variant
Public RegSeleccionado As Boolean
Public rutProveedor As Boolean
Public RutBuscado As String
Public RutVerdadero As String
Public NuevoRut As String
Public ClrCfoco As String
Public ClrSfoco As String
Public ClrDesha As String
Public ClienteEncontrado As Boolean
Public RealizaPago As Boolean
Public VDdoc As String
Public Sql As String
Public sql2 As String
Public Sp_Servidor As String
Public BG_FalloConexion As Boolean




Public QueImpresora As Integer
Public ImpresoraFacturas As String
Public ImpresoraBoletas As String
Public ImpresoraInformes As String
Public Connection_String As String
Public cn As ADODB.Connection 'variable para la base de datos y el record
Public RutAnalizar As String
Public ClienteOproveedor As String
Public TipoDocumentoVenta As String
Public SaldarInmediato As Boolean
Public ElNumeroDoc As Double
Public CargaEnseguida As Boolean
Public TextoDelCredito As String
Public ElDetalleCheque As String
Public VentaEspecial As Boolean
Public Archivo As String
'Recordset
Public RsTmp As Recordset
Public RsTmp2 As Recordset
Public RsCom As Recordset
Public RsTmp3 As Recordset
Public objExcel As Excel.Application
'boton espcial
Public EsCosto As String
Public EsDescp As String
Public EsPrecio As String
Public EsCodigo As String
Public EsComision As String
Public Graba As Boolean

Public IP_familia As Integer
Public IP_Doc_ID As Integer
Public lp_Recargo As Double
Public SG_codigo As String
Public SG_codigo2 As String
Public SG_codigo3 As String
Public G_Filtro_Fecha As String
Public DG_IVA As Double
Public IG_documentoIvaRetenido As Integer ' id del documento que lleva iva retenido
Public DG_ID_Unico As Double ' id unico multiples tabblas
Public IG_id_OT As Integer ' id del documento OT

Public IG_id_Nota_Credito As Integer ' Id del documento Nota de Credito
Public IG_id_Nota_Debito As Integer ' Id del documento Nota de debito
Public IG_id_Nota_Credito_Electronica As Integer ' Id del documento Nota de Credito
Public IG_id_Nota_Debito_Electronica As Integer ' Id del documento Nota de debito
Public IG_id_Factura_Manual As Integer ' Id de la factura
Public IG_id_Boleta_Manual As Integer ' Id de boleta
Public IG_id_Nota_Credito_Clientes As Integer ' Nota de Credito a clientes
Public IG_id_Nota_Debito_Clientes As Integer ' Nota de DEBITO a clientes
Public IG_id_GuiaProveedor As Integer ' Id Unico de Guias Proveedores
Public IG_id_GuiaClientes As Integer ' Id Unico de Guias Clientes
Public l_Pcompra As Double ' precio compra kardex - compras
Public IG_id_OrdenCompra As Long 'id de orden de compra
Public IG_Id_DocFondosRendir As Integer
Public bm_NoTimer As Boolean

Public IG_Ano_Contable As Integer
Public IG_Mes_Contable As Integer

Public Sp_extra As String
Public IP_IDMenu As Integer
Public LP_FactorValorFuturo As Long
Public SP_Rut_Activo As String
Public SP_Empresa_Activa As String
Public SP_Control_Inventario As String
Public SP_Nombre_Equipo As String
Public SP_Cuentas_Reservadas As String
Public SP_Facturas_De_Ventas As String
Public SG_Funcion_Equipo As String
Public IG_Version As Integer
Public Sm_ImpresoraSeleccionada As String
Public Sp_RutaDescarga As String
Public IG_IdCtaProveedores As Integer
Public IG_IdCtaClientes As Integer
Public IG_IdCtaFondo As Integer

Public SG_Presentacion_Factura As String
Public SG_Codigos_Alfanumericos As String * 2

' Declaraci�n de la Funci�n Api SendMessage, para odenar el listview asc o desc
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" ( _
    ByVal Hwnd As Long, _
    ByVal wMsg As Long, _
    ByVal wParam As Long, _
    lParam As Any) As Long
  
Public Const WM_SETREDRAW As Long = &HB& 'necesesario para odenar el listview asc o desc

Sub Main()
    'If Crear_DsN("Citroen", "190.121.87.42", "db_citroen", "MySQL ODBC " & "5.1" & " Driver", "citroen") Then
    '    Mdrv = "MySQL ODBC " & mODBC & " Driver"
    'Else
    '    MsgBox "No se pudo instalar el DNS, verificar que esta instalado el MyOdbc " & mODBC, vbCritical
    '    End
    'End If
'
    ConexionBD
    
    On Error GoTo Fallo
    Consulta RsTmp, "SELECT par_id,par_valor FROM tabla_parametros WHERE par_activo='SI'"
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
            If RsTmp!par_id = 1 Then DG_IVA = RsTmp!par_valor
            If RsTmp!par_id = 2 Then IG_documentoIvaRetenido = RsTmp!par_valor
            If RsTmp!par_id = 3 Then IG_id_OT = RsTmp!par_valor
            If RsTmp!par_id = 4 Then IG_id_Nota_Credito = RsTmp!par_valor
            If RsTmp!par_id = 5 Then IG_id_Nota_Debito = RsTmp!par_valor
            If RsTmp!par_id = 6 Then IG_id_Nota_Credito_Electronica = RsTmp!par_valor
            If RsTmp!par_id = 7 Then IG_id_Nota_Debito_Electronica = RsTmp!par_valor
            If RsTmp!par_id = 8 Then IG_id_Factura_Manual = RsTmp!par_valor
            If RsTmp!par_id = 9 Then IG_id_Boleta_Manual = RsTmp!par_valor
            If RsTmp!par_id = 10 Then IG_id_Nota_Credito_Clientes = RsTmp!par_valor
            If RsTmp!par_id = 11 Then IG_id_GuiaProveedor = RsTmp!par_valor
            If RsTmp!par_id = 12 Then IG_id_GuiaClientes = RsTmp!par_valor
            If RsTmp!par_id = 13 Then IG_Ano_Contable = RsTmp!par_valor
            If RsTmp!par_id = 14 Then IG_Mes_Contable = RsTmp!par_valor
            If RsTmp!par_id = 15 Then IG_id_Nota_Debito_Clientes = RsTmp!par_valor
            If RsTmp!par_id = 16 Then LP_FactorValorFuturo = RsTmp!par_valor
            If RsTmp!par_id = 17 Then SP_Rut_Activo = RsTmp!par_valor
            If RsTmp!par_id = 18 Then SP_Empresa_Activa = RsTmp!par_valor
            If RsTmp!par_id = 19 Then SP_Control_Inventario = RsTmp!par_valor
            If RsTmp!par_id = 20 Then SP_Cuentas_Reservadas = RsTmp!par_valor
            If RsTmp!par_id = 21 Then SP_Facturas_De_Ventas = RsTmp!par_valor
            If RsTmp!par_id = 22 Then IG_id_OrdenCompra = RsTmp!par_valor
            If RsTmp!par_id = 100 Then IG_Version = RsTmp!par_valor
            If RsTmp!par_id = 300 Then Sp_RutaDescarga = RsTmp!par_valor
            If RsTmp!par_id = 400 Then IG_IdCtaProveedores = RsTmp!par_valor 'ID Cta Proveedores
            If RsTmp!par_id = 401 Then IG_IdCtaClientes = RsTmp!par_valor 'ID Cta CLIENTES
            If RsTmp!par_id = 402 Then IG_IdCtaFondo = RsTmp!par_valor 'ID Cta FONDO POR RENDIR
            If RsTmp!par_id = 500 Then SG_Codigos_Alfanumericos = RsTmp!par_valor 'ESTO ES SI EL CODIGO DE PRODUCTOS SERA ALFANUMERICO
            If RsTmp!par_id = 600 Then IG_Id_DocFondosRendir = RsTmp!par_valor 'ESTE ES EL DOCUMENTO DE FONDOS POR RENDIR
            
            RsTmp.MoveNext
        Loop
    End If
    
    Exit Sub
Fallo:
BG_FalloConexion = True
MsgBox "Ocurrio un problema al intentar conectar con la Base de Datos...", vbInformation

End Sub
Public Sub LLenarCombo(Cbo As ComboBox, Campo As String, Indice As String, Tabla As String, Optional ByVal Condicion As String, Optional ByVal Ordena As String)
    Dim RsCombo As Recordset, s_Sql As String
    If Condicion = Empty Then
        s_Sql = "SELECT " & Campo & "," & Indice & "  FROM " & Tabla
    Else
        s_Sql = "SELECT " & Campo & "," & Indice & "  FROM " & Tabla & " WHERE " & Condicion
    End If
    If Ordena <> Empty Then s_Sql = s_Sql & " ORDER BY " & Ordena
    Consulta RsCombo, s_Sql
    If RsCombo.RecordCount > 0 Then
        RsCombo.MoveFirst
        Cbo.Clear
        Do While Not RsCombo.EOF
            Cbo.AddItem RsCombo.Fields(0)
            Cbo.ItemData(Cbo.ListCount - 1) = RsCombo.Fields(1)
            RsCombo.MoveNext
        Loop
    End If

End Sub
Public Sub Aplicar_skin(ByVal Formulario As Form, Optional ByVal RutaSkin As String)
  '  If RutaSkin = Empty Then RutaSkin =
  '  Cambiar Skin
    Acceso.Skin1.LoadSkin App.Path & "\skins\stxx.skn"
    Acceso.Skin1.ApplySkin Formulario.Hwnd
End Sub
Public Function SinDesborde(ElValor As Variant) As Boolean
    If Abs(Val(ElValor)) > 2147483647 Then
        SinDesborde = False
        MsgBox "Valor Ingresado fuera de limite...", vbInformation
    Else
        SinDesborde = True
    End If
End Function

Public Sub Skin2(ByVal Formulario As Form, Optional ByVal RutaSkin As String, Optional ByVal MSkin As Integer)
  '  If RutaSkin = Empty Then RutaSkin =
  '  Cambiar Skin
    Select Case MSkin
        Case 1
            Formulario.Skin1.LoadSkin App.Path & "\skins\media.skn"
        Case 2
            Formulario.Skin1.LoadSkin App.Path & "\skins\stmac.skn"
        Case 3
            Formulario.Skin1.LoadSkin App.Path & "\skins\sst.skn"
        Case 4
            Formulario.Skin1.LoadSkin App.Path & "\skins\stx.skn"
        Case 5
            Formulario.Skin1.LoadSkin App.Path & "\skins\VistaBlueNew.skn"
        Case 6
            Formulario.Skin1.LoadSkin App.Path & "\skins\MagnificBlue.skn"
        Case 7
            Formulario.Skin1.LoadSkin App.Path & "\skins\Winter.skn"
    End Select
    Formulario.Skin1.ApplySkin Formulario.Hwnd
End Sub
Public Function AceptaSoloNumeros(CodAscii As Integer)
    If IsNumeric(Chr(CodAscii)) Or CodAscii = 8 Or CodAscii = 44 Then  'pas
    Else
        If CodAscii = 13 Then
            SendKeys "{TAB}" 'envia un tab
        ElseIf CodAscii = 46 Then 'pase
        Else
            CodAscii = 0 'No acepta el caracter ingresado
        End If
    End If
    AceptaSoloNumeros = CodAscii
End Function
Public Function SoloNumeros(CodAscii As Integer)
    If IsNumeric(Chr(CodAscii)) Or CodAscii = 8 Then 'pas
    Else
        If CodAscii = 13 Then
            SendKeys "{TAB}" 'envia un tab
        Else
            CodAscii = 0 'No acepta el caracter ingresado
        End If
    End If
    SoloNumeros = CodAscii
End Function
Public Function VerificaRut(TextoRut As String, DevolverRut As String) As Boolean
    Dim LargoRut As Integer
    Dim SumaDelRut As Double
    Dim DigitoVerificador As String * 1
    Dim DigitoReal As String
    Dim DigitoResultado As Integer
    TextoRut = Replace(Trim(TextoRut), ".", "")
    TextoRut = Replace(TextoRut, "-", "")
    LargoRut = Len(TextoRut)
    If LargoRut < 6 Or Val(TextoRut) < 100000 Or LargoRut > 9 Then
        MsgBox "RUT no v�lido ... ", vbOKOnly + vbExclamation
        VerificaRut = False
    Else
        DigitoVerificador = Right(TextoRut, 1)
        LargoRut = LargoRut - 1
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut, 1)) * 9)       '9
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 1, 1)) * 8) '8
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 2, 1)) * 7) '7
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 3, 1)) * 6) '6
        SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 4, 1)) * 5) '5
        If LargoRut > 5 Then SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 5, 1)) * 4) '4
        If LargoRut > 6 Then SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 6, 1)) * 9)
        If LargoRut > 7 Then SumaDelRut = SumaDelRut + (Val(Mid(TextoRut, LargoRut - 7, 1)) * 8)
        If SumaDelRut Mod 11 = 10 Then
            DigitoReal = "K"
        Else
          DigitoResultado = SumaDelRut Mod 11
          DigitoReal = Trim(Str(DigitoResultado))
        End If
        If DigitoReal = DigitoVerificador Then
            VerificaRut = True        'rut correcto
            DevolverRut = Format(Mid(TextoRut, 1, LargoRut), "##,###") & "-" & DigitoReal
        Else
            MsgBox "RUT no v�lido ... " & Chr(13) & _
                   " El digito verificador es " & DigitoReal, vbOKOnly + vbExclamation
                VerificaRut = False
        End If
    End If
End Function


Function BrutoAletras(ByVal Number As Double, ByVal masculino As Boolean) As String
' By Francisco Castillo - september 2000.
' Devuelve la representaci�n textual del valor del n�-
' mero ENTERO que se pasa como argumento, (100 =
' "Cien"). El par�metro "masculino" ser� True cuando deban
' ponerse terminaciones masculinas,(210 = doscientOs
' diez), y False en caso contrario, (320 = trescientAs
' veinte). El valor m�ximo del n�mero es de
'                      999.999.999


    If Number = 0 Then
        NumberToString = "Cero"
        Exit Function
    End If
    If Number < 0 Then             ' Hacerlo positivo,
        Number = Number * -1
    End If
    X = CStr(Fix(Number))          ' ...entero,
    Do While Len(X) < 9         ' ...y de 9 cifras.
        X = "0" & X
    Loop
    a = ""
' Grupos de 3 cifras, de atr�s hacia adelante:
    For n = 7 To 1 Step -3
' �El grupo actual es cero?:
        If CInt(Mid(X, n, 3)) <> 0 Then
' No.
' Tratar casos especiales decena:
            Select Case CInt(Mid(X, n + 1, 2))
                Case 10
                    a = "diez " & a
                Case 11
                    a = "once " & a
                Case 11
                    a = "once " & a
                Case 12
                    a = "doce " & a
                Case 13
                    a = "trece " & a
                Case 14
                    a = "catorce " & a
                Case 15
                    a = "quince " & a
                Case 16
                    a = "dieciseis " & a
                Case 17
                    a = "diecisiete " & a
                Case 18
                    a = "dieciocho " & a
                Case 19
                    a = "diecinueve " & a
                Case 20
                    a = "veinte " & a
                Case 21
                    If n > 1 Then
                        a = "veintiun$ " & a
                    Else
                        a = "veintiun " & a
                    End If
                Case 22
                    a = "veintidos " & a
                Case 23
                    a = "veintitr�s " & a
                Case 24
                    a = "veinticuatro " & a
                Case 25
                    a = "veinticinco " & a
                Case 26
                    a = "veintiseis " & a
                Case 27
                    a = "veintisiete " & a
                Case 28
                    a = "veintiocho " & a
                Case 29
                    a = "veintinueve " & a
                Case Else
' Restantes casos; traducir unidad:
                    Select Case CInt(Mid(X, n + 2, 1))
                        Case 0
                        Case 1
                            Select Case n
                                Case 7
                                    a = "y un$ " & a
                                Case 4
                                    If masculino Then
                                        a = "y un " & a
                                    Else
                                        a = "y una " & a
                                    End If
                                Case 1
                                    a = "y un " & a
                            End Select
                        Case 2
                            a = "y dos " & a
                        Case 3
                            a = "y tres " & a
                        Case 4
                            a = "y cuatro " & a
                        Case 5
                            a = "y cinco " & a
                        Case 6
                            a = "y seis " & a
                        Case 7
                            a = "y siete " & a
                        Case 8
                            a = "y ocho " & a
                        Case 9
                            a = "y nueve " & a
                    End Select
' Traducir decena:
                    Select Case CInt(Mid(X, n + 1, 1))
                        Case 0
                        Case 3
                            a = "treinta " & a
                        Case 4
                            a = "cuarenta " & a
                        Case 5
                            a = "cincuenta " & a
                        Case 6
                            a = "sesenta " & a
                        Case 7
                            a = "setenta " & a
                        Case 8
                            a = "ochenta " & a
                        Case 9
                            a = "noventa " & a
                    End Select
            End Select
' Prever caso "ciento y tres":
            If Left(a, 1) = "y" Then
                a = Right(a, Len(a) - 2)
            End If
' Traducir centena:
            Select Case CInt(Mid(X, n, 1))
                Case 0
                Case 1
                    If CInt(Mid(X, n + 1, 2)) = 0 Then
                        a = "cien " & a
                    Else
                        a = "ciento " & a
                    End If
                Case 2
                    a = "doscient$s " & a
                Case 3
                    a = "trescient$s " & a
                Case 4
                    a = "cuatrocient$s " & a
                Case 5
                    a = "quinient$s " & a
                Case 6
                    a = "seiscient$s " & a
                Case 7
                    a = "setecient$s " & a
                Case 8
                    a = "ochocient$s " & a
                Case 9
                    a = "novecient$s " & a
                
            End Select
        End If
' Poner terminaci�n del grupo anterior:
' Puede haber quedado "y tres":
        If Left(a, 1) = "y" Then
            a = Right(a, Len(a) - 2)
        End If
' Millones:
        If n = 4 Then
            If CInt(Left(X, 3)) = 1 Then
                a = "mill�n " & a
            Else
                If CInt(Left(X, 3)) <> 0 Then
                    a = "millones " & a
                End If
            End If
        Else
            If n = 7 Then
' Miles:
                If CInt(Mid(X, 4, 3)) = 1 Then
                    a = "mil " & a
                Else
                    If CInt(Mid(X, 4, 3)) <> 0 Then
                        a = "mil " & a
                    End If
                End If
            End If
        End If
' Traducir g�nero, "$" en el texto. Para el grupo de los
' millones, se traduce siempre por masculino:
        If n = 1 Then
            masculino = True
        End If
        posic = 1
        Do While posic <> 0
            posic = InStr(a, "$")
            If posic <> 0 Then
                ante = Left(a, posic - 1)
                Post = Right(a, Len(a) - posic)
                If masculino Then
                    a = ante & "o" & Post
                Else
                    a = ante & "a" & Post
                End If
            End If
        Loop
    Next n
' Caso especial: puede haber quedado "unx mil "
    If Left(a, 7) = "un mil " Then
        a = Right(a, Len(a) - 3)
    Else
        If Left(a, 8) = "una mil " Then
            a = Right(a, Len(a) - 4)
        End If
    End If
' Inicial en may�sculas:
    If val_Dec = "" Then
        BrutoAletras = Trim(UCase(Left(a, 1)) & Right(a, Len(a) - 1))
    Else
        BrutoAletras = Trim(Left(a, 1) & Right(a, Len(a) - 1))
    End If
End Function


Function Establecer_Impresora(ByVal NamePrinter As String) As Boolean
On Error GoTo errSub
       
    'Variable de referencia
    Dim obj_Impresora As Object
       
    'Creamos la referencia
    Set obj_Impresora = CreateObject("WScript.Network")
        obj_Impresora.setdefaultprinter NamePrinter
       
    Set obj_Impresora = Nothing
           
        'La funci�n devuelve true y se cambi� con �xito
        Establecer_Impresora = True
       ' MsgBox "La impresora se cambi� correctamente", vbInformation
    Exit Function
       
       
'Error al cambiar la impresora
errSub:
If Err.Number = 0 Then Exit Function
   Establecer_Impresora = False
   MsgBox "error: " & Err.Number & Chr(13) & "Description: " & Err.Description
   On Error GoTo 0
End Function
Public Function AgregarIVA(VNeto As Double, IVA As Integer) As Double
    AgregarIVA = VNeto + Round(VNeto * IVA / 100, 1)
End Function
Public Function ExtraeNeto(Vbruto As Double) As Double
    ExtraeNeto = Round((Vbruto * 100 / 119), 0)
End Function

Function ConexionBD()
            On Error GoTo FalloConexion
                'Connection_String = "PROVIDER=MSDASQL;dsn=citroen;uid=;pwd=;" '   ' le agrega el path de la base de datos al Connectionstring
                Connection_String = "PROVIDER=MSDASQL;dsn=db_redmar;uid=root;pwd=eunice;server=" & Sp_Servidor & "; "
                '" '   ' le agrega el path de la base de datos al Connectionstring
                Set cn = New ADODB.Connection '  ' Nuevo objeto Connection
                cn.CursorLocation = adUseClient
                cn.Open Connection_String '  'Abre la conexi�n
            Exit Function
FalloConexion:
    MsgBox "No fue posible establecer la conecci�n con el Servidor..." & vbNewLine & "Verifique que la Ip ingresada sea la correcta", vbInformation
    Exit Function
End Function

Function Consulta(RecSet As ADODB.Recordset, CSQL As String)
            Set RecSet = New ADODB.Recordset '  'Nuevo recordset
            Debug.Print CSQL
            On Error GoTo FalloConsulta
            RecSet.Open CSQL, cn, adOpenStatic, adLockOptimistic '  ' abre
            Exit Function
FalloConsulta:
    If EjecucionIDE Then
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & CSQL & vbNewLine & Err.Number & " - " & Err.Description
    Else
        MsgBox "Fallo la consulta a la base de datos " & vbNewLine & vbNewLine & Err.Number & " - " & Err.Description
    End If
End Function
Function ConexionBdShape(RecSetSh As ADODB.Recordset, CSQL As String)
    Dim Sp_CadenaConexion As String
    On Error GoTo FalloConexion
    Sp_CadenaConexion = "PROVIDER=MSDataShape;Data PROVIDER=MSDASQL;dsn=db_redmar;uid=root;pwd=eunice;server=" & Sp_Servidor & "; "
    Set CnShape = New ADODB.Connection
    CnShape.CursorLocation = adUseClient
    CnShape.Open Sp_CadenaConexion
    Set RecSetSh = New ADODB.Recordset
    RecSetSh.Open CSQL, CnShape, adOpenStatic, adLockOptimistic
    Exit Function
FalloConexion:
    MsgBox "No fue posible establecer la conecci�n con el Servidor..." & vbNewLine & "Verifique que la Ip ingresada sea la correcta", vbInformation
End Function
Public Function Existe(sArchivo As String) As Integer
    Existe = Len(Dir$(sArchivo))
End Function
Public Function NumReal(Numtexto As String) As Double
    NumReal = Val(Replace(Numtexto, ".", ""))
   
End Function
Public Function NumFormat(Numdtexto As Variant) As String
   ' If IsNull(Numdtexto) = 0 Then Numdtexto = 0
    NumFormat = Format(Numdtexto, "#,##0")
    If Len(NumFormat) = 0 Then NumFormat = "0"
End Function
Function TipoLetra(Tamano As Integer, Negrita As Boolean, Cursiva As Boolean, Subrayada As Boolean)
    Printer.FontSize = Tamano
    Printer.FontBold = Negrita
    Printer.FontItalic = Cursiva
    Printer.FontUnderline = Subrayada
End Function
Function FormularioCargado(NombreFormulario As String) As Boolean
    Dim Formulario As Form
    FormularioCargado = False
    For Each Formulario In Forms
       If (UCase(Formulario.Name) = UCase(NombreFormulario)) Then
        FormularioCargado = True
        Exit For
        End If
    Next
End Function
'FECHA DE CRECION: 01/11/2008
'CREADA POR: ALDO NAVARRETE
'FECHA Y NOMBRE DE ACTUALIZACION: 14/01/2009 - ALDO NAVARRETE
'OBSERVACION TAG LISTVIEW : 1 CARACTER: TIPO DATO (T-F-N); 2 Y 3 CARACTER: LONGITUD IMPRESION; 4 CARACTER: NRO DECIMALES
'******************************************************
Public Sub Exportar(lv As ListView, tit() As String, Formulario As Form, Progreso As ProgressBar, LbProgreso As SkinLabel)
On Error GoTo Genera_Error

Dim Tipo_Dato As String, Cantidad_Decimales As Integer, Tipo_Formato As String, mDato As String

If lv.ListItems.Count = 0 Then Exit Sub
Dim J As Integer
Dim V As Integer
Dim H As Integer
Dim mFec As Date
Call Inicio_Excel
Formulario.MousePointer = 11
objExcel.Visible = False
With objExcel.ActiveSheet
    .Range(.Cells(1, 1), .Cells(3, 1)).Font.Bold = True
    .Range(.Cells(1, 1), .Cells(1, 1)).Font.Size = 14
    .Range(.Cells(2, 1), .Cells(3, 1)).Font.Size = 12
    .Range(.Cells(5, 1), .Cells(5, lv.ColumnHeaders.Count)).Font.Bold = True
    .Range(.Cells(5, 1), .Cells(5, lv.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
    .Cells(1, 1) = UCase(tit(0))
    .Cells(2, 1) = UCase(tit(1))
    .Cells(3, 1) = UCase(tit(2))
End With
For J = 1 To lv.ColumnHeaders.Count
    objExcel.ActiveSheet.Cells(5, J) = lv.ColumnHeaders(J).Text
    objExcel.ActiveSheet.Columns(J).ColumnWidth = (lv.ColumnHeaders(J).Width) / 100
Next J
Progreso.Min = 1
If lv.ListItems.Count = 1 Then Progreso.Max = 10 Else Progreso.Max = lv.ListItems.Count

For V = 1 To lv.ListItems.Count
    LbProgreso = Format(V / Progreso.Max * 100, "###") & "%"
    Progreso.Value = V
    H = 1
    If Mid(lv.ColumnHeaders(H).Tag, 1, 1) = "F" Then
        mFec = lv.ListItems(V)
        objExcel.ActiveSheet.Cells(V + 5, H) = mFec
    ElseIf Mid(lv.ColumnHeaders(H).Tag, 1, 1) = "N" Then
        objExcel.ActiveSheet.Cells(V + 5, H) = CDbl(lv.ListItems(V))
    Else
        objExcel.ActiveSheet.Cells(V + 5, H) = lv.ListItems(V)
    End If
    For H = 1 To lv.ColumnHeaders.Count - 1
        If Mid(lv.ColumnHeaders(H + 1).Tag, 1, 1) = "F" Then
            mFec = lv.ListItems(V).SubItems(H)
            objExcel.ActiveSheet.Cells(V + 5, H + 1) = mFec
        ElseIf Mid(lv.ColumnHeaders(H + 1).Tag, 1, 1) = "N" Then
            If lv.ListItems(V).SubItems(H) = "" Then
                objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
            Else
                If Not IsNumeric(lv.ListItems(V).SubItems(H)) Then
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
                Else
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = CDbl(lv.ListItems(V).SubItems(H))
                End If
            End If
        Else
            objExcel.ActiveSheet.Cells(V + 5, H + 1) = lv.ListItems(V).SubItems(H)
        End If
    Next H
Next V
'FORMATO COLUMNAS
For H = 1 To lv.ColumnHeaders.Count
    Tipo_Dato = Mid(lv.ColumnHeaders(H).Tag, 1, 1)
    If Len(lv.ColumnHeaders(H).Tag) = 4 Then Cantidad_Decimales = Mid(lv.ColumnHeaders(H).Tag, 4, 1)
    If Tipo_Dato = "N" Then
        Tipo_Formato = "#,##0"
        If Cantidad_Decimales > 0 Then Tipo_Formato = Tipo_Formato & "." & Mid("0000000000", 1, Cantidad_Decimales)
        If Cantidad_Decimales = 9 Then Tipo_Formato = "###0"
        With objExcel.ActiveSheet
            .Range(.Cells(5, H), .Cells(V + 8, H)).NumberFormat = Tipo_Formato
        End With
    End If
Next H

objExcel.Visible = True
Formulario.MousePointer = 0
Exit Sub

Genera_Error:
Formulario.MousePointer = 0
MsgBox "SE GENERO EL SIGUIENTE ERROR:" & Chr(13) & _
       UCase(Err.Description) & Chr(13) & _
       "NUMERO: " & Err.Number, vbCritical, mSis

End Sub
Public Sub ExportarNuevo(lv As ListView, tit() As String, Formulario As Form, Progreso As XP_ProgressBar)
On Error GoTo Genera_Error

Dim Tipo_Dato As String, Cantidad_Decimales As Integer, Tipo_Formato As String, mDato As String

If lv.ListItems.Count = 0 Then Exit Sub
Dim J As Integer
Dim V As Integer
Dim H As Integer
Dim mFec As Date
Call Inicio_Excel
Formulario.MousePointer = 11
objExcel.Visible = False
With objExcel.ActiveSheet
    .Range(.Cells(1, 1), .Cells(3, 1)).Font.Bold = True
    .Range(.Cells(1, 1), .Cells(1, 1)).Font.Size = 14
    .Range(.Cells(2, 1), .Cells(3, 1)).Font.Size = 12
    .Range(.Cells(5, 1), .Cells(5, lv.ColumnHeaders.Count)).Font.Bold = True
    .Range(.Cells(5, 1), .Cells(5, lv.ColumnHeaders.Count)).Borders.LineStyle = xlContinuous
    .Cells(1, 1) = UCase(tit(0))
    .Cells(2, 1) = UCase(tit(1))
    .Cells(3, 1) = UCase(tit(2))
   ' MsgBox UBound(tit)
End With
For J = 1 To lv.ColumnHeaders.Count
    objExcel.ActiveSheet.Cells(5, J) = lv.ColumnHeaders(J).Text
    objExcel.ActiveSheet.Columns(J).ColumnWidth = (lv.ColumnHeaders(J).Width) / 100
Next J
Progreso.Min = 1
If lv.ListItems.Count = 1 Then Progreso.Max = 10 Else Progreso.Max = lv.ListItems.Count

For V = 1 To lv.ListItems.Count
    LbProgreso = Format(V / Progreso.Max * 100, "###") & "%"
    Progreso.Value = V
    H = 1
    If Mid(lv.ColumnHeaders(H).Tag, 1, 1) = "F" Then
        mFec = lv.ListItems(V)
        objExcel.ActiveSheet.Cells(V + 5, H) = mFec
    ElseIf Mid(lv.ColumnHeaders(H).Tag, 1, 1) = "N" Then
        objExcel.ActiveSheet.Cells(V + 5, H) = CDbl(lv.ListItems(V))
    Else
        objExcel.ActiveSheet.Cells(V + 5, H) = lv.ListItems(V)
    End If
    For H = 1 To lv.ColumnHeaders.Count - 1
        If Mid(lv.ColumnHeaders(H + 1).Tag, 1, 1) = "F" Then
            mFec = lv.ListItems(V).SubItems(H)
            objExcel.ActiveSheet.Cells(V + 5, H + 1) = mFec
        ElseIf Mid(lv.ColumnHeaders(H + 1).Tag, 1, 1) = "N" Then
            If lv.ListItems(V).SubItems(H) = "" Then
                objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
            Else
                If Not IsNumeric(lv.ListItems(V).SubItems(H)) Then
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = 0
                Else
                    objExcel.ActiveSheet.Cells(V + 5, H + 1) = CDbl(lv.ListItems(V).SubItems(H))
                End If
            End If
        Else
            objExcel.ActiveSheet.Cells(V + 5, H + 1) = lv.ListItems(V).SubItems(H)
        End If
    Next H
Next V
'FORMATO COLUMNAS
For H = 1 To lv.ColumnHeaders.Count
    Tipo_Dato = Mid(lv.ColumnHeaders(H).Tag, 1, 1)
    If Len(lv.ColumnHeaders(H).Tag) = 4 Then Cantidad_Decimales = Mid(lv.ColumnHeaders(H).Tag, 4, 1)
    If Tipo_Dato = "N" Then
        Tipo_Formato = "#,##0"
        If Cantidad_Decimales > 0 Then Tipo_Formato = Tipo_Formato & "." & Mid("0000000000", 1, Cantidad_Decimales)
        If Cantidad_Decimales = 9 Then Tipo_Formato = "###0"
        With objExcel.ActiveSheet
            .Range(.Cells(5, H), .Cells(V + 8, H)).NumberFormat = Tipo_Formato
        End With
    End If
Next H

objExcel.Visible = True
Formulario.MousePointer = 0

Exit Sub

Genera_Error:
Formulario.MousePointer = 0
MsgBox "SE GENERO EL SIGUIENTE ERROR:" & Chr(13) & _
       UCase(Err.Description) & Chr(13) & _
       "NUMERO: " & Err.Number, vbCritical, mSis

End Sub

Public Sub LLenar_Grilla(Record_Set As ADODB.Recordset, Name_Formulario As Form, Name_ListView As ListView, Con_ChekBox As Boolean, Full_Row_Select As Boolean, Grid_line As Boolean, Mensaje_Grilla_Vacia As Boolean)
On Error GoTo Genera_Error
Dim Contador_i As Integer, Tipo_Dato As Variant, Cantidad_Decimales As Integer, Tipo_Formato As String, mDato As String
Name_Formulario.MousePointer = 11
Name_ListView.ListItems.Clear
Name_ListView.CheckBoxes = Con_ChekBox
If Record_Set.RecordCount > 0 Then
    Record_Set.MoveFirst
    While Not Record_Set.EOF
        For Contador_i = 1 To Record_Set.Fields.Count
            'DEFINE TIPO DE DATO Y FORMATO
            Tipo_Dato = Mid(Name_ListView.ColumnHeaders(Contador_i).Tag, 1, 1)
            Cantidad_Decimales = 0
            If Name_ListView.ColumnHeaders(Contador_i).Tag <> Empty Then Cantidad_Decimales = Mid(Name_ListView.ColumnHeaders(Contador_i).Tag, 4, 1)
            
            If Tipo_Dato = "T" Then
                mDato = ""
                Tipo_Formato = ">"
            End If
            If Tipo_Dato = "N" Then
                mDato = 0
                Tipo_Formato = "#,##0"
            End If
            If Tipo_Dato = "F" Then
                mDato = ""
                Tipo_Formato = "DD-MM-YYYY"
            End If
            If Tipo_Dato = "" Then
                mDato = ""
                Tipo_Formato = ">"
            End If
            
            If Cantidad_Decimales > 0 Then Tipo_Formato = Tipo_Formato & "." & Mid("0000000000", 1, Cantidad_Decimales)
            If Cantidad_Decimales = 9 Then Tipo_Formato = "##0"
            If Not IsNull(Record_Set.Fields(Contador_i - 1)) Then
                mDato = Format(Record_Set.Fields(Contador_i - 1), Tipo_Formato)
            End If
            
            If Contador_i = 1 Then
                Set itmx = Name_ListView.ListItems.Add(, , mDato)
            Else
                itmx.SubItems(Contador_i - 1) = mDato
            End If
        Next Contador_i
        Record_Set.MoveNext
    Wend
Else
    If Mensaje_Grilla_Vacia Then MsgBox "NO EXISTEN REGISTROS PARA VISUALIZAR !!!", vbCritical, mSis
End If
Name_ListView.View = lvwReport
Name_ListView.LabelEdit = lvwManual
Name_ListView.FullRowSelect = Full_Row_Select
Name_ListView.Gridlines = Grid_line
Name_Formulario.MousePointer = 0
Name_Formulario.Refresh
Exit Sub

Genera_Error:
Name_Formulario.MousePointer = 0
MsgBox "SE GENERO EL SIGUIENTE ERROR:" & Chr(13) & _
       UCase(Err.Description) & Chr(13) & _
       "NUMERO: " & Err.Number, vbCritical, mSis
End Sub
Public Function Inicio_Excel() As Boolean
    Dim J As Integer
    Set objExcel = New Excel.Application
    objExcel.Visible = True
    objExcel.SheetsInNewWorkbook = 1
    objExcel.Workbooks.Add
End Function

Public Function Formato_Excel(Num_Campos As Integer, Nombre_Campos() As String) As Boolean
    With objExcel.ActiveSheet
            .Range(.Cells(3, 1), .Cells(3, Num_Campos + 1)).Borders.LineStyle = xlContinuous
            .Range(.Cells(3, 1), .Cells(3, Num_Campos + 1)).Font.Bold = True
        For IG_I = 0 To Num_Campos Step 1
            .Cells(3, IG_I + 1) = Nombre_Campos(IG_I)
        Next IG_I
    End With
End Function
Public Function SaldoCliente(Rut As String, Rz As String)
    Dim RsSaldoCliente As Recordset
    Dim d_TotalDeuda As Double
    d_TotalDeuda = 0
    Sql = "SELECT d.Fecha,datediff(now(), d.fecha)AS Dias_Mora," & _
                 "c.tipo_doc AS Doc_Venta,c.no_doc AS Nro," & _
                 "Sum(c.debe)AS DEBE_,IFNULL(Sum(c.haber),0) AS HABER_,sum(c.debe) - IFNULL(SUM(c.haber),0) As SALDO_ " & _
            "FROM ctacte_clientes c, doc_venta d " & _
            "WHERE c.rut = '" & Rut & "' and  d.no_documento=c.no_doc " & _
            "GROUP BY c.rut, c.nombre, c.tipo_doc, c.no_doc, d.fecha " & _
            "HAVING Sum(debe) - IFNULL(SUM(haber),0) > 0 " & _
            "ORDER BY d.fecha"
    Call Consulta(RsSaldoCliente, Sql)
    
    If RsSaldoCliente.RecordCount > 0 Then
        With RsSaldoCliente
            .MoveFirst
            Do While Not .EOF
                d_TotalDeuda = d_TotalDeuda + NumReal(!saldo_)
                .MoveNext
            Loop
        End With
    
    
        With ClienteConDeuda
            .frmDeuda.Caption = Rut & " - " & Rz
            LLenar_Grilla RsSaldoCliente, ClienteConDeuda, ClienteConDeuda.LvDetalle, False, True, True, False
            .txtDuedaTotal = NumFormat(d_TotalDeuda)
            .txtCantidadDoc = RsSaldoCliente.RecordCount
            .Show 1
        End With
    End If

End Function
Sub Busca_Id_Combo(Combo As ComboBox, Codigo As Double)
For IG_I = 0 To Combo.ListCount - 1
    If Combo.ItemData(IG_I) = Codigo Then
       Combo.ListIndex = IG_I
       Exit Sub
    End If
Next IG_I
Combo.ListIndex = -1
End Sub
Sub Kardex(FechaKardex As Date, Movimiento As String, Id_Documento As Integer, _
           Nro_Documento As Double, Id_Bodega As Integer, ElCodigo As String, _
           LaCantidad As Double, DescripcionM As String, PrecioNeto As Double, _
           ValorCompra As Double, Optional ByVal ElRutProveedor As String, _
           Optional ByVal LaRazon As String, Optional ByVal ActualizaPcompra As String, _
           Optional ByVal ElPrecioCosto As Double, Optional ByVal IpDocID As Integer, _
           Optional ByVal SoloUnidades As String, Optional ByVal SoloValores As String, _
           Optional ByVal MovFinal As String)
           
           
    Dim l_Nuevo As Double, l_Anterior As Double, Existe As Boolean, lp_UPcompra As Double
    If IpDocID = Empty Then IpDocID = 0
    If SoloUnidades = Empty Then SoloUnidades = "NO"
    If Len(SoloValores) = 0 Then
        SoloValores = "NO"
    Else
        If SoloValores = "SI" Then Movimiento = "NN"
    End If
    
    'Lo primero es saber el saldo anterior y cual sera el nuevo
    'si se encuentra el registro se actualiza, de lo contrario se agrega
    If ElRutProveedor = Empty Then ElRutProveedor = Space(1)
    If LaRazon = Empty Then LaRazon = Space(1)
    Sql = "SELECT sto_stock,pro_ultimo_precio_compra pcompra " & _
          "FROM pro_stock s " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & ElCodigo & "' AND bod_id=" & Id_Bodega
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Existe = True
        l_Anterior = RsTmp2!sto_stock
        lp_UPcompra = RsTmp2!pcompra
        If Movimiento = "SALIDA" Then
            l_Nuevo = l_Anterior - LaCantidad
        ElseIf Movimiento = "ENTRADA" Then
            lp_UPcompra = PrecioNeto
            l_Nuevo = l_Anterior + LaCantidad
        ElseIf Movimiento = "NN" Then
            l_Nuevo = l_Anterior
        End If
        
        Sql = "UPDATE pro_stock " & _
              "SET sto_stock=" & CxP(Str(l_Nuevo)) & _
              ",pro_ultimo_precio_compra=" & CxP(lp_UPcompra) & _
              " WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & ElCodigo & "' AND bod_id=" & Id_Bodega
    Else
        'Nuevo registro
        If Movimiento = "SALIDA" Then
            l_Nuevo = 0 - LaCantidad
        ElseIf Movimiento = "ENTRADA" Then
            l_Nuevo = LaCantidad
            lp_UPcompra = PrecioNeto
     '   ElseIf Movimiento = "NN" Then
      '      l_Nuevo = LaCantidad
        End If
        Sql = "INSERT INTO pro_stock " & _
                          "(pro_codigo,bod_id,sto_stock,pro_ultimo_precio_compra,rut_emp) " & _
                   "VALUES('" & ElCodigo & "'," & Id_Bodega & "," & CxP(l_Nuevo) & "," & CxP(lp_UPcompra) & ",'" & SP_Rut_Activo & "')"
                           
        Existe = False
        l_Anterior = 0
        ' �If Movimiento = "SALIDA" Then
        '    l_Nuevo = l_Anterior - LaCantidad
        'ElseIf Movimiento = "ENTRADA" Then
        '    l_Nuevo = l_Anterior + LaCantidad
        'End If
        
    End If
    cn.Execute Sql
    
    If Movimiento = "ENTRADA" And ActualizaPcompra = "SI" Then
        
        Sql = "UPDATE maestro_productos " & _
              "SET precio_compra=" & CxP(CDbl(lp_UPcompra)) & " " & _
              "WHERE rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & ElCodigo & "'"
        cn.Execute Sql
    End If
    
    
    'Ahora insertamos el Kardex
    Dim dp_SaldoValor As Double
    Dim dp_NuevoSaldoValor As Double
    Dim dp_CostoPromedioActual As Double
    
    Sql = "SELECT kar_nuevo_saldo_valor,IFNULL(ROUND(kar_nuevo_saldo_valor/kar_nuevo_saldo,0),0) costo_promedio " & _
          "FROM inv_kardex " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo ='" & ElCodigo & "' AND bod_id=" & Id_Bodega & " " & _
          "ORDER BY kar_id DESC " & _
          "LIMIT 1"
          
    Consulta RsTmp2, Sql
    dp_NuevoSaldoValor = ValorCompra
    If RsTmp2.RecordCount > 0 Then
        dp_SaldoValor = RsTmp2!kar_nuevo_saldo_valor
        dp_CostoPromedioActual = Abs(RsTmp2!costo_promedio)
        If dp_CostoPromedioActual = 0 Then dp_CostoPromedioActual = ObtenerPromedio(Val(ElCodigo))
        If Movimiento = "ENTRADA" Then
            dp_NuevoSaldoValor = dp_SaldoValor + ValorCompra
        ElseIf Movimiento = "SALIDA" Then
            If ElPrecioCosto = 0 Then
                If SoloUnidades = "NO" Then
                    PrecioNeto = dp_CostoPromedioActual
                    ValorCompra = dp_CostoPromedioActual * LaCantidad
                    dp_NuevoSaldoValor = dp_SaldoValor - (dp_CostoPromedioActual * LaCantidad)
                Else
                    dp_NuevoSaldoValor = dp_SaldoValor
                End If
                
            Else
                ValorCompra = ElPrecioCosto * LaCantidad
                dp_NuevoSaldoValor = dp_SaldoValor - ValorCompra
            
            End If
            If ValorCompra = 0 Then
                If SoloUnidades = "NO" Then
                    ValorCompra = lp_UPcompra
                    dp_NuevoSaldoValor = lp_UPcompra * LaCantidad
                End If
                
            End If
            'dp_NuevoSaldoValor = dp_CostoPromedioActual
        ElseIf Movimiento = "NN" Then
            dp_NuevoSaldoValor = dp_SaldoValor - ValorCompra
        End If
    End If
    
          
    If Len(MovFinal) > 0 Then Movimiento = MovFinal
          
          
    
    Sql = "INSERT INTO inv_kardex " & _
          "(kar_fecha,kar_movimiento,kar_documento,kar_numero,bod_id,pro_codigo,kar_saldo_anterior," & _
          "kar_cantidad,kar_nuevo_saldo,kar_usuario,kar_descripcion,pro_precio_neto,kar_saldo_anterior_valor, " & _
          "kar_cantidad_valor,kar_nuevo_saldo_valor,rut,rsocial,rut_emp,doc_id) " & _
          "VALUES('" & Format(FechaKardex, "YYYY-MM-DD") & "','" & Movimiento & "'," & Id_Documento & "," & _
          Nro_Documento & "," & Id_Bodega & ",'" & ElCodigo & "'," & CxP(Str(l_Anterior)) & "," & CxP(Str(LaCantidad)) & _
          "," & CxP(Str(l_Nuevo)) & ",'" & LogUsuario & "','" & DescripcionM & "'," & CxP(Str(PrecioNeto)) & "," & _
          CxP(Str(dp_SaldoValor)) & "," & CxP(Str(ValorCompra)) & "," & CxP(Str(dp_NuevoSaldoValor)) & ",'" & ElRutProveedor & "','" & LaRazon & "','" & SP_Rut_Activo & "'," & IpDocID & ") "
    cn.Execute Sql
End Sub
Public Sub KardexNN_NotaDebito(FechaKardex As Date, Movimiento As String, Id_Documento As Integer, _
           Nro_Documento As Double, Id_Bodega As Integer, ElCodigo As String, _
           DescripcionM As String, ValorEntrada As Long, Optional ByVal ElPrecioCosto As Double, _
           Optional ByVal ElRutProveedor As String, _
           Optional ByVal LaRazon As String, Optional ByVal Signo As String, Optional ByVal IpDocID As Integer)
           
    Dim pl_SaldoAnteriorCantidad As Long
    Dim pl_SaldoAnteriorValor As Long
    Dim pl_PrecioNetoAnterior As Long
    Dim pl_Nuevo_PrecioCompra As Long
    Sql = "SELECT kar_nuevo_saldo cantidad,kar_nuevo_saldo_valor valor,pro_precio_neto neto " & _
          "FROM inv_kardex " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo ='" & ElCodigo & "' " & _
          "ORDER BY kar_id DESC " & _
          "LIMIT 1"
    Consulta RsTmp2, Sql
    If IpDocID = Empty Then IpDocID = 0
    If RsTmp2.RecordCount = 0 Then Exit Sub 'si no se encuentra ningun registro salimos, pero no debiera ocurrir.
    pl_SaldoAnteriorCantidad = RsTmp2!cantidad
    pl_SaldoAnteriorValor = RsTmp2!Valor
    pl_PrecioNetoAnterior = RsTmp2!Neto
    Sql = "INSERT INTO inv_kardex " & _
          "(kar_fecha,kar_movimiento,kar_documento,kar_numero,bod_id,pro_codigo,kar_saldo_anterior," & _
          "kar_cantidad,kar_nuevo_saldo,kar_usuario,kar_descripcion,pro_precio_neto,kar_saldo_anterior_valor, " & _
          "kar_cantidad_valor,kar_nuevo_saldo_valor,rut,rsocial,rut_emp,doc_id) " & _
          "VALUES('" & Format(FechaKardex, "YYYY-MM-DD") & "','" & Movimiento & "'," & Id_Documento & "," & _
          Nro_Documento & "," & Id_Bodega & ",'" & ElCodigo & "'," & CDbl(pl_SaldoAnteriorCantidad) & "," & 0 & _
          "," & pl_SaldoAnteriorCantidad & ",'" & LogUsuario & "','" & DescripcionM & "'," & Round(pl_PrecioNetoAnterior + (ValorEntrada / IIf(pl_SaldoAnteriorCantidad = 0, 1, pl_SaldoAnteriorCantidad))) & "," & _
          CDbl(pl_SaldoAnteriorValor) & "," & ValorEntrada & "," & CDbl(pl_SaldoAnteriorValor) & Signo & ValorEntrada & ",'" & ElRutProveedor & "','" & LaRazon & "','" & SP_Rut_Activo & "'," & IpDocID & ")"
    Call Consulta(RsTmp2, Sql)
    If Signo = "+" Then
        pl_Nuevo_PrecioCompra = (CDbl(pl_SaldoAnteriorValor) + ValorEntrada) / pl_SaldoAnteriorCantidad
    ElseIf Signo = "-" Then
        pl_Nuevo_PrecioCompra = (CDbl(pl_SaldoAnteriorValor) - ValorEntrada) / IIf(pl_SaldoAnteriorCantidad = 0, 1, pl_SaldoAnteriorCantidad)
    End If
    Sql = "UPDATE pro_stock SET pro_ultimo_precio_compra= " & pl_Nuevo_PrecioCompra & "  " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND  pro_codigo='" & ElCodigo & "' AND bod_id=1"
    Call Consulta(RsTmp2, Sql)
          
End Sub
Public Function UltimoNro(S_tabla As String, S_campo As String) As Long
    Dim s_Sql As String, rg_Rst As Recordset
    's_Sql = "SELECT MAX(" & S_campo & ") numero " & _
            "FROM " & S_tabla
            
    '17 Enero 2012 Toma
    s_Sql = "SHOW TABLE STATUS LIKE '" & S_tabla & "'"
    Consulta rg_Rst, s_Sql
    If rg_Rst.RecordCount > 0 Then
        UltimoNro = (0 & rg_Rst!Auto_increment)
    Else
        UltimoNro = 1
    End If
End Function


Public Sub En_Foco(ctl As TextBox)
    On Error Resume Next
    If Not IsNull(ctl.Text) Then
       ctl.SelStart = 0
      ctl.SelLength = Len(ctl.Text)
    End If
End Sub

Public Sub Centrar(fr As Form, Optional Tiene_Banner As Boolean)
fr.Move (Screen.Width - fr.Width) / 2, (Screen.Height - fr.Height) / 2
On Error Resume Next
fr.BackColor = &HFFFFFF
fr.KeyPreview = True
If Tiene_Banner Then
    fr.Logo = LoadPicture(App.Path & "\REDMAROK.jpg")
    '
End If
End Sub
Public Function TotalizaColumna(lv As ListView, Clave As String) As Double
    Dim ip_Columna As Integer, Lp_Total As Double, TColumna As Double
    If lv.ListItems.Count = 0 Then
        TotalizaColumna = 0
        Exit Function
    End If
    For i = 2 To lv.ColumnHeaders.Count
        If lv.ColumnHeaders(i).Key = Clave Then
            ip_Columna = i
            Exit For
        End If
    Next
    Lp_Total = 0
    For i = 1 To lv.ListItems.Count
        If lv.ListItems(i).SubItems(ip_Columna - 1) = "" Then lv.ListItems(i).SubItems(ip_Columna - 1) = "0"
        Lp_Total = Lp_Total + CDbl(lv.ListItems(i).SubItems(ip_Columna - 1))
    Next
    TotalizaColumna = Lp_Total
End Function
Public Function CtaCte(TxtRut As String, CliPro As String, Ip_IdDocumento As Integer, FechaDoc As String, Obs As String, Lp_Nro As Long, Lp_Monto As Long, Anular As Boolean)
    Dim Lp_SaldoCtaCte As Long
    Dim Lp_Abono As Long, Lp_Cargo As Long, Lp_Saldo As Long
     'CONSULTAMOS SALDO ACTUAL DE LA CTA CTE
    Sql = "SELECT cta_saldo " & _
          "FROM cta_corriente " & _
          "WHERE cta_cli_pro='" & CliPro & "' AND cta_rut='" & TxtRut & "' AND rut_emp='" & SP_Rut_Activo & "' " & _
          "ORDER BY cta_id DESC " & _
          "LIMIT 1 "
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then Lp_SaldoCtaCte = RsTmp!cta_saldo Else Lp_SaldoCtaCte = 0
    Lp_Abono = 0
    Lp_Cargo = 0
    If IG_id_Nota_Credito_Clientes = Ip_IdDocumento Or IG_id_Nota_Credito_Electronica = Ip_IdDocumento Or IG_id_Nota_Credito = Ip_IdDocumento Then
        Lp_Abono = CDbl(Lp_Monto)
        If Anular Then 'Solo si anula Nota de Credito (cliente o provedor)
            Lp_Cargo = CDbl(Lp_Monto)
            Lp_Abono = 0
        End If
            
    Else
        Lp_Cargo = CDbl(Lp_Monto)
        If Anular Then
            Lp_Abono = CDbl(Lp_Monto)
            Lp_Cargo = 0
        End If
        
    End If
    Lp_SaldoCtaCte = Lp_SaldoCtaCte + Lp_Cargo - Lp_Abono
    Sql = "INSERT INTO cta_corriente (cta_cli_pro,cta_fecha,cta_rut,cta_observacion,cta_cargo,cta_abono,cta_saldo,rut_emp) VALUES( " & _
          "'" & CliPro & "','" & FechaDoc & "','" & TxtRut & "','" & Obs & " " & lpnro & "'," & Lp_Cargo & "," & Lp_Abono & "," & Lp_SaldoCtaCte & ",'" & SP_Rut_Activo & "')"
    cn.Execute Sql
    'Fin cta corriente
End Function
Public Function PagoDocumento(lp_IDAbo As Long, Sm_Cli_Pro As String, AboRut As String, Fecha As Date, _
    TxtAbonos As Long, FormaPagoID As Long, FormaPagoTexto As String, Sp_EC As String, lUsuario As String, NroComprobante As Long)
    
    Sql = "INSERT INTO cta_abonos (abo_id,abo_cli_pro,abo_rut,abo_fecha,abo_monto,mpa_id,abo_observacion,usu_nombre,rut_emp,abo_fecha_pago,abo_nro_comprobante) " & _
          "VALUES(" & lp_IDAbo & ",'" & Sm_Cli_Pro & "','" & AboRut & "','" & _
          Format(Fecha, "YYYY-MM-DD") & "'," & CDbl(TxtAbonos) & "," & FormaPagoID & _
          ",'" & FormaPagoTexto & " " & Sp_EC & "','" & lUsuario & "','" & SP_Rut_Activo & "','" & Fql(Fecha) & "'," & NroComprobante & ")"
    cn.Execute Sql
    
    Sql = "INSERT INTO abo_tipos_de_pagos (abo_id,mpa_id,pad_valor) " & _
                         "VALUES(" & lp_IDAbo & "," & FormaPagoID & "," & CDbl(TxtAbonos) & ")"
    cn.Execute Sql
        
End Function

Public Function Fql(fp_Fecha As Date) As String
    Fql = Format(fp_Fecha, "YYYY-MM-DD")
End Function
Public Function CxP(Valor As Variant) As String
    CxP = Replace(Valor, ",", ".")
End Function
Public Function CostoAVG(CodPro As String) As Double
    Dim Qlc As String, RgCP As Recordset
    CostoAVG = 0
    Qlc = "SELECT IFNULL(SUM(kar_cantidad_valor)/SUM(kar_cantidad),0) promedio " & _
            "FROM inv_kardex k " & _
            "WHERE rut_emp='" & SP_Rut_Activo & " 'AND k.kar_movimiento='ENTRADA' " & _
            "GROUP BY k.pro_codigo "
    Consulta RgCP, Qlc
    If RgCP.RecordCount > 0 Then CostoAVG = RgCP!promedio
End Function
Public Function BoldColuna(lv As ListView, Col As Integer)
    If lv.ListItems.Count > 0 Then
        For i = 1 To lv.ListItems.Count
            lv.ListItems(i).ListSubItems(5).Bold = True
        Next
    End If
End Function
Public Function ObtID(Cbo As ComboBox) As Long
    If Cbo.ListIndex > -1 Then
        ObtID = Cbo.ItemData(Cbo.ListIndex)
    Else
        ObtID = 0
    End If
End Function
Private Function ObtenerPromedio(Cod As Long) As Long
    Dim Sqp As String, Rsp As Recordset
    Sqp = "SELECT SUM(k.pro_precio_neto)/COUNT(kar_id) promedio " & _
          "FROM inv_kardex k " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_precio_neto > 0 And pro_codigo =" & Cod
    Consulta Rsp, Sqp
    If Rsp.RecordCount > 0 Then
        ObtenerPromedio = 0 & Rsp!promedio
    Else
        ObtenerPromedio = 0
    End If
End Function
'Ordenar el ListView pinchando en el ancabezado de la columna
'jueves 13 de octubre 2011 23:21

Public Function ordListView(ByVal ColumnHeaderX As MSComctlLib.ColumnHeader, elFormulario As Form, listviewx As ListView)
      
    With listviewx
      
        Dim i As Long
        Dim Formato As String
        Dim strData() As String
          
        Dim Columna As Long
          
        Call SendMessage(elFormulario.Hwnd, WM_SETREDRAW, 0&, 0&)
          
          
        Columna = ColumnHeaderX.Index - 1
          
        '''''''''''''''''''''''''''''''''''''''''''''
        ' Tipo de dato a ordenar
        ''''''''''''''''''''''''''''''''''''''''''''''
        
        
        Select Case UCase$(Mid((ColumnHeaderX.Tag), 1, 1))
      
          
        ' Fecha
        '''''''''''''''''''''''''''''''''''''''''''''
        Case "F"
          
            Formato = "YYYYMMDDHhNnSs"
          
            ' Ordena alfab�ticamente la columna con Fechas _
              ( es la columna que tiene en el tag el valor FECHA )
          
            With .ListItems
                If (Columna > 0) Then
                    For i = 1 To .Count
                        With .item(i).ListSubItems(Columna)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    Formato)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next i
                Else
                    For i = 1 To .Count
                        With .item(i)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsDate(.Text) Then
                                .Text = Format(CDate(.Text), _
                                                    Formato)
                            Else
                                .Text = ""
                            End If
                        End With
                    Next i
                End If
            End With
              
            ' Ordena alfab�ticamente
              
            .SortOrder = (.SortOrder + 1) Mod 2
            .SortKey = ColumnHeaderX.Index - 1
            .Sorted = True
              
            With .ListItems
                If (Columna > 0) Then
                    For i = 1 To .Count
                        With .item(i).ListSubItems(Columna)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next i
                Else
                    For i = 1 To .Count
                        With .item(i)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next i
                End If
            End With
              
        ' Datos de num�ricos
        '''''''''''''''''''''''''''''''''''''''''''''
        Case "N"
          
            ' Ordena alfab�ticamente la columna con n�meros _
              ( es la columna que tiene en el tag el valor NUMBER )
          
            Formato = String(30, "0") & "." & String(30, "0")
                  
            With .ListItems
                If (Columna > 0) Then
                    For i = 1 To .Count
                        With .item(i).ListSubItems(Columna)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsNumeric(.Text) Then
                                If CDbl(.Text) >= 0 Then
                                    .Text = Format(CDbl(.Text), _
                                        Formato)
                                Else
                                    .Text = "&" & invNumeros( _
                                        Format(0 - CDbl(.Text), _
                                        Formato))
                                End If
                            Else
                                .Text = ""
                            End If
                        End With
                    Next i
                Else
                    For i = 1 To .Count
                        With .item(i)
                            .Tag = .Text & Chr$(0) & .Tag
                            If IsNumeric(.Text) Then
                                If CDbl(.Text) >= 0 Then
                                    .Text = Format(CDbl(.Text), _
                                        Formato)
                                Else
                                    .Text = "&" & invNumeros( _
                                        Format(0 - CDbl(.Text), _
                                        Formato))
                                End If
                            Else
                                .Text = ""
                            End If
                        End With
                    Next i
                End If
            End With
              
            ' Ordena alfab�ticamente
              
            .SortOrder = (.SortOrder + 1) Mod 2
            .SortKey = ColumnHeaderX.Index - 1
            .Sorted = True
              
            With .ListItems
                If (Columna > 0) Then
                    For i = 1 To .Count
                        With .item(i).ListSubItems(Columna)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next i
                Else
                    For i = 1 To .Count
                        With .item(i)
                            strData = Split(.Tag, Chr$(0))
                            .Text = strData(0)
                            .Tag = strData(1)
                        End With
                    Next i
                End If
            End With
          
        Case Else
                      
            .SortOrder = (.SortOrder + 1) Mod 2
            .SortKey = ColumnHeaderX.Index - 1
            .Sorted = True
              
        End Select
      
    End With
      
    Call SendMessage(elFormulario.Hwnd, WM_SETREDRAW, 1&, 0&)
    listviewx.Refresh

End Function
'invNumeros necesasios para ordenar listview asc o desc
Public Function invNumeros(ByVal Number As String) As String
    Static i As Integer
    For i = 1 To Len(Number)
        Select Case Mid$(Number, i, 1)
        Case "-": Mid$(Number, i, 1) = " "
        Case "0": Mid$(Number, i, 1) = "9"
        Case "1": Mid$(Number, i, 1) = "8"
        Case "2": Mid$(Number, i, 1) = "7"
        Case "3": Mid$(Number, i, 1) = "6"
        Case "4": Mid$(Number, i, 1) = "5"
        Case "5": Mid$(Number, i, 1) = "4"
        Case "6": Mid$(Number, i, 1) = "3"
        Case "7": Mid$(Number, i, 1) = "2"
        Case "8": Mid$(Number, i, 1) = "1"
        Case "9": Mid$(Number, i, 1) = "0"
        End Select
    Next
    invNumeros = Number
End Function


Public Function AutoIncremento(Accion As String, CodDoc As Integer, Optional ByVal ElNumero) As Long
    Dim aSql As String, aRs As Recordset, Ip_autoID As Integer
    
    aSql = "SELECT tau_id,tau_autoincremento numerito " & _
           "FROM sis_autoincrementos " & _
           "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & CodDoc
    Consulta aRs, aSql
    
    If Accion = "GUARDA" Then
        If aRs.RecordCount > 0 Then
            Ip_autoID = aRs!tau_id
            aSql = "UPDATE sis_autoincrementos SET tau_autoincremento=" & ElNumero & " " & _
                   "WHERE tau_id=" & Ip_autoID
        Else
            aSql = "INSERT INTO sis_autoincrementos (doc_id,tau_autoincremento,rut_emp) " & _
                   "VALUES(" & CodDoc & "," & ElNumero & ",'" & SP_Rut_Activo & "')"
        End If
        cn.Execute aSql
        AutoIncremento = 0
    Else 'ACCION DE LECTURA SOLAMENTE
        If aRs.RecordCount > 0 Then
            AutoIncremento = aRs!numerito + 1
        Else
            AutoIncremento = 1
        End If
    End If
End Function

Public Sub Coloca(H As Variant, V As Variant, Tx As String, Ng As Boolean, Tm As Integer)
    'Coloca impresion con todos los parametros
    '5 Febrero 2012
    Printer.CurrentX = H
    Printer.CurrentY = V
    Printer.FontSize = Tm
    Printer.FontBold = Ng
    Printer.Print Tx
End Sub
Public Function PrimerDiaMes(DtFecha As Date) As Date
   PrimerDiaMes = DateSerial(Year(DtFecha), Month(DtFecha) + 0, 1)
End Function

Public Function UltimoDiaMes(DtFecha As Date) As Date
   UltimoDiaMes = DateSerial(Year(DtFecha), Month(DtFecha) + 1, 0)
End Function
Private Function EjecucionIDE() As Boolean

   On Error GoTo errHandler

   ' Ocasionar un error (los m�todos del objeto Debug solo funcionan
   ' en tiempo de dise�o
   Debug.Print 1 / 0

   ' Si no hubo error, el programa se est� ejecutando a partir del ejecutable
   EjecucionIDE = False
   Exit Function

errHandler:
   ' Si hubo error, estamos en modo de dise�o
   EjecucionIDE = True
   Err.Clear
End Function
Public Function SaldoDocumento(IdDoc As Long) As Long
    Dim Sp_Ql As String, Rs_Saldo As Recordset
    SaldoDocumento = 0
    If IdDoc = 0 Then
        '
    Else
        Sp_Ql = "SELECT total -(IFNULL((SELECT SUM(t.ctd_monto) " & _
                                        "FROM cta_abono_documentos t,cta_abonos a " & _
                                        "WHERE t.abo_id = a.abo_id AND a.abo_cli_pro = 'PRO' AND t.id = v.id),0) " & _
                    " + IFNULL((SELECT SUM(ctd_monto) " & _
                                "FROM cta_abonos Z " & _
                                "INNER JOIN cta_abono_documentos y USING(abo_id) " & _
                                "WHERE z.rut_emp = '" & SP_Rut_Activo & "' AND y.rut_emp = '" & SP_Rut_Activo & "' AND y.id IN(" & _
                                        "SELECT ID " & _
                                        "FROM com_doc_compra w " & _
                                        "WHERE w.id_ref = V.ID)),0)) saldo " & _
                "FROM com_doc_compra v,maestro_proveedores c " & _
                "WHERE v.ID =" & IdDoc & " AND v.rut_emp = '" & SP_Rut_Activo & "' AND id_ref = 0 AND v.doc_id_factura = 0 AND v.Rut = c.rut_proveedor"
        
        Consulta Rs_Saldo, Sp_Ql
        If Rs_Saldo.RecordCount > 0 Then SaldoDocumento = Rs_Saldo!saldo
    End If
End Function

