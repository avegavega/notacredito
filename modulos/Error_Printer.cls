VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Error_Printer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
'variables locales para almacenar los valores de las propiedades
Private mvarDescripcion As Variant 'copia local
Private mvarN_Error As Variant 'copia local
Public Property Let N_Error(ByVal vData As Variant)
'se usa al asignar un valor a la propiedad, en la parte izquierda de una asignaci�n.
'Syntax: X.N_Error = 5
    mvarN_Error = vData
Select Case mvarN_Error
Case "0000"
mvarDescripcion = "Resultado Exitoso"
Case "0001"
mvarDescripcion = "Error interno"
Case "0002"
mvarDescripcion = "Error de inicializaci�n del equipo"
Case "0003"
mvarDescripcion = "Error de proceso interno"
Case "0101"
mvarDescripcion = "Comando inv�lido para el estado actual"
Case "0102"
mvarDescripcion = "Comando inv�lido para el documento actual"
Case "0103"
mvarDescripcion = "Comando s�lo aceptado en modo t�cnico"
Case "0104"
mvarDescripcion = "Comando s�lo aceptado con jumper de desbloqueo"
Case "0105"
mvarDescripcion = "Comando s�lo aceptado sin jumper de desbloqueo"
Case "0106"
mvarDescripcion = "Comando s�lo aceptado con switch de servicio"
Case "0107"
mvarDescripcion = "Comando s�lo aceptado sin switch de servcio"
Case "0201"
mvarDescripcion = "El frame no contiene el largo m�nimo aceptado"
Case "0202"
mvarDescripcion = "Comando inv�lido"
Case "0203"
mvarDescripcion = "Campos en exceso"
Case "0204"
mvarDescripcion = "Campos en defecto"
Case "0205"
mvarDescripcion = "Campo no opcional"
Case "0206"
mvarDescripcion = "Campo alfanum�rico inv�lido"
Case "0207"
mvarDescripcion = "Campo alfab�tico inv�lido"
Case "0208"
mvarDescripcion = "Campo num�rico inv�lido"
Case "0209"
mvarDescripcion = "Campo binario inv�lido"
Case "020A"
mvarDescripcion = "Campo imprimible inv�lido"
Case "020b"
mvarDescripcion = "Campo hexadecimal inv�lido"
Case "020C"
mvarDescripcion = "Campo fecha inv�lido"
Case "020D"
mvarDescripcion = "Campo hora inv�lido"
Case "020E"
mvarDescripcion = "Campo fiscal rich text inv�lido"
Case "020F"
mvarDescripcion = "Campo booleano inv�lido"
Case "0210"
mvarDescripcion = "Largo del campo inv�lido"
Case "0211"
mvarDescripcion = "Extensi�n del comando inv�lida"
Case "0212"
mvarDescripcion = "C�digo de barra no permitido"
Case "0213"
mvarDescripcion = "Atributos de impresi�n no permitidos"
Case "0301"
mvarDescripcion = "Error de hardware"
Case "0302"
mvarDescripcion = "Impresora fuera de l�nea"
Case "0303"
mvarDescripcion = "Error de Impresi�n"
Case "0304"
mvarDescripcion = "Falta de papel"
Case "0305"
mvarDescripcion = "Poco papel disponible"
Case "0307"
mvarDescripcion = "Caracter�stica de impresora no soportada"
Case "0401"
mvarDescripcion = "N�mero de serie inv�lido"
Case "0402"
mvarDescripcion = "Deben configurarse los datos de fiscalizaci�n"
Case "0501"
mvarDescripcion = "Fecha / Hora no configurada"
Case "0502"
mvarDescripcion = "Error en cambio de fecha"
Case "0503"
mvarDescripcion = "Fecha fuera de rango"
Case "0505"
mvarDescripcion = "N�mero de caja inv�lido"
Case "0506"
mvarDescripcion = "RUT inv�lido"
Case "0508"
mvarDescripcion = "N�mero de l�nea de Encabezado/Cola inv�lido"
Case "0509"
mvarDescripcion = "Demasiadas fiscalizaciones"
Case "050b"
mvarDescripcion = "Demasiados cambios de datos de fiscalizaci�n"
Case "050C"
mvarDescripcion = "Demasiados tipos de pagos definidos"
Case "050D"
mvarDescripcion = "Tipo de pago definido previamente"
Case "050E"
mvarDescripcion = "N�mero de pago inv�lido"
Case "050F"
mvarDescripcion = "Descripci�n de pago inv�lida"
Case "0510"
mvarDescripcion = "M�ximo porcentaje de descuento inv�lido"
Case "0511"
mvarDescripcion = "Claves de firma digital inv�lidas"
Case "0512"
mvarDescripcion = "Claves de firma digital no configuradas"
'5.2.7 Retornos sobre Memoria de Transacciones (06):

Case "0601"
mvarDescripcion = "Memoria de transacciones llena"
'5.2.8 Retornos sobre Jornada Fiscal (08):

Case "0801"
mvarDescripcion = "Comando inv�lido fuera de la jornada fiscal"
Case "0802"
mvarDescripcion = "Comando inv�lido dentro de la jornada fiscal"
Case "0803"
mvarDescripcion = "Memoria fiscal llena. Imposible la apertura de la jornada fiscal"
Case "0804"
mvarDescripcion = "M�s de 24 hrs desde la apertura de la jornada fiscal. Se requiere cierre Z"
'MsgBox mvarDescripcion, vbInformation, "Impresora Fiscal"
Case "0805"
mvarDescripcion = "Pagos no definidos"
Case "0806"
mvarDescripcion = "Demasiados pagos utilizados en la jornada fiscal"
Case "0807"
mvarDescripcion = "Periodo auditado sin datos"
'5.2.9 Retornos sobre Transacciones Gen�ricas (09):

Case "0901"
mvarDescripcion = "Overflow"
Case "0902"
mvarDescripcion = "Underflow"
Case "0903"
mvarDescripcion = "Demasiados �tems involucrados en la transacci�n"
Case "0904"
mvarDescripcion = "Demasiadas tasas de impuesto utilizadas"
Case "0905"
mvarDescripcion = "Demasiados descuentos/recargos sobre subtotal involucradas en la transacci�n"
Case "0906"
mvarDescripcion = "Demasiados pagos involucrados en la transacci�n"
Case "0907"
mvarDescripcion = "Item no encontrado"
Case "0908"
mvarDescripcion = "Pago no encontrado"
Case "0909"
mvarDescripcion = "El total debe ser mayor a cero"
Case "090C"
mvarDescripcion = "Tipo de pago no definido"
Case "090D"
mvarDescripcion = "Demasiadas donaciones"
Case "090E"
mvarDescripcion = "Donaci�n no encontrada"
'5.2.10 Retornos sobre Boleta Fiscal (0A):

Case "0A01"
mvarDescripcion = "No permitido luego de descuentos/recargos sobre el subtotal"
Case "0A02"
mvarDescripcion = "No permitido luego de iniciar la fase de pago"
Case "0A03"
mvarDescripcion = "Tipo de �tem inv�lido"
Case "0A04"
mvarDescripcion = "L�nea de descripci�n en blanco"
Case "0A05"
mvarDescripcion = "Cantidad resultante menor que cero"
Case "0A06"
mvarDescripcion = "Cantidad resultante mayor a lo permitido"
Case "0A07"
mvarDescripcion = "Precio total del �tem mayor al permitido"
Case "0A08"
mvarDescripcion = "No permitido antes de iniciar la fase pago"
Case "0A09"
mvarDescripcion = "Fase de pago no finalizada"
Case "0A0A"
mvarDescripcion = "Fase de pago finalizada"
Case "0A0B"
mvarDescripcion = "Monto de pago no permitido"
Case "0A0C"
mvarDescripcion = "Monto de descuento no permitido"
Case "0A0D"
mvarDescripcion = "Monto de donaci�n no permitido"
Case "0A0E"
mvarDescripcion = "Vuelto no mayo a cero"
Case "0A0F"
mvarDescripcion = "No permitido antes de un �tem"
Case "FFFF"
mvarDescripcion = "Error desconocido"

End Select
End Property


Public Property Set N_Error(ByVal vData As Variant)
'se usa al asignar un objeto a la propiedad, en la parte izquierda de una instrucci�n Set.
'Syntax: Set x.N_Error = Form1
    Set mvarN_Error = vData
End Property

Public Property Get N_Error() As Variant
'se usa al recuperar un valor de una propiedad, en la parte derecha de una asignaci�n.
'Syntax: Debug.Print X.N_Error
    If IsObject(mvarN_Error) Then
        Set N_Error = mvarN_Error
    Else
        N_Error = mvarN_Error
    End If
End Property



Public Property Let Descripcion(ByVal vData As Variant)
'se usa al asignar un valor a la propiedad, en la parte izquierda de una asignaci�n.
'Syntax: X.Descripcion = 5
    mvarDescripcion = vData
End Property


Public Property Set Descripcion(ByVal vData As Variant)
'se usa al asignar un objeto a la propiedad, en la parte izquierda de una instrucci�n Set.
'Syntax: Set x.Descripcion = Form1
    Set mvarDescripcion = vData
End Property


Public Property Get Descripcion() As Variant
'se usa al recuperar un valor de una propiedad, en la parte derecha de una asignaci�n.
'Syntax: Debug.Print X.Descripcion
    If IsObject(mvarDescripcion) Then
        Set Descripcion = mvarDescripcion
    Else
        Descripcion = mvarDescripcion
    End If
End Property




