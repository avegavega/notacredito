Attribute VB_Name = "mod_redondea"
Private Declare Function CreateEllipticRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function SetWindowRgn Lib "user32" (ByVal hWnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long

Public Sub RedondearControl(elControl As Control)
Dim ReghWnd As Long

ReghWnd = CreateEllipticRgn(0, 0, elControl.Width, elControl.Height)
SetWindowRgn elControl.hWnd, ReghWnd, True

End Sub
