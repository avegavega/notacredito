VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form vta_BuscaDocumentos 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Documentos disponibles"
   ClientHeight    =   4845
   ClientLeft      =   3750
   ClientTop       =   2040
   ClientWidth     =   13320
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4845
   ScaleWidth      =   13320
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Seleccione documento"
      Height          =   4245
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   13080
      Begin VB.CommandButton CmdNC 
         Caption         =   "Continuar"
         Height          =   330
         Left            =   3570
         TabIndex        =   5
         Top             =   3825
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.CommandButton CmdContinuar 
         Caption         =   "Continuar"
         Height          =   360
         Left            =   3480
         TabIndex        =   4
         Top             =   3795
         Width           =   1185
      End
      Begin VB.ComboBox CboTipoDoc 
         Height          =   315
         ItemData        =   "vta_BuscaDocumentos.frx":0000
         Left            =   210
         List            =   "vta_BuscaDocumentos.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   3840
         Width           =   3270
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3330
         Left            =   210
         TabIndex        =   2
         Top             =   420
         Width           =   12630
         _ExtentX        =   22278
         _ExtentY        =   5874
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1300"
            Text            =   "Fecha"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1200"
            Text            =   "Documento"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N109"
            Text            =   "Nro"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "RUT"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T3000"
            Text            =   "Nombre Proveedor"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N100"
            Text            =   "Neto"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Object.Tag             =   "N100"
            Text            =   "IVA"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   12105
      TabIndex        =   0
      Top             =   4395
      Width           =   1095
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   540
      Top             =   3900
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -30
      OleObjectBlob   =   "vta_BuscaDocumentos.frx":0004
      Top             =   3840
   End
End
Attribute VB_Name = "vta_BuscaDocumentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public B_Seleccion As Boolean
Public SM_SoloSelecciona As String
Private Sub CmdContinuar_Click()
    'CboTipoDoc.ListIndex = 1
    
    If CboTipoDoc.ListIndex = -1 Then
        MsgBox "Seleccione opcion...", vbInformation
        CboTipoDoc.SetFocus
        Exit Sub
    End If
    VtaDirecta.lP_Documento_Referenciado = 0
    
    
    
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            VtaDirecta.lP_Documento_Referenciado = LvDetalle.ListItems(i)
            Exit For
        End If
    Next
    If VtaDirecta.lP_Documento_Referenciado = 0 Then
        MsgBox "Debe seleccionar un documento... ", vbInformation
        LvDetalle.SetFocus
        Exit Sub
    End If
    VtaDirecta.iP_Tipo_NC = CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
    Unload Me
End Sub

Private Sub CmdNC_Click()
    If CboTipoDoc.Enabled Then
    
        If CboTipoDoc.ListIndex = -1 Then
            MsgBox "Seleccione opcion...", vbInformation
            CboTipoDoc.SetFocus
            Exit Sub
        End If
    End If
    venPosNC.lP_Documento_Referenciado = 0
    
    
    
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).Checked Then
            venPosNC.lP_Documento_Referenciado = LvDetalle.ListItems(i)
            Exit For
        End If
    Next
    If venPosNC.lP_Documento_Referenciado = 0 Then
        MsgBox "Debe seleccionar un documento... ", vbInformation
        LvDetalle.SetFocus
        Exit Sub
    End If
    If CboTipoDoc.Enabled Then
        venPosNC.iP_Tipo_NC = CboTipoDoc.ItemData(CboTipoDoc.ListIndex)
    Else
        venPosNC.iP_Tipo_NC = 1
    End If
    Unload Me
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    Aplicar_skin Me
    Consulta RsTmp, Sql
    Centrar Me
    
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    If Not B_Seleccion Then
        CmdSalir.Enabled = False
 '       LvDetalle.ColumnHeaders(1).Width = 0
    End If
    
    Filtro = Empty
    If IG_id_Nota_Debito_Clientes = Val(SG_codigo2) Then
        Filtro = " AND tnc_nota_debito='SI' "
    End If
    LLenarCombo Me.CboTipoDoc, "tnc_nombre", "tnc_id", "ntc_tipos", "1=1" & Filtro
    If SM_SoloSelecciona = "SI" Then
        CboTipoDoc.Enabled = False
    End If
End Sub
'ORDENA COLUMNAS
Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub
Private Sub LvDetalle_ItemCheck(ByVal Item As MSComctlLib.ListItem)
 '   ordListView ColumnHeader, Me, LvDetalle
    Dim lp_Seleccionado As Long
    lp_Seleccionado = Item
    For i = 1 To LvDetalle.ListItems.Count
        LvDetalle.ListItems(i).Checked = False
    Next
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i) = lp_Seleccionado Then
            LvDetalle.ListItems(i).Checked = True
            Exit For
        End If
            
    Next
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvDetalle.SetFocus
    Timer1.Enabled = False
End Sub
