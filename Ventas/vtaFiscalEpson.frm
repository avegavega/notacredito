VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{706C3604-A82B-4400-9EE4-3433F1D8DB08}#1.2#0"; "EpsonFPHostControlX.ocx"
Begin VB.Form vtaBoletaFiscalEpson 
   Caption         =   "Administrar Fiscal Epson"
   ClientHeight    =   9705
   ClientLeft      =   2640
   ClientTop       =   2085
   ClientWidth     =   12600
   LinkTopic       =   "Form1"
   ScaleHeight     =   9705
   ScaleWidth      =   12600
   Begin VB.CommandButton Command23 
      Caption         =   "Resumen Exentos"
      Height          =   390
      Left            =   9285
      TabIndex        =   51
      Top             =   2640
      Width           =   1155
   End
   Begin VB.Frame Frame6 
      Caption         =   "Configurar Pagos"
      Height          =   3570
      Left            =   4155
      TabIndex        =   42
      Top             =   6060
      Width           =   8130
      Begin VB.Frame Frame1 
         Caption         =   "Datos Para Enviar a la Imoresora Fiscal"
         Height          =   990
         Index           =   1
         Left            =   3105
         TabIndex        =   45
         Top             =   1110
         Width           =   4785
         Begin VB.CommandButton Command15 
            Caption         =   "Agregar"
            Height          =   315
            Left            =   2985
            TabIndex        =   47
            Top             =   330
            Width           =   1695
         End
         Begin VB.TextBox TxtPago 
            Height          =   315
            Left            =   210
            TabIndex        =   46
            Top             =   330
            Width           =   2745
         End
      End
      Begin VB.CommandButton Command16 
         Caption         =   "Lee Info IF."
         Height          =   420
         Left            =   765
         TabIndex        =   43
         Top             =   255
         Width           =   1650
      End
      Begin MSComctlLib.ListView LvCpago 
         Height          =   2370
         Left            =   135
         TabIndex        =   44
         ToolTipText     =   "Muestra Los Tipos de Pagos Configurados en la Impresora Fiscal"
         Top             =   690
         Width           =   2955
         _ExtentX        =   5212
         _ExtentY        =   4180
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "C�digo"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Descripci�n"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Configuracion Fecha Hora"
      Height          =   3465
      Left            =   285
      TabIndex        =   32
      Top             =   6060
      Width           =   3795
      Begin VB.Frame Frame5 
         Caption         =   "Fecha y Hora Actual"
         Height          =   1770
         Left            =   390
         TabIndex        =   36
         Top             =   1350
         Width           =   2730
         Begin VB.CommandButton CmdFija 
            Caption         =   "&Fijar"
            Height          =   375
            Left            =   870
            TabIndex        =   41
            Top             =   1185
            Width           =   1440
         End
         Begin MSComCtl2.DTPicker dpHora 
            Height          =   315
            Left            =   1200
            TabIndex        =   37
            Top             =   720
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   556
            _Version        =   393216
            Format          =   96665602
            CurrentDate     =   37981
         End
         Begin MSComCtl2.DTPicker DPFecha 
            Height          =   315
            Left            =   1200
            TabIndex        =   38
            Top             =   375
            Width           =   1305
            _ExtentX        =   2302
            _ExtentY        =   556
            _Version        =   393216
            CustomFormat    =   "dd/MM/yy"
            Format          =   96665603
            CurrentDate     =   37981
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Fecha"
            Height          =   195
            Left            =   690
            TabIndex        =   40
            Top             =   420
            Width           =   450
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Hora"
            Height          =   195
            Left            =   795
            TabIndex        =   39
            Top             =   795
            Width           =   345
         End
      End
      Begin VB.CommandButton Command22 
         Caption         =   "Fechas"
         Height          =   390
         Left            =   285
         TabIndex        =   35
         Top             =   405
         Width           =   825
      End
      Begin VB.TextBox TxtFecha 
         Height          =   285
         Left            =   1290
         TabIndex        =   34
         Top             =   465
         Width           =   1005
      End
      Begin VB.TextBox TxtHora 
         Height          =   300
         Left            =   2370
         TabIndex        =   33
         Top             =   465
         Width           =   1020
      End
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   15405
      TabIndex        =   30
      Text            =   "0"
      Top             =   4365
      Width           =   390
   End
   Begin VB.CommandButton Command21 
      Caption         =   "Command21"
      Height          =   540
      Left            =   1185
      TabIndex        =   29
      Top             =   10920
      Width           =   1350
   End
   Begin VB.CommandButton Command20 
      Caption         =   "Solo Nro Boleta"
      Height          =   540
      Left            =   1080
      TabIndex        =   28
      Top             =   10080
      Width           =   1605
   End
   Begin VB.TextBox TxtEfectivo 
      Height          =   675
      Left            =   17565
      TabIndex        =   27
      Text            =   "0"
      Top             =   7980
      Width           =   3105
   End
   Begin VB.CommandButton Command19 
      Caption         =   "Command19"
      Height          =   360
      Left            =   870
      TabIndex        =   26
      Top             =   11910
      Width           =   1800
   End
   Begin VB.TextBox TxtTotal 
      Height          =   975
      Left            =   16260
      TabIndex        =   25
      Text            =   "Text1"
      Top             =   4305
      Width           =   1905
   End
   Begin EpsonFPHostControlX.EpsonFPHostControl EpsonFPHostControl1 
      Left            =   45
      OleObjectBlob   =   "vtaFiscalEpson.frx":0000
      Top             =   6630
   End
   Begin VB.CommandButton Command18 
      Caption         =   "Command18"
      Height          =   735
      Left            =   6120
      TabIndex        =   23
      Top             =   5160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton Command17 
      Caption         =   "Docto &Slip"
      Height          =   735
      Left            =   4560
      TabIndex        =   22
      Top             =   5160
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox txt_subtotal 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9915
      TabIndex        =   21
      Text            =   "Text1"
      Top             =   3210
      Width           =   1455
   End
   Begin VB.Frame Frame3 
      Caption         =   "Rango de Z "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1965
      Left            =   240
      TabIndex        =   16
      Top             =   3960
      Width           =   3735
      Begin VB.CommandButton Command6 
         Caption         =   "Inf. Z X N� de Z"
         Height          =   615
         Left            =   240
         TabIndex        =   50
         Top             =   1230
         Width           =   1335
      End
      Begin VB.CommandButton Command8 
         Caption         =   "Inf. Trans.X N�Z"
         Height          =   615
         Left            =   1680
         TabIndex        =   49
         Top             =   1230
         Width           =   1335
      End
      Begin VB.TextBox Z_Final 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2058
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   20
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox Z_Inicial 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2058
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   19
         Top             =   480
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "N� Final de Z"
         Height          =   255
         Left            =   480
         TabIndex        =   18
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "N� Inicial de Z"
         Height          =   255
         Left            =   480
         TabIndex        =   17
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Configuraci�n Funciones de Boleta "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   240
      TabIndex        =   13
      Top             =   2160
      Width           =   3735
      Begin VB.CheckBox Check_ImpSub 
         Caption         =   "Imprime Subtotal"
         Height          =   375
         Left            =   480
         TabIndex        =   15
         Top             =   960
         Value           =   1  'Checked
         Width           =   1095
      End
      Begin VB.CheckBox Check_autocorte 
         Caption         =   "AutoCorte"
         Height          =   375
         Left            =   480
         TabIndex        =   14
         Top             =   480
         Value           =   1  'Checked
         Width           =   1215
      End
   End
   Begin VB.CommandButton Command14 
      Caption         =   "&Cerrar Boleta"
      Height          =   255
      Left            =   2400
      TabIndex        =   11
      Top             =   1440
      Width           =   1215
   End
   Begin VB.CommandButton Command13 
      Caption         =   "Devuelve &Item"
      Height          =   255
      Left            =   480
      TabIndex        =   10
      Top             =   1440
      Width           =   1215
   End
   Begin VB.CommandButton Command12 
      Caption         =   "&Realiza Pago"
      Height          =   255
      Left            =   2400
      TabIndex        =   9
      Top             =   1080
      Width           =   1215
   End
   Begin VB.CommandButton Command11 
      Caption         =   "&Subtotal"
      Height          =   255
      Left            =   2400
      TabIndex        =   8
      Top             =   720
      Width           =   1215
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Vende &Item"
      Height          =   255
      Left            =   480
      TabIndex        =   7
      Top             =   1080
      Width           =   1215
   End
   Begin VB.CommandButton Command9 
      Caption         =   "A&brir Boleta"
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   720
      Width           =   1215
   End
   Begin VB.CommandButton Command7 
      Caption         =   "&Informaci�n Jornada Fiscal en Curso"
      Height          =   615
      Left            =   10695
      TabIndex        =   5
      Top             =   1785
      Width           =   1335
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Caracteristicas &Fiscales"
      Height          =   615
      Left            =   9255
      TabIndex        =   4
      Top             =   1785
      Width           =   1335
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Reporte &Z"
      Height          =   615
      Left            =   9255
      TabIndex        =   3
      Top             =   1065
      Width           =   1335
   End
   Begin VB.CommandButton Command3 
      Caption         =   "&Corte Papel"
      Height          =   615
      Left            =   10695
      TabIndex        =   2
      Top             =   1080
      Width           =   1335
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Avance Papel"
      Height          =   615
      Left            =   10695
      TabIndex        =   1
      Top             =   345
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Reporte &X"
      Height          =   615
      Left            =   9255
      TabIndex        =   0
      Top             =   345
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Funciones de Boleta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Index           =   0
      Left            =   240
      TabIndex        =   12
      Top             =   240
      Width           =   3735
   End
   Begin MSComctlLib.ListView LvDetalle 
      Height          =   2730
      Left            =   14850
      TabIndex        =   24
      Top             =   825
      Width           =   6660
      _ExtentX        =   11748
      _ExtentY        =   4815
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "CANTI"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "DESCR"
         Object.Width           =   8043
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "PRECIO UNI"
         Object.Width           =   1614
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "CODIGO INTERNO/BARRA"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView LvPagos 
      Height          =   2115
      Left            =   3240
      TabIndex        =   31
      Top             =   10380
      Width           =   4980
      _ExtentX        =   8784
      _ExtentY        =   3731
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Id"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Detalle"
         Object.Width           =   8043
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Monto"
         Object.Width           =   1614
      EndProperty
   End
   Begin VB.Label Label4 
      Caption         =   "COMANDOS Y FUNCIONES PARA IMPRESORA FISCAL EPSON"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1725
      Left            =   4485
      TabIndex        =   48
      Top             =   465
      Width           =   4200
   End
End
Attribute VB_Name = "vtaBoletaFiscalEpson"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public resp As New Error_Printer

Private Sub CmdEnvia_Click()
    Dim Item As ListItem
    Numero = Numero + 1
    If Numero > 20 Then
        MsgBox "No puede Ingresar Mas de 20 Tipos de Pagos", vbCritical
        Exit Sub
    End If
    Set Item = LvPagos1.ListItems.Add(, , Numero)
    Item.SubItems(1) = UCase(Trim(TxtPago.Text))
End Sub

Private Sub CmdEnviar_Click()
    Call Estado_Fiscal(Me.EpsonFPHostControl1)
    If UCase(Trim(E_F.Periodo_ventas)) = UCase("Periodo de Ventas Abierto") Then
        MsgBox "El Periodo de ventas deve estar Cerrado, Realize un Cierre Z"
        Exit Sub
    End If
     
    If LvPagos1.ListItems.Count = 0 Then Exit Sub
    
   ' CommanD = Chr(5) + Chr(&HC)
   ' ExtensioN = Chr(0) + Chr(0)
    Dim i  As Integer
    For i = 1 To LvPagos1.ListItems.Count
    '    RespuestA = False
        Me.EpsonFPHostControl1.AddDataField Chr$(&H5) & Chr$(&HC)
        'bAnsWer = frmNewMain.Ec.AddDataField(CommanD)
        Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
        'bAnsWer = frmNewMain.Ec.AddDataField(ExtensioN)
        Me.EpsonFPHostControl1.AddDataField Trim(LvPagos1.ListItems(i))
        Me.EpsonFPHostControl1.AddDataField Trim(LvPagos1.ListItems(i).SubItems(1))
        Me.EpsonFPHostControl1.SendCommand
        While Me.EpsonFPHostControl1.State = EFP_S_Busy
            DoEvents
        Wend
    Next i
    Command16_Click
    LvPagos1.ListItems.Clear
End Sub

Private Sub CmdFija_Click()
    Dim fe As String
    Dim ho As String
    fe = ""
    fe = IIf(Len(DPFecha.Day) = 1, "0" & DPFecha.Day, DPFecha.Day)
    fe = fe & IIf(Len(DPFecha.Month) = 1, "0" & DPFecha.Month, DPFecha.Month)
    fe = fe & IIf(Len(DPFecha.Year) = 2, DPFecha.Year, Right(DPFecha.Year, 2))
    ho = ""
    ho = IIf(Len(dpHora.Hour) = 1, "0" & dpHora.Hour, dpHora.Hour)
    ho = ho & IIf(Len(dpHora.Minute) = 1, "0" & dpHora.Minute, dpHora.Minute)
    ho = ho & IIf(Len(dpHora.Second) = 1, "0" & dpHora.Second, dpHora.Second)
    
    Call Estado_Fiscal(Me.EpsonFPHostControl1)
    If UCase(Trim(E_F.Periodo_ventas)) = UCase("PERIODO DE VENTAS ABIERTO") Then
        MsgBox "EL PERIODO DE VENTA DEBE ESTAR CERRADO, REALICE UN CIERRE Z", vbInformation, mSis
        Exit Sub
    End If
   ' CommanD = Chr(5) + Chr(1)
   ' ExtensioN = Chr(0) + Chr(0)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H5) & Chr$(&H1)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    'bAnsWer = FrmNewMain.Ec.AddDataField(ExtensioN)
    Me.EpsonFPHostControl1.AddDataField fe
    Me.EpsonFPHostControl1.AddDataField ho
    Me.EpsonFPHostControl1.SendCommand
    While Me.EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
    Get_Fecha
End Sub

Private Sub Command1_Click()

'DoEvents
    MsgBox Me.EpsonFPHostControl1.State
    
    ' Reporte X
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H2)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand

    MsgBox Me.EpsonFPHostControl1.State

    ' Test de Agujas de la impresora
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H2) & Chr$(&H1)
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    'Me.EpsonFPHostControl1.SendCommand

    ' Ripple Test imprime 10 lineas
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H2) & Chr$(&H4)
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    'Me.EpsonFPHostControl1.AddDataField "10"
      
    'Me.EpsonFPHostControl1.SendCommand


    ' Ripple Test imprime 10 lineas
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H30) & Chr$(&H2)
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    'Me.EpsonFPHostControl1.AddDataField "HOLA que onda"
      
    'Me.EpsonFPHostControl1.SendCommand

'    While Me.EpsonFPHostControl1.State = EFP_S_Busy
'        DoEvents
'    Wend
    
    
End Sub
 
Private Sub Command10_Click()
    
' Venta de un Item

Me.EpsonFPHostControl1.AddDataField Chr$(&HA) & Chr$(&H2)

Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
Me.EpsonFPHostControl1.AddDataField ""
Me.EpsonFPHostControl1.AddDataField ""
Me.EpsonFPHostControl1.AddDataField ""
Me.EpsonFPHostControl1.AddDataField ""
Me.EpsonFPHostControl1.AddDataField "7802950002126"


Me.EpsonFPHostControl1.AddDataField "Descripcion principal"
Me.EpsonFPHostControl1.AddDataField "1800" 'canti '"9999","0" 'ultimo 4, son decimales de la cnatidad
Me.EpsonFPHostControl1.AddDataField "9900000" 'precio '"9999","9999" decimales del precio
Me.EpsonFPHostControl1.AddDataField "1900" 'impuesto





Me.EpsonFPHostControl1.SendCommand



End Sub

Private Sub Command11_Click()

Dim comando, campo1, campo2 As String
Dim ExtensioN As String
Dim enviar, envio As Boolean
Dim a
' Subtotal con impresi�n
If Check_ImpSub = 1 Then
    comando = Chr(10) + Chr(3)
    ExtensioN = Chr(0) + Chr(0)
Else
    comando = Chr(10) + Chr(3)
    ExtensioN = Chr(0) + Chr(1)
End If

enviar = Me.EpsonFPHostControl1.AddDataField(comando)
enviar = Me.EpsonFPHostControl1.AddDataField(ExtensioN)
envio = Me.EpsonFPHostControl1.SendCommand

While EpsonFPHostControl1.State = EFP_S_Busy
    DoEvents
    'tiempo
Wend

txt_subtotal = Me.EpsonFPHostControl1.GetExtraField(1)
txt_subtotal.Refresh

'If envio = False Then a = MsgBox("Problemas con el Puerto asegurese que este abierto", vbCritical, "Informacion al Usuario")
If envio = False Then a = MsgBox("Problemas con el Puerto asegurese que este abierto", vbCritical, "Informacion al Usuario")

End Sub

Private Sub Command12_Click()

' PAGO
Me.EpsonFPHostControl1.AddDataField Chr$(&HA) & Chr$(&H5)
Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)

Me.EpsonFPHostControl1.AddDataField "1"
Me.EpsonFPHostControl1.AddDataField "50000"

Me.EpsonFPHostControl1.SendCommand

End Sub

Private Sub Command13_Click()

' Anulaci�n de Item de Venta

Me.EpsonFPHostControl1.AddDataField Chr$(&HA) & Chr$(&H2)
Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H1)
    
Me.EpsonFPHostControl1.AddDataField "1"
Me.EpsonFPHostControl1.AddDataField "2"
Me.EpsonFPHostControl1.AddDataField "3"
Me.EpsonFPHostControl1.AddDataField "4"
Me.EpsonFPHostControl1.AddDataField "       5"

Me.EpsonFPHostControl1.AddDataField "Descripcion principal"
Me.EpsonFPHostControl1.AddDataField "100000"
Me.EpsonFPHostControl1.AddDataField "10000000"
Me.EpsonFPHostControl1.AddDataField "1800"
    
'Me.EpsonFPHostControl1.AddDataField ""
'Me.EpsonFPHostControl1.AddDataField ""
'Me.EpsonFPHostControl1.AddDataField ""
'Me.EpsonFPHostControl1.AddDataField ""
'Me.EpsonFPHostControl1.AddDataField ""

'Me.EpsonFPHostControl1.AddDataField "Descripcion principal"
'Me.EpsonFPHostControl1.AddDataField "100000"
'Me.EpsonFPHostControl1.AddDataField "10000000"
'Me.EpsonFPHostControl1.AddDataField "1800"


Me.EpsonFPHostControl1.SendCommand

End Sub

Private Sub Command14_Click()

'Cerrar Boleta

Me.EpsonFPHostControl1.AddDataField Chr$(&HA) & Chr$(&H6)

If Check_autocorte = 1 Then
   Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H1)
Else
   Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
End If
    
Me.EpsonFPHostControl1.AddDataField "3"
Me.EpsonFPHostControl1.AddDataField "HOLAALAALALALALALALAL"
Me.EpsonFPHostControl1.AddDataField ""
Me.EpsonFPHostControl1.AddDataField ""
Me.EpsonFPHostControl1.AddDataField ""
Me.EpsonFPHostControl1.AddDataField ""
           
Me.EpsonFPHostControl1.SendCommand
End Sub

Private Sub Command15_Click()
    ' Crea tipo de Pago
    Me.EpsonFPHostControl1.AddDataField Chr$(&H5) & Chr$(&HC)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    Me.EpsonFPHostControl1.AddDataField LvCpago.ListItems.Count + 1
    Me.EpsonFPHostControl1.AddDataField TxtPago
    
    Me.EpsonFPHostControl1.SendCommand
    
    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
    
    
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H5) & Chr$(&HC)
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    'Me.EpsonFPHostControl1.AddDataField "4"
    'Me.EpsonFPHostControl1.AddDataField "TARJETA DE DEBITO"
    
   ' Me.EpsonFPHostControl1.SendCommand
    Command16_Click
    
End Sub

Private Sub Command16_Click()
    Dim i As Integer
  '  CommanD = Chr(5) + Chr(&HD)
  '  ExtensioN = Chr(0) + Chr(0)
    LvCpago.ListItems.Clear
    For i = 1 To 20
 '   RespuestA = False
                    Me.EpsonFPHostControl1.AddDataField Chr$(&H5) & Chr$(&HD)
                    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
                     Me.EpsonFPHostControl1.AddDataField i
                    'bAnsWer = frmNewMain.Ec.AddDataField(ExtensioN)
                    'bAnsWer = frmNewMain.Ec.AddDataField(i)
                    
                    Me.EpsonFPHostControl1.SendCommand
                    While EpsonFPHostControl1.State = EFP_S_Busy
                        DoEvents
                    Wend
                    Set itmx = LvCpago.ListItems.Add(, , i)
                    'EsperA
               
                If Me.EpsonFPHostControl1.GetExtraField(1) = "" Then
                    LvCpago.ListItems.Remove (i)
                    Exit For
                    Else
                    itmx.SubItems(1) = Me.EpsonFPHostControl1.GetExtraField(1)
                    Numero = i
                    End If
    Next i










Exit Sub
    ' Leer Medio de Pago
    Me.EpsonFPHostControl1.AddDataField Chr$(&H5) & Chr$(&HD)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    Me.EpsonFPHostControl1.AddDataField txtNroPago
    
    Me.EpsonFPHostControl1.SendCommand
    
    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
    
    MsgBox "Descripcion tipo pago numero " & txtNroPago & " : " & Me.EpsonFPHostControl1.GetExtraField(1)

End Sub

Private Sub Command17_Click()

    'Mover Cheque a Posicion Inicial
    Me.EpsonFPHostControl1.AddDataField (Chr$(&H7) & Chr$(&H27))
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand

    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend

    Me.EpsonFPHostControl1.AddDataField (Chr$(&HEE) & Chr$(&H1))
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H1C) & Chr$(&H1C)
    Me.EpsonFPHostControl1.AddDataField ""
    Me.EpsonFPHostControl1.AddDataField ""
    Me.EpsonFPHostControl1.AddDataField ""
    Me.EpsonFPHostControl1.AddDataField ""
    Me.EpsonFPHostControl1.AddDataField ""
    Me.EpsonFPHostControl1.AddDataField ""
    
    Me.EpsonFPHostControl1.SendCommand

    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend

    ' Comando de Envio Linea Impresi�n Slip Horizontal Normal
    Me.EpsonFPHostControl1.AddDataField (Chr$(&HEE) & Chr$(&H2))
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.AddDataField ""
    
    Me.EpsonFPHostControl1.AddDataField "         1         2         3         4"
    
    Me.EpsonFPHostControl1.SendCommand

    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
''
    ' Comando de Envio Linea Impresi�n Slip Horizontal Normal
    Me.EpsonFPHostControl1.AddDataField (Chr$(&HEE) & Chr$(&H2))
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.AddDataField ""
    
    Me.EpsonFPHostControl1.AddDataField "12345678901234567890123456789012345678901234567"
    
    Me.EpsonFPHostControl1.SendCommand

    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend


''


    Me.EpsonFPHostControl1.AddDataField (Chr$(&HEE) & Chr$(&H6))
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H1)
    
    Me.EpsonFPHostControl1.SendCommand



End Sub

Private Sub Command18_Click()

    ba = Me.EpsonFPHostControl1.AddDataField(Chr$(&H0) & Chr$(&H1))
    
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    resp = Me.EpsonFPHostControl1.SendCommand


    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend

    MsgBox "QUE ? " & resp
    
    MsgBox "Fiscal: " & Hex(Me.EpsonFPHostControl1.FiscalStatus)
    MsgBox "Printer: " & Hex(Me.EpsonFPHostControl1.PrinterStatus)
    MsgBox "Return Code: " & Hex(Me.EpsonFPHostControl1.ReturnCode)

End Sub

Private Sub Command19_Click()
    Me.EmiteBoleta
End Sub

Private Sub Command2_Click()
Dim ba As Boolean
    
    Me.EpsonFPHostControl1.AddDataField (Chr$(&H7) & Chr$(&H1))
    
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    Me.EpsonFPHostControl1.AddDataField "10"
    
    Me.EpsonFPHostControl1.SendCommand

End Sub

Private Sub Command20_Click()
    Lp_Nro_Boleta_Obtenida_IF = 0
      
   ' Do While Lp_Nro_Boleta_Obtenida_IF = 0
            NroBoletaFiscalEpsopn
   ' Loop
 
    
    MsgBox "Ultimo Nro Boelta " & vbNewLine & Lp_Nro_Boleta_Obtenida_IF
End Sub

Private Sub Command21_Click()
    ' Informaci�n
    Dim PASOS As Variant
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&HA)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand

PASOS = Hex(Me.EpsonFPHostControl1.FiscalStatus)
PASOS = Hex(Me.EpsonFPHostControl1.PrinterStatus)
PASOS = Hex(Me.EpsonFPHostControl1.ReturnCode)
'
PASOS = Me.EpsonFPHostControl1.GetExtraField(1)
PASOS = Me.EpsonFPHostControl1.GetExtraField(2)
PASOS = Me.EpsonFPHostControl1.GetExtraField(3)
PASOS = Me.EpsonFPHostControl1.GetExtraField(4)
PASOS = Me.EpsonFPHostControl1.GetExtraField(5)
'
    MsgBox "Numero de oleta: " & Me.EpsonFPHostControl1.GetExtraField(5)
'
'
'    MsgBox "ID del mecanismo impresor: " & Me.EpsonFPHostControl1.GetExtraField(6)
'
'    MsgBox "Nombre Mecanismo Impresor: " & Me.EpsonFPHostControl1.GetExtraField(7)
'    MsgBox "Capacidad Memoria fiscal : " & Me.EpsonFPHostControl1.GetExtraField(8)
'    MsgBox "Capacidad Memoria Transac: " & Me.EpsonFPHostControl1.GetExtraField(9)
'    MsgBox "Capacidad Memoria Trabajo: " & Me.EpsonFPHostControl1.GetExtraField(10)
'    MsgBox "Jumper de Servicio Conect: " & Me.EpsonFPHostControl1.GetExtraField(11)
'    MsgBox "Estado de los dip-switche: " & Me.EpsonFPHostControl1.GetExtraField(12)
'    MsgBox "Jumper de Servicio Conect: " & Me.EpsonFPHostControl1.GetExtraField(13)
'    MsgBox "Estado de los dip-switche: " & Me.EpsonFPHostControl1.GetExtraField(14)
End Sub

Private Sub Command22_Click()
    DPFecha.Value = Date
    dpHora.Value = Time
    Get_Fecha
End Sub

Public Sub Exentos()

    Sql = "SELECT d.codigo,p.descripcion,precio_real,SUM(unidades) cant, SUM(subtotal) total " & _
                    "FROM ven_detalle  d " & _
                "JOIN ven_doc_venta v ON d.no_documento=v.no_documento AND d.doc_id=v.doc_id " & _
                "JOIN maestro_productos p on p.codigo=d.codigo " & _
                "WHERE p.nombre_familia='EXENTO' AND v.caj_id=" & LG_id_Caja & " " & _
                "GROUP BY codigo"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount = 0 Then Exit Sub
    
    
    
    RsTmp.MoveFirst
            
                                
    
    
    Dim t As Integer
    Dim e_Cant As String * 2
    Dim e_Desc As String * 20
    Dim e_Precio As String * 6
    Dim e_Total As String * 8
    

    

    'Dim t As Integer
    
    '*** APERTURA DE DNF ( Documento No Fiscal ) ***
    EpsonFPHostControl1.AddDataField Chr$(&HE) & Chr$(&H1)
    EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    EpsonFPHostControl1.AddDataField ""
    EpsonFPHostControl1.AddDataField ""
    
    EpsonFPHostControl1.SendCommand
    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
   

    '*** IMPRESION DE 35 LINEAS UNA A UNA ***

        '*** IMPRESION DE TEXTO ***
        Dim Lp_TotalExentos As Long
        Lp_TotalExentos = 0
        Do While Not RsTmp.EOF
                    EpsonFPHostControl1.AddDataField Chr$(&HE) & Chr$(&H2)
                    EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
                   ' EpsonFPHostControl1.AddDataField "             *** LINEA DE TEXTO No. " & IIf(Len(Trim(Str(linea))) = 1, "0" & Trim(Str(linea)), Trim(Str(linea))) & " ***"
                   RSet e_Cant = RsTmp!cant
                   e_Desc = RsTmp!Descripcion
                   RSet e_Precio = RsTmp!precio_real
                   RSet e_Total = RsTmp!Total
                   Lp_TotalExentos = Lp_TotalExentos + RsTmp!Total
                    Me.EpsonFPHostControl1.AddDataField e_Cant & " " & e_Desc & " " & e_Precio & " " & e_Total & "*"
                    
                    Me.EpsonFPHostControl1.SendCommand
                    While Me.EpsonFPHostControl1.State = EFP_S_Busy
                        DoEvents
                    Wend
                    RsTmp.MoveNext
        Loop
        RSet e_Cant = ""
        e_Desc = "Total"
        RSet e_Precio = ""
        RSet e_Total = NumFormat(Lp_TotalExentos)

        
        '*******************************
        
        EpsonFPHostControl1.AddDataField Chr$(&HE) & Chr$(&H2)
        EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
        'EpsonFPHostControl1.AddDataField "             *** LINEA DE TEXTO No. " & IIf(Len(Trim(Str(linea))) = 1, "0" & Trim(Str(linea)), Trim(Str(linea))) & " ***"
          Me.EpsonFPHostControl1.AddDataField e_Cant & " " & e_Desc & " " & e_Precio & " " & e_Total & "*"
        EpsonFPHostControl1.SendCommand
        While EpsonFPHostControl1.State = EFP_S_Busy
            DoEvents
        Wend
   
        
    '*** CIERRA DNF ***
    EpsonFPHostControl1.AddDataField Chr$(&HE) & Chr$(&H6)
    EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H1)
    EpsonFPHostControl1.AddDataField ""
    EpsonFPHostControl1.AddDataField ""
    EpsonFPHostControl1.AddDataField ""
    EpsonFPHostControl1.AddDataField ""
    EpsonFPHostControl1.AddDataField ""
    EpsonFPHostControl1.AddDataField ""
    
    EpsonFPHostControl1.SendCommand
    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
   
End Sub

Private Sub Command3_Click()
Dim ba As Boolean
    
    Me.EpsonFPHostControl1.AddDataField (Chr$(&H7) & Chr$(&H2))
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
   
    Me.EpsonFPHostControl1.SendCommand

End Sub

Private Sub Command4_Click()
    ' Reporte Z
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H1)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand

End Sub

Private Sub Command5_Click()
    
    ' Obtenci�n de las Caracteristicas fiscales
    Me.EpsonFPHostControl1.AddDataField Chr$(&H2) & Chr$(&HA)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H1)
    
    Me.EpsonFPHostControl1.SendCommand
    
    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend

    MsgBox "Fiscal: " & Hex(Me.EpsonFPHostControl1.FiscalStatus)
    MsgBox "Printer: " & Hex(Me.EpsonFPHostControl1.PrinterStatus)
    MsgBox "Return Code: " & Hex(Me.EpsonFPHostControl1.ReturnCode)
    
    MsgBox "Nombre de la Versi�n     : " & Me.EpsonFPHostControl1.GetExtraField(1)
    MsgBox "ID del Pa�s              : " & Me.EpsonFPHostControl1.GetExtraField(2)
    MsgBox "Versi�n de Firmware Mayor: " & Me.EpsonFPHostControl1.GetExtraField(3)
    MsgBox "Versi�n de Firmware Menor: " & Me.EpsonFPHostControl1.GetExtraField(4)
    MsgBox "Versi�n de Firmware Compi: " & Me.EpsonFPHostControl1.GetExtraField(5)
    MsgBox "ID del mecanismo impresor: " & Me.EpsonFPHostControl1.GetExtraField(6)

    MsgBox "Nombre Mecanismo Impresor: " & Me.EpsonFPHostControl1.GetExtraField(7)
    MsgBox "Capacidad Memoria fiscal : " & Me.EpsonFPHostControl1.GetExtraField(8)
    MsgBox "Capacidad Memoria Transac: " & Me.EpsonFPHostControl1.GetExtraField(9)
    MsgBox "Capacidad Memoria Trabajo: " & Me.EpsonFPHostControl1.GetExtraField(10)
    MsgBox "Jumper de Servicio Conect: " & Me.EpsonFPHostControl1.GetExtraField(11)
    MsgBox "Estado de los dip-switche: " & Me.EpsonFPHostControl1.GetExtraField(12)



End Sub



Private Sub Command6_Click()
Dim buf_fer, mas_dat
Dim fso, txtfile

'REPORTE INFORMES Z POR NUMERO
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H4)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H1)
    ' DESDE EL N� DE Z
    Me.EpsonFPHostControl1.AddDataField Z_Inicial
    ' HASTA EL N� DE Z
    Me.EpsonFPHostControl1.AddDataField Z_Final
    
    Me.EpsonFPHostControl1.SendCommand

    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend

    If Hex(Me.EpsonFPHostControl1.ReturnCode) <> 0 Then
        MsgBox "Hubo un Error, se cancelo el reporte"
        Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H20)
        Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
        Me.EpsonFPHostControl1.SendCommand
    End If
    
    'CREA ARCHIVO PARA GRABAR EL INFORME OBTENIDO
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set txtfile = fso.CreateTextFile("c:\FACTURAELECTRONICA\EPSON\InformesZ.ejf", True)
    
    'CICLO PARA LEER Y GRABAR LOS DATOS
    Do
    
    ' OBTIENE LOS SIGUIENTES DATOS DEL INFORME
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H20)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand

    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
    
    'GRABAR EL REGISTRO GET(1)
    buf_fer = Me.EpsonFPHostControl1.GetExtraField(1)
    mas_dat = Me.EpsonFPHostControl1.GetExtraField(2)
    txtfile.Write (buf_fer) ' Escribe una l�nea.
    
    Loop While Hex(Me.EpsonFPHostControl1.ReturnCode) <> 0 Or mas_dat <> "N"

    txtfile.Close
    Set fso = Nothing


    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H21)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand



End Sub

Private Sub Command7_Click()
    ' Informaci�n
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&HA)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand

    MsgBox "Fiscal: " & Hex(Me.EpsonFPHostControl1.FiscalStatus)
    MsgBox "Printer: " & Hex(Me.EpsonFPHostControl1.PrinterStatus)
    MsgBox "Return Code: " & Hex(Me.EpsonFPHostControl1.ReturnCode)
    
    MsgBox "Fecha de Apertura Jornada Fiscal: " & Me.EpsonFPHostControl1.GetExtraField(1)
    MsgBox "Hora de Apertura Jornada Fiscal : " & Me.EpsonFPHostControl1.GetExtraField(2)
    MsgBox "Versi�n de Firmware Mayor: " & Me.EpsonFPHostControl1.GetExtraField(3)
    MsgBox "Versi�n de Firmware Menor: " & Me.EpsonFPHostControl1.GetExtraField(4)
  '  MsgBox "Versi�n de Firmware Compi: " & Me.EpsonFPHostControl1.GetExtraField(5)
    
    MsgBox "Numero de oleta: " & Me.EpsonFPHostControl1.GetExtraField(5)
   
    
    MsgBox "ID del mecanismo impresor: " & Me.EpsonFPHostControl1.GetExtraField(6)

    MsgBox "Nombre Mecanismo Impresor: " & Me.EpsonFPHostControl1.GetExtraField(7)
    MsgBox "Capacidad Memoria fiscal : " & Me.EpsonFPHostControl1.GetExtraField(8)
    MsgBox "Capacidad Memoria Transac: " & Me.EpsonFPHostControl1.GetExtraField(9)
    MsgBox "Capacidad Memoria Trabajo: " & Me.EpsonFPHostControl1.GetExtraField(10)
    MsgBox "Jumper de Servicio Conect: " & Me.EpsonFPHostControl1.GetExtraField(11)
    MsgBox "Estado de los dip-switche: " & Me.EpsonFPHostControl1.GetExtraField(12)
    MsgBox "Jumper de Servicio Conect: " & Me.EpsonFPHostControl1.GetExtraField(13)
    MsgBox "Estado de los dip-switche: " & Me.EpsonFPHostControl1.GetExtraField(14)


End Sub
Private Sub NroBoletaFiscalEpsopn()
      ' Informaci�n
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&HA)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)

    Me.EpsonFPHostControl1.SendCommand
    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
    
    
    Lp_Nro_Boleta_Obtenida_IF = Val(Me.EpsonFPHostControl1.GetExtraField(5))
    
End Sub



Private Sub Command8_Click()
Dim buf_fer, mas_dat
Dim fso, txtfile

'REPORTE TRANSACCIONES POR NUMERO DE Z
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H11)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H1)
    ' DESDE EL N� DE Z
    Me.EpsonFPHostControl1.AddDataField Z_Inicial
    ' HASTA EL N� DE Z
    Me.EpsonFPHostControl1.AddDataField Z_Final
    
    Me.EpsonFPHostControl1.SendCommand

    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend

    If Hex(Me.EpsonFPHostControl1.ReturnCode) <> 0 Then
        MsgBox "Hubo un Error, se cancelo el reporte"
        Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H22)
        Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
        Me.EpsonFPHostControl1.SendCommand
    End If
    
    'CREA ARCHIVO PARA GRABAR EL INFORME OBTENIDO
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set txtfile = fso.CreateTextFile("c:\transacciones.ejf", True)
    
    'CICLO PARA LEER Y GRABAR LOS DATOS
    Do
    
    ' OBTIENE LOS SIGUIENTES DATOS DEL INFORME
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H20)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand

    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
    
    'GRABAR EL REGISTRO GET(1)
    buf_fer = Me.EpsonFPHostControl1.GetExtraField(1)
    mas_dat = Me.EpsonFPHostControl1.GetExtraField(2)
    txtfile.Write (buf_fer) ' Escribe una l�nea.
    
    Loop While Hex(Me.EpsonFPHostControl1.ReturnCode) <> 0 Or mas_dat <> "N"

    txtfile.Close
    Set fso = Nothing


    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H21)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand


End Sub

Private Sub Command9_Click()
    
' Inicio de Boleta
Me.EpsonFPHostControl1.AddDataField Chr$(&HA) & Chr$(&H1)
Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)

' N�mero del Logotipo
Me.EpsonFPHostControl1.AddDataField ""
' Resolucion del logo
Me.EpsonFPHostControl1.AddDataField ""
    
    
Me.EpsonFPHostControl1.SendCommand

End Sub

Private Sub EpsonFPHostControl1_OnError()
    MsgBox "Error: " & Hex(Me.EpsonFPHostControl1.LastError)
End Sub

Private Sub EpsonFPHostControl1_OnFinalAnswer()

'If Val(Hex(Me.EpsonFPHostControl1.ReturnCode)) <> 0 Or Val(Hex(Me.EpsonFPHostControl1.PrinterStatus)) <> 0 Then
If Val(Hex(Me.EpsonFPHostControl1.ReturnCode)) <> 0 Then
    
    If Hex(Me.EpsonFPHostControl1.FiscalStatus) = 8081 Then
        Call Sleep(200)
        Me.EpsonFPHostControl1.SendCommand
    End If
    
    MsgBox "Fiscal: " & Hex(Me.EpsonFPHostControl1.FiscalStatus)
    MsgBox "Printer: " & Hex(Me.EpsonFPHostControl1.PrinterStatus)
    MsgBox "Return Code: " & Hex(Me.EpsonFPHostControl1.ReturnCode)
   
    
    'MsgBox "Campo 1" & Me.EpsonFPHostControl1.GetExtraField(1)
    'MsgBox "Campo 2" & Me.EpsonFPHostControl1.GetExtraField(2)
    'MsgBox "Campo 3" & Me.EpsonFPHostControl1.GetExtraField(3)

End If

End Sub

Private Sub Form_Load()
Me.EpsonFPHostControl1.OpenPort
End Sub

Private Sub Form_Unload(Cancel As Integer)
Me.EpsonFPHostControl1.ClosePort
End Sub


Private Sub Get_Fecha()
  '  CommanD = Chr(5) + Chr(2)
  '  ExtensioN = Chr(0) + Chr(0)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H5) & Chr$(&H2)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    Me.EpsonFPHostControl1.SendCommand
    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
    TxtFecha = Left(Me.EpsonFPHostControl1.GetExtraField(1), 2) & "/" & Mid(Me.EpsonFPHostControl1.GetExtraField(1), 3, 2) & "/" & Right(Me.EpsonFPHostControl1.GetExtraField(1), 2)
    TxtHora = Me.EpsonFPHostControl1.GetExtraField(2)
End Sub
Public Sub EmiteInformeX()

'DoEvents
    MsgBox Me.EpsonFPHostControl1.State
    
    ' Reporte X
    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H2)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    Me.EpsonFPHostControl1.SendCommand

    MsgBox Me.EpsonFPHostControl1.State

    ' Test de Agujas de la impresora
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H2) & Chr$(&H1)
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    'Me.EpsonFPHostControl1.SendCommand

    ' Ripple Test imprime 10 lineas
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H2) & Chr$(&H4)
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    'Me.EpsonFPHostControl1.AddDataField "10"
      
    'Me.EpsonFPHostControl1.SendCommand


    ' Ripple Test imprime 10 lineas
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H30) & Chr$(&H2)
    'Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    'Me.EpsonFPHostControl1.AddDataField "HOLA que onda"
      
    'Me.EpsonFPHostControl1.SendCommand

'    While Me.EpsonFPHostControl1.State = EFP_S_Busy
'        DoEvents
'    Wend
    


End Sub

Public Sub EmiteInformeZ()
    Dim Lp_TotalExentos As Long
    If SP_Rut_Activo = "76.158.157-0" Then
        'falta colocar el rut de bluemarket.
        Exentos
    End If

    Me.EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H1)
    Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    While Me.EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
    Me.EpsonFPHostControl1.SendCommand
    
    
    
End Sub

Public Sub EmiteBoleta()
        Dim Sp_Cantidad As String
        
        Dim Sp_Decimales  As String * 4
        
Dim comando, campo1, campo2 As String
Dim ExtensioN As String
Dim enviar, envio As Boolean
Dim a

     

        ' Inicio de Boleta
        Me.EpsonFPHostControl1.AddDataField Chr$(&HA) & Chr$(&H1)
        Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
        
        ' N�mero del Logotipo
        Me.EpsonFPHostControl1.AddDataField ""
        ' Resolucion del logo
        Me.EpsonFPHostControl1.AddDataField ""
            
            
        Me.EpsonFPHostControl1.SendCommand
        While EpsonFPHostControl1.State = EFP_S_Busy
            DoEvents
        Wend
    
        'Call Sleep(350) 'espera por 1 segundos
            ' Venta de un Item
        For i = 1 To Me.LVDetalle.ListItems.Count
           ' Venta de un Item

            Me.EpsonFPHostControl1.AddDataField Chr$(&HA) & Chr$(&H2)
            
            Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
                
            Me.EpsonFPHostControl1.AddDataField ""
            Me.EpsonFPHostControl1.AddDataField ""
            Me.EpsonFPHostControl1.AddDataField ""
            Me.EpsonFPHostControl1.AddDataField ""
            Me.EpsonFPHostControl1.AddDataField "7802950002126"
            
            If InStr(1, LVDetalle.ListItems(i), ".") > 0 Then
                Sp_Decimales = Mid(LVDetalle.ListItems(i), InStr(1, LVDetalle.ListItems(i), ".") + 1) & "0000"
                
                
                Sp_Cantidad = Right("0000" & Mid(LVDetalle.ListItems(i), 1, InStr(1, LVDetalle.ListItems(i), ".") - 1), 4)
                
            Else
                Sp_Decimales = "0000"
                
                Sp_Cantidad = Trim(LVDetalle.ListItems(i))
            End If
            
            
            Me.EpsonFPHostControl1.AddDataField LVDetalle.ListItems(i).SubItems(1)
            Me.EpsonFPHostControl1.AddDataField Sp_Cantidad & Sp_Decimales   '"1800"       'canti '"9999","0" 'ultimo 4, son decimales de la cnatidad
            Me.EpsonFPHostControl1.AddDataField CDbl(LVDetalle.ListItems(i).SubItems(2)) & "0000"    '"9900000" 'precio '"9999","9999" decimales del precio
            Me.EpsonFPHostControl1.AddDataField "1900" 'impuesto
            
            
            Me.EpsonFPHostControl1.SendCommand
            While EpsonFPHostControl1.State = EFP_S_Busy
                DoEvents
            Wend
            'Call Sleep(450) 'espera por 1 segundos
            
        Next

        ' Subtotal con impresi�n
        If Check_ImpSub = 1 Then
            comando = Chr(10) + Chr(3)
            ExtensioN = Chr(0) + Chr(0)
        Else
            comando = Chr(10) + Chr(3)
            ExtensioN = Chr(0) + Chr(1)
        End If
        
        enviar = Me.EpsonFPHostControl1.AddDataField(comando)
        enviar = Me.EpsonFPHostControl1.AddDataField(ExtensioN)
        envio = Me.EpsonFPHostControl1.SendCommand
        
        While EpsonFPHostControl1.State = EFP_S_Busy
            DoEvents
            'tiempo
        Wend
        
        txt_subtotal = Me.EpsonFPHostControl1.GetExtraField(1)
        txt_subtotal.Refresh
        While EpsonFPHostControl1.State = EFP_S_Busy
            DoEvents
        Wend

        
        ' PAGO
        Me.EpsonFPHostControl1.AddDataField Chr$(&HA) & Chr$(&H5)
        Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
        
        
        
         cuantopaso = 0
                    
        For i = 1 To Me.LvPagos.ListItems.Count
            cuantopaso = cuantopaso + CDbl(Me.LvPagos.ListItems(i).SubItems(2))
            'Vr = OcxFiscal.agregapago(Me.LvPagos.ListItems(i), CDbl(Me.LvPagos.ListItems(i).SubItems(2)))
             Me.EpsonFPHostControl1.AddDataField Me.LvPagos.ListItems(i)
             Me.EpsonFPHostControl1.AddDataField CDbl(Me.LvPagos.ListItems(i).SubItems(2))
            
        Next
        Lp_CuantoPaga = CDbl(txtTotal)
        If Lp_CuantoPaga > cuantopaso Then
            'Vr = OcxFiscal.agregapago(0, Lp_CuantoPaga - cuantopaso)
             Me.EpsonFPHostControl1.AddDataField "1"
             Me.EpsonFPHostControl1.AddDataField Lp_CuantoPaga - cuantopaso
        End If
        
        'Me.EpsonFPHostControl1.AddDataField "1"
        'Me.EpsonFPHostControl1.AddDataField CDbl(TxtEfectivo)
        
        Me.EpsonFPHostControl1.SendCommand
        'Call Sleep(350) 'espera por 1 segundos
    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
        
        'Cerrar Boleta

        Me.EpsonFPHostControl1.AddDataField Chr$(&HA) & Chr$(&H6)
        
     'autocorte
       Me.EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H1)
      
            
        Me.EpsonFPHostControl1.AddDataField "3"
        Me.EpsonFPHostControl1.AddDataField LogUsuario
        Me.EpsonFPHostControl1.AddDataField ""
        Me.EpsonFPHostControl1.AddDataField ""
        Me.EpsonFPHostControl1.AddDataField ""
        Me.EpsonFPHostControl1.AddDataField ""
                   
        Me.EpsonFPHostControl1.SendCommand

        While EpsonFPHostControl1.State = EFP_S_Busy
            DoEvents
        Wend
           'Nro Boleta
        NroBoletaFiscalEpsopn
End Sub

Private Function Estado(Control As EpsonFPHostControl) As String
    'CommanD = Chr(&H0) + Chr(&H1)
    'bAnsWer = Control.AddDataField(CommanD)
           Control.AddDataField Chr$(&H0) & Chr$(&H1)
 '   ExtensioN = Chr(0) + Chr(0)
 '   bAnsWer = Control.AddDataField(ExtensioN)
        Control.AddDataField Chr$(&H0) & Chr$(&H0)
        Control.SendCommand
        While EpsonFPHostControl1.State = EFP_S_Busy
            DoEvents
        Wend
    
   ' RespuestA = False
   ' EsperA
    Estado = resp.N_Error
End Function


Public Function error_grave() As Boolean
    Dim mvarDescripcion As String
    Select Case resp.N_Error
        Case "0301"
            mvarDescripcion = "Error de hardware"
            MsgBox mvarDescripcion, vbCritical
            error_grave = True
        Case "0302"
            mvarDescripcion = "Impresora fuera de l�nea, Intente restaurar la Conexion"
            If MsgBox(mvarDescripcion, vbCritical + vbYesNo) = vbOK Then
            error_grave = False
              'Form_Load
            Else
                error_grave = True
            End If
        Case "0303"
            mvarDescripcion = "Error de Impresi�n"
            MsgBox mvarDescripcion, vbCritical
            error_grave = True
        Case "0304"
            mvarDescripcion = "Falta de papel"
            MsgBox mvarDescripcion, vbCritical
            error_grave = True
        Case "0305"
            mvarDescripcion = "Poco papel disponible"
            MsgBox mvarDescripcion, vbCritical
            error_grave = False
        Case "0801"
            mvarDescripcion = "Comando inv�lido fuera de la jornada fiscal"
            MsgBox mvarDescripcion, vbCritical
            error_grave = True
        Case "0802"
            mvarDescripcion = "Comando inv�lido dentro de la jornada fiscal"
            MsgBox mvarDescripcion, vbCritical
            error_grave = True
        Case "0803"
            mvarDescripcion = "Memoria fiscal llena. Imposible la apertura de la jornada fiscal"
            MsgBox mvarDescripcion, vbCritical
            error_grave = True
        Case "0804"
            mvarDescripcion = "M�s de 24 hs desde la apertura de la jornada fiscal. Se requiere cierre Z"
            MsgBox mvarDescripcion, vbInformation, "Impresora Fiscal"
            error_grave = True
    End Select
End Function

Public Function Estado_Impresora(Ec As EpsonFPHostControl) As Boolean
    Dim m As String
    Estado_Impresora = True
    Call Estado(Ec)
    m = HexToBin(Format(Hex(Ec.PrinterStatus), "0000"))
    If Trim(m) <> "" Then
        If Mid(m, 1, 1) = "0" Then
            E_I.Estado = True
            'Estado_Impresora = True
        Else
            E_I.Estado = False
            Estado_Impresora = False
        End If
        If Mid(m, 2, 1) = "0" Then
            E_I.Error = True
            'Estado_Impresora = True
        Else
            E_I.Error = False
            Estado_Impresora = False
        End If
        If Mid(m, 3, 1) = "0" Then
            E_I.Tapa = True
            'Estado_Impresora = True
        Else
            E_I.Tapa = False
            Estado_Impresora = False
        End If
        
        If Mid(m, 4, 1) = "0" Then
            E_I.Cajon = True
            'Estado_Impresora = True
        Else
            E_I.Cajon = False
            'Estado_Impresora = True
        End If
        If Mid(m, 14, 2) = "00" Then
            E_I.Papel = "Sin Problemas"
            'Estado_Impresora = True
        ElseIf Mid(m, 14, 2) = "01" Then
            E_I.Papel = "Poco Papel Disponible"
            'Estado_Impresora = True
            ElseIf Mid(m, 14, 2) = "10" Then
            E_I.Papel = "Papel NO Disponible"
            Estado_Impresora = False
        End If
    End If
End Function
Public Function Estado_Fiscal(Ec As EpsonFPHostControl) As Boolean
    Dim m As String
    Estado_Fiscal = True
    Call Estado(Ec)
    m = HexToBin(Format(Hex(Ec.FiscalStatus), "0000"))
    If Trim(m) <> "" Then
        If Mid(m, 1, 2) = "00" Then
            E_F.Funcionamiento = "La Impresora Esta En Modo Bloqueado"
            Estado_Fiscal = False
            End If
            If Mid(m, 1, 2) = "01" Then
            E_F.Funcionamiento = "La Impresora Esta En Modo Fiscal"
            Estado_Fiscal = False
            End If
            If Mid(m, 1, 2) = "10" Then
            E_F.Funcionamiento = "La Impresora Esta En Modo Entrenamiento"
            'Estado_Fiscal = True
            End If
            If Mid(m, 1, 2) = "11" Then
            E_F.Funcionamiento = "La Impresora Esta En Modo Fiscal"
            'Estado_Fiscal = True
            End If
            
            If Mid(m, 4, 1) = "0" Then
            E_F.Modo = "Modo Tecnico Inactivo"
            'Estado_Fiscal = True
            End If
            If Mid(m, 4, 1) = "1" Then
            E_F.Modo = "Modo Tecnico Activo"
            Estado_Fiscal = False
            End If
            
            If Mid(m, 5, 2) = "00" Then
            E_F.Memoria = "Memoria Fiscal en Perfecto Estado"
            'Estado_Fiscal = True
            ElseIf Mid(m, 5, 2) = "01" Then
            E_F.Memoria = "Memoria Fiscal Cerca de su Llenado"
            'Estado_Fiscal = True
            ElseIf Mid(m, 5, 2) = "10" Then
            E_F.Memoria = "Memoria Fiscal LLENA, Debe llamar al Servicio Tecnico"
            Estado_Fiscal = False
            ElseIf Mid(m, 5, 2) = "11" Then
            E_F.Memoria = "Memoria Fiscal Con Desperfectos"
            Estado_Fiscal = False
            End If
            
            If Mid(m, 9, 1) = "0" Then
            E_F.Periodo_ventas = "Periodo de Ventas Cerrado"
            'Estado_Fiscal = True
            ElseIf Mid(m, 9, 1) = "1" Then
            E_F.Periodo_ventas = "Periodo de Ventas Abierto"
            'Estado_Fiscal = True
            End If
        
    End If
End Function


Public Function HexToBin(hexvalue) As String
Dim i, s, ilen, Value, values
Set values = CreateObject("Scripting.Dictionary")
values.Add "0", "0000"
values.Add "1", "0001"
values.Add "2", "0010"
values.Add "3", "0011"
values.Add "4", "0100"
values.Add "5", "0101"
values.Add "6", "0110"
values.Add "7", "0111"
values.Add "8", "1000"
values.Add "9", "1001"
values.Add "A", "1010"
values.Add "B", "1011"
values.Add "C", "1100"
values.Add "D", "1101"
values.Add "E", "1110"
values.Add "F", "1111"
Value = Null
s = UCase(hexvalue)
Value = ""
ilen = Len(s)
For i = 1 To ilen
    Value = Value & values(Mid(s, i, 1))
Next
values.removeAll
Set values = Nothing
HexToBin = Value
End Function

Public Sub HideX(frm As Form)
    Dim hMenu As Long
    Dim menuItemCount As Long
    'Obtenemos el puntero del menu system del Formulario
    hMenu = GetSystemMenu(frm.hWnd, 0)
    If hMenu Then
        'Obtenemos el N�mero de Items del menuObtain the number of items in the menu
         menuItemCount = GetMenuItemCount(hMenu)
        'Remove the system menu Close menu item.
        'The menu item is 0-based, so the last
        'item on the menu is menuItemCount - 1
         Call RemoveMenu(hMenu, menuItemCount - 1, MF_REMOVE Or MF_BYPOSITION)
        'Remove the system menu separator line
         Call RemoveMenu(hMenu, menuItemCount - 2, MF_REMOVE Or MF_BYPOSITION)
        'Force a redraw of the menu. This
        'refreshes the titlebar, dimming the X
         Call DrawMenuBar(frm.hWnd)
    End If '
End Sub

Public Sub CierreCajero()


    '*** CIERRE DE CAJERO ( Cierre de Turno ) ***
    EpsonFPHostControl1.AddDataField Chr$(&H8) & Chr$(&H2)
    '*** Se imprime Informe ***
    EpsonFPHostControl1.AddDataField Chr$(&H0) & Chr$(&H0)
    
    EpsonFPHostControl1.SendCommand
    While EpsonFPHostControl1.State = EFP_S_Busy
        DoEvents
    Wend
'    TxtFiscalStatus.Text = Format(Hex(EpsonFPHostControl1.FiscalStatus), "0000")
'    TxtPrinterStatus.Text = Format(Hex(EpsonFPHostControl1.PrinterStatus), "0000")
 '   TxtReturnCode.Text = EpsonFPHostControl1.ReturnCode
    'Texto_Return_Code


End Sub
