VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "gif89.dll"
Begin VB.Form VenNCBeta 
   Caption         =   "Nota Credito V.Beta"
   ClientHeight    =   10500
   ClientLeft      =   2250
   ClientTop       =   345
   ClientWidth     =   18150
   LinkTopic       =   "Form1"
   ScaleHeight     =   10500
   ScaleWidth      =   18150
   Begin VB.Frame Frame4 
      Caption         =   "Datos de pago de documento"
      Height          =   1770
      Left            =   9090
      TabIndex        =   88
      Top             =   60
      Width           =   4020
      Begin VB.ListBox List1 
         Height          =   840
         Left            =   405
         TabIndex        =   91
         Top             =   780
         Width           =   3315
      End
      Begin VB.TextBox TxtFPago 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         Height          =   360
         Left            =   1695
         TabIndex        =   89
         ToolTipText     =   "Forma de pago"
         Top             =   255
         Width           =   2085
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   300
         Index           =   4
         Left            =   210
         OleObjectBlob   =   "VenNCBeta.frx":0000
         TabIndex        =   90
         Top             =   315
         Width           =   1500
      End
   End
   Begin VB.Frame FrmLoad 
      Height          =   1785
      Left            =   7710
      TabIndex        =   81
      Top             =   4905
      Visible         =   0   'False
      Width           =   2985
      Begin GIF89LibCtl.Gif89a Gif89a1 
         Height          =   1410
         Left            =   210
         OleObjectBlob   =   "VenNCBeta.frx":0070
         TabIndex        =   82
         Top             =   285
         Width           =   2535
      End
   End
   Begin VB.Frame FraProductos 
      Caption         =   "Detalle Nota de Credito"
      Enabled         =   0   'False
      Height          =   3195
      Left            =   345
      TabIndex        =   67
      Top             =   5835
      Width           =   9780
      Begin VB.CommandButton CmdEliminaMarcados 
         Caption         =   "x"
         Height          =   225
         Left            =   435
         TabIndex        =   87
         ToolTipText     =   "Elimina los marcados"
         Top             =   1155
         Visible         =   0   'False
         Width           =   225
      End
      Begin VB.TextBox TxtCodigo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   390
         Locked          =   -1  'True
         TabIndex        =   73
         Top             =   660
         Width           =   1335
      End
      Begin VB.CommandButton CmdAceptaLinea 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         Caption         =   "Ok"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8640
         TabIndex        =   72
         Top             =   645
         Width           =   495
      End
      Begin VB.TextBox TxtTotalLinea 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   7320
         Locked          =   -1  'True
         TabIndex        =   71
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   660
         Width           =   1290
      End
      Begin VB.TextBox TxtPrecio 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6315
         TabIndex        =   70
         Text            =   "0"
         Top             =   660
         Width           =   975
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5505
         TabIndex        =   69
         Text            =   "1"
         Top             =   660
         Width           =   795
      End
      Begin VB.TextBox TxtDescripcion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1725
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   68
         TabStop         =   0   'False
         Top             =   660
         Width           =   3765
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   8
         Left            =   390
         OleObjectBlob   =   "VenNCBeta.frx":00F6
         TabIndex        =   74
         Top             =   420
         Width           =   660
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   9
         Left            =   2805
         OleObjectBlob   =   "VenNCBeta.frx":0158
         TabIndex        =   75
         Top             =   435
         Width           =   1530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   3
         Left            =   4935
         OleObjectBlob   =   "VenNCBeta.frx":01C4
         TabIndex        =   76
         Top             =   435
         Width           =   1395
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   10
         Left            =   6420
         OleObjectBlob   =   "VenNCBeta.frx":022A
         TabIndex        =   77
         Top             =   420
         Width           =   795
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   12
         Left            =   7980
         OleObjectBlob   =   "VenNCBeta.frx":028C
         TabIndex        =   78
         Top             =   420
         Width           =   600
      End
      Begin MSComctlLib.ListView LvNotaCredito 
         Height          =   2010
         Left            =   390
         TabIndex        =   84
         Top             =   1125
         Width           =   8940
         _ExtentX        =   15769
         _ExtentY        =   3545
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483646
         BackColor       =   -2147483643
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripci�n"
            Object.Width           =   6526
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Precio"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "Stock"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Inventario"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Precio Costo"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "TOTAL NOTA DE CREDITO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3390
      Index           =   2
      Left            =   10440
      TabIndex        =   53
      Top             =   5670
      Width           =   7500
      Begin VB.TextBox txtTtotalImpuestos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   5490
         Locked          =   -1  'True
         TabIndex        =   62
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   2295
         Width           =   1200
      End
      Begin VB.TextBox Text4 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   345
         Left            =   5220
         TabIndex        =   61
         Tag             =   "N"
         Text            =   "0"
         Top             =   975
         Width           =   1155
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Ok"
         Height          =   330
         Left            =   6390
         TabIndex        =   60
         Top             =   975
         Width           =   345
      End
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "VenNCBeta.frx":02EC
         Left            =   2070
         List            =   "VenNCBeta.frx":02EE
         Style           =   2  'Dropdown List
         TabIndex        =   59
         Top             =   975
         Width           =   3165
      End
      Begin VB.TextBox TxtTNeto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   5490
         TabIndex        =   58
         Text            =   "0"
         Top             =   360
         Width           =   1200
      End
      Begin VB.TextBox TxtTiva 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   5490
         TabIndex        =   57
         Text            =   "0"
         Top             =   660
         Width           =   1200
      End
      Begin VB.Frame Frame3 
         Height          =   555
         Index           =   3
         Left            =   2790
         TabIndex        =   54
         Top             =   2595
         Width           =   4035
         Begin VB.TextBox txtTtotal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H0000FF00&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1425
            TabIndex        =   55
            Text            =   "0"
            Top             =   165
            Width           =   2475
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   300
            Index           =   1
            Left            =   150
            OleObjectBlob   =   "VenNCBeta.frx":02F0
            TabIndex        =   56
            Top             =   210
            Width           =   1005
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Index           =   2
         Left            =   3330
         OleObjectBlob   =   "VenNCBeta.frx":0350
         TabIndex        =   63
         Top             =   2325
         Width           =   1830
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   1
         Left            =   4350
         OleObjectBlob   =   "VenNCBeta.frx":03CC
         TabIndex        =   64
         Top             =   660
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   2
         Left            =   4335
         OleObjectBlob   =   "VenNCBeta.frx":042E
         TabIndex        =   65
         Top             =   285
         Width           =   915
      End
      Begin MSComctlLib.ListView LvTimpuestos 
         Height          =   945
         Left            =   2085
         TabIndex        =   66
         Top             =   1335
         Width           =   4650
         _ExtentX        =   8202
         _ExtentY        =   1667
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T5000"
            Text            =   "Impuesto"
            Object.Width           =   5186
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "FACTOR"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "costo/credito"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.TextBox TxtComentario 
      Height          =   390
      Left            =   2070
      TabIndex        =   42
      Top             =   9225
      Width           =   4740
   End
   Begin VB.CommandButton CmdAceptar 
      Caption         =   "&Generar Nota de Credito"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   705
      Left            =   285
      TabIndex        =   40
      Top             =   9720
      Width           =   9735
   End
   Begin VB.Frame Frame3 
      Caption         =   "Total del documento original"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2925
      Index           =   1
      Left            =   10440
      TabIndex        =   39
      Top             =   135
      Width           =   7500
      Begin VB.TextBox TxtFechaDocumento 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         Height          =   360
         Left            =   405
         TabIndex        =   86
         Text            =   "(auto)"
         ToolTipText     =   "Nro NC"
         Top             =   2415
         Width           =   1845
      End
      Begin VB.Frame Frame3 
         Height          =   555
         Index           =   0
         Left            =   2745
         TabIndex        =   47
         Top             =   2250
         Width           =   4035
         Begin VB.TextBox txtTotal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1425
            TabIndex        =   48
            Text            =   "0"
            Top             =   165
            Width           =   2475
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   300
            Index           =   0
            Left            =   150
            OleObjectBlob   =   "VenNCBeta.frx":048C
            TabIndex        =   49
            Top             =   210
            Width           =   1005
         End
      End
      Begin VB.TextBox TxtIva 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   5490
         TabIndex        =   46
         Text            =   "0"
         Top             =   660
         Width           =   1200
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   5490
         TabIndex        =   45
         Text            =   "0"
         Top             =   360
         Width           =   1200
      End
      Begin VB.TextBox TxtTotalImpuestos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   5535
         Locked          =   -1  'True
         TabIndex        =   44
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1950
         Width           =   1200
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   240
         Index           =   1
         Left            =   3375
         OleObjectBlob   =   "VenNCBeta.frx":04EC
         TabIndex        =   43
         Top             =   1980
         Width           =   1830
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   4
         Left            =   4350
         OleObjectBlob   =   "VenNCBeta.frx":0568
         TabIndex        =   50
         Top             =   660
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   5
         Left            =   4335
         OleObjectBlob   =   "VenNCBeta.frx":05CA
         TabIndex        =   51
         Top             =   285
         Width           =   915
      End
      Begin MSComctlLib.ListView LvImpuestos 
         Height          =   945
         Left            =   2085
         TabIndex        =   52
         Top             =   990
         Width           =   4650
         _ExtentX        =   8202
         _ExtentY        =   1667
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Object.Tag             =   "T5000"
            Text            =   "Impuesto"
            Object.Width           =   5997
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Key             =   "valor"
            Object.Tag             =   "N100"
            Text            =   "Valor"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "FACTOR"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Tag             =   "T1000"
            Text            =   "costo/credito"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   300
         Index           =   0
         Left            =   540
         OleObjectBlob   =   "VenNCBeta.frx":0628
         TabIndex        =   85
         Top             =   2175
         Width           =   1590
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Cliente"
      Height          =   2400
      Left            =   10455
      TabIndex        =   19
      Top             =   3120
      Width           =   7515
      Begin VB.TextBox TxtCupoUtilizado 
         Height          =   285
         Left            =   6750
         TabIndex        =   29
         Text            =   "Text1"
         Top             =   2370
         Visible         =   0   'False
         Width           =   2010
      End
      Begin VB.TextBox TxtMontoCredito 
         Height          =   255
         Left            =   6735
         TabIndex        =   28
         Text            =   "Text1"
         Top             =   2040
         Visible         =   0   'False
         Width           =   1995
      End
      Begin VB.TextBox TxtEmail 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   4020
         TabIndex        =   27
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1860
         Width           =   2370
      End
      Begin VB.TextBox txtFono 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1260
         TabIndex        =   26
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1860
         Width           =   2175
      End
      Begin VB.TextBox txtComuna 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1275
         TabIndex        =   25
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1545
         Width           =   2160
      End
      Begin VB.TextBox TxtDireccion 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1275
         TabIndex        =   24
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1200
         Width           =   5670
      End
      Begin VB.TextBox TxtGiro 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1275
         TabIndex        =   23
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   870
         Width           =   5715
      End
      Begin VB.TextBox TxtRut 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1275
         TabIndex        =   22
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   210
         Width           =   1395
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "F1 - Buscar"
         Height          =   255
         Left            =   2700
         TabIndex        =   21
         Top             =   225
         Width           =   1395
      End
      Begin VB.TextBox TxtRazonSocial 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1275
         TabIndex        =   20
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   540
         Width           =   5730
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCupoCredito 
         Height          =   270
         Left            =   4605
         OleObjectBlob   =   "VenNCBeta.frx":069C
         TabIndex        =   30
         Top             =   1545
         Width           =   1020
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   0
         Left            =   720
         OleObjectBlob   =   "VenNCBeta.frx":06FC
         TabIndex        =   31
         Top             =   240
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Index           =   2
         Left            =   555
         OleObjectBlob   =   "VenNCBeta.frx":0766
         TabIndex        =   32
         Top             =   585
         Width           =   660
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   150
         OleObjectBlob   =   "VenNCBeta.frx":07D0
         TabIndex        =   33
         Top             =   1215
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   0
         Left            =   360
         OleObjectBlob   =   "VenNCBeta.frx":0840
         TabIndex        =   34
         Top             =   1560
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   795
         OleObjectBlob   =   "VenNCBeta.frx":08AA
         TabIndex        =   35
         Top             =   900
         Width           =   420
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   1
         Left            =   330
         OleObjectBlob   =   "VenNCBeta.frx":0910
         TabIndex        =   36
         Top             =   1890
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   2
         Left            =   3765
         OleObjectBlob   =   "VenNCBeta.frx":0976
         TabIndex        =   37
         Top             =   2130
         Width           =   480
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   3
         Left            =   3525
         OleObjectBlob   =   "VenNCBeta.frx":09E0
         TabIndex        =   38
         Top             =   1545
         Width           =   1035
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   270
      OleObjectBlob   =   "VenNCBeta.frx":0A58
      Top             =   7650
   End
   Begin VB.Timer Timer1 
      Interval        =   200
      Left            =   120
      Top             =   6495
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "Retornar"
      Height          =   450
      Left            =   16605
      TabIndex        =   18
      Top             =   9675
      Width           =   1290
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tipo de NC / Tipo Egreso"
      Height          =   990
      Left            =   330
      TabIndex        =   15
      Top             =   4740
      Width           =   9915
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   195
         Index           =   0
         Left            =   285
         OleObjectBlob   =   "VenNCBeta.frx":0C8C
         TabIndex        =   79
         Top             =   330
         Width           =   2790
      End
      Begin VB.ComboBox CboCajaAbono 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "VenNCBeta.frx":0D14
         Left            =   5715
         List            =   "VenNCBeta.frx":0D27
         Style           =   2  'Dropdown List
         TabIndex        =   17
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   525
         Width           =   4080
      End
      Begin VB.ComboBox CboTipoNC 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "VenNCBeta.frx":0D99
         Left            =   270
         List            =   "VenNCBeta.frx":0D9B
         Style           =   2  'Dropdown List
         TabIndex        =   16
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   540
         Width           =   4170
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   195
         Index           =   1
         Left            =   5715
         OleObjectBlob   =   "VenNCBeta.frx":0D9D
         TabIndex        =   80
         Top             =   315
         Width           =   2790
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Documento Actual"
      Height          =   1215
      Left            =   360
      TabIndex        =   6
      Top             =   135
      Width           =   9900
      Begin VB.ComboBox CboNC 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "VenNCBeta.frx":0E2B
         Left            =   525
         List            =   "VenNCBeta.frx":0E2D
         Style           =   2  'Dropdown List
         TabIndex        =   11
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   525
         Width           =   4425
      End
      Begin VB.TextBox TxtDocActual 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         Height          =   360
         Left            =   4980
         TabIndex        =   10
         Text            =   "(auto)"
         ToolTipText     =   "Nro NC"
         Top             =   525
         Width           =   1845
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   270
         Index           =   0
         Left            =   510
         OleObjectBlob   =   "VenNCBeta.frx":0E2F
         TabIndex        =   7
         Top             =   330
         Width           =   1230
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   300
         Index           =   2
         Left            =   5025
         OleObjectBlob   =   "VenNCBeta.frx":0EA9
         TabIndex        =   8
         Top             =   225
         Width           =   1560
      End
      Begin MSComCtl2.DTPicker DtFecha 
         Height          =   345
         Left            =   6855
         TabIndex        =   9
         Top             =   525
         Width           =   1425
         _ExtentX        =   2514
         _ExtentY        =   609
         _Version        =   393216
         Format          =   87425025
         CurrentDate     =   42269
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   300
         Index           =   3
         Left            =   7020
         OleObjectBlob   =   "VenNCBeta.frx":0F24
         TabIndex        =   12
         Top             =   270
         Width           =   1140
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkFoliosDisponibles 
         Height          =   300
         Left            =   5040
         OleObjectBlob   =   "VenNCBeta.frx":0F96
         TabIndex        =   13
         Top             =   885
         Width           =   3420
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Documento Referencia"
      Height          =   3345
      Left            =   360
      TabIndex        =   0
      Top             =   1365
      Width           =   9885
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   225
         Index           =   0
         Left            =   210
         OleObjectBlob   =   "VenNCBeta.frx":0FF8
         TabIndex        =   14
         Top             =   915
         Width           =   1815
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         Height          =   315
         Left            =   6060
         TabIndex        =   3
         Top             =   480
         Width           =   3555
      End
      Begin VB.TextBox txtNroDocumentoBuscar 
         BackColor       =   &H8000000E&
         Height          =   315
         Left            =   4350
         TabIndex        =   2
         Top             =   495
         Width           =   1650
      End
      Begin VB.ComboBox CboDocVenta 
         Height          =   315
         ItemData        =   "VenNCBeta.frx":108A
         Left            =   165
         List            =   "VenNCBeta.frx":108C
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   495
         Width           =   4170
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Index           =   9
         Left            =   180
         OleObjectBlob   =   "VenNCBeta.frx":108E
         TabIndex        =   4
         Top             =   300
         Width           =   1980
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Index           =   10
         Left            =   4365
         OleObjectBlob   =   "VenNCBeta.frx":10FE
         TabIndex        =   5
         Top             =   300
         Width           =   1020
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   2010
         Left            =   210
         TabIndex        =   83
         Top             =   1155
         Width           =   9435
         _ExtentX        =   16642
         _ExtentY        =   3545
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483646
         BackColor       =   -2147483643
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripci�n"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   2443
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Precio"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "Stock"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Inventario"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Precio Costo"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SknComentario 
      Height          =   255
      Left            =   345
      OleObjectBlob   =   "VenNCBeta.frx":1162
      TabIndex        =   41
      Top             =   9285
      Width           =   1605
   End
End
Attribute VB_Name = "VenNCBeta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub CboTipoNC_Click()
    If CboTipoNC.ListIndex = -1 Then Exit Sub
    
    If CboTipoNC.Text = "SELECCIONE..." Then Exit Sub
    
    Sql = "SELECT tnc_id, tnc_modifica_inventario modifica, tnc_anula_documento anula " & _
            "FROM cont_ntc_tipos " & _
            "WHERE tnc_id=" & CboTipoNC.ItemData(CboTipoNC.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!anula = "SI" Or RsTmp!tnc_id = 2 Then
            If RsTmp!tnc_id = 2 Then
                Me.CmdEliminaMarcados.Visible = True
                LvNotaCredito.ColumnHeaders(1).Width = 400
                LvNotaCredito.CheckBoxes = True
            
                
                
            Else
                LvNotaCredito.CheckBoxes = False
                Me.CmdEliminaMarcados.Visible = False
            End If
        
        
            TxtTNeto = TxtNeto
            TxtTiva = TxtIva
            txtTtotalImpuestos = TxtTotalImpuestos
            txtTtotal = txtTotal
            Me.LvTimpuestos.ListItems.Clear
            Me.LvNotaCredito.ListItems.Clear
            If LvImpuestos.ListItems.Count > 0 Then
                For i = 1 To LvImpuestos.ListItems.Count
                    LvTimpuestos.ListItems.Add LvImpuestos.ListItems(i).Index
                    LvTimpuestos.ListItems(i).SubItems(1) = LvImpuestos.ListItems(i).SubItems(1)
                    LvTimpuestos.ListItems(i).SubItems(2) = LvImpuestos.ListItems(i).SubItems(2)
                Next
            End If
        
            For i = 1 To LvDetalle.ListItems.Count
                LvNotaCredito.ListItems.Add LvDetalle.ListItems(i).Index
                LvNotaCredito.ListItems(i).SubItems(1) = LvDetalle.ListItems(i).SubItems(1)
                LvNotaCredito.ListItems(i).SubItems(2) = LvDetalle.ListItems(i).SubItems(2)
                LvNotaCredito.ListItems(i).SubItems(3) = LvDetalle.ListItems(i).SubItems(3)
                LvNotaCredito.ListItems(i).SubItems(4) = LvDetalle.ListItems(i).SubItems(4)
                LvNotaCredito.ListItems(i).SubItems(5) = LvDetalle.ListItems(i).SubItems(5)
            
        
            Next
            
            If RsTmp!tnc_id = 2 Then
                FraProductos.Enabled = True
                
            Else
                FraProductos.Enabled = False
            End If
            
        End If
    End If
End Sub

Private Sub SumaParcial()
    txtTtotal = 0
    For i = 1 To LvNotaCredito.ListItems.Count
        txtTtotal = CDbl(txtTtotal) + CDbl(LvNotaCredito.ListItems(i).SubItems(5))
    Next
    txtTtotal = NumFormat(txtTtotal)
    TxtTNeto = NumFormat(CDbl(txtTtotal) / Val("1." & DG_IVA))
    TxtTiva = NumFormat(CDbl(txtTtotal) - CDbl(TxtTNeto))
End Sub


Private Sub CmdAceptaLinea_Click()
    If Len(TxtCodigo) = 0 Then
        MsgBox "No hay producto seleccionado...", vbInformation
        LvNotaCredito.SetFocus
        Exit Sub
    End If
    
    If Val(TxtCantidad) = 0 Then
        MsgBox "Debe ingresar cantidad...", vbInformation
        TxtCantidad.SetFocus
        Exit Sub
    End If
    If Val(TxtPrecio) = 0 Then
        MsgBox "Debe ingresar precio...", vbInformation
        TxtPrecio.SetFocus
        Exit Sub
    End If
    
    LvNotaCredito.ListItems.Add , , ""
    LvNotaCredito.ListItems(LvNotaCredito.ListItems.Count).SubItems(1) = TxtCodigo
    LvNotaCredito.ListItems(LvNotaCredito.ListItems.Count).SubItems(2) = TxtDescripcion
    LvNotaCredito.ListItems(LvNotaCredito.ListItems.Count).SubItems(3) = TxtCantidad
    LvNotaCredito.ListItems(LvNotaCredito.ListItems.Count).SubItems(4) = NumFormat(TxtPrecio)
    LvNotaCredito.ListItems(LvNotaCredito.ListItems.Count).SubItems(5) = NumFormat(TxtTotalLinea)
    SumaParcial
        
End Sub

Private Sub CmdAceptar_Click()
    Dim Sp_Folio As String
    If Val(txtNroDocumentoBuscar) = 0 Then
        MsgBox "Debe ingresar documento a refernciar...", vbInformation
        txtNroDocumentoBuscar.SetFocus
        Exit Sub
    End If
    
    If CboTipoNC.ListIndex = -1 Then
        MsgBox "Seleccion que tipo de NC emitir�...", vbInformation
        CboTipoNC.SetFocus
        Exit Sub
    End If
    
    If Len(TxtRazonSocial) = 0 Then
        MsgBox "Debe ingresar cliente para emitir NC...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    If Val(txtTtotal) = 0 Then
        MsgBox "No puede emitir NC sin valor...", vbInformation
        CboTipoNC.SetFocus
        Exit Sub
    End If
    
    If LvNotaCredito.ListItems.Count = 0 Then
        MsgBox "La NC debe contener detalle...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    
    'Obtener folio disponible
     Sql = "call sp_sel_DteDisponible('" & SP_Rut_Activo & "',61);"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        TxtDocActual = RsTmp!dte_folio
        TxtDocActual.Tag = RsTmp!dte_id
    Else
        TxtDocActual = 0
        MsgBox "No cuenta con folios disponibles para continuar..."
        Exit Sub
    End If
    
    'Si llegamos hasta aqui, guardaremos el registro.
    
    
    
End Sub

Private Sub cmdBuscar_Click()
    Dim Lp_Id_Venta As Long
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione Documeneto de Venta...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    If Val(txtNroDocumentoBuscar) = 0 Then
        MsgBox "Ingrese Nro Valido...", vbInformation
        txtNroDocumentoBuscar.SetFocus
        txtNroDocumentoBuscar = 0
        Exit Sub
    End If
    LimpiaDatos
    FrmLoad.Visible = True
    
    DoEvents
    'Buscar Totales e Impuestos del documento
    Sql = "SELECT neto,iva,bruto,rut_cliente,id,fecha, " & _
            "(SELECT doc_bruto_neto_en_venta " & _
                    "FROM sis_documentos d WHERE d.doc_id=v.doc_id ) bruto_neto " & _
            "FROM ven_doc_venta v " & _
            "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & Me.txtNroDocumentoBuscar & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount = 0 Then
        FrmLoad.Visible = False
        MsgBox "Documento No encontrado...", vbInformation
        txtNroDocumentoBuscar.SetFocus
        Exit Sub
    End If
    
    Lp_Id_Venta = RsTmp!ID
    'Ahota buscar la(s) formas de pago
    Sql = "SELECT t.mpa_id,m.mpa_nombre " & _
                "FROM abo_tipos_de_pagos t " & _
                    "JOIN par_medios_de_pago m ON t.mpa_id=m.mpa_id " & _
                "JOIN cta_abono_documentos d ON  t.abo_id=d.abo_id " & _
                "WHERE d.id =" & Lp_Id_Venta
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        TxtFPago = RsTmp2!mpa_nombre
        TxtFPago.Tag = RsTmp2!mpa_id
        Me.CboCajaAbono.Clear
        CboCajaAbono.AddItem "SELECCIONAR..."
        
        If LG_id_Caja > 0 Then
                CboCajaAbono.AddItem "DEVOLUCION EN EFECTIVO"
                CboCajaAbono.ItemData(CboCajaAbono.ListCount - 1) = 1
        End If
            
        CboCajaAbono.AddItem "ABONO CTA. CTE."
        CboCajaAbono.ItemData(CboCajaAbono.ListCount - 1) = 2
    
        Select Case Val(TxtFPago.Tag)
            Case 2
                CboCajaAbono.AddItem "DEVOLUCION DE CHEQUE"
                CboCajaAbono.ItemData(CboCajaAbono.ListCount - 1) = 3
            Case 9
                CboCajaAbono.AddItem "ANULA TRANSACCION T.CREDITO"
                CboCajaAbono.ItemData(CboCajaAbono.ListCount - 1) = 4
        End Select
    End If
    
    TxtFechaDocumento = RsTmp!Fecha
    
    TxtNeto = NumFormat(RsTmp!Neto)
    TxtIva = NumFormat(RsTmp!Iva)
    txtTotal = NumFormat(RsTmp!bruto)
    txtTotal.Tag = NumFormat(RsTmp!bruto_neto)
    TxtRut = "" & RsTmp!rut_cliente
    
    
    
    
    
    
    Sql = "SELECT v.imp_id,imp_nombre,coi_valor " & _
            "FROM ven_impuestos v " & _
            "JOIN par_impuestos p ON v.imp_id=p.imp_id " & _
            "WHERE id=" & RsTmp!ID
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        LLenar_Grilla RsTmp, Me, Me.LvImpuestos, False, True, True, False
        TxtTotalImpuestos = NumFormat(TotalizaColumna(LvImpuestos, "valor"))
    End If
    
    
    'Detalle del documento
   ' Sql = "call sp_sel_VenDetalleDocVenta( " & CboDocVenta.ItemData(CboDocVenta.ListIndex) & ", " & Me.txtNroDocumentoBuscar & ", '" & SP_Rut_Activo & "');"
   
   
    'Buscar desuento
    Sql = "SELECT (nve_valor_descuento+nve_valor_descuento_adicional-nve_valor_recargo) / " & _
            "(nve_total +  (nve_valor_descuento+nve_valor_descuento_adicional-nve_valor_recargo)) " & _
            "*100 dscto " & _
    "From ven_nota_venta " & _
        "Where ID =" & Lp_Id_Venta
   Consulta RsTmp, Sql
   If RsTmp.RecordCount > 0 Then
    
   End If
   
    If txtTotal.Tag = "BRUTO" Then
        
        Sql = "SELECT v.id, d.codigo,descripcion,nvd_cantidad,ROUND(nvd_precio_unitario - (nvd_precio_unitario* " & CxP(RsTmp!dscto) & "/100)) unitario, " & _
                "ROUND(nvd_precio_total - (nvd_precio_total*  " & CxP(RsTmp!dscto) & "  /100)) total,'' x,pro_inventariable,nvd_precio_costo " & _
                    "FROM ven_nota_venta_detalle d " & _
                    "JOIN ven_nota_venta v ON d.nve_id=v.nve_id " & _
                    "JOIN maestro_productos p ON d.codigo=p.codigo " & _
                    "Where v.id =" & Lp_Id_Venta
        
    
    '    Sql = "SELECT d.id,d.codigo,d.descripcion,unidades,ved_precio_venta_bruto/unidades, ved_precio_venta_bruto,'' x," & _
                "pro_inventariable,precio_costo " & _
                "FROM ven_detalle d " & _
                "JOIN maestro_productos p ON d.codigo=p.codigo " & _
                "WHERE no_documento=" & txtNroDocumentoBuscar & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND d.rut_emp='" & SP_Rut_Activo & "' AND p.rut_emp='" & SP_Rut_Activo & "'"
        
                
                
    
    Else
    Sql = "SELECT d.id,d.codigo,d.descripcion,unidades,ved_precio_venta_neto/unidades, ved_precio_venta_neto,'' x," & _
                "pro_inventariable,precio_costo " & _
                "FROM ven_detalle d " & _
                "JOIN maestro_productos p ON d.codigo=p.codigo " & _
                "WHERE no_documento=" & txtNroDocumentoBuscar & " AND doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND d.rut_emp='" & SP_Rut_Activo & "' AND p.rut_emp='" & SP_Rut_Activo & "'"
    
    End If
   
   
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    
    
    TxtRut_Validate True
    
    FrmLoad.Visible = False
End Sub

Private Sub CmdEliminaMarcados_Click()
    For i = Me.LvNotaCredito.ListItems.Count To 1 Step -1
        If Me.LvNotaCredito.ListItems(i).Checked Then
            LvNotaCredito.ListItems.Remove LvNotaCredito.ListItems(i).Index
        End If
    Next
    SumaParcial
End Sub

Private Sub CmdSalir_Click()
    End
End Sub

Private Sub LimpiaDatos()
    TxtNeto = 0
    TxtIva = 0
    txtTotal = 0
    TxtTotalImpuestos = 0
    LvImpuestos.ListItems.Clear
    TxtRut = ""
    TxtRazonSocial = ""
    TxtGiro = ""
    TxtDireccion = ""
    txtFono = ""
    txtComuna = ""
    TxtCiudad = ""
    TxtEmail = 0
    TxtTNeto = 0
    TxtTiva = 0
    txtTtotal = 0
    txtTtotalImpuestos = 0
    LvTimpuestos.ListItems.Clear
    LvDetalle.ListItems.Clear
    LvNotaCredito.ListItems.Clear
    CboTipoNC.ListIndex = CboTipoNC.ListCount - 1
    


End Sub


Private Sub Form_Load()
    
    
    Me.Visible = True
    Skin2 Me, 4, 4
   ' Centrar Me
   
  CargaDatos
End Sub

Private Sub LvNotaCredito_DblClick()
    If LvNotaCredito.SelectedItem Is Nothing Then Exit Sub
    
    TxtCodigo = LvNotaCredito.SelectedItem.SubItems(1)
    TxtDescripcion = LvNotaCredito.SelectedItem.SubItems(2)
    TxtCantidad = LvNotaCredito.SelectedItem.SubItems(3)
    TxtPrecio = CDbl(LvNotaCredito.SelectedItem.SubItems(4))
    TxtTotalLinea = LvNotaCredito.SelectedItem.SubItems(5)
    
    LvNotaCredito.ListItems.Remove LvNotaCredito.SelectedItem.Index
    
    SumaParcial
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboNC.SetFocus
    Timer1.Enabled = False
    
    
End Sub
Private Sub CargaDatos()
        'Timer1.Enabled = False
        FrmLoad.Visible = True
        DoEvents
        
        DtFecha = Date
        LLenarCombo CboNC, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='SI' AND doc_dte='SI'", "doc_orden"
        CboNC.ListIndex = CboNC.ListCount - 1
        SkFoliosDisponibles = ""

        Sql = "SELECT count(dte_id) dte " & _
                "FROM dte_folios " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & 61 & " AND dte_disponible='SI' " & _
                "GROUP BY doc_id"
        Consulta RsTmp3, Sql
        If RsTmp3.RecordCount > 0 Then
            SkFoliosDisponibles = "Folios disponibles:" & RsTmp3!Dte
        End If
        
        LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_contable = 'SI' AND doc_documento='VENTA' AND doc_nota_de_credito='NO' AND doc_activo='SI'", "doc_orden"
        CboDocVenta.ListIndex = CboDocVenta.ListIndex = 0
        
        LLenarCombo CboTipoNC, "tnc_nombre", "tnc_id", "cont_ntc_tipos", "tnc_activo='SI'"
        CboTipoNC.AddItem "SELECCIONE..."
        CboTipoNC.ListIndex = CboTipoNC.ListCount - 1
        FrmLoad.Visible = False
End Sub

Private Sub TxtCantidad_Change()
    TxtTotalLinea = Val(TxtCantidad) * Val(TxtPrecio)
End Sub

Private Sub TxtCantidad_GotFocus()
    En_Foco TxtCantidad
    
End Sub

Private Sub TxtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtCantidad_Validate(Cancel As Boolean)
    If Val(TxtCantidad) = 0 Then TxtCantidad = 0
End Sub

Private Sub txtNroDocumentoBuscar_GotFocus()
    En_Foco txtNroDocumentoBuscar
End Sub

Private Sub txtNroDocumentoBuscar_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
End Sub

Private Sub TxtPrecio_GotFocus()
    En_Foco TxtPrecio
End Sub

Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPrecio_Validate(Cancel As Boolean)
    If Val(TxtPrecio) = 0 Then TxtPrecio = 0
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
    
    If CboNC.ListIndex = -1 Then
        MsgBox "Selecione Nota de Credito...", vbOKCancel
        CboNC.SetFocus
        Exit Sub
    End If
    
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
   
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
                Sql = ""
                Sql = "call sp_sel_MaestroClteAsoc('" & SP_Rut_Activo & "', '" & Me.TxtRut.Text & "');"
                
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtMontoCredito = !cli_monto_credito
         
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
            End With
        End If
    End If
End Sub
