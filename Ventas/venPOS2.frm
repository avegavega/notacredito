VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form venPOS 
   Caption         =   "POS ALVAMAR"
   ClientHeight    =   9495
   ClientLeft      =   1740
   ClientTop       =   1350
   ClientWidth     =   17910
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9495
   ScaleWidth      =   17910
   Begin VB.Frame Frame7 
      Caption         =   "Cliente"
      Height          =   2985
      Left            =   45
      TabIndex        =   7
      Top             =   105
      Width           =   7425
      Begin VB.ComboBox CboSucursal 
         Height          =   315
         Left            =   1545
         Style           =   2  'Dropdown List
         TabIndex        =   93
         Top             =   1440
         Width           =   5760
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkCupoCredito 
         Height          =   270
         Left            =   4890
         OleObjectBlob   =   "venPOS2.frx":0000
         TabIndex        =   70
         Top             =   1800
         Width           =   1020
      End
      Begin VB.TextBox TxtCupoUtilizado 
         Height          =   285
         Left            =   6405
         TabIndex        =   68
         Text            =   "Text1"
         Top             =   2370
         Visible         =   0   'False
         Width           =   2010
      End
      Begin VB.TextBox TxtMontoCredito 
         Height          =   255
         Left            =   6420
         TabIndex        =   67
         Text            =   "Text1"
         Top             =   2040
         Visible         =   0   'False
         Width           =   1995
      End
      Begin VB.TextBox TxtEmail 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   22
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   2445
         Width           =   3855
      End
      Begin VB.TextBox txtFono 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1545
         TabIndex        =   20
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   2115
         Width           =   2175
      End
      Begin VB.TextBox txtComuna 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   18
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1785
         Width           =   2160
      End
      Begin VB.TextBox TxtDireccion 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   7785
         TabIndex        =   17
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1425
         Width           =   5670
      End
      Begin VB.TextBox TxtGiro 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   16
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1125
         Width           =   5715
      End
      Begin VB.TextBox TxtRut 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   10
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   465
         Width           =   1395
      End
      Begin VB.CommandButton CmdBuscaCliente 
         Caption         =   "F1 - Buscar"
         Height          =   255
         Left            =   2985
         TabIndex        =   9
         Top             =   465
         Width           =   1395
      End
      Begin VB.TextBox TxtRazonSocial 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   8
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   795
         Width           =   5730
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   0
         Left            =   1005
         OleObjectBlob   =   "venPOS2.frx":0060
         TabIndex        =   11
         Top             =   495
         Width           =   495
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   210
         Index           =   2
         Left            =   840
         OleObjectBlob   =   "venPOS2.frx":00CA
         TabIndex        =   12
         Top             =   840
         Width           =   660
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Index           =   1
         Left            =   435
         OleObjectBlob   =   "venPOS2.frx":0134
         TabIndex        =   13
         Top             =   1470
         Width           =   1065
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   0
         Left            =   645
         OleObjectBlob   =   "venPOS2.frx":01A4
         TabIndex        =   14
         Top             =   1815
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
         Height          =   255
         Left            =   1080
         OleObjectBlob   =   "venPOS2.frx":020E
         TabIndex        =   15
         Top             =   1155
         Width           =   420
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   1
         Left            =   615
         OleObjectBlob   =   "venPOS2.frx":0274
         TabIndex        =   19
         Top             =   2145
         Width           =   855
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   2
         Left            =   195
         OleObjectBlob   =   "venPOS2.frx":02DA
         TabIndex        =   21
         Top             =   2445
         Width           =   1320
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
         Height          =   255
         Index           =   3
         Left            =   3810
         OleObjectBlob   =   "venPOS2.frx":035C
         TabIndex        =   69
         Top             =   1800
         Width           =   1035
      End
   End
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   17865
      Top             =   7080
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox TxtEmpMail 
      Height          =   405
      Left            =   18375
      TabIndex        =   85
      Text            =   "Text3"
      Top             =   8115
      Width           =   3300
   End
   Begin VB.TextBox TxtEmpFono 
      Height          =   285
      Left            =   18270
      TabIndex        =   84
      Text            =   "Text2"
      Top             =   7470
      Width           =   3255
   End
   Begin VB.TextBox TxtEmpDireccion 
      Height          =   315
      Left            =   18330
      TabIndex        =   83
      Text            =   "Text1"
      Top             =   6885
      Width           =   3510
   End
   Begin VB.TextBox TxtBarCode 
      Height          =   285
      Left            =   9210
      TabIndex        =   82
      Text            =   "Text1"
      Top             =   9825
      Width           =   1080
   End
   Begin VB.TextBox TxtUbicacion 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   6330
      TabIndex        =   72
      Top             =   10575
      Width           =   2805
   End
   Begin VB.TextBox TxtStock 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   6315
      TabIndex        =   71
      Top             =   9810
      Width           =   2760
   End
   Begin VB.Frame Frame5 
      Caption         =   "Documento de venta"
      Height          =   2985
      Left            =   7470
      TabIndex        =   23
      Top             =   105
      Width           =   10215
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1935
         MaxLength       =   100
         TabIndex        =   95
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1995
         Width           =   8085
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkPlazoEntrega 
         Height          =   315
         Left            =   150
         OleObjectBlob   =   "venPOS2.frx":03D4
         TabIndex        =   91
         Top             =   1680
         Visible         =   0   'False
         Width           =   1680
      End
      Begin VB.TextBox TxtPlazoEntrega 
         Height          =   315
         Left            =   1935
         TabIndex        =   90
         Text            =   "Plazo entrega: 10 dias, Pago Contado"
         Top             =   1635
         Visible         =   0   'False
         Width           =   3870
      End
      Begin VB.TextBox TxtOC 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1950
         MaxLength       =   18
         TabIndex        =   64
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   1245
         Width           =   1395
      End
      Begin VB.Frame Frame3 
         Height          =   735
         Left            =   4620
         TabIndex        =   46
         Top             =   2205
         Width           =   5430
         Begin VB.TextBox txtTotal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFC0&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   24
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   1875
            Locked          =   -1  'True
            TabIndex        =   48
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   150
            Width           =   3465
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   480
            Left            =   90
            OleObjectBlob   =   "venPOS2.frx":0452
            TabIndex        =   47
            Top             =   150
            Width           =   2055
         End
      End
      Begin VB.TextBox TxtNeto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   8790
         Locked          =   -1  'True
         TabIndex        =   44
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1110
         Width           =   1200
      End
      Begin VB.TextBox TxtIva 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   285
         Left            =   8805
         Locked          =   -1  'True
         TabIndex        =   42
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   1485
         Width           =   1200
      End
      Begin VB.TextBox Text11 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7035
         TabIndex        =   40
         ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
         Top             =   2460
         Width           =   1200
      End
      Begin VB.TextBox TxtRecargoAjuste 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   6855
         TabIndex        =   27
         Text            =   "0"
         Top             =   1485
         Width           =   615
      End
      Begin VB.TextBox TxtDsctoAjuste 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   6855
         TabIndex        =   26
         Text            =   "0"
         Top             =   1065
         Width           =   615
      End
      Begin VB.ComboBox CboVendedor 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "venPOS2.frx":04B2
         Left            =   1950
         List            =   "venPOS2.frx":04B4
         Style           =   2  'Dropdown List
         TabIndex        =   32
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   825
         Width           =   3300
      End
      Begin VB.ComboBox CboDocInidicio 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "venPOS2.frx":04B6
         Left            =   1950
         List            =   "venPOS2.frx":04B8
         Style           =   2  'Dropdown List
         TabIndex        =   31
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   450
         Width           =   3285
      End
      Begin VB.ComboBox CboDocVenta 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "venPOS2.frx":04BA
         Left            =   435
         List            =   "venPOS2.frx":04BC
         Style           =   2  'Dropdown List
         TabIndex        =   30
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   105
         Visible         =   0   'False
         Width           =   2370
      End
      Begin VB.TextBox TxtListaPrecio 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   8670
         Locked          =   -1  'True
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   90
         Visible         =   0   'False
         Width           =   2775
      End
      Begin VB.TextBox TxtSubTotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   7770
         Locked          =   -1  'True
         TabIndex        =   28
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   210
         Width           =   1530
      End
      Begin VB.TextBox TxtDescuentoX100 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   6825
         TabIndex        =   25
         Text            =   "0"
         Top             =   585
         Width           =   615
      End
      Begin VB.TextBox TxtValorDescuento 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   7770
         Locked          =   -1  'True
         TabIndex        =   24
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   600
         Width           =   1530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   300
         Index           =   0
         Left            =   150
         OleObjectBlob   =   "venPOS2.frx":04BE
         TabIndex        =   33
         Top             =   480
         Width           =   1635
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   330
         Left            =   120
         OleObjectBlob   =   "venPOS2.frx":0534
         TabIndex        =   34
         Top             =   915
         Width           =   1710
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   330
         Left            =   150
         OleObjectBlob   =   "venPOS2.frx":059A
         TabIndex        =   35
         Top             =   1230
         Width           =   1710
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
         Height          =   330
         Left            =   5985
         OleObjectBlob   =   "venPOS2.frx":0600
         TabIndex        =   36
         Top             =   210
         Width           =   1710
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   0
         Left            =   5895
         OleObjectBlob   =   "venPOS2.frx":0666
         TabIndex        =   37
         Top             =   615
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   1
         Left            =   5805
         OleObjectBlob   =   "venPOS2.frx":06CA
         TabIndex        =   38
         Top             =   1125
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   2
         Left            =   5805
         OleObjectBlob   =   "venPOS2.frx":0738
         TabIndex        =   39
         Top             =   1545
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   3
         Left            =   5910
         OleObjectBlob   =   "venPOS2.frx":079C
         TabIndex        =   41
         Top             =   2460
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   4
         Left            =   7665
         OleObjectBlob   =   "venPOS2.frx":0803
         TabIndex        =   43
         Top             =   1485
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
         Height          =   330
         Index           =   5
         Left            =   7650
         OleObjectBlob   =   "venPOS2.frx":0865
         TabIndex        =   45
         Top             =   1110
         Width           =   915
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   330
         Left            =   165
         OleObjectBlob   =   "venPOS2.frx":08C3
         TabIndex        =   96
         Top             =   1980
         Width           =   1710
      End
   End
   Begin VB.TextBox TxtPOS 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   18240
      Locked          =   -1  'True
      TabIndex        =   6
      TabStop         =   0   'False
      Text            =   "POS"
      Top             =   -30
      Width           =   1275
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   285
      Index           =   1
      Left            =   18315
      OleObjectBlob   =   "venPOS2.frx":092D
      TabIndex        =   5
      Top             =   165
      Width           =   1650
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   75
      OleObjectBlob   =   "venPOS2.frx":0991
      Top             =   735
   End
   Begin VB.Timer Timer1 
      Left            =   60
      Top             =   60
   End
   Begin VB.Frame Frame1 
      Caption         =   "AlvaMar-POS"
      Height          =   6255
      Left            =   45
      TabIndex        =   4
      Top             =   3120
      Width           =   17655
      Begin ACTIVESKINLibCtl.SkinLabel SkUltimoInventario 
         Height          =   255
         Left            =   13695
         OleObjectBlob   =   "venPOS2.frx":0BC5
         TabIndex        =   94
         ToolTipText     =   "Ultimo ajuste de inventario"
         Top             =   165
         Width           =   3720
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkUm 
         Height          =   345
         Left            =   9825
         OleObjectBlob   =   "venPOS2.frx":0C3B
         TabIndex        =   92
         Top             =   285
         Width           =   1410
      End
      Begin VB.CommandButton CmdVisorCotizaciones 
         Caption         =   "V"
         Height          =   495
         Left            =   12090
         TabIndex        =   89
         Top             =   5520
         Width           =   225
      End
      Begin VB.CommandButton CmdCargarCotizacion 
         Caption         =   "Cotizacion"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   10320
         TabIndex        =   88
         Top             =   5520
         Width           =   1740
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkUbicacion 
         Height          =   300
         Left            =   3855
         OleObjectBlob   =   "venPOS2.frx":0C9D
         TabIndex        =   87
         Top             =   585
         Width           =   5760
      End
      Begin VB.CommandButton CmdF8 
         Caption         =   "F8 - Ficha Producto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   7620
         TabIndex        =   86
         Top             =   5520
         Width           =   2670
      End
      Begin VB.Frame FrmPago 
         Caption         =   "Ultima Venta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   1080
         Left            =   11715
         TabIndex        =   75
         Top             =   4995
         Visible         =   0   'False
         Width           =   5670
         Begin VB.TextBox TxtSumaPagos 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   2265
            Locked          =   -1  'True
            TabIndex        =   79
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   540
            Width           =   1605
         End
         Begin VB.TextBox TxtAPagar 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   645
            Locked          =   -1  'True
            TabIndex        =   77
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   540
            Width           =   1605
         End
         Begin VB.TextBox TxtSaldoPago 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF80&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   3885
            Locked          =   -1  'True
            TabIndex        =   76
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   540
            Width           =   1605
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   375
            Index           =   0
            Left            =   2235
            OleObjectBlob   =   "venPOS2.frx":0CFA
            TabIndex        =   78
            Top             =   285
            Width           =   1605
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   375
            Index           =   1
            Left            =   360
            OleObjectBlob   =   "venPOS2.frx":0D68
            TabIndex        =   80
            Top             =   300
            Width           =   1845
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
            Height          =   375
            Index           =   2
            Left            =   3645
            OleObjectBlob   =   "venPOS2.frx":0DCC
            TabIndex        =   81
            Top             =   285
            Width           =   1830
         End
      End
      Begin VB.CommandButton CmdF5 
         Caption         =   "F5 - Emitir"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   360
         TabIndex        =   62
         Top             =   5520
         Width           =   2235
      End
      Begin VB.CommandButton CmdF7 
         Caption         =   "F7 - Anular Marcado"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   2640
         TabIndex        =   61
         Top             =   5520
         Width           =   2670
      End
      Begin VB.CommandButton CmdF9 
         Caption         =   "F9 - Anular Todo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   5340
         TabIndex        =   60
         Top             =   5520
         Width           =   2235
      End
      Begin VB.TextBox TxtCodigo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   390
         TabIndex        =   0
         Top             =   885
         Width           =   2370
      End
      Begin VB.CommandButton CmdAceptaLinea 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         Caption         =   "Ok"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   16140
         TabIndex        =   3
         Top             =   885
         Width           =   1245
      End
      Begin VB.TextBox TxtStockLinea 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   14610
         Locked          =   -1  'True
         TabIndex        =   53
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   885
         Width           =   1455
      End
      Begin VB.TextBox TxtTotalLinea 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   12930
         Locked          =   -1  'True
         TabIndex        =   52
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   885
         Width           =   1650
      End
      Begin VB.TextBox TxtPrecio 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   11085
         TabIndex        =   2
         Text            =   "0"
         Top             =   885
         Width           =   1815
      End
      Begin VB.TextBox TxtCantidad 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   9690
         TabIndex        =   1
         Text            =   "1"
         Top             =   885
         Width           =   1380
      End
      Begin VB.TextBox TxtDescripcion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   2790
         MultiLine       =   -1  'True
         TabIndex        =   51
         TabStop         =   0   'False
         Top             =   885
         Width           =   6855
      End
      Begin VB.TextBox TxtTemp 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   465
         Left            =   6615
         TabIndex        =   49
         Text            =   "0"
         Top             =   90
         Visible         =   0   'False
         Width           =   1545
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   8
         Left            =   390
         OleObjectBlob   =   "venPOS2.frx":0E2E
         TabIndex        =   50
         Top             =   600
         Width           =   660
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   225
         Index           =   9
         Left            =   2805
         OleObjectBlob   =   "venPOS2.frx":0E90
         TabIndex        =   54
         Top             =   660
         Width           =   1530
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   3
         Left            =   9660
         OleObjectBlob   =   "venPOS2.frx":0EFC
         TabIndex        =   55
         Top             =   645
         Width           =   1395
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   10
         Left            =   11220
         OleObjectBlob   =   "venPOS2.frx":0F62
         TabIndex        =   56
         Top             =   645
         Width           =   1635
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   12
         Left            =   13155
         OleObjectBlob   =   "venPOS2.frx":0FC4
         TabIndex        =   57
         Top             =   645
         Width           =   1440
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   240
         Index           =   11
         Left            =   14640
         OleObjectBlob   =   "venPOS2.frx":1024
         TabIndex        =   58
         Top             =   645
         Width           =   1440
      End
      Begin MSComctlLib.ListView LvVenta 
         Height          =   3990
         Left            =   390
         TabIndex        =   59
         Top             =   1440
         Width           =   17025
         _ExtentX        =   30030
         _ExtentY        =   7038
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483646
         BackColor       =   -2147483643
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   11
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Interno"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Codigo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T3000"
            Text            =   "Descripci�n"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "N102"
            Text            =   "Cantidad"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N100"
            Text            =   "Precio"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "Stock"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T1000"
            Text            =   "Inventario"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Precio Costo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Dscto Volumen"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Object.Tag             =   "T1000"
            Text            =   "UM"
            Object.Width           =   2540
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkInventariable 
         Height          =   240
         Left            =   16050
         OleObjectBlob   =   "venPOS2.frx":1084
         TabIndex        =   63
         Top             =   495
         Width           =   1440
      End
   End
   Begin MSComctlLib.ListView LVFpago 
      Height          =   3450
      Left            =   18750
      TabIndex        =   65
      Top             =   465
      Width           =   2940
      _ExtentX        =   5186
      _ExtentY        =   6085
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483646
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N109"
         Text            =   "Id Interno"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "N109"
         Text            =   "valor"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "id IF"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView LvCheques 
      Height          =   1260
      Left            =   18225
      TabIndex        =   66
      Top             =   5025
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   2223
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12648384
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "N100"
         Text            =   "id doc"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Tag             =   "T1000"
         Text            =   "RUT"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Tag             =   "N109"
         Text            =   "Numero"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Tag             =   "T1500"
         Text            =   "Banco"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "T1000"
         Text            =   "Plaza"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   2196
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Object.Tag             =   "N100"
         Text            =   "Monto"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Tag             =   "T1000"
         Text            =   "Observacion"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Tag             =   "N109"
         Text            =   "banco_id"
         Object.Width           =   2540
      EndProperty
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   225
      Index           =   5
      Left            =   5355
      OleObjectBlob   =   "venPOS2.frx":10EE
      TabIndex        =   73
      Top             =   9765
      Width           =   660
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   225
      Index           =   6
      Left            =   6315
      OleObjectBlob   =   "venPOS2.frx":1156
      TabIndex        =   74
      Top             =   10290
      Width           =   1035
   End
End
Attribute VB_Name = "venPOS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sm_TipoPos As String
Dim Sm_PermiteDscto As String
Dim Sm_PrecioVtaModificable As String
Dim Sm_VentaRapida As String
Dim Dp_Descuento As Double
Dim Im_Redondear As Integer
Dim Lm_CuentaVentas As Long
Dim Sm_UtilizaCodigoInterno As String * 2
Dim Im_Descuento_Maximo As Double
Dim Im_Recargo_Maximo As Double
Dim Sm_Codigo_Barra_Pesable As String * 2
Dim Lp_Id_Unico_Venta As Long 'id del documento
Dim Lp_Id_Nueva_Venta As Long 'Nro documento
Dim Bm_BoletaEnProceso As Boolean
Dim Sm_ImprimeTicketNv As String * 2
Dim Sp_EmpresaRepuestos As String * 2
Dim Sp_MatrizDescuentos As String * 2


Private Function Redondear(Valor As Double, Redo As Integer) As Long
    If Redo = 100 Then 'Redondeamos a 100
    
        If Val(Right(Str(Valor), 2)) > 0 Then
            If Val(Right(Str(Valor), 2)) >= 50 Then
                Valor = Valor + (100 - Val(Right(Str(Valor), 2)))
            Else
                Valor = Valor - Val(Right(Str(Valor), 2))
            End If
        End If
        
    End If
    Redondear = Valor

End Function



Private Sub CboDocInidicio_Click()
    Sql = "SELECT doc_requiere_rut,doc_cantidad_lineas " & _
            "FROM sis_documentos " & _
            "WHERE doc_id=" & Me.CboDocInidicio.ItemData(Me.CboDocInidicio.ListIndex)
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        'SkLimiteLineas.Tag = RsTmp2!doc_cantidad_lineas
        CboDocInidicio.Tag = RsTmp2!doc_requiere_rut
    End If
    If CboDocInidicio.Text = "COTIZACION" Then
        If SP_Rut_Activo = "78.967.170-2" Then
            Me.SkPlazoEntrega.Visible = True
            Me.TxtPlazoEntrega.Visible = True
        End If
    Else
            Me.SkPlazoEntrega.Visible = False
            Me.TxtPlazoEntrega.Visible = False
    End If
End Sub





Private Sub CboSucursal_Click()
    Dim Ip_IdSuc As Integer
    If CboSucursal.ListIndex = -1 Then Exit Sub
    
    Ip_IdSuc = CboSucursal.ItemData(CboSucursal.ListIndex)
    If Ip_IdSuc = 0 Then
        'Busca_Id_Combo CboGiros, 0
        Exit Sub
    End If
    Sql = "SELECT suc_direccion,suc_ciudad,suc_contacto suc_comuna,gir_id " & _
          "FROM par_sucursales " & _
          "WHERE suc_id=" & Ip_IdSuc
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        TxtDireccion = RsTmp2!suc_direccion
        TxtCiudad = RsTmp2!suc_ciudad
        txtComuna = RsTmp2!suc_comuna
    End If
End Sub




Private Sub CmdAceptaLinea_Click()
    Dim p As Integer
    If Len(TxtCodigo) = 0 Then
        MsgBox "Ingrese c�digo...", vbInformation
        On Error Resume Next
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Len(TxtDescripcion) = 0 Then
        MsgBox "Faltan datos para agregar linea...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Val(CxP(TxtCantidad)) = 0 Then
        MsgBox "Falta cantidad...", vbInformation
        TxtCantidad.SetFocus
        Exit Sub
    End If
    If Val(TxtPrecio) = 0 Then
        MsgBox "Falta precio...", vbInformation
        TxtPrecio.SetFocus
        Exit Sub
    End If
    
    '04-10-2016' verificar que la empresa venda con o sin stock
    If SG_VenderSinStock = "NO" Then
    
        If Val(TxtStockLinea) < 1 Then
            MsgBox "Producto sin stock...", vbCritical
            Exit Sub
        End If
    
    End If
    
    
    LvVenta.ListItems.Add , , TxtCodigo.Tag
    p = LvVenta.ListItems.Count
    LvVenta.ListItems(p).SubItems(1) = TxtCodigo
    LvVenta.ListItems(p).SubItems(2) = TxtDescripcion
    LvVenta.ListItems(p).SubItems(3) = TxtCantidad
    LvVenta.ListItems(p).SubItems(4) = TxtPrecio
    
   ' If Sm_VentaRapida = "SI" Then
        LvVenta.ListItems(p).SubItems(5) = Round(CDbl(TxtPrecio) * Val(CxP(TxtCantidad)), 0)
   ' Else
   '     LvVenta.ListItems(p).SubItems(5) = TxtTotalLinea
   'End If
    LvVenta.ListItems(p).SubItems(6) = TxtStockLinea
    LvVenta.ListItems(p).SubItems(7) = SkInventariable
    LvVenta.ListItems(p).SubItems(8) = Val(TxtStockLinea.Tag)
    
    If Sp_MatrizDescuentos = "SI" Then
        
        LvVenta.ListItems(p).SubItems(9) = TxtCantidad.Tag
    
    End If
    
    
    If SP_Rut_Activo = "78.967.170-2" Then
        LvVenta.ListItems(p).SubItems(10) = "UN"
        Sql = "SELECT ume_nombre " & _
                "FROM sis_unidad_medida " & _
                "WHERE ume_id=" & Val(SkUm)
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
            LvVenta.ListItems(p).SubItems(10) = RsTmp!ume_nombre
        End If
    
        
        
    End If
    
    LimpiaTxt
    CalculaGrilla
    
    LvVenta.ListItems(LvVenta.ListItems.Count).Selected = True
    LvVenta.ListItems(LvVenta.ListItems.Count).EnsureVisible
    
    
        
    
    TxtCodigo.SetFocus
    
    
End Sub

Private Sub CalculaGrilla()
    Dim Dp_DctoX100 As Double
    Dim Lp_DsctoAjuste As Long
    Dim Lp_RecargoAjuste As Long
    If LvVenta.ListItems.Count > 0 Then
     '   If Val(TxtDescuentoX100) > 0 Then
            Dp_DctoX100 = TxtDescuentoX100
             Lp_DsctoAjuste = TxtDsctoAjuste
            Lp_RecargoAjuste = TxtRecargoAjuste
            
            TotalesEn0 'en 0 todos los totales
            TxtSubTotal = 0
            
            For i = 1 To LvVenta.ListItems.Count
                TxtSubTotal = Val(TxtSubTotal) + CDbl(LvVenta.ListItems(i).SubItems(5))
            Next
            
            TxtSubTotal = NumFormat(TxtSubTotal)
            txtTotal = TxtSubTotal
            
            
            
            If Dp_DctoX100 > 0 Then
                TxtDescuentoX100 = Dp_DctoX100
                
                If Sp_MatrizDescuentos = "SI" Then
                    CalculaDescuentox100Matriz
                Else
                    CalculaDescuentox100
                End If
            End If
            
            
            
            TxtDsctoAjuste = Lp_DsctoAjuste
            TxtRecargoAjuste = Lp_RecargoAjuste
            If Val(Me.TxtDsctoAjuste) > 0 Then 'Ajuste de descuento
                txtTotal = NumFormat(CDbl(txtTotal) - CDbl(TxtDsctoAjuste))
            
            End If
            If Val(Me.TxtRecargoAjuste) > 0 Then
                txtTotal = NumFormat(CDbl(txtTotal) + (TxtRecargoAjuste))
            End If
      '  End If
    
    Else
        TotalesEn0
    End If
    
    
    'Ver codigo repetidos y cambiarles color
    NormalizaColores
End Sub
Private Sub NormalizaColores()
    LvVenta.Refresh
    For X = 1 To LvVenta.ListItems.Count
        'Normalizar antes
        For Z = 1 To LvVenta.ColumnHeaders.Count - 3
            LvVenta.ListItems(X).ListSubItems(Z).ForeColor = vbBlack
            LvVenta.ListItems(X).ListSubItems(Z).Bold = False
        Next
    Next
    
    LvVenta.Refresh
    
    
    For i = 1 To LvVenta.ListItems.Count
      '  Sp_VerCodigo LvVenta.ListItems(i).SubItems(1)
        
        For X = 1 To LvVenta.ListItems.Count
            If X <> i Then
                If LvVenta.ListItems(X).SubItems(1) = LvVenta.ListItems(i).SubItems(1) Then
                    'codigo igual
                    'pintar
                    For Z = 1 To LvVenta.ColumnHeaders.Count - 5
                        LvVenta.ListItems(X).ListSubItems(Z).ForeColor = vbRed
                        LvVenta.ListItems(X).ListSubItems(Z).Bold = True
                    Next
                End If
            End If
        Next
    
    Next
    LvVenta.Refresh
End Sub


Private Sub CalculaDescuentox100()
    If CDbl(TxtSubTotal) > 0 Then
        TxtValorDescuento = NumFormat(CDbl(TxtSubTotal) / 100 * Val(TxtDescuentoX100))
        txtTotal = NumFormat(CDbl(TxtSubTotal) - CDbl(TxtValorDescuento))
    End If
    
End Sub

Private Sub CalculaDescuentox100Matriz()
    Dim Lp_SubTotalVolumen As Long
    Lp_SubTotalVolumen = 0
    For i = 1 To LvVenta.ListItems.Count
        If LvVenta.ListItems(i).SubItems(9) = "NO" Then
                Lp_SubTotalVolumen = Val(Lp_SubTotalVolumen) + CDbl(LvVenta.ListItems(i).SubItems(5))
        End If
    Next

    If CDbl(Lp_SubTotalVolumen) > 0 Then
        TxtValorDescuento = NumFormat(CDbl(Lp_SubTotalVolumen) / 100 * Val(TxtDescuentoX100))
        txtTotal = NumFormat(CDbl(TxtSubTotal) - CDbl(TxtValorDescuento))
    End If
    
End Sub

Private Sub TotalesEn0()
    TxtSubTotal = 0
    Me.TxtDescuentoX100 = 0
    'Me.TxtDsctoTotal = 0
    Me.TxtDsctoAjuste = 0
    Me.TxtRecargoAjuste = 0
    TxtNeto = 0
    TxtIva = 0
    txtTotal = 0
End Sub

Private Sub LimpiaTxt()
    TxtCodigo.Tag = ""
    TxtCodigo = ""
    TxtDescripcion = ""
    TxtCantidad = "1"
    TxtPrecio = "0"
    SkInventariable = "SI"
    TxtStockLinea = "0"
    TxtTotalLinea = "0"
End Sub


'Private Sub CmdAgrega_Click()
'    If Len(TxtPLU) = 0 Then
'        MsgBox "Falta codigo de producto...", vbOKOnly + vbInformation
'        TxtPLU.SetFocus
'        Exit Sub
'    End If
'    If Val(TxtCant) = 0 Then
'        MsgBox "Falta cantidad...", vbOKOnly + vbInformation
'        TxtCant.SetFocus
'        Exit Sub
'    End If
'
'    If LvDetalle.ListItems.Count = Val(SkLimiteLineas.Tag) Then
'        MsgBox "Alcanz� el limite de art�culos para el documento...", vbInformation
'        Exit Sub
'    End If
'
'    If Val(TxtPLU) = 0 Then
'        MsgBox "Ingrese c�digo (PLU) articulos...", vbInformation
'        TxtPLU.SetFocus
'        Exit Sub
'    End If
'    If Val(TxtPrecioFinal) = 0 Then Exit Sub
'
'
'
'
'    Dp_Total = CDbl(TxtPrecioFinal) * Val(CxP(TxtCant))
'    LvDetalle.ListItems.Add , , LvDetalle.ListItems.Count + 1
'    Ip_C = LvDetalle.ListItems.Count
'    LvDetalle.ListItems(Ip_C).SubItems(1) = RsTmp!Id
'    LvDetalle.ListItems(Ip_C).SubItems(2) = TxtCant
'    LvDetalle.ListItems(Ip_C).SubItems(3) = RsTmp!Descripcion
'    LvDetalle.ListItems(Ip_C).SubItems(4) = NumFormat(TxtPrecioFinal)
'    LvDetalle.ListItems(Ip_C).SubItems(5) = NumFormat(Dp_Total)
'    LvDetalle.ListItems(Ip_C).SubItems(6) = RsTmp!pro_inventariable
'    LvDetalle.ListItems(Ip_C).SubItems(7) = RsTmp!precio_compra
'    LvDetalle.ListItems(Ip_C).SubItems(8) = RsTmp!precio_venta
'    LvDetalle.ListItems(Ip_C).SubItems(9) = Dp_Descuento
'
'
'    TxtCant = "1,000"
'    CboDescuento.ListIndex = 0
'    TxtPLU = ""
'    TxtPrecioFinal = ""
'    TxtDescripcion = ""
'    TxtPrecioOriginal = ""
'    TxtPLU.SetFocus
'    ElTotal
'    'CuentaLineas
'
'    LvDetalle.ListItems(LvDetalle.ListItems.Count).Selected = True
'End Sub

'Private Sub CmdAnulaTodo_Click()
'    LvDetalle.ListItems.Clear
'    TxtRut = ""
'    TxtRazonSocial = ""
'    TxtPLU.SetFocus
'    ElTotal
'End Sub

Private Sub CmdBuscaCliente_Click()
     LlamaClienteDe = "VD"
    
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    TxtRut.SetFocus
End Sub


Private Sub CmdCargarCotizacion_Click()
    sis_InputBox.Sm_TipoDato = "N"
    sis_InputBox.Caption = "CARGAR COTIZACION"
    sis_InputBox.texto.PasswordChar = ""
    sis_InputBox.texto.Tag = "N"
    sis_InputBox.FramBox = "Nro de Cotizacion"
    sis_InputBox.Show 1
    Sp_Llave = UCase(SG_codigo2)
    If Len(Sp_Llave) = 0 Then Exit Sub
    
   CargaCotizacion Val(Sp_Llave)
    
    
    
End Sub
Private Sub CargaCotizacion(Nro As Long)
 
    Sql = "SELECT * " & _
            "FROM ven_nota_venta " & _
            "WHERE nve_cotizacion='SI' AND nve_terminada='NO' AND  nve_id=" & Val(Nro)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        
        Busca_Id_Combo CboDocInidicio, Val(RsTmp!doc_id)
        Busca_Id_Combo CboVendedor, Val(RsTmp!ven_id)
        txtTotal = NumFormat(RsTmp!nve_total)
        Me.TxtDescuentoX100 = RsTmp!nve_descuentoxcien
        TxtValorDescuento = RsTmp!nve_valor_descuento
        TxtDsctoAjuste = RsTmp!nve_valor_descuento_adicional
        TxtRecargoAjuste = RsTmp!nve_valor_recargo
        TxtNeto = RsTmp!nve_neto
        TxtIva = RsTmp!nve_iva
        TxtRut = RsTmp!rut_cliente
        Sql = "SELECT codigo cod1,pro_codigo_interno cod2,nvd_descripcion,nvd_cantidad,nvd_precio_unitario,nvd_precio_total,0,nvd_inventariable,round(nvd_precio_costo/nvd_Cantidad,0) " & _
                "FROM ven_nota_venta_detalle " & _
                "WHERE nve_id=" & Val(Nro)
        Consulta RsTmp, Sql
        LLenar_Grilla RsTmp, Me, LvVenta, False, True, True, False
        
        
        TxtRut_Validate True
    
    Else
        MsgBox "Cotizacion no encontrada...", vbExclamation
        
    
    End If
    
End Sub



Private Sub CmdF5_Click()
    Dim Sp_ElRut As String
    Dim Ip_Id_Doc As Integer
    Dim Sp_Nombre_doc As String
    Dim Lp_IdAbo As Long
    Dim Lp_Nro_Comprobante As Long
    Dim Ip_Id_Suc As Integer
    Dim Sp_ProductosSS As String
    Ip_Id_Suc = 0
    
 '   If CboDocInidicio.Text = "COTIZACION" Then
 '
  '  End If
    
    If SP_Rut_Activo = "76.169.962-8" Then
        If CboVendedor.Text = "SELECCIONE VENDEDOR..." Then
            
            MsgBox "No ha seleccionado vendedor...", vbExclamation
            CboVendedor.SetFocus
            Exit Sub
        End If
    
    End If
    CmdF5.Enabled = False
    
    If Mid(CboDocInidicio.Text, 1, 13) = "BOLETA FISCAL" Then
        
            If SG_ImpresoraFiscalBixolon = "SI" Then
                If SG_Marca_Impresora_Fiscal = "EPSON" Then
                
                    If Sm_VentaRapida = "SI" Then
                        If Bm_BoletaEnProceso Then Exit Sub
                    End If
                End If
            End If
    End If
                        
                   
    If Val(TxtDescuentoX100) = 0 Then
        TxtDescuentoX100 = "0"
        TxtValorDescuento = "0"
    End If
    If Val(TxtDsctoAjuste) = 0 Then TxtDsctoAjuste = "0"
    CalculaGrilla
                        
    
    If LvVenta.ListItems.Count = 0 Then
        CmdF5.Enabled = True
        Exit Sub
    End If
            'POS SOLO EMITE NOTA DE VENTA
    If Me.CboDocInidicio.ListIndex = -1 Then
        MsgBox "Falta documento final...", vbOKOnly + vbInformation
         CmdF5.Enabled = True
        Exit Sub
    End If
    
    If SG_CondicionaVentaSinStock = "SI" Then
        'CONDICIONA PROUDCCTOS SNI STOCK
        Sp_ProductosSS = ""
        For i = 1 To LvVenta.ListItems.Count
            If Val(LvVenta.ListItems(i).SubItems(6)) < 1 Then
                Sp_ProductosSS = Sp_ProductosSS & LvVenta.ListItems(i).SubItems(1) & " - " & LvVenta.ListItems(i).SubItems(2) & vbNewLine
            End If
        Next
        
        If Len(Sp_ProductosSS) > 0 Then
            If MsgBox("SIN  STOCK:" & vbNewLine & Sp_ProductosSS & "�Desea continuar..?", vbYesNo + vbQuestion) = vbYes Then
                
                
            Else
                Me.CmdF5.Enabled = True
                LvVenta.SetFocus
                Exit Sub
                
            End If
        End If
    
    
    End If
    
    
    If CboDocInidicio.Tag = "SI" Then
        If Len(TxtRut) = 0 Then
            MsgBox "Requiere ingresar cliente...", vbInformation
            TxtRut.SetFocus
            CmdF5.Enabled = True
            Exit Sub
        End If
    End If
    
    'verificar descuentos/recargos maximos.
    If Not PermiteAjuste Then
        MsgBox "Ajustes fuera de rango permitido...", vbInformation
        On Error Resume Next
        Me.TxtDescuentoX100.SetFocus
        CmdF5.Enabled = True
        Exit Sub
    End If
    
    If Me.CboSucursal.ListIndex > -1 Then
        Ip_Id_Suc = CboSucursal.ItemData(CboSucursal.ListIndex)
    End If
    
    If CboDocInidicio.Text = "COTIZACION" Then
        
        
         
        'Guardando Cotizacion para poder llamarla en el futuro
        elbruto = CDbl(txtTotal)
        elneto = Round(CDbl(txtTotal) / Val("1." & DG_IVA), 0)
    
        If Len(TxtRut) = 0 Then TxtRut = "11.111.111-1"
        Lp_Id_Unico_Venta = UltimoNro("ven_nota_venta", "nve_id")
        Sql = "INSERT INTO ven_nota_venta (nve_id,doc_id,nve_fecha,ven_id,rut_cliente,nve_neto,nve_iva,nve_total,nve_descuentoxcien,nve_valor_descuento,nve_valor_descuento_adicional," & _
                                            "nve_valor_recargo,nve_terminada,nve_hora,nve_equipo_emitida,nve_login,nve_cotizacion,suc_id,sue_id) " & _
                "VALUES(" & Lp_Id_Unico_Venta & "," & Me.CboDocInidicio.ItemData(Me.CboDocInidicio.ListIndex) & ",'" & Fql(Date) & "'," & CboVendedor.ItemData(CboVendedor.ListIndex) & ",'" & TxtRut & "'," & elneto & "," & elbruto - elneto & "," & _
                elbruto & "," & CDbl(TxtDescuentoX100) & "," & CDbl(TxtValorDescuento) & "," & CDbl(TxtDsctoAjuste) & "," & CDbl(TxtRecargoAjuste) & ",'NO',CURTIME(),'" & SP_Nombre_Equipo & "','" & LogUsuario & "','SI'," & Ip_Id_Suc & "," & IG_id_Sucursal_Empresa & ")"
 
        cn.Execute Sql
 
        Sql = "INSERT INTO ven_nota_venta_detalle (nve_id,codigo,pro_codigo_interno,nvd_cantidad,nvd_precio_unitario,nvd_precio_total,nvd_inventariable,nvd_descripcion,nvd_precio_costo,nvd_precio_total_venta_neto) VALUES  "
 
        sql2 = ""
        With LvVenta
            For i = 1 To .ListItems.Count
                        elbruto = CDbl(.ListItems(i).SubItems(5))
                        elneto = Round(CxP(CDbl(.ListItems(i).SubItems(5)) / Val("1." & DG_IVA)), 0)
                        elneto = Round(CDbl(.ListItems(i).SubItems(5) / Val("1." & DG_IVA)), 0)
                        precioreal = CxP(CDbl(.ListItems(i).SubItems(4))) '+ CDbl(.ListItems(i).SubItems(9))
                        sql2 = sql2 & "(" & Lp_Id_Unico_Venta & "," & .ListItems(i) & ",'" & .ListItems(i).SubItems(1) & "'," & CxP(.ListItems(i).SubItems(3)) & "," & _
                                CxP(CDbl(.ListItems(i).SubItems(4))) & "," & CDbl(.ListItems(i).SubItems(5)) & ",'" & .ListItems(i).SubItems(7) & "','" & .ListItems(i).SubItems(2) & "'," & CxP(CDbl(.ListItems(i).SubItems(8)) * Val(CxP(.ListItems(i).SubItems(3)))) & "," & elneto & "),"
            Next
            If Len(sql2) > 0 Then
                sql2 = Mid(sql2, 1, Len(sql2) - 1)
                cn.Execute Sql & sql2
            End If
        End With
    
    
    
    
    
    
    
        PrevisualizaCotizacion
        
    
        If SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.239.518-5" Then
            'kyr y alcalde
            
            If SG_codigo = "print" Then
                    On Error GoTo CancelaImpesionNV
                    Dialogo.CancelError = True
                    Dialogo.ShowPrinter
                    La_Establecer_Impresora Printer.DeviceName
                    
                    
                  '  EstableImpresora Printer.DeviceName
                    ImprimeCOTIZACION
            End If
        ElseIf SP_Rut_Activo = "78.967.170-2" Then
                'Plasticos aldunate
                If SG_codigo = "print" Then
                    On Error GoTo CancelaImpesionNV
                    Dialogo.CancelError = True
                    Dialogo.ShowPrinter
                    
                    If Printer.DeviceName = "TERMICA" Then
                        ProcCOTIZACION
                    Else
                        La_Establecer_Impresora Printer.DeviceName
                        ImprimeCOTIZACION
                    End If
                End If
            
        Else
        
            'Termica
            If SG_codigo = "print" Then ProcCOTIZACION
        End If
    
    
        
        LimpiaTodo
        TxtCodigo.SetFocus
        CmdF5.Enabled = True
        Exit Sub
    End If
    
    
    
    'Hasta aqui se podria grabar la nota de venta.
    '22 Agosto
    If Sm_VentaRapida = "SI" Then
        SG_Pago_Correcto = "NO"
        Me.FrmPago.Visible = True
        Im_Nv = 1
        VenPosCaja.TxtAPagar = txtTotal
        TxtAPagar = txtTotal
        VenPosCaja.TxtRutCliente.Tag = Val(TxtMontoCredito) - Val(Me.TxtCupoUtilizado)
        If Len(TxtRut) > 0 Then
            VenPosCaja.TxtRutCliente = TxtRut
            VenPosCaja.TxtNombreCliente = Me.TxtRazonSocial
        End If
        VenPosCaja.Show 1
        If SG_Pago_Correcto = "NO" Then
            TxtCodigo.SetFocus
            CmdF5.Enabled = True
            Exit Sub
        Else
            Set VenPosCaja = Nothing
           ' Unload VenPosCaja
        End If
        
        'Si las validaciones son correctas _
        procedemos a grabar venta, pagos, inventarios, etc etc
    End If

    'Lp_Id_Nueva_Venta = UltimoNro("ven_doc_venta", "no_documento")
    'Lp_Id_Nueva_Venta = AutoIncremento("LEE", , 100, 1)
    
    
    If Len(TxtRut) = 0 Then Sp_ElRut = "11.111.111-1" Else Sp_ElRut = TxtRut
    
    
    elbruto = CDbl(txtTotal)
    elneto = Round(CDbl(txtTotal) / Val("1." & DG_IVA), 0)
    
    If Sm_VentaRapida = "SI" Then
        Ip_Id_Doc = CboDocInidicio.ItemData(CboDocInidicio.ListIndex)
        Sp_Nombre_doc = CboDocInidicio.Text
    Else
        Ip_Id_Doc = CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Sp_Nombre_doc = CboDocVenta.Text
    End If
    
    
    If Sm_VentaRapida = "SI" Then
    
        'SAMSUNG SAMSUNG SAMSUNG SAMSUNG SAMSUNG SAMSUNG
        '17-10-2015
        Lp_Id_Nueva_Venta = 0
        If Mid(CboDocInidicio.Text, 1, 13) = "BOLETA FISCAL" Then
                'Lo hacemos aqui , para obtener de la IF el numero de boleta fiscal
                If SG_ImpresoraFiscalBixolon = "SI" Then
                        If SG_Marca_Impresora_Fiscal = "SAMSUNG" Then
                            EmiteBoletaSamsung
                            Lp_Id_Nueva_Venta = Lp_Nro_Boleta_Obtenida_IF
                        End If
                        If SG_Marca_Impresora_Fiscal = "EPSON" Then
                            EmiteBoletaEpson
                            Lp_Id_Nueva_Venta = Lp_Nro_Boleta_Obtenida_IF
                        End If
                End If
        End If
        If Lp_Id_Nueva_Venta = 0 Then
            Lp_Id_Nueva_Venta = AutoIncremento("lee", Val(CboDocInidicio.ItemData(CboDocInidicio.ListIndex)), , IG_id_Sucursal_Empresa)
        End If
        Lp_Id_Unico_Venta = UltimoNro("ven_doc_venta", "id")
        Sql = "INSERT INTO ven_doc_venta " & _
              "(id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
              "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento," & _
              "usu_nombre,id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario," & _
              "ven_ordendecompra,ven_tipo_calculo,caj_id,sue_id," & _
              "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_id_indicio,doc_time,ven_sub_total,ven_ajuste_descuento,ven_ajuste_recargo,ven_descuento_valor,ven_descuentoxcien) " & _
              "VALUES(" & Lp_Id_Unico_Venta & "," & Lp_Id_Nueva_Venta & "," & _
             "'" & Format(Date, "YYYY-MM-DD") & "','" & Sp_ElRut & "','" & TxtRazonSocial & "','VD'," & elneto & "," & _
              CDbl(txtTotal) & "," & elbruto - elneto & "," & "'VENTA POS','CONTADO'," & CboVendedor.ItemData(CboVendedor.ListIndex) & ",'" & CboVendedor.Text & "',0" & _
              ",'','" & Sp_Nombre_doc & "'," & Ip_Id_Doc & ",0,'" & Format(Date, "YYYY-MM-DD") & "','" & LogUsuario & _
              "',0,'" & SP_Rut_Activo & "'," & 0 & "," & 0 & "," & 0 & _
              ",0,0," & IG_id_Bodega_Ventas & "," & _
              0 & ",'',''," & _
              0 & "," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & ",0," & 0 & "," & 0 & "," & Lp_Id_Nueva_Venta & "," & CboDocInidicio.ItemData(CboDocInidicio.ListIndex)
            Sql = Sql & ",curtime()," & CDbl(TxtSubTotal) & "," & Val(TxtDsctoAjuste) & "," & Val(TxtRecargoAjuste) & "," & CDbl(TxtValorDescuento) & "," & Val(TxtDescuentoX100) & ")"
    Else
        
        'Solo nota de venta
        '2-9 2015, utilizaremos tablas para notas de ventas.
        'ven_nota_venta
        'ven_nota_venta_detalle
        If Len(TxtRut) = 0 Then TxtRut = "11.111.111-1"
        Lp_Id_Unico_Venta = UltimoNro("ven_nota_venta", "nve_id")
        Sql = "INSERT INTO ven_nota_venta (nve_id,doc_id,nve_fecha,ven_id,rut_cliente,nve_neto,nve_iva,nve_total,nve_descuentoxcien,nve_valor_descuento,nve_valor_descuento_adicional," & _
                                            "nve_valor_recargo,nve_terminada,nve_hora,nve_equipo_emitida,nve_login,nve_oc,suc_id,sue_id) " & _
                "VALUES(" & Lp_Id_Unico_Venta & "," & Me.CboDocInidicio.ItemData(Me.CboDocInidicio.ListIndex) & ",'" & Fql(Date) & "'," & CboVendedor.ItemData(CboVendedor.ListIndex) & ",'" & TxtRut & "'," & elneto & "," & elbruto - elneto & "," & _
                elbruto & "," & CDbl(TxtDescuentoX100) & "," & CDbl(TxtValorDescuento) & "," & CDbl(TxtDsctoAjuste) & "," & CDbl(TxtRecargoAjuste) & ",'NO',CURTIME(),'" & SP_Nombre_Equipo & "','" & LogUsuario & "','" & Me.TxtOC & "'," & Ip_Id_Suc & "," & IG_id_Sucursal_Empresa & ")"
         
    End If
    
    cn.Execute Sql
              
    
    If Sm_VentaRapida = "SI" Then
    
        Sql = "INSERT INTO ven_detalle (codigo,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                    "precio_costo,fecha,doc_id,no_documento,rut_emp,pla_id,ved_precio_venta_bruto," & _
                                    "ved_precio_venta_neto,ved_iva,ved_codigo_interno) VALUES "
            
        AutoIncremento "GUARDA", Val(CboDocInidicio.ItemData(CboDocInidicio.ListIndex)), Lp_Id_Nueva_Venta, IG_id_Sucursal_Empresa
    Else
         'ven_nota_venta_detalle
        Sql = "INSERT INTO ven_nota_venta_detalle (nve_id,codigo,pro_codigo_interno,nvd_cantidad,nvd_precio_unitario,nvd_precio_total,nvd_inventariable,nvd_descripcion,nvd_precio_costo,nvd_precio_total_venta_neto) VALUES  "
    End If
        
        
        
    sql2 = ""
    With LvVenta
        
            For i = 1 To .ListItems.Count
                        
                        elbruto = CDbl(.ListItems(i).SubItems(5))
                        elneto = Round(CxP(CDbl(.ListItems(i).SubItems(5)) / Val("1." & DG_IVA)), 0)
                        elneto = Round(CDbl(.ListItems(i).SubItems(5) / Val("1." & DG_IVA)), 0)
                        
                       ' If CDbl(TxtDsctoTotal) = 0 Then
                            'cuando un precio se ajusta hacia arriba
                         '   precioreal = CDbl(.ListItems(i).SubItems(8))
                         '   If precioreal < CDbl(.ListItems(i).SubItems(4)) Then
                         '       precioreal = CDbl(.ListItems(i).SubItems(4))
                         '   End If
                            precioreal = CxP(CDbl(.ListItems(i).SubItems(4))) '+ CDbl(.ListItems(i).SubItems(9))
                            
                            If SP_Rut_Activo = "76.337.408-4" Then
                                precioreal = .ListItems(i).SubItems(4) '+ CDbl(.ListItems(i).SubItems(9))
                            End If
                        'Else
                        '   precioreal = CDbl(.ListItems(i).SubItems(4))
                        
                       ' End If
                       
                        If Sm_VentaRapida = "SI" Then
                             sql2 = sql2 & "('" & _
                            .ListItems(i) & "','" & .ListItems(i).SubItems(2) & "'," & _
                             precioreal & ",0," & _
                             CxP(.ListItems(i).SubItems(3)) & "," & CDbl(.ListItems(i).SubItems(4)) & "," & _
                             CDbl(.ListItems(i).SubItems(5)) & "," & CxP(CDbl(.ListItems(i).SubItems(8))) & "," & _
                             "'" & Fql(Date) & "'," & CboDocInidicio.ItemData(CboDocInidicio.ListIndex) & "," & Lp_Id_Nueva_Venta & ",'" & _
                             SP_Rut_Activo & "'," & Lm_CuentaVentas & "," & elbruto & "," & elneto & "," & elbruto - elneto & ",'" & .ListItems(i).SubItems(1) & "'),"
                        Else
                            If SP_Rut_Activo = "76.337.408-4" Then
                                sql2 = sql2 & "(" & Lp_Id_Unico_Venta & "," & .ListItems(i) & ",'" & .ListItems(i).SubItems(1) & "'," & CxP(.ListItems(i).SubItems(3)) & "," & _
                                    precioreal & "," & CxP(CDbl(.ListItems(i).SubItems(5))) & ",'" & .ListItems(i).SubItems(7) & "','" & .ListItems(i).SubItems(2) & "'," & CxP(.ListItems(i).SubItems(8) * Val(CxP(.ListItems(i).SubItems(3)))) & "," & elneto & "),"
                            Else
                            sql2 = sql2 & "(" & Lp_Id_Unico_Venta & "," & .ListItems(i) & ",'" & .ListItems(i).SubItems(1) & "'," & CxP(.ListItems(i).SubItems(3)) & "," & _
                                CxP(CDbl(.ListItems(i).SubItems(4))) & "," & CxP(CDbl(.ListItems(i).SubItems(5))) & ",'" & .ListItems(i).SubItems(7) & "','" & .ListItems(i).SubItems(2) & "'," & CxP(.ListItems(i).SubItems(8) * Val(CxP(.ListItems(i).SubItems(3)))) & "," & elneto & "),"
                            End If
                        End If
                        
                        
                        
                        
            Next
            If Len(sql2) > 0 Then
                sql2 = Mid(sql2, 1, Len(sql2) - 1)
                cn.Execute Sql & sql2
            End If
         
      End With
      If Sm_VentaRapida = "SI" Then
        If SG_Pago_Correcto = "SI" Then
            '22 Agosto
            'Grabamos el pago
            
            'debemos consultar si hay formas de pago distintas a credito
            venPOS.TxtAPagar.Refresh
            For i = 1 To venPOS.LVFpago.ListItems.Count
                If Val(venPOS.LVFpago.ListItems(i)) = 0 And CDbl(venPOS.LVFpago.ListItems(i).SubItems(1)) = CDbl(txtTotal) Then GoTo final
                    
            
            Next
            
            
            
            
            Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
             Lp_Nro_Comprobante = AutoIncremento("lee", 100, , IG_id_Sucursal_Empresa)
            If Len(TxtRut) = 0 Then TxtRut = "11.111.111-1"
            
            'Aqui registra el pago con su formas
            Sql = "INSERT INTO cta_abonos (abo_id,abo_cli_pro,abo_rut,abo_fecha,abo_fecha_pago,abo_monto,abo_observacion,usu_nombre,suc_id,abo_obs_extra,rut_emp,abo_nro_comprobante,caj_id,abo_origen) " & _
                  "VALUES(" & Lp_IdAbo & ",'CLI','" & TxtRut & "','" & _
                  Format(Date, "YYYY-MM-DD") & "','" & Format(Date, "YYYY-MM-DD") & "'," & CDbl(txtTotal) & _
                  ",'PAGO POS VENTA','" & LogUsuario & "'," & 0 & ",'VENTA RAPIDA','" & SP_Rut_Activo & "'," & Lp_Nro_Comprobante & "," & LG_id_Caja & ",'VENTA')"
                  
            cn.Execute Sql
                
            Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
           ' For i = 1 To LVDetalle.ListItems.Count
                Sql = Sql & "(" & Lp_IdAbo & "," & Lp_Id_Unico_Venta & "," & CDbl(txtTotal) & ",'" & SP_Rut_Activo & "')"
           ' Next
            
            cn.Execute Sql
            
            AutoIncremento "GUARDA", 100, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                        
                    
            If venPOS.LvCheques.ListItems.Count > 0 Then
                'CHEQUE, AQUI GRABAMOS EL DETALLE DE LOS CHEQUES
                
                    Sql = "INSERT INTO abo_cheques (abo_id,ban_id,che_plaza,che_numero,che_monto,che_fecha,che_estado,che_autorizacion,rut_emp,che_ban_nombre) " & _
                             "VALUES "
                    With venPOS.LvCheques
                        For i = 1 To .ListItems.Count
                            Sql = Sql & "(" & Lp_IdAbo & "," & .ListItems(i).SubItems(8) & ",'" & .ListItems(i).SubItems(4) & _
                            "'," & .ListItems(i).SubItems(2) & "," & CDbl(.ListItems(i).SubItems(6)) & ",'" & _
                            Format(.ListItems(i).SubItems(5), "YYYY-MM-DD") & "','CARTERA','','" & SP_Rut_Activo & "','" & .ListItems(i).SubItems(3) & "'),"
                        Next
                    End With
                    Sql = Mid(Sql, 1, Len(Sql) - 1)
                    cn.Execute Sql
               
                
               
            End If
            'Grabaremos la(s) formas de pago en que se pago o pagaron los documentos
            '26 Marzo 2012
            Sql = "INSERT INTO abo_tipos_de_pagos (abo_id,mpa_id,pad_valor,caj_id) VALUES"
            
            For i = 1 To venPOS.LVFpago.ListItems.Count
                If CDbl(venPOS.LVFpago.ListItems(i).SubItems(1)) > 0 Then
                    Sql = Sql & "(" & Lp_IdAbo & "," & venPOS.LVFpago.ListItems(i) & "," & CDbl(venPOS.LVFpago.ListItems(i).SubItems(1)) & "," & LG_id_Caja & "),"
                End If
            Next
            Sql = Mid(Sql, 1, Len(Sql) - 1)
            cn.Execute Sql
        End If
        
     Else
     
        If Sm_ImprimeTicketNv = "SI" Then
            ProcImprimeTicket
            
            If SP_Rut_Activo = "78.967.170-2" Then
                ProcImprimeTicket
            End If
            
        End If
      
        MsgBox "Nota de venta Nro:" & vbNewLine & Lp_Id_Unico_Venta, vbInformation + vbOKOnly
    End If
    
    
    
    
    
final:
    If Sm_VentaRapida = "SI" Then
        If SG_Pago_Correcto = "SI" Then
            '22 Agosto 2015
            'inventarios
            Me.ActualizaStock
        
        
            If Mid(CboDocInidicio.Text, 1, 13) = "BOLETA FISCAL" Then
                
                If SG_ImpresoraFiscalBixolon = "SI" Then
                
                        'EPSON EPSON EPSON
                        
                        
                      
                End If
            End If
        
        End If
    End If
    
CancelaImpesionNV:
    CmdF5.Enabled = True
    LimpiaTodo
    
End Sub
Private Sub EmiteBoletaEpson()
        Bm_BoletaEnProceso = True
        CmdF5.Enabled = False
        Frame1.Enabled = False
        Me.Enabled = False
        With vtaBoletaFiscalEpson.LVDetalle
            .ListItems.Clear
            For d = 1 To Me.LvVenta.ListItems.Count
                .ListItems.Add , , LvVenta.ListItems(d).SubItems(3)
                .ListItems(d).SubItems(1) = LvVenta.ListItems(d).SubItems(2)
                .ListItems(d).SubItems(2) = CDbl(LvVenta.ListItems(d).SubItems(4))
                .ListItems(d).SubItems(3) = CDbl(LvVenta.ListItems(d).SubItems(1))


            Next
            vtaBoletaFiscalEpson.LvPagos.ListItems.Clear
            For i = 1 To venPOS.LVFpago.ListItems.Count
                 vtaBoletaFiscalEpson.LvPagos.ListItems.Add , , venPOS.LVFpago.ListItems(i).SubItems(2)
                 vtaBoletaFiscalEpson.LvPagos.ListItems(vtaBoletaFiscalEpson.LvPagos.ListItems.Count).SubItems(2) = CDbl(venPOS.LVFpago.ListItems(i).SubItems(1))
            Next

            vtaBoletaFiscalEpson.txtTotal = txtTotal
            vtaBoletaFiscalEpson.TxtEfectivo = CDbl(venPOS.TxtSumaPagos)
            vtaBoletaFiscalEpson.EmiteBoleta


        End With

        TxtMontoPago = CDbl(venPOS.TxtSumaPagos)

'        With Epson.LvDetalle
'                .ListItems.Clear
'                For d = 1 To Me.LvVenta.ListItems.Count
'                    .ListItems.Add , , LvVenta.ListItems(d).SubItems(3)
'                    .ListItems(d).SubItems(1) = LvVenta.ListItems(d).SubItems(2)
'                    .ListItems(d).SubItems(2) = CDbl(LvVenta.ListItems(d).SubItems(4))
'                    .ListItems(d).SubItems(3) = CDbl(LvVenta.ListItems(d).SubItems(1))
'
'
'                Next
'
'        End With
'        Epson.HaceteLaBoleta
    


        Bm_BoletaEnProceso = False
        CmdF5.Enabled = True
        Frame1.Enabled = True
        Me.Enabled = True
        
    
End Sub
Private Sub EmiteBoletaSamsung()
            With vtaBoletaFiscalSamsumg
                .LVDetalle.ListItems.Clear
                For d = 1 To Me.LvVenta.ListItems.Count
                    .LVDetalle.ListItems.Add , , Me.LvVenta.ListItems(d).SubItems(3)
                    .LVDetalle.ListItems(.LVDetalle.ListItems.Count).SubItems(1) = "COD:" & Me.LvVenta.ListItems(d).SubItems(1) & " - " & Me.LvVenta.ListItems(d).SubItems(2) & " - " & Me.LvVenta.ListItems(d).SubItems(10)
                    .LVDetalle.ListItems(.LVDetalle.ListItems.Count).SubItems(2) = Me.LvVenta.ListItems(d).SubItems(4)
                Next
                
                .LvDescuentos.ListItems.Clear
                If Val(Me.TxtValorDescuento) > 0 Then
                    .LvDescuentos.ListItems.Add , , ""
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(1) = TxtDescuentoX100 & " % DESCUENTO"
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(2) = CDbl(TxtValorDescuento)
                End If
                If Val(Me.TxtDsctoAjuste) > 0 Then
                    .LvDescuentos.ListItems.Add , , ""
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(1) = "DESC. ADIC."
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(2) = CDbl(Me.TxtDsctoAjuste)
                End If
                If Val(Me.TxtRecargoAjuste) > 0 Then
                    .LvDescuentos.ListItems.Add , , ""
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(1) = "AJUSTE SIMPLE"
                    .LvDescuentos.ListItems(.LvDescuentos.ListItems.Count).SubItems(2) = CDbl(Me.TxtRecargoAjuste)
                End If
                
                 .LvPagos.ListItems.Clear
                For i = 1 To venPOS.LVFpago.ListItems.Count
                     .LvPagos.ListItems.Add , , venPOS.LVFpago.ListItems(i).SubItems(2)
                     .LvPagos.ListItems(.LvPagos.ListItems.Count).SubItems(2) = CDbl(venPOS.LVFpago.ListItems(i).SubItems(1))
                Next
                
                .txtTotal = LG_Monto_Pagado_BF
                .EmiteBoleta
            End With
                        
End Sub
Private Sub LimpiaTodo()
    CboSucursal.Clear
    TxtRut = ""
    Me.TxtRazonSocial = ""
    TxtGiro = ""
    TxtDireccion = ""
    txtComuna = ""
    TxtCiudad = ""
    txtFono = ""
    TxtEmail = ""
    TxtValorDescuento = 0
    LvVenta.ListItems.Clear
    CalculaGrilla
    TxtSumaPagos = venPOS.TxtSumaPagos
    TxtSaldoPago = venPOS.TxtSaldoPago
    TxtOC = ""
    If SP_Rut_Activo = "76.169.962-8" Then
        'Solo para alcalde, seleccionaremos vendedor
    
        CboVendedor.ListIndex = CboVendedor.ListCount - 1
    End If
    
    
    On Error Resume Next
    TxtCodigo.SetFocus
End Sub



Private Function PermiteAjuste() As Boolean
    PermiteAjuste = True
    If CDbl(TxtSubTotal) > CDbl(txtTotal) Then
        'Valor con descue
            If (1 - (CDbl(txtTotal) / CDbl(TxtSubTotal))) * 100 > Im_Descuento_Maximo Then
                
                PermiteAjuste = False
            End If
        
        
    
    ElseIf CDbl(TxtSubTotal) < CDbl(txtTotal) Then
            'Con recargo
             If (CDbl(TxtSubTotal) / CDbl(txtTotal)) > Im_Recargo_Maximo Then
                   
                    PermiteAjuste = False
            End If
    End If
    
    


End Function


Private Sub CmdF7_Click()
    If LvVenta.SelectedItem Is Nothing Then Exit Sub
    
    LvVenta.ListItems.Remove LvVenta.SelectedItem.Index
    
    CalculaGrilla
    TxtCodigo.SetFocus
End Sub

Private Sub CmdF8_Click()
            If Val(TxtCodigo.Tag) = 0 Then
                    SG_codigo2 = ""
                 
                    If SG_Es_la_Flor = "SI" Then
                        AgregarProductoFlor.Bm_Nuevo = True
                        AgregarProductoFlor.TxtCodigoInterno = TxtCodigo
                        AgregarProductoFlor.Show 1
                    Else
                        AgregarProducto.Bm_Nuevo = True
                        AgregarProducto.Show 1
                    End If
                    If Len(SG_codigo2) > 0 Then
                        TxtCodigo = SG_codigo2
                    End If
            Else
                    SG_codigo2 = ""
                    SG_codigo = TxtCodigo.Tag
                    AgregarProducto.Bm_Nuevo = False
                    If SG_Es_la_Flor = "SI" Then
                        AgregarProductoFlor.Show 1
                    Else
                        AgregarProducto.Show 1
                    End If
                    If Len(SG_codigo2) > 0 Then
                        TxtCodigo = SG_codigo2
                    End If
            End If
            TxtCodigo.SetFocus
End Sub

Private Sub CmdF9_Click()
    If MsgBox("Quitar todos los productos...", vbQuestion + vbOKCancel) = vbOK Then
        LvVenta.ListItems.Clear
        TxtValorDescuento = 0
        CalculaGrilla
        TxtCodigo.SetFocus
    End If
    
End Sub

'Private Sub CmdOk_Click()
'    If Len(TxtPLU) = 0 Then Exit Sub
'    If CDbl(TxtCant) = 0 Then Exit Sub
'
'    Dim Dp_Total As Double
'    Dim Ip_C As Integer
'
'    Dim Dp_Unitario As Double
'    Sql = ""
'    Filtro = "codigo = '" & TxtPLU & "' "
'    If Sm_UtilizaCodigoInterno = "SI" Then
'                Sp_FiltroCI = "pro_codigo_interno='" & TxtPLU & "' "
'                Sql = "SELECT id,pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
'                        "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
'                         "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
'                        "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
'                        "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
'                        "(SELECT sto_stock FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo) stock,ubicacion_bodega " & _
'                    "FROM maestro_productos m " & _
'                    "WHERE pro_activo='SI' AND m.rut_emp='" & SP_Rut_Activo & "' AND " & Sp_FiltroCI & " UNION "
'    End If
'    Sql = Sql & "SELECT id, pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
'            "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
'             "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
'            "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(TxtListaPrecio.Tag) & " AND m.id=d.id),precio_venta)) precio_venta," & _
'            "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
'            "(SELECT sto_stock FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo) stock,ubicacion_bodega " & _
'          "FROM maestro_productos m " & _
'          "WHERE pro_activo='SI' AND  m.rut_emp='" & SP_Rut_Activo & "' AND " & Filtro & " LIMIT 1"
'    Consulta RsTmp, Sql
'    If RsTmp.RecordCount Then
'
'        'If Sm_PermiteDscto = "NO" And Sm_VentaRapida = "SI" Then
'        'probar
'        If Sm_VentaRapida = "SI" Then
'
'                Dp_Descuento = 0
'                Dp_Unitario = RsTmp!precio_venta - Dp_Descuento
'
'                Dp_Total = Dp_Unitario * Val(CxP(TxtCant))
'                LvDetalle.ListItems.Add , , LvDetalle.ListItems.Count + 1
'                Ip_C = LvDetalle.ListItems.Count
'                LvDetalle.ListItems(Ip_C).SubItems(1) = RsTmp!Id
'                LvDetalle.ListItems(Ip_C).SubItems(2) = TxtCant
'                LvDetalle.ListItems(Ip_C).SubItems(3) = RsTmp!Descripcion
'                LvDetalle.ListItems(Ip_C).SubItems(4) = NumFormat(RsTmp!precio_venta) 'unitario
'                LvDetalle.ListItems(Ip_C).SubItems(5) = NumFormat(Dp_Total) ' cant x unitario
'                LvDetalle.ListItems(Ip_C).SubItems(6) = RsTmp!pro_inventariable
'                LvDetalle.ListItems(Ip_C).SubItems(7) = RsTmp!precio_compra
'                LvDetalle.ListItems(Ip_C).SubItems(8) = RsTmp!precio_venta
'                LvDetalle.ListItems(Ip_C).SubItems(9) = Dp_Descuento
'
'                TxtStock = Val("" & RsTmp!stock)
'                TxtUbicacion = "" & RsTmp!ubicacion_bodega
'                TxtDescripcion = RsTmp!Descripcion
'                TxtCant = "1,000"
'                CboDescuento.ListIndex = 0
'                TxtPLU = ""
'
'                If LvDetalle.ListItems.Count > 0 Then
'                    LvDetalle.ListItems(LvDetalle.ListItems.Count).Selected = True
'                End If
'
'
'
'                ElTotal
'                TxtPLU.SetFocus
'        Else
'
'                '
'                TxtDescripcion = RsTmp!Descripcion
'                TxtPrecioOriginal = RsTmp!precio_venta
'                TxtStock = Val("0" & RsTmp!stock)
'                TxtUbicacion = RsTmp!ubicacion_bodega
'                On Error Resume Next
'                CboDescuento.SetFocus
'
'
'        End If
'
'    End If
'
'
'
'End Sub
Private Sub ElTotal()
    'txtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
End Sub


Private Sub CmdVisorCotizaciones_Click()
    SG_codigo2 = ""
    ven_visor_cotizaciones.Show 1
    If Val(SG_codigo) > 0 Then
        CargaCotizacion Val(SG_codigo)
        If Val(SG_codigo2) > 0 Then
            'Imprimir cotizzcion
           ' MsgBox "Imprimir cotizacion Nro " & SG_codigo2
            CargaCotizacion Val(SG_codigo)
            PrevisualizaCotizacion
            On Error GoTo CancelaImpesionNV
            If SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "76.169.962-8" Then
            'kyr y alcalde
            
                    If SG_codigo = "print" Then
                            
                            Dialogo.CancelError = True
                            Dialogo.ShowPrinter
                            La_Establecer_Impresora Printer.DeviceName
                            
                            
                          '  EstableImpresora Printer.DeviceName
                            ImprimeCOTIZACION
                    End If
            ElseIf SP_Rut_Activo = "78.967.170-2" Then
                'Plasticos aldunate
                If SG_codigo = "print" Then
                    Dialogo.CancelError = True
                    Dialogo.ShowPrinter
                    
                    If Printer.DeviceName = "TERMICA" Then
                        ProcCOTIZACION
                    Else
                        La_Establecer_Impresora Printer.DeviceName
                        ImprimeCOTIZACION
                    End If
                End If
    
            
            
            Else
            
                'Termica
                If SG_codigo = "print" Then ProcCOTIZACION
            End If
            
            GoTo CancelaImpesionNV
            
        End If
    End If
    Exit Sub
    
CancelaImpesionNV:
    CmdF5.Enabled = True
    LimpiaTodo
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then
        If CmdF5.Enabled Then CmdF5_Click
    End If
    
    If KeyCode = vbKeyF8 Then
        CmdF8_Click
    End If
    
    If KeyCode = vbKeyF7 Then
        CmdF7_Click
    End If
    If KeyCode = vbKeyF9 Then
        CmdF9_Click
    End If
End Sub


Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me, False
    
    
    If SP_Rut_Activo = "76.370.578-1" Then
        'talentos del sur, bodega, aeropuerto
            Sql = "SELECT bod_id " & _
                    "FROM par_bodegas " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND sue_id=" & IG_id_Sucursal_Empresa
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                IG_id_Bodega_Ventas = RsTmp!bod_id
            End If
    End If
    
    If SG_Equipo_Solo_Nota_de_Venta = "SI" Then
        TxtPOS = "POS"
        LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_nota_de_venta='SI'"
        If CboDocVenta.ListCount = 0 Then
            CboDocVenta.AddItem "X"
            CboDocVenta.ListIndex = 0
        End If
        
        CboDocVenta.ListIndex = 0
        LLenarCombo CboDocInidicio, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA'   AND doc_utiliza_en_pos='SI'", "doc_orden"
        If SP_Rut_Activo = "76.370.578-1" Then
            LLenarCombo CboDocInidicio, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA'   AND doc_utiliza_en_pos='SI' AND doc_id NOT IN(72) ", "doc_orden"
        End If
        
        CboDocInidicio.ListIndex = 0
    Else
        TxtPOS = "CAJA"
        LLenarCombo CboDocInidicio, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_utiliza_en_pos='SI'", "doc_orden"
        CboDocInidicio.ListIndex = 0
    End If
    
    LLenarCombo CboVendedor, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'"
    CboVendedor.ListIndex = 0
    
    If SP_Rut_Activo = "76.169.962-8" Then
        'Solo para alcalde, seleccionaremos vendedor
        CboVendedor.AddItem "SELECCIONE VENDEDOR..."
        CboVendedor.ListIndex = CboVendedor.ListCount - 1
    End If
    
    Sql = "SELECT pde_permite,pde_descuento_maximo,pde_recargo_maximo " & _
                "FROM par_perfiles_atributo_descuento " & _
                "WHERE per_id=" & LogPerfil & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    
    Sm_PermiteDscto = "NO"
    If RsTmp.RecordCount > 0 Then
        If RsTmp!pde_permite = "SI" Then
                Im_Descuento_Maximo = RsTmp!pde_descuento_maximo
                Im_Recargo_Maximo = RsTmp!pde_recargo_maximo
        
        
              '  For i = 0 To RsTmp!pde_descuento_maximo
              '      CboDescuento.AddItem i
              '      CboDsctoTotal.AddItem i
              '  Next
                Sm_PermiteDscto = "SI"
        Else
            CboDescuento.Enabled = False
            CboDescuento.AddItem "0"
        End If
    Else
'        CboDescuento.Enabled = False
'        CboDescuento.AddItem "0"
    End If
    If Sm_PermiteDscto = "NO" Then
        Me.TxtDescuentoX100.Locked = True
        Me.TxtDsctoAjuste.Locked = True
        'Me.TxtDsctoTotal.Locked = True
        Me.TxtRecargoAjuste.Locked = True
    End If
    
   ' CboDescuento.ListIndex = 0
    Sm_PrecioVtaModificable = "NO"
    Sql = "SELECT  direccion,fono,email,   emp_precio_venta_modificable pv,emp_redondear_descuento,emp_cuenta_ventas,emp_utiliza_codigos_internos_productos,emp_de_repuestos " & _
            "FROM sis_empresas " & _
            "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sm_PrecioVtaModificable = RsTmp!PV
        Im_Redondear = RsTmp!emp_redondear_descuento
        Lm_CuentaVentas = RsTmp!emp_cuenta_ventas
        Sm_UtilizaCodigoInterno = RsTmp!emp_utiliza_codigos_internos_productos
        TxtEmpDireccion = RsTmp!direccion
        TxtEmpFono = RsTmp!fono
        TxtEmpMail = RsTmp!email
        Sp_EmpresaRepuestos = RsTmp!emp_de_repuestos
        
    End If
    
'    If Sm_PrecioVtaModificable = "NO" Then
'        TxtPrecioFinal.BackColor = ClrDesha
'        TxtPrecioFinal.Locked = True
'    End If
    
    Sm_VentaRapida = "NO"
    Sm_Codigo_Barra_Pesable = "NO"
    Sql = "SELECT ema_venta_rapida,ema_codigos_barra_pesables,ema_imprime_ticket_nota_venta imp_ticket " & _
          "FROM sis_empresa_activa " & _
          "WHERE UPPER(ema_nombre_equipo)='" & UCase(SP_Nombre_Equipo) & "'"
    Consulta RsTmp2, Sql

    If RsTmp2.RecordCount > 0 Then
        Sm_VentaRapida = RsTmp2!ema_venta_rapida
        Sm_Codigo_Barra_Pesable = RsTmp2!ema_codigos_barra_pesables
        Sm_ImprimeTicketNv = RsTmp2!imp_ticket
    End If
    If Sm_VentaRapida = "SI" Then
   '     TxtDescripcion.Visible = False
'        TxtPrecioOriginal.Visible = False
'        CboDescuento.Visible = False
'        TxtPrecioFinal.Visible = False
'        CmdAgrega.Visible = False
        TxtPrecio.Locked = True
        TxtCantidad.Locked = True
        TxtCantidad.TabStop = False
        TxtPrecio.TabStop = False
    End If
    
    LvVenta.ColumnHeaders(2).Width = TxtCodigo.Width + 10
    LvVenta.ColumnHeaders(3).Width = TxtDescripcion.Width + 20
    LvVenta.ColumnHeaders(4).Width = TxtCantidad.Width + 10
    LvVenta.ColumnHeaders(5).Width = TxtPrecio.Width + 20
    LvVenta.ColumnHeaders(6).Width = TxtTotalLinea.Width + 10
    LvVenta.ColumnHeaders(7).Width = TxtStockLinea.Width + 10
    
    
    If SG_ImpresoraFiscalBixolon = "SI" Then
                '4-2 2016
                'Cargar Formulario de Impresora Fiscal Epson, pero no mostrar el formulario
                'Pero tenerlo cargado para emitir boletas cuando sea necesario
                If SG_Marca_Impresora_Fiscal = "   xEPSON" Then
                    
                         With Epson.CmbCom
                            .AddItem "Com1"
                    '        .AddItem "Com2"
                    '        .AddItem "Com3"
                    '        .AddItem "Com4"
                    '        .AddItem "Com5"
                    '        .AddItem "Com6"
                    '        .AddItem "Com7"
                    '        .AddItem "Com8"
                    '        .AddItem "Com9"
                    '        .AddItem "Com10"
                    '        .AddItem "Com11"
                    '        .AddItem "Com12"
                    '        .AddItem "Com13"
                    '        .AddItem "Com14"
                    '        .AddItem "Com15"
                    '        .AddItem "Com16"
                        End With
                        
                        '*** DEFINE VELOCIDADES PARA PUERTO ***
                        With Epson.CmbBaudRate
                            
                            .AddItem "1200"
                            .AddItem "2400"
                            .AddItem "4800"
                            .AddItem "9600"
                            .AddItem "19200"
                            .AddItem "38400"
                        End With
                        Epson.CmbCom.ListIndex = 0
                        Epson.CmbBaudRate.ListIndex = 3
                        Epson.AbrePuerto
                       ' MsgBox "Puerto Abierto"
                End If
    End If
    
    If SP_Rut_Activo = "78.967.170-2" Then
        'solo comercial aldunate
        Sp_MatrizDescuentos = "SI"
    End If
    
    
    
End Sub

Private Sub LvVenta_DblClick()
    If LvVenta.SelectedItem Is Nothing Then Exit Sub
    With LvVenta
        TxtCodigo.Tag = LvVenta.SelectedItem
        TxtCodigo = .SelectedItem.SubItems(1)
        Me.TxtDescripcion = .SelectedItem.SubItems(2)
        TxtCantidad = .SelectedItem.SubItems(3)
        TxtPrecio = .SelectedItem.SubItems(4)
        TxtTotalLinea = .SelectedItem.SubItems(5)
        TxtStock = .SelectedItem.SubItems(6)
        TxtCodigo.SetFocus
        LvVenta.ListItems.Remove .SelectedItem.Index
    End With
    CalculaGrilla
    NormalizaColores
    TxtCodigo.SetFocus
     
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    TxtCodigo.SetFocus
    Timer1.Enabled = False
End Sub

Private Sub txtCantidad_GotFocus()
    En_Foco TxtCantidad
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub txtCantidad_Validate(Cancel As Boolean)
 '   Dim Dp_Cantidad As decim
    SkUm.Tag = ""
    If Val(CxP(TxtCantidad)) > 0 And Val(TxtCodigo) > 0 Then
        If Sp_MatrizDescuentos = "SI" Then
            'solo plastcos aldunate
            
            Sql = "SELECT   mpr_precio_unitario " & _
                    "FROM  par_productos_matriz_descuento " & _
                    "WHERE pro_codigo=" & Val(TxtCodigo.Tag) & " AND " & CxP(TxtCantidad) & " BETWEEN mpr_desde AND mpr_hasta"
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                TxtCantidad.Tag = "SI"
                TxtPrecio.Tag = RsTmp!mpr_precio_unitario
                TxtPrecio = RsTmp!mpr_precio_unitario
            Else
                'No lleva descuento de la matriz.
                TxtCantidad.Tag = "NO"
                If Val(TxtDescripcion.Tag) > 0 Then
                    TxtPrecio.Tag = Val(TxtDescripcion.Tag)
                    TxtPrecio = Val(TxtDescripcion.Tag)
                End If
                
                'Verificaremos si lleva descuento por tipo
                
                Sql = "SELECT  dti_descuento " & _
                        "FROM par_descuento_por_tipo d " & _
                        "WHERE tip_id=(SELECT tip_id " & _
                                        "FROM maestro_productos " & _
                                        "WHERE codigo=" & Val(TxtCodigo.Tag) & " " & _
                                        "LIMIT 1) AND " & CxP(TxtCantidad) & ">=dti_desde"
                
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    'Aplicar descuento por tipo
                    TxtPrecio = Val(CxP(CDbl(TxtPrecio.Tag))) - Round((CDbl(TxtPrecio.Tag) / 100 * RsTmp!dti_descuento), 0)
                    SkUm.Tag = TxtPrecio
                
                
                End If
                
                
                
            End If
            
        End If
    
    
        CalculaCantPrecio
    Else
       TxtCantidad = "0"
    End If
End Sub
Private Sub CalculaCantPrecio()
    If Val(CxP(TxtCantidad)) > 0 Then
        TxtCantidad = Format(TxtCantidad, "#0.00")
        'Dp_Cantidad = TxtCantidad
        If SP_Rut_Activo = "76.337.408-4" Then
            TxtTotalLinea = Format(TxtCantidad * Val(TxtPrecio), "#.#0")
        Else
        
            TxtTotalLinea = NumFormat(TxtCantidad * CDbl(TxtPrecio))
        End If
       ' If SP_Rut_Activo = "78.967.170-2" Then
       '     TxtTotalLinea = NumFormat(TxtCantidad * Val(CxP(TxtPrecio)))
       ' End If
    End If
End Sub

Private Sub TxtCodigo_GotFocus()
    En_Foco TxtCodigo
End Sub


Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    If SG_Codigos_Alfanumericos = "SI" Then
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Else
        KeyAscii = SoloNumeros(KeyAscii)
    End If
    If KeyAscii = 39 Then KeyAscii = 0
    
        
End Sub


Private Sub TxtCodigo_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then
                If Sp_EmpresaRepuestos = "SI" Then
                    SG_codigo = ""
                    SG_codigo2 = ""
                    Busca_Producto_Repuestos.Show 1
                    If Sm_UtilizaCodigoInterno = "SI" Then
                        TxtCodigo = SG_codigo2
                    Else
                        TxtCodigo = SG_codigo
                    End If
                    TxtCodigo_Validate True
                    TxtCantidad.SetFocus
                Else
                    BuscaProducto.Show 1
                    TxtCodigo = SG_codigo
                    TxtCodigo_Validate True
                End If
               
    Else
        If KeyCode = 107 Then
            TxtCantidad = Val(TxtCantidad) + 1
            
        ElseIf KeyCode = 109 Then
            TxtCantidad = Val(TxtCantidad) - 1
            If Val(TxtCantidad) = 0 Then TxtCantidad = 1
        End If
    End If
End Sub


Private Sub TxtCodigo_Validate(Cancel As Boolean)
    Dim Dp_Cantidad_Pesable As Double
    Dim Sp_PVenta As String
    
    If Len(TxtCodigo) = 0 Then Exit Sub
   
    Dim Dp_Total As Double
    Dim Ip_C As Integer
   
    Dim Dp_Unitario As Double
    
    TxtStockLinea.BackColor = &HE0E0E0
    TxtStockLinea.ForeColor = vbBlack
    
    Sql = ""
    Filtro = "codigo = '" & TxtCodigo & "' "
    
     Sp_PVenta = "IF((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')=0 " & _
                         "OR ISNULL((SELECT lst_id FROM maestro_clientes WHERE rut_cliente='" & TxtRut & "')) ,precio_venta," & _
                        "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE lst_id=" & Val(Principal.SkSucursal.Tag) & " AND m.id=d.id),precio_venta)) precio_venta,"

    
    If Sm_UtilizaCodigoInterno = "SI" Then
                Sp_FiltroCI = "pro_codigo_interno='" & TxtCodigo & "' "
                
                If Sm_Codigo_Barra_Pesable = "SI" Then
                    If Mid(TxtCodigo, 1, 2) = 26 Then 'UNITARIOS
                        Sp_FiltroCI = "codigo=" & Val(Mid(TxtCodigo, 3, 5)) & " "
                    ElseIf Mid(TxtCodigo, 1, 2) = 25 Then 'PESABLES
                        Sp_FiltroCI = "codigo=" & Val(Mid(TxtCodigo, 3, 5)) & " "
                        'Dp_Cantidad_Pesable = Mid(TxtCodigo, 8, 2) & "." & Mid(TxtCodigo, 10, 3)'
                         TxtCantidad = Mid(TxtCodigo, 8, 2) & "." & Mid(TxtCodigo, 10, 3)
                    End If
                End If
                
              
                If SP_Rut_Activo = "76.370.578-1" Then
                    'Este codigo es especial para talentos aeropuerto, para tomar otra lista de precios
                    If Val(Principal.SkSucursal.Tag) > 0 Then
                        Sp_PVenta = "IFNULL((SELECT lsd_precio FROM par_lista_precios_detalle d WHERE m.id=d.id AND lst_id=" & Val(Principal.SkSucursal.Tag) & "),precio_venta) precio_venta,"
                    End If
                End If
                Sql = "SELECT ume_id,id,pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
                            Sp_PVenta & _
                        " IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
                        "IFNULL((SELECT SUM(sto_stock) FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo),0) stock,ubicacion_bodega,codigo " & _
                         "FROM maestro_productos m " & _
                        "WHERE pro_activo='SI' AND m.rut_emp='" & SP_Rut_Activo & "' AND " & Sp_FiltroCI & " UNION "
    
                
    End If
    Sql = Sql & "SELECT ume_id,id, pro_inventariable,descripcion,marca,ubicacion_bodega, " & _
            Sp_PVenta & _
            "IFNULL((SELECT AVG(pro_ultimo_precio_compra) FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=m.codigo),0) precio_compra, " & _
            "IFNULL((SELECT SUM(sto_stock) FROM pro_stock WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=m.codigo),0) stock,ubicacion_bodega,codigo " & _
          "FROM maestro_productos m " & _
          "WHERE pro_activo='SI' AND  m.rut_emp='" & SP_Rut_Activo & "' AND " & Filtro & " LIMIT 1"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
                Me.FrmPago.Visible = False
        'If Sm_PermiteDscto = "NO" And Sm_VentaRapida = "SI" Then
        'probar
 
                
                '
                Me.SkUm = "" & RsTmp!ume_id
                TxtCodigo.Tag = RsTmp!Codigo
                TxtDescripcion = RsTmp!Descripcion
                If SP_Rut_Activo = "76.337.408-4" Then
                        
                        TxtPrecio.Tag = RsTmp!precio_venta
                        TxtPrecio = CxP(RsTmp!precio_venta)
                        TxtDescripcion.Tag = CxP(TxtPrecio.Tag)
                
                                
                Else
                
                        
                        TxtPrecio.Tag = RsTmp!precio_venta
                        TxtPrecio = NumFormat(RsTmp!precio_venta)
                        TxtDescripcion.Tag = TxtPrecio.Tag
                End If
                
                If SP_Rut_Activo = "76.337.408-4" Then
                    TxtTotalLinea = Format(RsTmp!precio_venta, "#,#0")
                Else
                    TxtTotalLinea = NumFormat(RsTmp!precio_venta)
                End If
                TxtStockLinea = CxP(RsTmp!stock)
                TxtStockLinea.Tag = CxP(RsTmp!precio_compra)
                
                If CDbl(TxtStockLinea) < 1 Then
                    TxtStockLinea.BackColor = vbRed
                    TxtStockLinea.ForeColor = vbGreen
                End If
                
                TxtUbicacion = RsTmp!ubicacion_bodega
                If Len(TxtUbicacion) > 0 Then SkUbicacion = "UBICACION: " & TxtUbicacion Else SkUbicacion = ""
                
                SkInventariable = RsTmp!pro_inventariable
                If Sm_VentaRapida = "SI" Then
                    CmdAceptaLinea_Click
                    Cancel = True
                End If
                
                
                '03/10/2016
                'Buscar ultimo ajuste de inventario, y mostrarlo en Label SkUltimoInventario.
                SkUltimoInventario = ""
                Sql = "SELECT tmi_fecha,tmi_cantidad " & _
                        "FROM inv_toma_inventario_detalle d " & _
                        "JOIN inv_toma_inventario i ON d.tmi_id=i.tmi_id " & _
                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo=" & TxtCodigo.Tag & " AND tdi_estado='OK' " & _
                        "ORDER BY tmi_fecha DESC " & _
                        "LIMIT 1"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    Me.SkUltimoInventario = RsTmp!tmi_fecha & " - Cant.:" & RsTmp!tmi_cantidad
                
                End If
                
                
        
    Else
        TxtCodigo.Tag = ""
        TxtDescripcion = ""
        TxtPrecio.Tag = ""
        TxtPrecio = ""
        TxtTotalLinea = ""
        TxtStockLinea = ""
        TxtStockLinea.Tag = ""
        TxtUbicacion = ""
        SkInventariable = ""
        Cancel = True
        
    End If
End Sub



Private Sub TxtDescripcion_GotFocus()
    En_Foco TxtDescripcion
End Sub


Private Sub txtDescripcion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtDescuentoX100_GotFocus()
    En_Foco TxtDescuentoX100
End Sub

Private Sub TxtDescuentoX100_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDescuentoX100_Validate(Cancel As Boolean)
    If Val(TxtDescuentoX100) = 0 Then
        TxtDescuentoX100 = "0"
        TxtValorDescuento = "0"
    End If
    If Val(TxtDescuentoX100) > Im_Descuento_Maximo Then
        MsgBox "Descuento sobrepasa el permitido..", vbInformation
        Cancel = True
        Exit Sub
    End If
        
    
    CalculaGrilla
End Sub



Private Sub TxtDsctoAjuste_GotFocus()
    En_Foco TxtDsctoAjuste
End Sub

Private Sub TxtDsctoAjuste_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDsctoAjuste_Validate(Cancel As Boolean)
    If Val(TxtDsctoAjuste) = 0 Then TxtDsctoAjuste = "0"
    
    
    If SP_Rut_Activo = "76.169.962-8" Then
        If Val(TxtDsctoAjuste) > 100 Then
            MsgBox "Limite para este campo $100..", vbExclamation
            Cancel = True
            Exit Sub
        End If
    
    End If
    
    CalculaGrilla
End Sub

Private Sub TxtPlazoEntrega_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub TxtPrecio_GotFocus()
    En_Foco TxtPrecio
End Sub


Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtPrecio_Validate(Cancel As Boolean)
    If Val(TxtPrecio) = 0 Then
        TxtPrecio = 0
    Else
    
    
        
        
        If Val(SkUm.Tag) = 0 Then
            If CDbl(TxtPrecio) < CDbl(TxtPrecio.Tag) Then
                MsgBox "Precio no valido..", vbInformation
                TxtPrecio = NumFormat(TxtPrecio.Tag)
                Cancel = True
            End If
        Else
            'esto es si hay descuento por tipo de producto, plasticos aldunate
            If CDbl(TxtPrecio) < CDbl(SkUm.Tag) Then
                MsgBox "Precio no valido..", vbInformation
                TxtPrecio = NumFormat(TxtPrecio.Tag)
                Cancel = True
            End If
        
        End If
        
        
        
        If Val(CxP(TxtPrecio)) > Int(TxtPrecio) Then
          '  TxtPrecio = CxP(TxtPrecio)
        Else
            If SP_Rut_Activo = "76.337.408-4" Then
                '
            Else
                TxtPrecio = NumFormat(TxtPrecio)
            End If
           
        End If
        CalculaCantPrecio
    End If
End Sub



Private Sub TxtRecargoAjuste_GotFocus()
        En_Foco TxtRecargoAjuste
End Sub

Private Sub TxtRecargoAjuste_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRecargoAjuste_Validate(Cancel As Boolean)
    If Val(TxtRecargoAjuste) = 0 Then TxtRecargoAjuste = "0"
    CalculaGrilla
End Sub

Private Sub TxtRut_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Exit Sub
    End If
    If ClienteEncontrado Then
    Else       ' TxtRut.SetFocus
    End If
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    TxtListaPrecio = "NORMAL"
    TxtListaPrecio.Tag = 0
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento...", vbInformation
        On Error Resume Next
        CboDocVenta.SetFocus
        Exit Sub
    End If
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
   
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,giro,direccion,comuna,fono,email, " & _
                "IFNULL(lst_nombre,'LISTA PRECIO GENERAL') listaprecios,m.lst_id,ven_id,cli_monto_credito " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l USING(lst_id) " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtMontoCredito = !cli_monto_credito
                TxtListaPrecio = !listaprecios
                TxtListaPrecio.Tag = !lst_id
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
              
                
                Sql = "SELECT a.lst_id,lst_nombre " & _
                      "FROM par_asociacion_lista_precios  a " & _
                      "INNER JOIN par_lista_precios l USING(lst_id) " & _
                      "WHERE l.rut_emp='" & SP_Rut_Activo & "' AND cli_rut='" & TxtRut & "'"
                Consulta RsTmp2, Sql
                TxtListaPrecio.Tag = !lst_id
                TxtListaPrecio = "LISTA DE PRECIOS PRINCIPAL"
                If RsTmp2.RecordCount > 0 Then
                    TxtListaPrecio = RsTmp2!lst_nombre
                    TxtListaPrecio.Tag = RsTmp2!lst_id
                End If
                
                If Val(TxtMontoCredito) > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                    TxtCupoUtilizado = ConsultaSaldoCliente(TxtRut)
                    SkCupoCredito = NumFormat(Val(TxtMontoCredito) - Val(TxtCupoUtilizado))
                End If
                CboSucursal.Clear
                LLenarCombo CboSucursal, "CONCAT(suc_ciudad,' ',suc_direccion,' ',suc_contacto)", "suc_id", "par_sucursales", "suc_activo='SI' AND rut_cliente='" & TxtRut & "'", "suc_id"
                CboSucursal.AddItem "CASA MATRIZ"
                CboSucursal.ItemData(CboSucursal.ListCount - 1) = 0
                CboSucursal.ListIndex = CboSucursal.ListCount - 1
                
                
            End With
                
            If bm_SoloVistaDoc Then Exit Sub
                                
        
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        TxtRut_Validate False
                       
                        TxtCodigo.SetFocus
                    Else
                        TxtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
          
        End If
       
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub



Private Sub TxtTemp_GotFocus()
    En_Foco TxtTemp
End Sub

Private Sub TxtTemp_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 13 Then
        If Val(TxtTemp) <= CDbl(LVDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(8)) Then
            MsgBox "No puede bajar el valor...", vbInformation
            TxtTemp = LVDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(4)
        Else
            LVDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(4) = NumFormat(TxtTemp)
            LVDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(5) = CDbl(LVDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(4)) * Val(LVDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(2))
            LVDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(12) = LVDetalle.ListItems(Val(TxtTemp.Tag)).SubItems(4)
            ElTotal
        End If
        TxtTemp.Visible = False
        
    End If
    If KeyAscii = 27 Then
        TxtTemp.Visible = False
    End If
    
End Sub

Private Sub TxtTemp_LostFocus()
    TxtTemp.Visible = False
End Sub

Public Sub ActualizaStock()
        Dim Dp_Promedio As Double
        With Me.LvVenta
            If .ListItems.Count > 0 Then
                For i = 1 To .ListItems.Count
                   
                    
                        If .ListItems(i).SubItems(7) = "SI" Then 'inventariable
                          
                              Sql = "UPDATE maestro_productos " & _
                                    "SET stock_Actual = stock_actual -" & .ListItems(i).SubItems(6) & _
                                    " WHERE  rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & .ListItems(i) & "'"
                              cn.Execute Sql
                              
                         
                              Dp_Promedio = 0
                             
                              'Dp_Promedio = CostoAVG(.ListItems(i).SubItems(1), IG_id_Bodega_Ventas)
                              
                               Sql = "SELECT pro_precio_neto promedio " & _
                                        "FROM inv_kardex k " & _
                                        "WHERE pro_codigo='" & .ListItems(i) & "' AND rut_emp='" & SP_Rut_Activo & "' AND bod_id=" & IG_id_Bodega_Ventas & " " & _
                                        "ORDER BY kar_id DESC " & _
                                         "LIMIT 1 "
                              Consulta RsTmp, Sql
                              If RsTmp.RecordCount > 0 Then Dp_Promedio = RsTmp!promedio
                              
                              
                              
                           
                              KardexVenta Format(Date, "YYYY-MM-DD"), "SALIDA", _
                               CboDocInidicio.ItemData(CboDocInidicio.ListIndex), Val(Lp_Id_Nueva_Venta), IG_id_Bodega_Ventas, .ListItems(i), _
                               Val(.ListItems(i).SubItems(3)), "VENTA " & CboDocVenta.Text & " " & Lp_Id_Nueva_Venta, _
                              Dp_Promedio, Dp_Promedio * Val(.ListItems(i).SubItems(3)), Me.TxtRut.Text, Me.TxtRazonSocial, , , CboDocInidicio.ItemData(CboDocInidicio.ListIndex), , , , Lp_Id_Unico_Venta
                              '    Kardex Format(.ListItems(i).SubItems(14), "YYYY-MM-DD"), "SALIDA", _
                              CboDocVenta.ItemData(CboDocVenta.ListIndex), TxtNroDocumento, CboBodega.ItemData(CboBodega.ListIndex), .ListItems(i).SubItems(1), _
                              Val(.ListItems(i).SubItems(6)), "VENTA " & CboDocVenta.Text & " " & TxtNroDocumento, _
                              0, 0, Me.TxtRut.Text, Me.TxtRazonSocial, , , CboDocVenta.ItemData(CboDocVenta.ListIndex)
                        End If
                    
                Next
            End If
        End With
        
End Sub
Private Sub ProcImprimeTicket()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 6
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 9
    Dim p_Total As String * 9
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
    
    If Printer.DeviceName <> "TERMICA" Then
            '28 agosto 2015 _
        se imprime ticket para ser leido en caja
        For Each pr In Printers
            If pr.DeviceName = "TERMICA" Then
                Establecer_Impresora pr.DeviceName
                Set Printer = pr 'Cambiamos la impresora por defecto
                Exit For        ' a la tengamos configurada en los parametros
            End If               'para las FACTURAs
        Next
    End If
    On Error GoTo ProblemaImpresora
    Printer.FontName = "Arial"
    Printer.FontSize = 12
    Printer.FontBold = True
    Printer.FontItalic = False
    Printer.ScaleMode = 7
        
        Cx = 0.1 'horizontal
        Cy = 0.1 'vertical
    
    
    Dp_S = 0.1
   
    

    Printer.CurrentX = Cx
    
    pos = Printer.CurrentY
    Printer.Print "NOTA DE VENTA " & Lp_Id_Unico_Venta
    'Printer.CurrentY = POS + 0.3
    'Printer.CurrentX = Cx
    'POS = Printer.CurrentY
    'Printer.Print "NRO " & Lp_Id_Nueva_Venta
    '
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "-------------------------------------------"
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.FontBold = False
    Printer.ScaleMode = 7
    
    Printer.FontBold = True
    
    Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "DOC: " & Me.CboDocInidicio.Text
   
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "VEN:" & CboVendedor.Text
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "FECHA " & Date & " HORA:" & Time
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "-----[ DATOS CLIENTE ]-----"
    
    
     Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "CLIENTE: " & TxtRazonSocial
   
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT    : " & TxtRut
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "---------------------------------"
    
   ' GoTo fin_impresion
   Printer.FontName = "Courier New"
    Printer.CurrentY = pos + 0.5
    pos = Printer.CurrentY
    For i = 1 To Me.LvVenta.ListItems.Count
        Printer.CurrentY = pos + 0.1
        p_Codigo = Me.LvVenta.ListItems(i).SubItems(1)
        LSet p_Cantidad = Me.LvVenta.ListItems(i).SubItems(3)

        p_Detalle = Me.LvVenta.ListItems(i).SubItems(2)
        LSet p_Unitario = Me.LvVenta.ListItems(i).SubItems(4)
        LSet p_Total = Me.LvVenta.ListItems(i).SubItems(5)
        Printer.CurrentX = Cx
        Printer.Print "COD.:" & p_Codigo & "    CANT.:" & p_Cantidad
        Printer.CurrentX = Cx
        Printer.Print "P.U.:" & p_Unitario & " TOTAL:" & p_Total
        Printer.CurrentX = Cx
        Printer.Print p_Detalle
        pos = Printer.CurrentY
    Next
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "------------------------------------"
    
    Printer.FontBold = True
    Printer.FontSize = 14
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "TOTAL:" & txtTotal
    
    Printer.FontBold = False
    Printer.FontSize = 10
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "-----------------------"
    
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    Printer.FontName = "C39HrP48DhTt" ' codigo de barras
    Printer.FontSize = 40
    SP_Ean = Right("0000000000000" & Lp_Id_Unico_Venta, 15)
    TxtBarCode = Lp_Id_Unico_Venta ' SP_Ean
    Printer.Print "    "; "*" & TxtBarCode & "*"
    

     
fin_impresion:
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub



Private Sub ProcCOTIZACION()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_GuiasFacturadas As String
    
    Dim p_Codigo As String * 6
    Dim p_Cantidad As String * 7
    Dim p_UM As String * 4
    Dim p_Detalle As String * 40
    Dim p_Unitario As String * 9
    Dim p_Total As String * 9
    Dim p_Mes As String * 10
    Dim p_CiudadF As String * 20
    
        '28 agosto 2015 _
    se imprime ticket para ser leido en caja
    
    
    If Printer.DeviceName <> "TERMICA" Then
        For Each pr In Printers
            If pr.DeviceName = "TERMICA" Then
                Establecer_Impresora pr.DeviceName
                Set Printer = pr 'Cambiamos la impresora por defecto
                Exit For        ' a la tengamos configurada en los parametros
            End If               'para las FACTURAs
        Next
    End If
    On Error GoTo ProblemaImpresora
        Cx = 0.1 'horizontal
        Cy = 0.1 'vertical
    
    Printer.FontName = "Arial"
    Printer.FontSize = 12
    Printer.FontBold = True
    Printer.FontItalic = False
    Printer.ScaleMode = 7
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    
    Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    pos = Printer.CurrentY + 2.5
        
    Dp_S = 0.1
   
    

    Printer.CurrentX = Cx
    
    
    
    If SP_Rut_Activo = "76.005.337-6" Then
        
            'gempp
            
            Printer.CurrentY = pos + 0.1
            pos = Printer.CurrentY
            Printer.Print "       RUT: 76.005.337-6"
            pos = Printer.CurrentY
            Printer.CurrentY = pos + 0.5
            
            
    End If
    
    
    
       ' Printer.CurrentY = POS + 0.3
    Printer.CurrentY = pos
    Printer.Print "     " & TxtEmpDireccion
    pos = Printer.CurrentY
    Printer.CurrentY = pos + 0.3
    
     Printer.CurrentY = pos + 0.1
    pos = Printer.CurrentY
    Printer.Print "          FONO:" & TxtEmpFono
    pos = Printer.CurrentY
    Printer.CurrentY = pos + 0.3

    


    Printer.CurrentY = pos + 0.3
    pos = Printer.CurrentY
    Printer.Print "email:" & TxtEmpMail
    'Printer.CurrentY = pos + 0.5

    Printer.CurrentY = pos + 1
        pos = Printer.CurrentY
    Printer.Print "           COTIZACION NRO " & Lp_Id_Unico_Venta
    Printer.CurrentY = pos + 0.3
    'Printer.CurrentX = Cx
    'POS = Printer.CurrentY
    'Printer.Print "NRO " & Lp_Id_Nueva_Venta
    '
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "----------------------------------------------------"
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.FontBold = False
    Printer.ScaleMode = 7
    
    
'    Printer.CurrentY = pos + 0.5
'    Printer.CurrentX = Cx
'    pos = Printer.CurrentY
'    Printer.Print "DOC: BOLETA FISCAL"
   
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "VEN:" & Me.CboVendedor.Text
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "FECHA " & Date & " HORA:" & Time
    
    Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "----[ DATOS CLIENTE ]-----"
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "CLIENTE:" & Me.TxtRazonSocial
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT    : " & Me.TxtRut
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "DIRECC.:" & Me.TxtDireccion
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "GIRO   : " & Me.TxtGiro
    
    
    Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    
    
    
    
    Printer.Print "----------------------------------------------------"
   ' GoTo fin_impresion
   Printer.FontName = "Courier New"
    Printer.CurrentY = pos + 0.6
    pos = Printer.CurrentY
    For i = 1 To Me.LvVenta.ListItems.Count
        Printer.CurrentY = pos + 0.3
        p_Codigo = Me.LvVenta.ListItems(i).SubItems(1)
        LSet p_Cantidad = Me.LvVenta.ListItems(i).SubItems(3)

        p_Detalle = Me.LvVenta.ListItems(i).SubItems(2)
        LSet p_Unitario = Me.LvVenta.ListItems(i).SubItems(4)
        LSet p_Total = Me.LvVenta.ListItems(i).SubItems(5)
        Printer.CurrentX = Cx
        Printer.Print "COD.:" & p_Codigo & "    CANT.:" & p_Cantidad
        Printer.CurrentX = Cx
        Printer.Print "P.U.:" & p_Unitario & " TOTAL:" & p_Total
        Printer.CurrentX = Cx
        Printer.Print p_Detalle
        pos = Printer.CurrentY
    Next
    Printer.CurrentY = pos + 0.5
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "------------------------------------"
    
    Printer.FontBold = True
    Printer.FontSize = 14
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "        TOTAL:" & txtTotal
    
    Printer.FontBold = False
    Printer.FontSize = 10
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "---------------------------------------------"
    
    Printer.CurrentY = pos + 0.3
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "Validez: 3 dias."
'    Printer.CurrentY = pos + 0.3
'    Printer.CurrentX = Cx
'    Printer.FontName = "C39HrP48DhTt" ' codigo de barras
'    Printer.FontSize = 40
'    SP_Ean = Right("0000000000000" & Lp_Id_Unico_Venta, 15)
'    TxtBarCode = Lp_Id_Unico_Venta ' SP_Ean
'    Printer.Print "    "; "*" & TxtBarCode & "*"
    

     
fin_impresion:
    
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ProblemaImpresora:
    MsgBox "Ocurrio un error al intentar imprimir..." & vbNewLine & Err.Description & vbNewLine & Err.Number

End Sub


Private Sub ImprimeCOTIZACION()
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Fono As String * 12
    Dim Sp_Mail As String
    Dim Sp_Ciudad As String * 17
    Dim Sp_Comuna As String * 17
    
    
    Dim PSp_Empresa As String
    Dim PSp_Giro As String
    Dim PSp_Direccion As String
    Dim PSp_Fono As String
    Dim PSp_Mail As String
    Dim PSp_Ciudad As String
    Dim PSp_Comuna As String
    Dim PSp_Sucursal As String
    
    
    'Variables para detalle de articulos
    Dim Sp_CodigoInt As String * 13
    Dim Sp_Descripcin As String * 42
    Dim Sp_Cod_Acalde As String * 6
    Dim Sp_PU As String * 11
    Dim Sp_Cant As String * 7
    Dim Sp_UM As String * 3
    Dim Sp_TotalL As String * 12
    
    Dim sp_AExento As String * 15
    Dim Sp_ANeto As String * 15
    Dim Sp_AIva As String * 15
    Dim Sp_ATotal As String * 15
    
    
    
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    
    
    
    
    
    Dim Sp_Observacion As String * 80
    
    On Error GoTo ERRORIMPRESION
    Sql = "SELECT giro,direccion,ciudad,fono,email,comuna " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion
        Sp_Fono = RsTmp2!fono
        Sp_Mail = RsTmp2!email
        Sp_Ciudad = RsTmp2!ciudad
        Sp_Comuna = RsTmp2!comuna
    End If
            
    Sql = "SELECT direccion,ciudad,fono,email,comuna,ciudad " & _
         "FROM maestro_clientes " & _
         "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_Direccion = Me.TxtDireccion
        PSp_Fono = Mid(RsTmp2!fono, 1, 10)
        PSp_Mail = Trim(RsTmp2!email)
        PSp_Ciudad = Mid(RsTmp2!ciudad, 1, 20)
        PSp_Comuna = Mid(RsTmp2!comuna, 1, 20)
    End If
 '   PSp_Sucursal = CboSucursal.Text
            
            
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Cx = 2
    Cy = 3
    Dp_S = 0.1
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Cy
    Printer.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 18
    Printer.FontBold = True
    
    Printer.Print "           COTIZACION  NRO " & Lp_Id_Unico_Venta
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
            
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print SP_Empresa_Activa
    
    
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + Dp_S
  
    Printer.FontSize = 12
  
    pos = Printer.CurrentY
    Printer.Print "R.U.T.   :" & SP_Rut_Activo
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 10
    Printer.Print "FECHA:" & Date
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontBold = False
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "GIRO     :" & Sp_Giro
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & Sp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    'email
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & Sp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    Printer.FontSize = 12
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & Sp_Ciudad & " COMUNA:" & Sp_Comuna & " FONO:" & Sp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    'FIN SECCION EMPRESA
    
    
    
    'AHORA SECCION CLIENTE
    'Printer.FontSize = 10
    pos = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.Print "SE�ORES  :" & TxtRazonSocial
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    Printer.CurrentX = Cx
    pos = Printer.CurrentY
    Printer.Print "RUT      :" & TxtRut
    
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 8
    Printer.Print "CONDICION DE PAGO:" ' & Me.CboFpago.Text & " " & Mid(CboPlazos.Text, 1, 25)
    
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    
    
    
    Printer.CurrentX = Cx
    Printer.Print "DIRECCION:" & PSp_Direccion
    Printer.CurrentY = Printer.CurrentY + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "EMAIL    :" & PSp_Mail
    Printer.CurrentY = Printer.CurrentY + Dp_S
        
    Printer.CurrentX = Cx
    Printer.Print "CIUDAD   :" & PSp_Ciudad & "    COMUNA:" & PSp_Comuna & "    FONO:" & PSp_Fono
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S
    
    Printer.CurrentX = Cx
    Printer.Print "SUCURSAL   :" & PSp_Sucursal
    Printer.CurrentY = Printer.CurrentY + Dp_S + (Dp_S * 3)
    
    
    'Fin seccion CLIENTE
    
    
    
    'Inicio SECCION DETALLE DE ARTICULOS
            With LvVenta
                If .ListItems.Count > 0 Then
                    Printer.CurrentX = Cx
                    Printer.Print "Detalle de Art�culos"
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    Sp_CodigoInt = "Cod.Int."
                    Sp_Descripcin = "Descripcion"
                    RSet Sp_PU = "Pre. Unitario"
                    RSet Sp_Cant = "Cant."
                    RSet Sp_UM = "      "
                    RSet Sp_TotalL = "Total"
                    
                    Printer.FontSize = 10
                    Printer.FontBold = False
                    
                    Printer.CurrentX = Cx
                    If SP_Rut_Activo = "78.967.170-2" Then
                        Printer.Print Mid(Sp_CodigoInt, 1, 6) & "  " & Sp_Cant & " " & Sp_Descripcin & "  " & Sp_PU & " " & Sp_TotalL
                    Else
                    
                        Printer.Print Mid(Sp_CodigoInt, 1, 6) & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                    End If
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    
                    For i = 1 To .ListItems.Count
                        Sp_CodigoInt = .ListItems(i).SubItems(1)
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "78.967.170-2" Then
                            Sp_Cod_Acalde = .ListItems(i).SubItems(1)
                        End If
                        Sp_Descripcin = .ListItems(i).SubItems(2)
                        RSet Sp_PU = .ListItems(i).SubItems(4)
                        If SP_Rut_Activo = "76.553.302-3" Then
                             RSet Sp_Cant = Round(CDbl(.ListItems(i).SubItems(3)), 0)
                        Else
                            RSet Sp_Cant = .ListItems(i).SubItems(3)
                        End If
                        If SP_Rut_Activo = "78.967.170-2" Then
                        
                            RSet Sp_UM = .ListItems(i).SubItems(10) 'DSCTO
                        End If
                        
                      '  if
                        RSet Sp_TotalL = .ListItems(i).SubItems(5)
                        Printer.CurrentX = Cx
                        
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Then
                            Printer.Print Sp_Cod_Acalde & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        ElseIf SP_Rut_Activo = "78.967.170-2" Then
                            'Plasticos alduante, lleva Unidad de medida
                            Printer.Print Sp_Cod_Acalde & "- " & Sp_Cant & " " & Sp_UM & " " & Mid(Sp_Descripcin, 1, 39) & " " & Sp_PU & " " & Sp_TotalL
                        ElseIf SP_Rut_Activo = "76.239.518-5" Then
                            Printer.Print Mid(Sp_CodigoInt, 1, 7) & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                            
                        Else
                            Printer.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        End If
                        
                        Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
                    Next
                    Printer.CurrentY = Printer.CurrentY + Dp_S
                    posdetalle = Printer.CurrentY
                End If
            End With
    'FIN SECCION DETALLE DE ARTICULOS
    Printer.CurrentX = Cx
    Printer.FontBold = True
    
    
                 'Totales
    Dp_Total = CDbl(txtTotal)
    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
    Dp_Iva = Dp_Total - Dp_Neto
    
    
    
    'Dim Sp_ANeto As String * 15
    'Dim Sp_AIva As String * 15
    'Dim Sp_ATotal As String * 15
  '  RSet sp_AExento = TxtExentos
    RSet Sp_ANeto = NumFormat(Dp_Neto)
    RSet Sp_AIva = NumFormat(Dp_Iva)
    RSet Sp_ATotal = txtTotal
    
   ' RSet sp_AExento = TxtExentos
    'RSet Sp_ANeto = TxtNeto
    'RSet Sp_AIva = TxtIva
   ' RSet Sp_ATotal = txtTotal
    
  '  Printer.CurrentX = Cx + 12
    If SP_Rut_Activo = "78.967.170-2" Then
            tac = Printer.FontSize
            Printer.FontBold = False
            Printer.FontSize = 8
            posx = Printer.CurrentX
            Printer.Print "Validez: 7 dias, Valores con IVA incluido, "
            Printer.CurrentX = posx
            Printer.Print TxtPlazoEntrega
            Printer.FontSize = tac
  
    End If
    Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
  '  pos = Printer.CurrentY
  '  Printer.Print "E X E N T O:"
  '  Printer.CurrentY = pos
  '  Printer.CurrentX = Cx + 14.5
  '  Printer.Print sp_AExento
    If CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste) > 0 Then
    
        Printer.CurrentX = Cx + 12
       Printer.CurrentY = Printer.CurrentY + Dp_S + Dp_S + Dp_S
        pos = Printer.CurrentY
        
        Printer.FontBold = True
       Printer.Print "D E S C U E N T O:"
        Printer.CurrentY = pos
        Printer.CurrentX = Cx + 14.5
        RSet sp_AExento = NumFormat(CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste))
        Printer.Print sp_AExento
        
    End If
  
  
  
  
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "N E T O    :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ANeto
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "I.V.A.     :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_AIva
    
    Printer.CurrentX = Cx + 12
    Printer.CurrentY = Printer.CurrentY + Dp_S - 0.2
    pos = Printer.CurrentY
    Printer.Print "T O T A L  :"
    Printer.CurrentY = pos
    Printer.CurrentX = Cx + 14.5
    Printer.Print Sp_ATotal
    
    Printer.FontBold = False
        
   ' Printer.DrawMode = 1
    Printer.DrawWidth = 3
    
    Printer.Line (1.2, 4)-(20, 1), , B  'Rectangulo Encabezado y N� Nota de Venta
    Printer.Line (1.2, 4)-(20, 7.7), , B  'Datos de la Empresa
    Printer.Line (1.2, 4)-(20, 11.2), , B 'Datos del Proveedor
    Printer.Line (1.2, 4)-(20, posdetalle), , B 'Detalle de Nota de Vena
    Printer.Line (1.2, posdetalle)-(20, posdetalle + 2.3), , B 'Detalle del Exento,Neto,Iva,Total
       
    Printer.NewPage
    Printer.EndDoc
    Exit Sub
ERRORIMPRESION:
    MsgBox Err.Number & vbNewLine & Err.Description
End Sub

Private Sub PrevisualizaCotizacion()
 Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12
    
    Dim Sp_Nro_Comprobante As String
    
    Dim Sp_Empresa As String
    Dim Sp_Giro As String
    Dim Sp_Direccion As String
    Dim Sp_Fono As String * 12
    Dim Sp_Mail As String
    Dim Sp_Ciudad As String * 17
    Dim Sp_Comuna As String * 17
    
    
    Dim PSp_Empresa As String
    Dim PSp_Giro As String
    Dim PSp_Direccion As String
    Dim PSp_Fono As String
    Dim PSp_Mail As String
    Dim PSp_Ciudad As String
    Dim PSp_Comuna As String
    Dim PSp_Sucursal As String
    
    
    'Variables para detalle de articulos
    Dim Sp_CodigoInt As String * 13
    Dim Sp_Descripcin As String * 42
    Dim Sp_Cod_Acalde As String * 6
    Dim Sp_PU As String * 11
    Dim Sp_Cant As String * 7
    Dim Sp_UM As String * 8
    Dim Sp_TotalL As String * 12
    
    Dim sp_AExento As String * 15
    Dim Sp_ANeto As String * 15
    Dim Sp_AIva As String * 15
    Dim Sp_ATotal As String * 15
    
    
    
    
    
    Dim Sp_Nombre As String * 45
    Dim Sp_Rut As String * 15
    Dim Sp_Concepto As String
    
    Dim Sp_Nro_Doc As String * 12
    Dim Sp_Documento As String * 35
    Dim Sp_Valor As String * 15
    Dim Sp_Saldo As String * 15
    Dim Sp_Fpago As String * 24
    Dim Sp_Total As String * 12
    
    Dim Sp_Banco As String * 20
    Dim Sp_NroCheque As String * 10
    Dim Sp_FechaCheque As String * 10
    Dim Sp_ValorCheque As String * 15
    Dim Sp_SumaCheque As String * 15
    
    
    
    
    
    
    Dim Sp_Observacion As String * 80
    
    On Error GoTo ERRORIMPRESION
    Sql = "SELECT giro,direccion,ciudad,fono,email,comuna " & _
         "FROM sis_empresas " & _
         "WHERE rut='" & SP_Rut_Activo & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        Sp_Giro = RsTmp2!giro
        Sp_Direccion = RsTmp2!direccion
        Sp_Fono = RsTmp2!fono
        Sp_Mail = RsTmp2!email
        Sp_Ciudad = RsTmp2!ciudad
        Sp_Comuna = RsTmp2!comuna
    End If
            
    Sql = "SELECT direccion,ciudad,fono,email,comuna,ciudad " & _
         "FROM maestro_clientes " & _
         "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp2, Sql
    If RsTmp2.RecordCount > 0 Then
        PSp_Direccion = Me.TxtDireccion
        PSp_Fono = Mid(RsTmp2!fono, 1, 10)
        PSp_Mail = Trim(RsTmp2!email)
        PSp_Ciudad = Mid(RsTmp2!ciudad, 1, 20)
        PSp_Comuna = Mid(RsTmp2!comuna, 1, 20)
    End If
 '   PSp_Sucursal = CboSucursal.Text
            
       Sis_Previsualizar.Pic.ScaleMode = vbCentimeters
    Sis_Previsualizar.Pic.BackColor = vbWhite
    Sis_Previsualizar.Pic.AutoRedraw = True
    Sis_Previsualizar.Pic.DrawWidth = 1
    Sis_Previsualizar.Pic.DrawMode = 1
    
    Sis_Previsualizar.Pic.FontName = "Courier New"
    Sis_Previsualizar.Pic.FontSize = 10
   
    Cx = 2
    Cy = 3
    Dp_S = 0.1
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.FontSize = 16  'tama�o de letra
    Sis_Previsualizar.Pic.FontBold = True
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Cy
    Sis_Previsualizar.Pic.PaintPicture LoadPicture(App.Path & "\REDMAROK.jpg"), Cx + 0.62, 1
    
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.FontSize = 18
    Sis_Previsualizar.Pic.FontBold = True
    
    Sis_Previsualizar.Pic.Print "           COTIZACION NRO " & Lp_Id_Unico_Venta
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
            
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print SP_Empresa_Activa
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
  
    Sis_Previsualizar.Pic.FontSize = 12
  
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "R.U.T.   :" & SP_Rut_Activo
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 10
    Sis_Previsualizar.Pic.Print "FECHA:" & Date
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.FontBold = False
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "GIRO     :" & Sp_Giro
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:" & Sp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    'email
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "EMAIL    :" & Sp_Mail
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    Sis_Previsualizar.Pic.FontSize = 12
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "CIUDAD   :" & Sp_Ciudad & "   COMUNA:" & Sp_Comuna & "   FONO:" & Sp_Fono
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + (Dp_S * 3)
    
    'FIN SECCION EMPRESA
    
    
    
    'AHORA SECCION CLIENTE
    'sis_previsualizar.pic.FontSize = 10
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "SE�ORES  :" & TxtRazonSocial
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "RUT      :" & TxtRut
    
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 8
    Sis_Previsualizar.Pic.Print "CONDICION DE PAGO:" ' & Me.CboFpago.Text & " " & Mid(CboPlazos.Text, 1, 25)
    
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    
    
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "DIRECCION:" & PSp_Direccion
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "EMAIL    :" & PSp_Mail
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
        
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "CIUDAD   :" & PSp_Ciudad & "    COMUNA:" & PSp_Comuna & "    FONO:" & PSp_Fono
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S
    
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.Print "SUCURSAL   :" & PSp_Sucursal
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + (Dp_S * 3)
    
    
    'Fin seccion CLIENTE
    
    
    
    'Inicio SECCION DETALLE DE ARTICULOS
            With LvVenta
                If .ListItems.Count > 0 Then
                    Sis_Previsualizar.Pic.CurrentX = Cx
                    Sis_Previsualizar.Pic.Print "Detalle de Art�culos"
                    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
                    Sp_CodigoInt = "Cod.Int."
                    Sp_Descripcin = "Descripcion"
                    RSet Sp_PU = "Pre. Unitario"
                    RSet Sp_Cant = "Cant."
                    RSet Sp_UM = "      "
                    RSet Sp_TotalL = "Total"
                    
                    Sis_Previsualizar.Pic.FontSize = 10
                    Sis_Previsualizar.Pic.FontBold = False
                    
                    Sis_Previsualizar.Pic.CurrentX = Cx
                    
                    Sis_Previsualizar.Pic.Print Mid(Sp_CodigoInt, 1, 6) & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
                    
                    For i = 1 To .ListItems.Count
                        Sp_CodigoInt = .ListItems(i).SubItems(1)
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "78.967.170-2" Then
                            Sp_Cod_Acalde = .ListItems(i).SubItems(1)
                        End If
                        Sp_Descripcin = .ListItems(i).SubItems(2)
                        RSet Sp_PU = .ListItems(i).SubItems(4)
                        If SP_Rut_Activo = "76.553.302-3" Then
                             RSet Sp_Cant = Round(CDbl(.ListItems(i).SubItems(3)), 0)
                        Else
                            RSet Sp_Cant = .ListItems(i).SubItems(3)
                        End If
                        'RSet Sp_UM = .ListItems(i).SubItems(5) 'DSCTO
                      '  if
                        RSet Sp_TotalL = .ListItems(i).SubItems(5)
                        Sis_Previsualizar.Pic.CurrentX = Cx
                        
                        If SP_Rut_Activo = "76.169.962-8" Or SP_Rut_Activo = "76.553.302-3" Or SP_Rut_Activo = "78.967.170-2" Then
                            Sis_Previsualizar.Pic.Print Sp_Cod_Acalde & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        Else
                            Sis_Previsualizar.Pic.Print Sp_CodigoInt & " " & Sp_Descripcin & " " & Sp_PU & " " & Sp_Cant & " " & Sp_TotalL
                        End If
                        
                        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
                    Next
                    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S
                    posdetalle = Sis_Previsualizar.Pic.CurrentY
                End If
            End With
    'FIN SECCION DETALLE DE ARTICULOS
    Sis_Previsualizar.Pic.CurrentX = Cx
    Sis_Previsualizar.Pic.FontBold = True
    
    
                 'Totales
    Dp_Total = CDbl(txtTotal)
    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
    Dp_Iva = Dp_Total - Dp_Neto
    
    
    
    'Dim Sp_ANeto As String * 15
    'Dim Sp_AIva As String * 15
    'Dim Sp_ATotal As String * 15
  '  RSet sp_AExento = TxtExentos
    RSet Sp_ANeto = NumFormat(Dp_Neto)
    RSet Sp_AIva = NumFormat(Dp_Iva)
    RSet Sp_ATotal = txtTotal
    
   ' RSet sp_AExento = TxtExentos
    'RSet Sp_ANeto = TxtNeto
    'RSet Sp_AIva = TxtIva
   ' RSet Sp_ATotal = txtTotal
    
  
      
    If CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste) > 0 Then
    
        Sis_Previsualizar.Pic.CurrentX = Cx + 12
        Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S + Dp_S + Dp_S
        pos = Sis_Previsualizar.Pic.CurrentY
        tl = Sis_Previsualizar.Pic.FontSize
        Sis_Previsualizar.Pic.FontBold = True
        Sis_Previsualizar.Pic.Print "D E S C U E N T O:"
        Sis_Previsualizar.Pic.CurrentY = pos
        Sis_Previsualizar.Pic.CurrentX = Cx + 14.5
        RSet sp_AExento = NumFormat(CDbl(TxtValorDescuento) + CDbl(TxtDsctoAjuste) - CDbl(TxtRecargoAjuste))
        Sis_Previsualizar.Pic.Print sp_AExento
        Sis_Previsualizar.Pic.FontSize = tl
    End If
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "N E T O    :"
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14.5
    Sis_Previsualizar.Pic.Print Sp_ANeto
    
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "I.V.A.     :"
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14.5
    Sis_Previsualizar.Pic.Print Sp_AIva
    
    Sis_Previsualizar.Pic.CurrentX = Cx + 12
    Sis_Previsualizar.Pic.CurrentY = Sis_Previsualizar.Pic.CurrentY + Dp_S - 0.2
    pos = Sis_Previsualizar.Pic.CurrentY
    Sis_Previsualizar.Pic.Print "T O T A L  :"
    Sis_Previsualizar.Pic.CurrentY = pos
    Sis_Previsualizar.Pic.CurrentX = Cx + 14.5
    Sis_Previsualizar.Pic.Print Sp_ATotal
    
    Sis_Previsualizar.Pic.FontBold = False
        
   ' sis_previsualizar.pic.DrawMode = 1
    Sis_Previsualizar.Pic.DrawWidth = 3
    
    Sis_Previsualizar.Pic.Line (1.2, 4)-(21, 1), , B  'Rectangulo Encabezado y N� Nota de Venta
    Sis_Previsualizar.Pic.Line (1.2, 4)-(21, 7.7), , B  'Datos de la Empresa
    Sis_Previsualizar.Pic.Line (1.2, 4)-(21, 11.2), , B 'Datos del Proveedor
    Sis_Previsualizar.Pic.Line (1.2, 4)-(21, posdetalle), , B 'Detalle de Nota de Vena
    Sis_Previsualizar.Pic.Line (1.2, posdetalle)-(21, posdetalle + 2.3), , B 'Detalle del Exento,Neto,Iva,Total
    Sis_Previsualizar.Caption = "COTIZACION"
    Sis_Previsualizar.Show 1
    Exit Sub
ERRORIMPRESION:
    MsgBox Err.Number & vbNewLine & Err.Description
End Sub


Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub
Private Function La_Establecer_Impresora(ByVal NamePrinter As String) As Boolean
  
On Error GoTo errSub
  
     
  
    'Variable de referencia
  
    Dim obj_Impresora As Object
  
     
  
    'Creamos la referencia
  
    Set obj_Impresora = CreateObject("WScript.Network")
  
        obj_Impresora.setdefaultprinter NamePrinter
  
     
  
    Set obj_Impresora = Nothing
  
         
  
        'La funci�n devuelve true y se cambi� con �xito
  
       La_Establecer_Impresora = True
  
  '      MsgBox "La impresora se cambi� correctamente", vbInformation
  
    Exit Function
  
     
  
     
  
'Error al cambiar la impresora
  
errSub:
  
If Err.Number = 0 Then Exit Function
  
   La_Establecer_Impresora = False
  
   MsgBox "error: " & Err.Number & Chr(13) & "Description: " & Err.Description
  
   On Error GoTo 0
  
End Function


