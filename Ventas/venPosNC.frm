VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form venPosNC 
   Caption         =   "Emision Nota de Credito Electronica"
   ClientHeight    =   10350
   ClientLeft      =   1800
   ClientTop       =   1740
   ClientWidth     =   18000
   LinkTopic       =   "Form1"
   ScaleHeight     =   10350
   ScaleWidth      =   18000
   Begin VB.Frame FraProcesandoDTE 
      Height          =   1485
      Left            =   2475
      TabIndex        =   69
      Top             =   4290
      Visible         =   0   'False
      Width           =   13020
      Begin VB.Timer Timer2 
         Enabled         =   0   'False
         Interval        =   20
         Left            =   2940
         Top             =   930
      End
      Begin Proyecto2.XP_ProgressBar XP_ProgressBar1 
         Height          =   255
         Left            =   4050
         TabIndex        =   71
         Top             =   975
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16750899
         Scrolling       =   2
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   510
         Left            =   1350
         OleObjectBlob   =   "venPosNC.frx":0000
         TabIndex        =   70
         Top             =   420
         Width           =   10275
      End
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkNCManual 
      Height          =   255
      Left            =   3480
      OleObjectBlob   =   "venPosNC.frx":00C8
      TabIndex        =   46
      Top             =   9075
      Width           =   1635
   End
   Begin VB.CommandButton CmdAceptar 
      Caption         =   "&Generar Nota de Credito"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   705
      Left            =   5970
      TabIndex        =   41
      Top             =   8940
      Width           =   4620
   End
   Begin VB.TextBox TxtNroManual 
      Height          =   285
      Left            =   3450
      TabIndex        =   40
      Top             =   9315
      Width           =   2175
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Electronica"
      Height          =   255
      Left            =   2250
      TabIndex        =   39
      Top             =   9315
      Value           =   1  'Checked
      Width           =   1155
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   450
      TabIndex        =   38
      Top             =   9150
      Width           =   1680
   End
   Begin VB.Frame FrmNV 
      Caption         =   "Datos para nota de credito"
      Height          =   10335
      Left            =   300
      TabIndex        =   7
      Top             =   15
      Width           =   17475
      Begin VB.Frame FrameSelecciona 
         Caption         =   "Caja/Abono CtaCte"
         Height          =   660
         Left            =   13080
         TabIndex        =   67
         Top             =   4065
         Width           =   4080
         Begin VB.ComboBox CboCajaAbono 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "venPosNC.frx":0140
            Left            =   255
            List            =   "venPosNC.frx":014D
            Style           =   2  'Dropdown List
            TabIndex        =   68
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   255
            Width           =   3585
         End
      End
      Begin VB.Frame FraProductos 
         Caption         =   "Productos"
         Enabled         =   0   'False
         Height          =   4050
         Left            =   645
         TabIndex        =   47
         Top             =   4710
         Width           =   15585
         Begin VB.CheckBox ChkProductos 
            Caption         =   "Check2"
            Height          =   285
            Left            =   420
            TabIndex        =   61
            Top             =   1260
            Width           =   270
         End
         Begin VB.TextBox TxtDescripcion 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   2790
            MultiLine       =   -1  'True
            TabIndex        =   54
            TabStop         =   0   'False
            Top             =   720
            Width           =   6855
         End
         Begin VB.TextBox TxtCantidad 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   9690
            TabIndex        =   53
            Text            =   "1"
            Top             =   720
            Width           =   1380
         End
         Begin VB.TextBox TxtPrecio 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   11085
            TabIndex        =   52
            Text            =   "0"
            Top             =   720
            Width           =   1815
         End
         Begin VB.TextBox TxtTotalLinea 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   12930
            Locked          =   -1  'True
            TabIndex        =   51
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   720
            Width           =   1650
         End
         Begin VB.CommandButton CmdAceptaLinea 
            Appearance      =   0  'Flat
            BackColor       =   &H8000000A&
            Caption         =   "Ok"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   14610
            TabIndex        =   50
            Top             =   720
            Width           =   720
         End
         Begin VB.TextBox TxtCodigo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   390
            TabIndex        =   49
            Top             =   720
            Width           =   2370
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkUbicacion 
            Height          =   300
            Left            =   3855
            OleObjectBlob   =   "venPosNC.frx":0189
            TabIndex        =   48
            Top             =   585
            Width           =   5760
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   8
            Left            =   390
            OleObjectBlob   =   "venPosNC.frx":01E6
            TabIndex        =   55
            Top             =   435
            Width           =   660
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Index           =   9
            Left            =   2805
            OleObjectBlob   =   "venPosNC.frx":0248
            TabIndex        =   56
            Top             =   495
            Width           =   1530
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   3
            Left            =   9660
            OleObjectBlob   =   "venPosNC.frx":02B4
            TabIndex        =   57
            Top             =   480
            Width           =   1395
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   10
            Left            =   11220
            OleObjectBlob   =   "venPosNC.frx":031A
            TabIndex        =   58
            Top             =   480
            Width           =   1635
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   12
            Left            =   13155
            OleObjectBlob   =   "venPosNC.frx":037C
            TabIndex        =   59
            Top             =   480
            Width           =   1440
         End
         Begin MSComctlLib.ListView LvVenta 
            Height          =   2430
            Left            =   375
            TabIndex        =   60
            Top             =   1260
            Width           =   14955
            _ExtentX        =   26379
            _ExtentY        =   4286
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Object.Width           =   529
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Codigo"
               Object.Width           =   3660
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Descripci�n"
               Object.Width           =   12153
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   2443
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N100"
               Text            =   "Precio"
               Object.Width           =   3201
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   3201
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "Stock"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Tag             =   "T1000"
               Text            =   "Inventario"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Precio Costo"
               Object.Width           =   0
            EndProperty
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   1
         Left            =   7440
         OleObjectBlob   =   "venPosNC.frx":03DC
         TabIndex        =   45
         Top             =   4380
         Width           =   1905
      End
      Begin VB.ComboBox CboModalidadNC 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "venPosNC.frx":046C
         Left            =   9420
         List            =   "venPosNC.frx":0476
         Style           =   2  'Dropdown List
         TabIndex        =   44
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   4305
         Width           =   2865
      End
      Begin VB.Frame Frame2 
         Caption         =   "Total Nota de Credito"
         Height          =   1395
         Left            =   10515
         TabIndex        =   42
         Top             =   8835
         Width           =   6480
         Begin VB.TextBox TxtOtrosImpuestos 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1560
            TabIndex        =   72
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   975
            Width           =   1470
         End
         Begin VB.TextBox TxtSubTotal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4275
            TabIndex        =   66
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   405
            Width           =   2085
         End
         Begin VB.TextBox TxtDescuentoNC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4275
            TabIndex        =   64
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   720
            Width           =   2085
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   195
            Left            =   2535
            OleObjectBlob   =   "venPosNC.frx":0498
            TabIndex        =   62
            Top             =   735
            Width           =   1530
         End
         Begin VB.TextBox TxtTotalNC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H0080FF80&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4290
            TabIndex        =   43
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   1035
            Width           =   2085
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   195
            Left            =   2565
            OleObjectBlob   =   "venPosNC.frx":050A
            TabIndex        =   63
            Top             =   1065
            Width           =   1500
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   195
            Left            =   2535
            OleObjectBlob   =   "venPosNC.frx":0572
            TabIndex        =   65
            Top             =   435
            Width           =   1530
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   195
            Left            =   180
            OleObjectBlob   =   "venPosNC.frx":05E0
            TabIndex        =   73
            Top             =   1065
            Width           =   1290
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Documento Referencia"
         Height          =   3675
         Left            =   7380
         TabIndex        =   32
         Top             =   375
         Width           =   9885
         Begin VB.CommandButton CmdModificaValor 
            Caption         =   "Modificar Valor"
            Height          =   285
            Left            =   7995
            TabIndex        =   74
            Top             =   3240
            Visible         =   0   'False
            Width           =   1410
         End
         Begin MSComCtl2.DTPicker DtFechaBoleta 
            Height          =   315
            Left            =   5985
            TabIndex        =   4
            Top             =   480
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   556
            _Version        =   393216
            Format          =   89718785
            CurrentDate     =   42279
         End
         Begin VB.ComboBox CboDocVenta 
            Height          =   315
            ItemData        =   "venPosNC.frx":065C
            Left            =   165
            List            =   "venPosNC.frx":065E
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   495
            Width           =   4170
         End
         Begin VB.TextBox txtNroDocumentoBuscar 
            BackColor       =   &H8000000E&
            Height          =   315
            Left            =   4335
            TabIndex        =   3
            Top             =   495
            Width           =   1650
         End
         Begin VB.TextBox txtTotalDocBuscado 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7170
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   495
            Width           =   1305
         End
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "Buscar"
            Height          =   315
            Left            =   8475
            TabIndex        =   6
            Top             =   480
            Width           =   1125
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   9
            Left            =   180
            OleObjectBlob   =   "venPosNC.frx":0660
            TabIndex        =   33
            Top             =   315
            Width           =   1980
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   10
            Left            =   4365
            OleObjectBlob   =   "venPosNC.frx":06D0
            TabIndex        =   34
            Top             =   300
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   195
            Index           =   11
            Left            =   6090
            OleObjectBlob   =   "venPosNC.frx":0734
            TabIndex        =   35
            Top             =   285
            Width           =   870
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   12
            Left            =   7215
            OleObjectBlob   =   "venPosNC.frx":079C
            TabIndex        =   36
            Top             =   315
            Width           =   1185
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   2775
            Left            =   150
            TabIndex        =   37
            Top             =   855
            Width           =   9480
            _ExtentX        =   16722
            _ExtentY        =   4895
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Unico"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Documento"
               Object.Width           =   7355
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Nro"
               Object.Width           =   2910
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "F1000"
               Text            =   "Fecha"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "N109"
               Text            =   "doc_id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "doc_sii"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Text            =   "Factura Por Guias"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Neto-Bruto"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Cliente"
         Height          =   2940
         Left            =   405
         TabIndex        =   9
         Top             =   1680
         Width           =   6840
         Begin VB.TextBox TxtRazonSocial 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   19
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   795
            Width           =   5730
         End
         Begin VB.CommandButton CmdBuscaCliente 
            Caption         =   "F1 - Buscar"
            Height          =   255
            Left            =   2985
            TabIndex        =   18
            Top             =   465
            Width           =   1395
         End
         Begin VB.TextBox TxtRut 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   17
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   465
            Width           =   1395
         End
         Begin VB.TextBox TxtGiro 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   16
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1125
            Width           =   5715
         End
         Begin VB.TextBox TxtDireccion 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   15
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1455
            Width           =   5670
         End
         Begin VB.TextBox txtComuna 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   14
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1785
            Width           =   2160
         End
         Begin VB.TextBox txtFono 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1545
            TabIndex        =   13
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   2115
            Width           =   2175
         End
         Begin VB.TextBox TxtEmail 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   12
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   2445
            Width           =   3855
         End
         Begin VB.TextBox TxtMontoCredito 
            Height          =   255
            Left            =   6420
            TabIndex        =   11
            Text            =   "Text1"
            Top             =   2040
            Visible         =   0   'False
            Width           =   1995
         End
         Begin VB.TextBox TxtCupoUtilizado 
            Height          =   285
            Left            =   6405
            TabIndex        =   10
            Text            =   "Text1"
            Top             =   2370
            Visible         =   0   'False
            Width           =   2010
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCupoCredito 
            Height          =   270
            Left            =   4890
            OleObjectBlob   =   "venPosNC.frx":080E
            TabIndex        =   20
            Top             =   1800
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   0
            Left            =   1005
            OleObjectBlob   =   "venPosNC.frx":086E
            TabIndex        =   21
            Top             =   495
            Width           =   495
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   210
            Index           =   2
            Left            =   840
            OleObjectBlob   =   "venPosNC.frx":08D8
            TabIndex        =   22
            Top             =   840
            Width           =   660
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   1
            Left            =   435
            OleObjectBlob   =   "venPosNC.frx":0942
            TabIndex        =   23
            Top             =   1470
            Width           =   1065
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   0
            Left            =   645
            OleObjectBlob   =   "venPosNC.frx":09B2
            TabIndex        =   24
            Top             =   1815
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
            Height          =   255
            Left            =   1080
            OleObjectBlob   =   "venPosNC.frx":0A1C
            TabIndex        =   25
            Top             =   1155
            Width           =   420
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   1
            Left            =   615
            OleObjectBlob   =   "venPosNC.frx":0A82
            TabIndex        =   26
            Top             =   2145
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   2
            Left            =   195
            OleObjectBlob   =   "venPosNC.frx":0AE8
            TabIndex        =   27
            Top             =   2445
            Width           =   1320
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   3
            Left            =   3810
            OleObjectBlob   =   "venPosNC.frx":0B6A
            TabIndex        =   28
            Top             =   1800
            Width           =   1035
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento Actual"
         Height          =   1290
         Left            =   420
         TabIndex        =   8
         Top             =   375
         Width           =   6750
         Begin MSComCtl2.DTPicker DtFecha 
            Height          =   285
            Left            =   5310
            TabIndex        =   1
            Top             =   900
            Width           =   1395
            _ExtentX        =   2461
            _ExtentY        =   503
            _Version        =   393216
            Format          =   89718785
            CurrentDate     =   42269
         End
         Begin VB.TextBox TxtDocActual 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   2520
            TabIndex        =   29
            Text            =   "(auto)"
            ToolTipText     =   "Nro NC"
            Top             =   465
            Width           =   3225
         End
         Begin VB.ComboBox CboNC 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "venPosNC.frx":0BE2
            Left            =   225
            List            =   "venPosNC.frx":0BE4
            Style           =   2  'Dropdown List
            TabIndex        =   0
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   840
            Width           =   4155
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   300
            Index           =   2
            Left            =   180
            OleObjectBlob   =   "venPosNC.frx":0BE6
            TabIndex        =   30
            Top             =   465
            Width           =   2250
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   300
            Index           =   3
            Left            =   4275
            OleObjectBlob   =   "venPosNC.frx":0C56
            TabIndex        =   31
            Top             =   900
            Width           =   960
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkFoliosDisponibles 
            Height          =   195
            Left            =   2445
            OleObjectBlob   =   "venPosNC.frx":0CB6
            TabIndex        =   75
            Top             =   180
            Width           =   3660
         End
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   0
      Top             =   0
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   60
      OleObjectBlob   =   "venPosNC.frx":0D18
      Top             =   5220
   End
End
Attribute VB_Name = "venPosNC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public lP_Documento_Referenciado As Long
Public iP_Tipo_NC As Integer
Dim Sm_Nc_Boleta As String * 2
Dim Lp_Id_Venta As Long
Dim Bm_AfectarCaja As Boolean
Dim Sm_Sql_Productos_NC As String
Dim Lp_Folio As Long
Dim Im_TipoNC As Integer
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long




Private Sub CboModalidadNC_Click()
    Im_TipoNC = 1
    TxtSubTotal = "0"
    TxtDescuentoNC = "0"
    If LVDetalle.ListItems.Count = 0 Then Exit Sub
    If CboModalidadNC.ListIndex = 0 Then
        FraProductos.Enabled = False
        LvDetalle_Click
        SumaSeleccion
        BuscaDescuentoNC
        
    ElseIf CboModalidadNC.ListIndex = 1 Then
    
        
        Im_TipoNC = 3
        FraProductos.Enabled = True
        SumaSeleccion
        
    Else
        'Ingreso manual
        LvVenta.ListItems.Clear
        FraProductos.Enabled = True
        TxtSubTotal = 0
        TxtDescuentoNC = 0
        TxtTotalNC = 0
    End If
End Sub
Private Sub BuscaDescuentoNC()

        'busca descuentos del documento que no estaba considerando
        '19 Marzo,
        Sql = "SELECT ven_descuento_valor+ven_ajuste_recargo+ven_ajuste_descuento descuento,bruto " & _
                "FROM ven_doc_venta " & _
                "WHERE no_documento =" & LVDetalle.ListItems(1).SubItems(2) & " AND doc_id = " & LVDetalle.ListItems(1).SubItems(5) & " AND rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                TxtSubTotal = NumFormat(RsTmp!bruto + RsTmp!Descuento)
                TxtDescuentoNC = NumFormat(RsTmp!Descuento)
                TxtTotalNC = NumFormat(CDbl(TxtSubTotal) - RsTmp!Descuento)
                
        
        End If
End Sub



Private Sub CboNC_Click()
    Dim Rp_DocVenta As Recordset
    If CboNC.ListIndex = -1 Then Exit Sub
    
    Sql = "SELECT doc_nc_por_boleta NC " & _
            "FROM sis_documentos " & _
            "WHERE doc_id=" & CboNC.ItemData(CboNC.ListIndex)
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!NC = "SI" Then
            LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_boleta_venta='SI'"
             Sm_Nc_Boleta = "SI"
            
        Else
            LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_boleta_venta='NO' AND doc_documento='VENTA' AND doc_nota_de_credito='NO' AND doc_contable='SI' AND doc_nota_de_venta='NO'"
            Sm_Nc_Boleta = "NO"
        End If
    End If
    
    If SP_Rut_Activo = "78.967.170-2" Then
           LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA'  AND doc_nota_de_credito='NO' AND doc_contable='SI' "
    End If

    If CboNC.ListIndex = -1 Then Exit Sub
        Ip_DocIdDocumento = CboNC.ItemData(CboNC.ListIndex)
  
        Sql = "SELECT doc_dte,doc_cod_sii " & _
                "FROM sis_documentos " & _
                "WHERE doc_id=" & Ip_DocIdDocumento
        Consulta Rp_DocVenta, Sql
        If Rp_DocVenta.RecordCount > 0 Then
            If Rp_DocVenta!doc_dte = "SI" Then
                Sql = "SELECT dte_id,dte_folio numero " & _
                    "FROM dte_folios " & _
                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & Rp_DocVenta!doc_cod_sii & " AND dte_venta_compra='VENTA' AND dte_disponible='SI' " & _
                    "ORDER BY dte_folio " & _
                    "LIMIT 1"
                Consulta Rp_DocVenta, Sql
                TxtNroDocumento = 0
                If Rp_DocVenta.RecordCount > 0 Then
                    TxtNroDocumento = Rp_DocVenta!Numero
                Else
                    Me.SkFoliosDisponibles = "No quedan folios disponibles.."
                End If
            End If
        End If


End Sub

Private Sub ChkProductos_Click()
    If ChkProductos.Value = 1 Then
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        Next
    Else
    
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = False
        Next
    
    End If
    SumaSeleccion
End Sub

Private Sub SumaSeleccion()
    TxtTotalNC = 0
    For i = 1 To LvVenta.ListItems.Count
        
        If LvVenta.ListItems(i).Checked Then TxtTotalNC = NumFormat(CDbl(TxtTotalNC) + CDbl(LvVenta.ListItems(i).SubItems(5)))
    Next

    If LVDetalle.ListItems(1).SubItems(8) = "NETO" Then
    
        TxtTotalNC = NumFormat(CDbl(TxtTotalNC) * Val("1." & DG_IVA))
    End If
End Sub

Private Sub CmdAceptaLinea_Click()
    Dim p As Integer
    If Len(TxtCodigo) = 0 Then
        MsgBox "Ingrese c�digo...", vbInformation
        On Error Resume Next
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Len(TxtDescripcion) = 0 Then
        MsgBox "Faltan datos para agregar linea...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Val(CxP(TxtCantidad)) = 0 Then
        MsgBox "Falta cantidad...", vbInformation
        TxtCantidad.SetFocus
        Exit Sub
    End If
    If Val(TxtPrecio) = 0 Then
        MsgBox "Falta precio...", vbInformation
        TxtPrecio.SetFocus
        Exit Sub
    End If
    
    LvVenta.ListItems.Add , , TxtCodigo.Tag
    p = LvVenta.ListItems.Count
    LvVenta.ListItems(p).SubItems(1) = TxtCodigo
    LvVenta.ListItems(p).SubItems(2) = TxtDescripcion
    LvVenta.ListItems(p).SubItems(3) = TxtCantidad
    LvVenta.ListItems(p).SubItems(4) = TxtPrecio
    
    If Sm_VentaRapida = "SI" Then
        LvVenta.ListItems(p).SubItems(5) = Round(CDbl(TxtPrecio) * Val(TxtCantidad), 0)
    Else
        LvVenta.ListItems(p).SubItems(5) = TxtTotalLinea
    End If
    LvVenta.ListItems(p).SubItems(6) = TxtStockLinea
    LvVenta.ListItems(p).SubItems(7) = SkInventariable
  '  LvVenta.ListItems(p).SubItems(8) = Val(TxtStockLinea.Tag)
    'LimpiaTxt
    'CalculaGrilla
    
    LvVenta.ListItems(LvVenta.ListItems.Count).Selected = True
    LvVenta.ListItems(LvVenta.ListItems.Count).EnsureVisible
    TxtTotalNC = NumFormat(TotalizaColumna(LvVenta, "total"))
    SumaSeleccion
    
    TxtCodigo.SetFocus
    
End Sub

Private Sub CmdAceptar_Click()
    
        
    
    Dim Lp_SaldoDoc As Long
    Dim Sp_MontoACtaCte As String
    If LvVenta.ListItems.Count = 0 And Sm_Nc_Boleta = "NO" Then Exit Sub
    
    
    If CboNC.ListIndex = -1 Then
        MsgBox "Seleccione NC...", vbInformation
        CboNC.SetFocus
        Exit Sub
    End If
    

    If LVDetalle.ListItems.Count = 0 Then
        MsgBox "No hay documento referenciado...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    
    If Len(TxtRut) = 0 Then
        MsgBox "Debe seleccionar/ingresar cliente...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    
    If CDbl(TxtTotalNC) = 0 Then
    
        MsgBox "Haga clic en el documento de la grilla..."
        LVDetalle.SetFocus
        Exit Sub
    End If
    
    Sp_MontoACtaCte = "NO"
    If CboCajaAbono.ListIndex = 0 Then
        MsgBox "Seleccione Accion de NC en caja...", vbInformation
        CboCajaAbono.SetFocus
        Exit Sub
    Else
        Lp_SaldoDoc = 0
        If Sm_Nc_Boleta = "NO" Then Lp_SaldoDoc = SaldoDocumento(LVDetalle.ListItems(1), "CLI")
        
        If Lp_SaldoDoc > 0 Then
            MsgBox "Se abonar� el monto de la NC al documento..." & vbNewLine & "    (documento Ref fue al credito)   ", vbInformation
            
        Else
            If CboCajaAbono.ListIndex = 2 Then
                Sp_MontoACtaCte = "SI"
            End If
        End If
    End If
    
    

            DoEvents
    Me.Timer2.Enabled = True
    FraProcesandoDTE.Visible = True
 
    
    
    Dim Dp_Total As Double
    Dim Dp_Neto As Double
    Dim Dp_Iva As Double
    Dim Sp_Documento_Referencia As String
    
    CmdAceptar.Enabled = False
    
    If Me.Check1 = 1 Then
             Sql = "SELECT dte_id,dte_folio numero " & _
                                "FROM dte_folios " & _
                                "WHERE doc_id=61 AND dte_venta_compra='VENTA' AND dte_disponible='SI' AND rut_emp='" & SP_Rut_Activo & "' " & _
                                "ORDER BY dte_folio " & _
                                "LIMIT 1"
                                
            
            Consulta RsTmp, Sql
            
            If RsTmp.RecordCount = 0 Then
                MsgBox "No cuenta con folios disponibles para emitir NC Electronica..."
                 CmdAceptar.Enabled = True
                Exit Sub
            
            End If
            
            
            dte_id = RsTmp!dte_id
            Lp_Folio = RsTmp!Numero
            
            
            
            Sql = "SELECT id " & _
                    "FROM ven_doc_venta " & _
                    "WHERE doc_id=61 AND rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_Folio
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                MsgBox "Folio ya ha sido utilizado... consulte para liberar este Nro de NC...", vbInformation
                Exit Sub
            End If
            

    Else
'            Sql = "SELECT IFNULL(no_documento,0) +1 nro " & _
'                    "FROM ven_doc_venta " & _
'                    "WHERE doc_id=" & CboNC.ItemData(CboNC.ListIndex)
'            Consulta RsTmp, Sql
            'If RsTmp.RecordCount > 0 Then'
            '    Lp_Folio = RsTmp!Nro
            'End If
            Lp_Folio = Me.TxtNroManual
    End If
    Lp_Id_Venta = UltimoNro("ven_doc_venta", "id")
    'Se debe generar la NC
    'con los mismos datos de la venta
    
    
    
    'TAMBIEN HAY QUE VERIFICAR COMO SE PAGA LA NC
    
    If Sm_Nc_Boleta = "NO" Then
        'Nc por factura
        Bm_AfectarCaja = True
        'verificamos si el doc es a credito,
        AbonaNCCredito LVDetalle.ListItems(1)
        
        lacajita = LG_id_Caja
        If Not Bm_AfectarCaja Then lacajita = 0
        
        
        If Sp_MontoACtaCte = "SI" Then
            lacajita = 0
            
                           'Guardaremos estos valores en la tabla cta_pozo
                    
            Sql = "INSERT INTO cta_pozo (abo_id,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                    "VALUES(" & 0 & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & TxtRut & "'," & CDbl(TxtTotalNC) & ")"
            cn.Execute Sql
        End If
            
            
            
            
    
        
        
        
        
        
        If Im_TipoNC = 1 Then
                    'Nota de Credito por anulacion de documento *****************************
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "',rut_cliente,nombre_cliente,'VD',neto,bruto,iva," & _
                                    "'ANULA DOCUMENTO','CONTADO',ven_id,ven_nombre,0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",0,'" & Fql(DtFecha) & "','" & LogUsuario & "'," & LVDetalle.ListItems(1) & "," & _
                                    "rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo," & _
                                    "ven_comentario,ven_ordendecompra,ven_tipo_calculo,ven_venta_arriendo," & _
                                    "'SI'," & lacajita & ",sue_id,ven_plazo_id,'SI',gir_id,rso_id,1,1,curtime(),doc_genero " & _
                            "FROM ven_doc_venta " & _
                            "WHERE id=" & LVDetalle.ListItems(1) & " AND rut_emp='" & SP_Rut_Activo & "'"
                    cn.Execute Sql
                        
                    
                    If LVDetalle.SelectedItem.SubItems(7) = "NO" Then
                    
                            Sql = "INSERT INTO ven_detalle (" & _
                                        "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                        "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                        "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) " & _
                                    "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                        "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                        "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                                    "FROM ven_detalle " & _
                                    "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LVDetalle.ListItems(1).SubItems(2) & "  AND doc_id=" & LVDetalle.ListItems(1).SubItems(5)
                    Else
                        'nota de credito por factura de guias _
                        13 Mayo 2016
                            Sql = "INSERT INTO ven_detalle (" & _
                                        "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                        "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                        "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) " & _
                                    "SELECT codigo,marca,descripcion,precio_real,ROUND(AVG(descuento),0),SUM(unidades),ROUND(AVG(precio_final),0),ROUND(SUM(subtotal),0)," & _
                                        "SUM(precio_costo),btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",d.rut_emp,d.pla_id,d.are_id,d.cen_id," & _
                                        "aim_id , SUM(ved_precio_venta_bruto), ROUND(SUM(ved_precio_venta_neto),0) " & _
                                    "FROM ven_detalle d " & _
                                    "JOIN ven_doc_venta v ON d.no_documento=v.no_documento AND d.doc_id=v.doc_id " & _
                                    "WHERE v.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND nro_factura=" & LVDetalle.ListItems(1).SubItems(2) & "  AND doc_id_factura=" & LVDetalle.ListItems(1).SubItems(5) & " " & _
                                    "GROUP BY codigo"
                    
                    End If
                    cn.Execute Sql
                    
                    
                    
                    Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                            "FROM ven_detalle " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LVDetalle.ListItems(1).SubItems(2) & "  AND doc_id=" & LVDetalle.ListItems(1).SubItems(5)
        Else
            ' Nota de Credito Parcial, solo los productos de la grilla
                 'Nota de Credito por anulacion de documento *****************************
                                 'Totales
                    If LVDetalle.ListItems(1).SubItems(8) = "NETO" Then
                        Dp_Total = CDbl(TxtTotalNC)
                        Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                        Dp_Iva = Dp_Total - Dp_Neto
                    
                    Else
                                     
                        Dp_Total = CDbl(TxtTotalNC)
                        Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                        Dp_Iva = Dp_Total - Dp_Neto
                    End If
                    
                 
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "',rut_cliente,nombre_cliente,'VD'," & Dp_Neto & "," & Dp_Total & "," & Dp_Iva & "," & _
                                    "'DEVOLUCION DE PRODUCTOS','CONTADO',ven_id,ven_nombre,0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",0,'" & Fql(DtFecha) & "','" & LogUsuario & "'," & LVDetalle.ListItems(1) & "," & _
                                    "rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo," & _
                                    "ven_comentario,ven_ordendecompra,ven_tipo_calculo,ven_venta_arriendo," & _
                                    "'SI'," & lacajita & ",sue_id,ven_plazo_id,'SI',gir_id,rso_id,1,1,curtime(),doc_genero " & _
                            "FROM ven_doc_venta " & _
                            "WHERE id=" & LVDetalle.ListItems(1) & " AND rut_emp='" & SP_Rut_Activo & "'"
                    cn.Execute Sql
                    
                    '***** Detalle va solo con los productos de la grilla
                    
                    
                    
                    
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) VALUES "
                    For i = 1 To LvVenta.ListItems.Count
                        If LvVenta.ListItems(i).Checked Then
                        
                             If LVDetalle.ListItems(1).SubItems(6) = "NETO" Then
                             
                                 Dp_Neto = CDbl(LvVenta.ListItems(i).SubItems(5))
                                 Dp_Total = Round(Dp_Total * Val("1." & DG_IVA), 0)
                             Else
                                
                                 Dp_Total = CDbl(LvVenta.ListItems(i).SubItems(5))
                                 Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                             End If
                             sql2 = "SELECT precio_costo,pla_id,are_id,cen_id,aim_id " & _
                                    "FROM ven_detalle " & _
                                    "WHERE id=" & LvVenta.ListItems(i)
                            Consulta RsTmp, sql2
                            If RsTmp.RecordCount > 0 Then
                                Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CxP(CDbl(LvVenta.ListItems(i).SubItems(3))) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                CxP(RsTmp!precio_costo) & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & RsTmp!pla_id & "," & _
                                RsTmp!are_id & "," & RsTmp!cen_id & "," & _
                                RsTmp!aim_id & "," & Dp_Total & "," & Dp_Neto & "),"
                            End If
                            
                        End If
                    Next
                    Sql = Mid(Sql, 1, Len(Sql) - 1)
                    cn.Execute Sql
                    
                    
                    
                       Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                            "FROM ven_detalle " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_Folio & "  AND doc_id=" & CboNC.ItemData(CboNC.ListIndex)
                    
        
        
        
        End If
            
        NotaCreditoElectronica 61, Lp_Folio, CboNC.ItemData(CboNC.ListIndex), False
               
        'Folio ya no esta disponible
        cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                            "WHERE dte_id=" & dte_id
                            
                        
                
        ActualizaNC
        
      '  MsgBox "Se genero NC" & vbNewLine & "NRO:" & Lp_Folio, vbInformation
    Else
        'POR BOLETA
        If Im_TipoNC = 1 Then
                    Dp_Total = CDbl(Me.TxtTotalNC)
                    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                    Dp_Iva = Dp_Total - Dp_Neto
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "','" & TxtRut & "','" & TxtRazonSocial & "','VD'," & Dp_Neto & "," & Dp_Total & "," & Dp_Iva & "," & _
                                    "'ANULA DOCUMENTO','CONTADO',1,'EMPRESA',0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",0,'" & Fql(DtFecha) & "','" & LogUsuario & "',0" & LVDetalle.ListItems(1) & ",'" & _
                                    SP_Rut_Activo & "',99999,99999,99999,0,0,1,1," & _
                                    "'NC',0,1,'VTA'," & _
                                    "'SI'," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & ",1,'SI',0,0,1," & Lp_Folio & ",curtime(),'F'"
            
                    cn.Execute Sql
                        
                    
                    
                    
                   ' Dim Sp_Detalle_Boletas
                    
                    Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                            "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                            "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                                        "FROM ven_detalle " & _
                                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LVDetalle.ListItems(1).SubItems(2) & "  AND doc_id=" & LVDetalle.ListItems(1).SubItems(5)
                    If LVDetalle.ListItems.Count > 1 Then
                        For i = 2 To LVDetalle.ListItems.Count
                        
                            Sm_Sql_Productos_NC = Sm_Sql_Productos_NC & " UNION SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                                            "FROM ven_detalle " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LVDetalle.ListItems(i).SubItems(2) & "  AND doc_id=" & LVDetalle.ListItems(i).SubItems(5)
                        Next
                    
                    End If
                    
                    
                    
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) VALUES "
                    sql2 = ""
                    
                    
                    If LvVenta.ListItems.Count > 0 Then
                    
                        For i = 1 To LvVenta.ListItems.Count
                            If LvVenta.ListItems(i).Checked Then
                                 Dp_Total = CDbl(LvVenta.ListItems(i).SubItems(5))
                                 Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                                
                                sql2 = "SELECT precio_costo,pla_id,are_id,cen_id,aim_id " & _
                                        "FROM ven_detalle " & _
                                        "WHERE id=" & LvVenta.ListItems(i)
                                Consulta RsTmp, sql2
                                If RsTmp.RecordCount > 0 Then
                                    Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CxP(CDbl(LvVenta.ListItems(i).SubItems(3))) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                    CxP(RsTmp!precio_costo) & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & RsTmp!pla_id & "," & _
                                    RsTmp!are_id & "," & RsTmp!cen_id & "," & _
                                    RsTmp!aim_id & "," & Dp_Total & "," & Dp_Neto & "),"
                                End If
                                
                            End If
                        Next
                        
                        cn.Execute Mid(Sql, 1, Len(Sql) - 1)
                    Else
                        
                        For i = 1 To LVDetalle.ListItems.Count
                                
                            dp_netito = Round(CDbl(TxtTotalNC) / 1.19, 0)
                        
                            sql2 = sql2 & "(0,'','" & LVDetalle.ListItems(i).SubItems(1) & " " & LVDetalle.ListItems(i).SubItems(2) & "'," & CDbl(LVDetalle.ListItems(i).SubItems(4)) & ",0,1," & CDbl(LVDetalle.ListItems(i).SubItems(4)) & "," & CDbl(LVDetalle.ListItems(i).SubItems(4)) & "," & _
                                                "0,'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "',99999,99999,99999," & _
                                    "0 ," & LVDetalle.ListItems(i).SubItems(4) & "," & dp_netito & "),"
                
                        Next
                        cn.Execute Sql & Mid(sql2, 1, Len(sql2) - 1)
                    End If
                    
                
        Else
            ' NOTA DE CREDITO PARCIAL POR BOLETA
                    Dp_Total = CDbl(Me.TxtTotalNC)
                    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                    Dp_Iva = Dp_Total - Dp_Neto
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "','" & TxtRut & "','" & TxtRazonSocial & "','VD'," & Dp_Neto & "," & Dp_Total & "," & Dp_Iva & "," & _
                                    "'DEVOLUCION DE PRODUCTOS','CONTADO',1,'EMPRESA',0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",0,'" & Fql(DtFecha) & "','" & LogUsuario & "',0" & LVDetalle.ListItems(1) & ",'" & _
                                    SP_Rut_Activo & "',99999,99999,99999,0,0,1,1," & _
                                    "'NC',0,1,'VTA'," & _
                                    "'SI'," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & ",1,'SI',0,0,1," & Lp_Folio & ",curtime(),'F'"
            
                    cn.Execute Sql
                                         
                    
                    
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) VALUES "
                    For i = 1 To LvVenta.ListItems.Count
                        If LvVenta.ListItems(i).Checked Then
                             Dp_Total = CDbl(LvVenta.ListItems(i).SubItems(5))
                             Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                            
                            sql2 = "SELECT precio_costo,pla_id,are_id,cen_id,aim_id " & _
                                    "FROM ven_detalle " & _
                                    "WHERE id=" & LvVenta.ListItems(i)
                            Consulta RsTmp, sql2
                            If RsTmp.RecordCount > 0 Then
                                Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CxP(CDbl(LvVenta.ListItems(i).SubItems(3))) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                CxP(RsTmp!precio_costo) & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & RsTmp!pla_id & "," & _
                                RsTmp!are_id & "," & RsTmp!cen_id & "," & _
                                RsTmp!aim_id & "," & Dp_Total & "," & Dp_Neto & "),"
                            End If
                            
                        End If
                    Next
                    Sql = Mid(Sql, 1, Len(Sql) - 1)
                    cn.Execute Sql
                    
                    
                    
                       Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                            "FROM ven_detalle " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_Folio & "  AND doc_id=" & CboNC.ItemData(CboNC.ListIndex)
                    
                    
               
        
        
        End If
        
        
                
        If Me.Check1.Value = 1 Then
                NotaCreditoElectronica 61, Lp_Folio, CboNC.ItemData(CboNC.ListIndex), True
                'Folio ya no esta disponible
                cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                                    "WHERE dte_id=" & dte_id
        End If
        ActualizaNC
        
        'MsgBox "Se genero NC" & vbNewLine & "NRO:" & Lp_Folio, vbInformation
        LVDetalle.ListItems.Clear
       ' TxtTotalNC = 0
    
    End If
    
    
    '16 Enero 2016 _
    Solo para briones
    If SP_Rut_Activo = "3.630.608-4" Or SP_Rut_Activo = "76.178.895-7" Then
            If Principal.LvImpresorasDefecto.ListItems.Count > 0 Then
                If UCase(Printer.DeviceName) = UCase(Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)) Then
                        'SIGUE CON LA MISMA
                Else
                        EstableImpresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)
                End If
                'Copia empresa
                ImprimeFacturaTermica Str(Lp_Folio), CboNC.ItemData(CboNC.ListIndex), False, False, "NOTA DE CREDITO"
                ImprimeFacturaTermica Str(Lp_Folio), CboNC.ItemData(CboNC.ListIndex), False, True, "NOTA DE CREDITO"
            End If
    End If
    
    

    SkFoliosDisponibles = ""

        Sql = "SELECT count(dte_id) dte " & _
                "FROM dte_folios " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & 61 & " AND dte_disponible='SI' " & _
                "GROUP BY doc_id"
        Consulta RsTmp3, Sql
        If RsTmp3.RecordCount > 0 Then
            SkFoliosDisponibles = "Folios disponibles:" & RsTmp3!Dte
        End If
                        
                 
        
        MsgBox "Documento emitido... " & vbNewLine & "Nro : " & Lp_Folio & vbNewLine & SkFoliosDisponibles, vbInformation
    
    
    Sql = ""
    LVDetalle.ListItems.Clear
    TxtTotalNC = 0
    TxtRut = ""
    TxtDireccion = ""
    TxtGiro = ""
    txtFono = ""
    txtComuna = ""
    TxtCiudad = ""
    Me.TxtRazonSocial = ""
    Me.txtNroDocumentoBuscar = ""
    Me.txtTotalDocBuscado = ""
    LvVenta.ListItems.Clear
    LvVenta.Enabled = False
    Me.CboModalidadNC.ListIndex = 0
    TxtSubTotal = 0
    Me.TxtDescuentoNC = 0
    CmdAceptar.Enabled = True
    
    Me.Timer2.Enabled = False
    
    FraProcesandoDTE.Visible = False
    
End Sub

Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"
    
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    TxtRut.SetFocus
End Sub

Private Sub CmdBuscar_Click()
    For i = 1 To LVDetalle.ListItems.Count
        If LVDetalle.ListItems(i).SubItems(6) = 33 Then
            MsgBox "Es posible hacer notas de credito por una sola Factura Electronica...", vbInformation
            Exit Sub
        End If
    Next
    
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    
    If Val(txtNroDocumentoBuscar) = 0 Then
        MsgBox "Debe ingresar Nro para de " & CboDocVenta.Text & " ...", vbInformation
        txtNroDocumentoBuscar.SetFocus
        Exit Sub
    End If
    
    If Val(txtTotalDocBuscado) = 0 Then
        MsgBox "Ingrese valor del documento...", vbInformation
        txtTotalDocBuscado.SetFocus
        Exit Sub
    End If
    
    Filtro = ""
    If Sm_Nc_Boleta = "NO" Then
        Filtro = "rut_cliente='" & TxtRut & "' AND "
    
    
    End If
    
    Sql = "SELECT id,doc_nombre,no_documento,fecha,bruto,doc_cod_sii,v.doc_id " & _
                                "FROM ven_doc_venta v " & _
                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                "WHERE doc_nota_de_credito='NO' AND " & Filtro & "  v.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & txtNroDocumentoBuscar
 '   GoTo sigue
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        LVDetalle.ListItems.Add , , RsTmp!Id
        p = LVDetalle.ListItems.Count
        LVDetalle.ListItems(p).SubItems(1) = RsTmp!doc_nombre
        LVDetalle.ListItems(p).SubItems(2) = RsTmp!no_documento
        LVDetalle.ListItems(p).SubItems(3) = RsTmp!Fecha
        LVDetalle.ListItems(p).SubItems(4) = NumFormat(RsTmp!bruto)
        LVDetalle.ListItems(p).SubItems(5) = RsTmp!doc_id
        LVDetalle.ListItems(p).SubItems(6) = RsTmp!doc_cod_sii
        
    Else
sigue:
        If Sm_Nc_Boleta = "NO" Then
            MsgBox "Documento no encontrado para este cliente...", vbInformation
            CboDocVenta.SetFocus
            Exit Sub
        End If
    
        
        If MsgBox("Documento no se encontr� en los registros del sistema..." & vbNewLine & " �Agregar de todas formas...?", vbQuestion + vbOKCancel) = vbCancel Then Exit Sub
        Sql = "SELECT doc_cod_sii " & _
                "FROM sis_documentos " & _
                "WHERE doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex)
        Consulta RsTmp, Sql
        LVDetalle.ListItems.Add , , ""
        p = LVDetalle.ListItems.Count
        LVDetalle.ListItems(p).SubItems(1) = CboDocVenta.Text
        LVDetalle.ListItems(p).SubItems(2) = txtNroDocumentoBuscar
        LVDetalle.ListItems(p).SubItems(3) = DtFechaBoleta
        LVDetalle.ListItems(p).SubItems(4) = NumFormat(txtTotalDocBuscado)
        LVDetalle.ListItems(p).SubItems(5) = CboDocVenta.ItemData(CboDocVenta.ListIndex)
        LVDetalle.ListItems(p).SubItems(6) = RsTmp!doc_cod_sii
    End If
    SumaGrilla
    BuscaDescuentoNC
    txtTotalDocBuscado = ""
    txtFechaDocBuscado = ""
    txtTotalDocBuscado = ""
    CboDocVenta.SetFocus
    
End Sub

Private Sub CmdModificaValor_Click()
    Dim Lp_NuevoValor As Variant
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
        Lp_NuevoValor = InputBox("Ingrese Valor", "Cambiar monto", LVDetalle.SelectedItem.SubItems(4))
        If Val(Lp_NuevoValor) > 0 Then
            LVDetalle.SelectedItem.SubItems(4) = NumFormat(Val(Lp_NuevoValor))
        End If
    
        
        
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub



Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    CboCajaAbono.ListIndex = 0
    Sql = "SELECT bod_id " & _
            "FROM par_bodegas " & _
            "WHERE rut_emp='" & SP_Rut_Activo & "' AND sue_id=" & IG_id_Sucursal_Empresa
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        IG_id_Bodega_Ventas = RsTmp!bod_id
    End If
    Me.SkNCManual.Visible = False
    Me.TxtNroManual.Visible = False
    If SP_Rut_Activo = "77.774.580-8" Or SP_Rut_Activo = "76.039.757-1" Then
        LLenarCombo CboNC, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='SI'", "doc_orden"
        Me.SkNCManual.Visible = True
        Me.TxtNroManual.Visible = True
        
    Else
        LLenarCombo CboNC, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='SI' AND doc_dte='SI'", "doc_orden"
    End If
    CboNC.ListIndex = 0
  '  LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='SI'", "doc_orden"
  '  CboDocVenta.ListIndex = 0
    DtFecha = Date
    DtFechaBoleta = Date
    CboModalidadNC.ListIndex = 0
    
    If SP_Rut_Activo = "78.967.170-2" Then
        CmdModificaValor.Visible = True
    End If
   '      Me.CboModalidadNC.AddItem "PERSONALIZADA"
    
    
End Sub




Private Sub LvDetalle_Click()
  '  2 nro 5 id
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
    
    If Sm_Nc_Boleta = "NO" Then
        If LVDetalle.SelectedItem.SubItems(7) = "NO" Then
            Sql = "SELECT d.id,d.codigo,d.descripcion,unidades,precio_final,subtotal,(SELECT pro_inventariable FROM maestro_productos p WHERE p.codigo=d.codigo AND p.rut_emp='" & SP_Rut_Activo & "') pro_inventariable " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.no_documento =v.no_documento AND v.doc_id=d.doc_id " & _
                    "WHERE d.no_documento=" & LVDetalle.SelectedItem.SubItems(2) & " AND d.doc_id=" & LVDetalle.SelectedItem.SubItems(5) & " AND " & _
                    "d.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "'"
        Else
            Sql = "SELECT d.id,d.codigo,d.descripcion,SUM(unidades),ROUND(AVG(precio_final),0),SUM(subtotal) " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.no_documento =v.no_documento AND v.doc_id=d.doc_id " & _
                    "JOIN maestro_productos m ON d.codigo=m.codigo " & _
                    "WHERE v.nro_factura=" & LVDetalle.SelectedItem.SubItems(2) & " AND v.doc_id_factura=" & LVDetalle.SelectedItem.SubItems(5) & " AND " & _
                    "d.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "' " & _
                    "GROUP BY codigo"
        
        
        End If
        Consulta RsTmp, Sql
        
        LLenar_Grilla RsTmp, Me, LvVenta, True, True, True, False
        Me.ChkProductos.Value = 1
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        
        Next
        
        BuscaDescuentoNC
        
    Else
        'Por boleta
        Sql = "SELECT d.id,d.codigo,d.descripcion,unidades,precio_final,subtotal,pro_inventariable " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.no_documento =v.no_documento AND v.doc_id=d.doc_id " & _
                    "JOIN maestro_productos m ON d.codigo=m.codigo " & _
                    "WHERE d.no_documento=" & LVDetalle.SelectedItem.SubItems(2) & " AND d.doc_id=" & LVDetalle.SelectedItem.SubItems(5) & " AND " & _
                    "d.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        
        LLenar_Grilla RsTmp, Me, LvVenta, True, True, True, False
        Me.ChkProductos.Value = 1
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        
        Next
        
        BuscaDescuentoNC
    
    
    
    End If
   TxtTotalNC = LVDetalle.ListItems(1).SubItems(4)
    
    
    
End Sub

Private Sub LvDetalle_DblClick()
    If LVDetalle.SelectedItem Is Nothing Then Exit Sub
    
    Busca_Id_Combo CboDocVenta, Val(LVDetalle.SelectedItem.SubItems(5))
    txtNroDocumentoBuscar = LVDetalle.SelectedItem.SubItems(2)
    txtFechaDocBuscado = LVDetalle.SelectedItem.SubItems(3)
    txtTotalDocBuscado = LVDetalle.SelectedItem.SubItems(4)
    
    LVDetalle.ListItems.Remove LVDetalle.SelectedItem.Index
    
    CboDocVenta.SetFocus
End Sub





Private Sub LvVenta_DblClick()
        If LvVenta.SelectedItem Is Nothing Then Exit Sub
        With LvVenta
            TxtCodigo.Tag = LvVenta.SelectedItem
            TxtCodigo = .SelectedItem.SubItems(1)
            Me.TxtDescripcion = .SelectedItem.SubItems(2)
            TxtCantidad = .SelectedItem.SubItems(3)
            TxtPrecio = .SelectedItem.SubItems(4)
            TxtTotalLinea = .SelectedItem.SubItems(5)
            TxtStock = .SelectedItem.SubItems(6)
            TxtCodigo.SetFocus
            LvVenta.ListItems.Remove .SelectedItem.Index
        End With
End Sub

Private Sub LvVenta_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    SumaSeleccion
End Sub



Private Sub Timer2_Timer()
    Me.XP_ProgressBar1.Value = Me.XP_ProgressBar1.Value + 10
    If XP_ProgressBar1.Value = 100 Then XP_ProgressBar1.Value = 1
End Sub

Private Sub txtCantidad_GotFocus()
    En_Foco TxtCantidad
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub txtCantidad_Validate(Cancel As Boolean)
 '   Dim Dp_Cantidad As decim
    If Val(CxP(TxtCantidad)) > 0 Then
        CalculaCantPrecio
    Else
       TxtCantidad = "0"
    End If
End Sub
Private Sub CalculaCantPrecio()
    If Val(CxP(TxtCantidad)) > 0 Then
        TxtCantidad = Format(TxtCantidad, "#0.000")
        'Dp_Cantidad = TxtCantidad
        TxtTotalLinea = NumFormat(TxtCantidad * CDbl(TxtPrecio))
    End If
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)

    If TxtCodigo = "DESCNC" Then
        TxtDescripcion = "DESCUENTO CLIENTE"
        Exit Sub
    End If


    If Me.CboModalidadNC.ListIndex = -1 Then Exit Sub
    
    If Me.CboModalidadNC.ListIndex < 2 Then
        Sql = "SELECT d.id " & _
                "FROM ven_detalle d " & _
                "WHERE d.no_documento=" & LVDetalle.SelectedItem.SubItems(2) & " AND d.doc_id=" & LVDetalle.SelectedItem.SubItems(5) & " AND " & _
                "d.rut_emp='" & SP_Rut_Activo & "' AND codigo='" & TxtCodigo & "'"
                
                
        Consulta RsTmp, Sql
        If RsTmp.RecordCount = 0 Then
            
        
        
            'MsgBox "Producto no encontrado en su Nota de Credito...", vbInformation
            'TxtCodigo = ""
        End If
    Else
        '
    
    
    End If
End Sub

Private Sub txtNroDocumentoBuscar_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub txtComuna_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtDireccion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtGiro_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtPrecio_GotFocus()
    En_Foco TxtPrecio
End Sub


Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub


Private Sub TxtPrecio_Validate(Cancel As Boolean)
    If Val(TxtPrecio) = 0 Then
        TxtPrecio = 0
    Else
    
        TxtPrecio = NumFormat(TxtPrecio)
        CalculaCantPrecio
    End If
End Sub

Private Sub TxtRazonSocial_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Exit Sub
    End If
    
    
    If ClienteEncontrado Then
     
      
        
    Else
       ' TxtRut.SetFocus
    End If
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
    
    If CboNC.ListIndex = -1 Then
        MsgBox "Selecione Nota de Credito...", vbOKCancel
        CboNC.SetFocus
        Exit Sub
    End If
    
    LVDetalle.ListItems.Clear
    
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
   
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
        Sql = "SELECT rut_cliente,nombre_rsocial,giro,direccion,comuna,fono,email, " & _
               "cli_monto_credito " & _
              "FROM maestro_clientes m " & _
              "INNER JOIN par_asociacion_ruts  a ON m.rut_cliente=a.rut_cli " & _
              "LEFT JOIN par_lista_precios l ON m.lst_id=l.lst_id " & _
              "WHERE habilitado='SI' AND a.rut_emp='" & SP_Rut_Activo & "' AND rut_cliente = '" & Me.TxtRut.Text & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtMontoCredito = !cli_monto_credito
         
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
              
                
     
                
                If Val(TxtMontoCredito) > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                    TxtCupoUtilizado = ConsultaSaldoCliente(TxtRut)
                    SkCupoCredito = NumFormat(Val(TxtMontoCredito) - Val(TxtCupoUtilizado))
                End If
                
                
                'buscaremos documentos emitidos a este cliente
                '23-9-2015
                Filtro = "doc_nc_por_boleta='" & Sm_Nc_Boleta & "' AND "
                Sql = "SELECT v.id,fecha,CONCAT(tipo_doc,' ',IF(doc_factura_guias='SI','POR GUIA(S)','')) tipo_d ,no_documento,v.rut_cliente,IFNULL(cli_nombre_fantasia,nombre_rsocial) nombre,neto,iva,bruto " & _
                      "FROM ven_doc_venta v,sis_documentos d,maestro_clientes c " & _
                      "WHERE doc_nota_de_credito='NO' AND " & Filtro & "v.rut_emp='" & SP_Rut_Activo & "' AND c.rut_cliente=v.rut_cliente AND v.doc_id=d.doc_id AND doc_referenciable='SI' AND v.rut_cliente='" & TxtRut & "' " & _
                      "ORDER BY id DESC"
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    iP_Tipo_NC = 0
                    SG_codigo2 = CboNC.ItemData(CboNC.ListIndex)
                    vta_BuscaDocumentos.SM_SoloSelecciona = "SI"
                    vta_BuscaDocumentos.CmdContinuar.Visible = False
                    vta_BuscaDocumentos.CmdNC.Visible = True
                    vta_BuscaDocumentos.B_Seleccion = True
                    'ver documentos del cliente
                    
                    vta_BuscaDocumentos.Show 1
                    If venPosNC.iP_Tipo_NC > 0 Then
                        Sql = "SELECT id,doc_nombre,no_documento,fecha,bruto,v.doc_id,doc_cod_sii,doc_factura_guias,doc_bruto_neto_en_venta netobruto " & _
                                "FROM ven_doc_venta v " & _
                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                "WHERE id=" & venPosNC.lP_Documento_Referenciado
                         Consulta RsTmp, Sql
                         If RsTmp.RecordCount > 0 Then
                            TxtTotalNC = NumFormat(RsTmp!bruto)
                            LLenar_Grilla RsTmp, Me, LVDetalle, False, True, True, False
                            SumaGrilla
                         
                         End If
                         
                    End If
                    
                    'if DG_ID_Unico = Me.LP_Documento_Referenciado
                Else
                    If Sm_Nc_Boleta = "NO" Then
                        MsgBox "El cliente no tiene facturas registradas en el sistema...", vbInformation
                        TxtRut.SetFocus
                        Exit Sub
                    End If
                    
                End If
                
                
                
                
            End With
                
            If bm_SoloVistaDoc Then Exit Sub
                                
        
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        TxtRut_Validate False
                       
                        TxtCodigo.SetFocus
                    Else
                        TxtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        
          
        End If
       
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub

Private Sub txtTotalDocBuscado_GotFocus()
    En_Foco txtTotalDocBuscado
End Sub

Private Sub txtTotalDocBuscado_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub txtTotalDocBuscado_Validate(Cancel As Boolean)
    If Val(txtTotalDocBuscado) = 0 Then txtTotalDocBuscado = "0"
End Sub
Private Sub SumaGrilla()
    TxtTotalNC = NumFormat(TotalizaColumna(LVDetalle, "total"))
End Sub
Private Sub NotaCreditoElectronica(Tipo As String, NroDocumento As Long, Docid As Integer, PorBoletas As Boolean)
    Dim Sp_Xml As String, Sp_XmlAdicional As String
    Dim obj As DTECloud.Integracion
    Dim Lp_Sangria As Long
    Dim Sp_Sql As String
    Dim Bp_Adicional As Boolean
    Dim Dp_Neto As Long
    Dim Dp_Iva As Long
    Dim Dp_Total As Long
    '3 Octubre 2015 Pos
    'Modulo genera NC electronica
    'Autor: alvamar
    Bp_Adicional = False
    
    '1ro Crear xml
    Lp_Sangria = 4
    X = FreeFile
    
   ' Sp_Xml = App.Path & "\dte\" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & "_" & TxtNroDocumento & ".xml"
    Sp_Xml = "C:\FACTURAELECTRONICA\nueva.xml"
      'Open Sp_Archivo For Output As X
    Open Sp_Xml For Output As X
        '<?xml version="1.0" encoding="ISO-8859-1"?>
        Print #X, "<DTE version=" & Chr(34) & "1.0" & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria) & "<Documento ID=" & Chr(34) & "ID" & Lp_Id_Venta & Chr(34) & ">"
        'Encabezado
        Print #X, "" & Space(Lp_Sangria) & "<Encabezado>"
            'id documento
            Print #X, "" & Space(Lp_Sangria * 2) & "<IdDoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDTE>" & Tipo & "</TipoDTE>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Folio>" & NroDocumento & "</Folio>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchEmis>" & Fql(DtFecha) & "</FchEmis>"
           '     Print #X, "" & Space(Lp_Sangria * 3) & "<FchVenc>" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "; YYYY - MM - DD; ") & "</FchVenc>"
                If Sm_Con_Precios_Brutos = "SI" Then
                    '30 Mayo 2015, cuando las lineas de detalle son precios brutos se debe identificar aqui.
                 '   Print #X, "" & Space(Lp_Sangria * 2) & "<MntBruto>1</MntBruto>"
                End If
            Print #X, "" & Space(Lp_Sangria * 2) & "</IdDoc>"
            'Emisor
            'Select empresa para obtener los datos del emisor
            Sp_Sql = "SELECT nombre_empresa,direccion,comuna,ciudad,giro,emp_codigo_actividad_economica actividad " & _
                    "FROM sis_empresas " & _
                    "WHERE rut='" & SP_Rut_Activo & "'"
            Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Emisor>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSoc>" & Replace(RsTmp!nombre_empresa, "�", "N") & "</RznSoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroEmis>" & Replace(RsTmp!giro, "�", "N") & "</GiroEmis>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Acteco>" & Replace(RsTmp!actividad, "�", "N") & "</Acteco>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirOrigen>" & Replace(RsTmp!direccion, "�", "N") & "</DirOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaOrigen>" & Replace(RsTmp!comuna, "�", "N") & "</CmnaOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadOrigen>" & Replace(RsTmp!ciudad, "�", "N") & "</CiudadOrigen>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Emisor>"
            
            'Receptor
            
            'Select datos cliente
            Sp_Sql = "SELECT nombre_rsocial,giro,direccion,comuna,ciudad " & _
                        "FROM maestro_clientes " & _
                        "WHERE rut_cliente='" & TxtRut & "'"
            Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Receptor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTRecep>" & Replace(TxtRut, ".", "") & "</RUTRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSocRecep>" & QuitarAcentos(Mid(Replace(RsTmp!nombre_rsocial, "�", "N"), 1, 100)) & "</RznSocRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroRecep>" & QuitarAcentos(Mid(Replace(RsTmp!giro, "�", "N"), 1, 40)) & "</GiroRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Contacto></Contacto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirRecep>" & QuitarAcentos(Mid(Replace(RsTmp!direccion, "�", "N"), 1, 60)) & "</DirRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaRecep>" & QuitarAcentos(Mid(Replace(RsTmp!comuna, "�", "N"), 1, 20)) & "</CmnaRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadRecep>" & QuitarAcentos(Mid(Replace(RsTmp!ciudad, "�", "N"), 1, 20)) & "</CiudadRecep>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Receptor>"
            
             'Totales
            Dp_Total = CDbl(TxtTotalNC)
            Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
            Dp_Iva = Dp_Total - Dp_Neto
             
            Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & Dp_Neto & "</MntNeto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & 0 & "</MntExe>"
                Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>" & DG_IVA & "</TasaIVA>"
                Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>" & Dp_Iva & "</IVA>"
                    'Antes del monto total debemos verificar si la factura incluye impuestos retenidos
    '                                         <ImptoReten>
    '                                            <TipoImp>18</TipoImp>
    '                                            <TasaImp>5</TasaImp>
    '                                            <MontoImp>8887</MontoImp>
    '                                        </ImptoReten>
                Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & Dp_Total & "</MntTotal>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
                
            'Cerramos encabezado
            Print #X, "" & Space(Lp_Sangria) & "</Encabezado>"
                
                
           'Comenzamos el detalle
           
           If SP_Control_Inventario = "SI" Then
           'Recorremos la grilla con productos
           
                If Im_TipoNC = 1 Then
                        'SI ES NC POR ANULACION,
                        'LEEMOS LOS MISMO DATOS DE LA VENTA
                    '    If LvDetalle.SelectedItem.SubItems(7) = "NO" Then
                            Sql = "SELECT codigo,unidades,descripcion,precio_final,ved_codigo_interno,ved_precio_venta_neto,ROUND(ved_precio_venta_neto/unidades,2) unitario  " & _
                                    "FROM ven_detalle " & _
                                    "WHERE doc_id=" & Docid & " AND no_documento=" & NroDocumento & " AND rut_emp='" & SP_Rut_Activo & "'"
                    '    Else
                    '        Sql = "SELECT unidades,descripcion,precio_final,ved_codigo_interno,ved_precio_venta_neto,ROUND(ved_precio_venta_neto/unidades,2) unitario  " & _
                    '                "FROM ven_detalle d " & _
                                    "JOIN  ven_doc_venta v ON d.doc_id=v.doc_id AND d.no_documento=v.no_documento " & _
                                    "WHERE doc_id_factura=" & Docid & " AND nro_factura=" & NroDocumento & " d.AND rut_emp='" & SP_Rut_Activo & "' AND v.rut_activo='" & SP_Rut_Activo & "'"
                        
                    '    End If
                        Consulta RsTmp, Sql
                        i = 1
                        RsTmp.MoveFirst
                        Do While Not RsTmp.EOF
                            'For i = 1 To LvMateriales.ListItems.Count
                              Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & i & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & "" & RsTmp!Codigo & "</VlrCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & QuitarAcentos(Replace(Mid(RsTmp!Descripcion, 1, 80), "�", "N")) & "</NmbItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & Replace(RsTmp!Unidades, ",", ".") & "</QtyItem>"
                       '       Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & Replace(RsTmp!unitario, ",", ".") & "</PrcItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & Replace(Round(RsTmp!unitario, 0), ",", ".") & "</PrcItem>"
              '                    <DescuentoPct>15</DescuentoPct>
              '                    <DescuentoMonto>4212</DescuentoMonto>
              '                             <!- Solo se indica el c�digo del impuesto adicional '
              '                    <CodImpAdic>17</CodImpAdic>
                              Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & Replace(Round(RsTmp!ved_precio_venta_neto, 0), ",", ".") & "</MontoItem>"
                      
                            Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                             'Next
                            RsTmp.MoveNext
                            i = i + 1
                        Loop
                        
                         dsctoGeneral = CDbl(TxtDescuentoNC)
                        If dsctoGeneral > 0 Then
                            dsctoGeneral = Round(dsctoGeneral / (1.19), 0)
                            Print #X, "" & Space(Lp_Sangria) & "<DscRcgGlobal>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDR>1</NroLinDR>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<TpoMov>D</TpoMov>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<GlosaDR>Dscto. Cliente</GlosaDR>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<TpoValor>$</TpoValor>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<ValorDR>" & dsctoGeneral & "</ValorDR>"
                            Print #X, "" & Space(Lp_Sangria) & "</DscRcgGlobal>"
                        
                                '<DscRcgGlobal>
                                '    <NroLinDR>1</NroLinDR>
                                '    <TpoMov>D</TpoMov>
                                '    <GlosaDR>Descuento global al documento</GlosaDR>
                                ''    <TpoValor>%</TpoValor>
                                 '   <ValorDR>12</ValorDR>
                                '</DscRcgGlobal>
                        End If
                        
                        
                        
                        
                        
                        
                        
                Else
                    'CUANDO ES PARCIAL, SOLO SE HARA POR LOS PRODUCTOS QUE ESTA SACANDO
                    'LOS QUE ESTAN EN LA GRILLA MARCADOS
                    n = 1
                    For i = 1 To LvVenta.ListItems.Count
                        If LvVenta.ListItems(i).Checked Then
                            Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & n & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & LvVenta.ListItems(i).SubItems(1) & "</VlrCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & QuitarAcentos(LvVenta.ListItems(i).SubItems(2)) & "</NmbItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & CxP(LvVenta.ListItems(i).SubItems(3)) & "</QtyItem>"
                              If LVDetalle.ListItems(1).SubItems(8) = "BRUTO" Then
                                
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & Round(CDbl(LvVenta.ListItems(i).SubItems(4)) / (1.19), 0) & "</PrcItem>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & Round(CDbl(LvVenta.ListItems(i).SubItems(5)) / (1.19), 0) & "</MontoItem>"
                              Else
                                  
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & CDbl(LvVenta.ListItems(i).SubItems(4)) & "</PrcItem>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & CDbl(LvVenta.ListItems(i).SubItems(5)) & "</MontoItem>"
                              End If
                              
                            Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                            n = n + 1
                        
                        End If
                        
                    Next
                
                
                
                
                End If
            Else
                'CUANDO ES SIN INVENTARIO
                
            
            
            
            End If
            'Cerramos detalle
            
                
'        If FrmReferencia.Visible Then
'                'CREAR REFERENCIA
                Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
                    If Not PorBoletas Then
                        'NC por factura
                        
                     
                            Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>33</TpoDocRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & LVDetalle.ListItems(1).SubItems(2) & "</FolioRef>"
        
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(LVDetalle.ListItems(1).SubItems(3)) & "</FchRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & Im_TipoNC & "</CodRef>"
                            If Im_TipoNC = 1 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>ANULA DOCUMENTO</RazonRef>"
                            Else
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>DEVOLUCION DE MERCADERIAS</RazonRef>"
                            End If
                    Else
                        
                            Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>35</TpoDocRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & LVDetalle.ListItems(1).SubItems(2) & "</FolioRef>"
        
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(LVDetalle.ListItems(1).SubItems(3)) & "</FchRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & Im_TipoNC & "</CodRef>"
                            If Im_TipoNC = 1 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>ANULA DOCUMENTO</RazonRef>"
                            Else
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>DEVOLUCION DE MERCADERIAS</RazonRef>"
                            End If
                        'Nc por boletas
                        
                        
                    End If
                    
                 Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
                        
'                    If SkCodigoReferencia = 4 Then SkCodigoReferencia = 3
'                    SkFechaReferencia = Format(Me.SkFechaReferencia, "YYYY-MM-DD")
'                          '1  SkDocIdReferencia = 33
'
'                    Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>" & SkDocIdReferencia & "</TpoDocRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & TxtNroReferencia & "</FolioRef>"
'
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & SkFechaReferencia & "</FchRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & SkCodigoReferencia & "</CodRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>" & TxtMotivoRef & "</RazonRef>"
'
'                Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
'
'
'            'CERRAR REFERNCIA
'        End If

        Print #X, "" & Space(Lp_Sangria) & "</Documento>"
        Print #X, "" & Space(Lp_Sangria) & "</DTE>"
    Close #X
        Bp_Adicional = False
'        End
'    If Len(TxtComentario) > 0 Or Len(TxtOrdenesCompra) > 0 Then
'        Bp_Adicional = True
'        X = FreeFile
'        Sp_XmlAdicional = "C:\nueva_adicional.xml"
'        Open Sp_XmlAdicional For Output As X
'            Print #X, "<Adicional>"
'            If Len(TxtComentario) > 0 Then
'                Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtComentario & "</Uno>"
'                If Len(TxtOrdenesCompra) > 0 Then
'                    Print #X, "" & Space(Lp_Sangria) & "<Dos>" & TxtOrdenesCompra & "</Dos>"
'                End If
'            Else
'                If Len(TxtOrdenesCompra) > 0 Then
'                    Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtOrdenesCompra & "</Uno>"
'                End If
'            End If
'            Print #X, "" & Space(Lp_Sangria) & "<Tres></Tres>"
'            Print #X, "" & Space(Lp_Sangria) & "<Cuatro></Cuatro>"
'            Print #X, "" & Space(Lp_Sangria) & "<Cinco></Cinco>"
'            Print #X, "" & Space(Lp_Sangria) & "<Seis></Seis>"
'            Print #X, "" & Space(Lp_Sangria) & "<Siete></Siete>"
'            Print #X, "" & Space(Lp_Sangria) & "<Ocho></Ocho>"
'            Print #X, "" & Space(Lp_Sangria) & "<Nueve></Nueve>"
'            Print #X, "" & Space(Lp_Sangria) & "<Diez></Diez>"
'            Print #X, "</Adicional>"
'        Close #X
'
'
'    End If
        
        Set obj = New DTECloud.Integracion
   
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica '"http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj.HabilitarDescarga = True
        obj.FormatoNombrePDF = "0"
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF"
        obj.Password = "1234"
        
        obj.AsignarFolio = False
        obj.IncluirCopiaCesion = True
       ' obj.FolioAsignado
        
        
        'MSXML.DOMDocument
        Dim Documento As MSXML2.DOMDocument30
        Set Documento = New DOMDocument30
        
        Documento.preserveWhiteSpace = True
        Documento.Load (Sp_Xml)
        '  Dim oXMLAdicional As New xml.XMLDocument
        Dim documento2 As MSXML2.DOMDocument30
        Set documento2 = New DOMDocument30
        
        documento2.preserveWhiteSpace = True
        If Bp_Adicional Then
            documento2.Load (Sp_XmlAdicional)
        Else
            documento2.Load ("")
        End If
        
        '******************************* modo productivo
        
        obj.Productivo = BG_ModoProductivoDTE
        
        '***********************************
        If obj.ProcesarXMLBasico(Documento.XML, documento2.XML) Then
             'MsgBox obj.URLPDF
             Archivo = "C:\FACTURAELECTRONICA\PDF\T" & Tipo & "F" & NroDocumento & ".PDF"
            ' dte_MostrarPDF.Dte "33", Str(NroDocumento)
         '   If SP_Rut_Activo = "76.178.895-7" Then 'Solo total gomas
         '       DescargaForzadaDTE Trim(Str(NroDocumento))
            
         '   Else
                '16-09-2015
                'Se supone que esto es lo normal, sin embargo, totalgomas no funcionan
               ShellExecute Me.hWnd, "open", Archivo, "", "", 4
          '  End If
            
            
            ' MsgBox obj.URLPDF
        Else
            MsgBox (CStr(obj.IDResultado) + " " + obj.DescripcionResultado)
        End If
    
        

        
End Sub


Private Sub AbonaNCCredito(IdDocumento As String)
    Dim Lp_SaldoRef As Long
    Dim Lp_Id As Long
    Dim Lp_IdAbo As Long
    Dim Lp_Nro_Comprobante As Long
    
  'Buscar el documento referenciado    /// 20-10-2012
            'y ver si su saldo.
            'si el saldo es mayor o igual a la NC abonar el monto de la nota de credito
            
            Lp_SaldoRef = SaldoDocumento(IdDocumento, "CLI")
            
            Dim Lp_Abono_a_Realizar As Long
            Dim Lp_Abono_a_Pozo As Long
            Lp_Id = Val(IdDocumento)
            'If Lp_SaldoRef >= CDbl(TxtTotal) Then
            If Lp_SaldoRef > 0 Then
                    Bm_AfectarCaja = False
                    'hacer abono
                    lp_abono_a_plazo = 0
                    
                    If Lp_SaldoRef >= CDbl(Me.TxtTotalNC) Then
                        Lp_Abono_a_Realizar = CDbl(TxtTotalNC)
                    Else
                        Lp_Abono_a_Realizar = Lp_SaldoRef
                        Lp_Abono_a_Pozo = CDbl(TxtTotalNC) - Lp_SaldoRef
                    End If

                    
                    
                    MsgBox "El monto " & NumFormat(Lp_Abono_a_Realizar) & " de esta NC se abono al documento original...", vbInformation
                    Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
                
                    'Dim lp_IDAbo As Long
                    Bp_Comprobante_Pago = True
                    Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                    PagoDocumento Lp_IdAbo, "CLI", TxtRut, DtFecha, CDbl(Lp_Abono_a_Realizar), 8888, "NC", "ABONO CON NC", LogUsuario, Lp_Nro_Comprobante, Lp_Id, "NCVENTA"
                    Sql = "INSERT INTO cta_abono_documentos (abo_id,id,ctd_monto,rut_emp) VALUES "
                    Sql = Sql & "(" & Lp_IdAbo & "," & IdDocumento & "," & CDbl(Lp_Abono_a_Realizar) & ",'" & SP_Rut_Activo & "')"
                    cn.Execute Sql
                    
                    AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                    
                    Sp_NC_Utilizada = "SI"
                    If Lp_Abono_a_Pozo > 0 Then
                        
                        Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                            "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & Me.TxtRut & "'," & Lp_Abono_a_Pozo & ")"
                        cn.Execute Sql
                    
                    End If
                    
                    
            Else
                    'PagoDocumento Lp_Id, "PRO", Me.TxtRutProveedor, Me.DtFecha, Lp_SaldoRef, 8888, "DESCUENTA NC", "", LogUsuario,
                    Sql = "INSERT INTO cta_pozo (id_ref,pzo_fecha,rut_emp,pzo_cli_pro,pzo_rut,pzo_abono) " & _
                        "VALUES(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & TxtRut & "'," & Lp_Abono_a_Pozo & ")"
                    cn.Execute Sql
                    Sp_NC_Utilizada = "SI"
            End If
End Sub

Private Sub ImprimeFacturaTermica(Numero As String, Doc As String, Cedible As Boolean, Firma As Boolean, NombreDocumento As String)
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_Cantidad As String * 7
    Dim Sp_Detalle As String * 40
    Dim Sp_Codigo As String * 10
    Dim Sp_Unitario As String * 11
    Dim Sp_Total_linea As String * 11
    Dim Sp_GuiasFacturadas As String
    Dim Sp_Segun_Guias As String
    Dim Sp_Sql As String
    Dim Sp_Neto_P As String * 10
    Dim Sp_IVA_P As String * 10
    Dim Sp_Total_P As String * 10
    
    
    RSet Sp_Neto_P = "NETO    $:"
    RSet Sp_IVA_P = "IVA " & DG_IVA & "% $:"
    RSet Sp_Total_P = "TOTAL   $:"
    Printer.DrawWidth = 2
    Printer.FontName = "Courier New"
    Printer.FontBold = False
    Printer.ScaleMode = vbCentimeters
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Printer.FontBold = False
    Cx = 0.5
    'Cy = 5.9
    Cy = 1
    Dp_S = 0.17 'interlinea
    Printer.FontName = "Arial"
    Printer.FontSize = 11
    Printer.FontBold = True
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print "                  REPUESTOS BRIONES"
    Else
    
        Printer.Print "                     TOTALGOMAS"
    End If
    linini = Printer.CurrentY + 0.1
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.CurrentX = Cx
    Printer.FontSize = 10
    Printer.FontName = "Courier"
    
    Printer.Print "                R.U.T.:" & SP_Rut_Activo
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    Printer.Print "       NOTA DE CREDITO"
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    
    Printer.Print "                    N� " & Numero
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    linfinm = Printer.CurrentY
    
    Printer.Print "                      S.I.I. - TEMUCO"
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    
    Printer.Line (Cy, linini)-(6.7, linfinm), , B  'Rectangulo Encabezado y N� Nota de Venta
     
    
    Printer.FontBold = False
    Printer.FontSize = 9
     
    Printer.CurrentX = Cx
    Printer.CurrentY = linfinm + 0.7
    
    
    Printer.FontBold = True
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print " SERGIO ADOLFO BRIONES VALDERRAMA"
    Else
        Printer.Print " SOC. COMERCIAL TOTALGOMAS LTDA."
    End If
 '   Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    Printer.FontBold = False
    
        
    Printer.Print " VENTA DE PARTES, PIEZAS Y ACCESORIOS"
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
        
    Printer.Print " DE VEHICULOS AUTOMOTORES."
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print " MANUEL MONTT 1117 - TEMUCO - TEMUCO"
    Else
        Printer.Print " MANUEL MONTT 1299 - TEMUCO - TEMUCO"
    End If
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    If SP_Rut_Activo = "3.630.608-4" Then
    
    Else
        Printer.Print " FONO 0452730022"
    End If
   ' Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print "repuestosbrionestemuco@gmail.com"
    Else
        Printer.Print " ventastotalgomas@gmail.com"
    End If
    'Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    
    Printer.FontName = "Arial"
    Printer.FontSize = "8"
    
    md = 1.3
    'pos = Printer.CurrentY
    Sp_Fecha = Date
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + 0.6
    Cy = Printer.CurrentY
    Printer.FontBold = True
    Printer.Print "FECHA:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    
    Printer.Print Day(Date)
    Printer.CurrentX = Cx + md + 0.5
    Printer.CurrentY = Cy
    Printer.Print MonthName(Month(Date))
    Printer.CurrentX = Cx + md + 2.5
    Printer.CurrentY = Cy
    Printer.Print Year(Date)
    
    'Printer.Print Format(Sp_Fecha, "dd mmmm yyyy")
   ' Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
   ' Printer.Print Space(43) & Mid(Sp_Fecha, 1, 2) & Space(7) & UCase(MonthName(Ip_Mes))
   ' Printer.CurrentX = Cx + 6.7
   ' Printer.CurrentY = pos
   ' Printer.Print Space(49) & Mid(Sp_Fecha, 9, 2)  'A�O DE LA FECHA
   ' Printer.CurrentX = Cx + 0.6
   ' 'Printer.CurrentY = Printer.CurrentY + Dp_S
   ' Printer.CurrentY = Printer.CurrentY + Dp_S + 0.1
    
    
    
    
    
    
    'pos = Printer.CurrentY
    
    
     'Select datos cliente
    Sp_Sql = "SELECT nombre_rsocial,giro,direccion,comuna,ciudad " & _
                        "FROM maestro_clientes " & _
                        "WHERE rut_cliente='" & TxtRut & "'"
    Consulta RsTmp, Sp_Sql
    
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "RUT:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print TxtRut
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "R. Social:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!nombre_rsocial
    
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Giro.:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!giro
        
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Direcc.:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!direccion
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Comuna:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!comuna
    
    

    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Ciudad:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!ciudad
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "C. Pago:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print Sm_Fpago
    
    
    Printer.CurrentY = Printer.CurrentY + 0.6
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Cant   / Codigo / Descripcion        / Valor"
    Printer.FontBold = False
    
    
    Cy = Printer.CurrentY + 2
    
     Sql = "SELECT unidades,descripcion,precio_final,ved_codigo_interno " & _
            "FROM ven_detalle " & _
            "WHERE doc_id=" & Doc & " AND no_documento=" & Numero & " AND rut_emp='" & SP_Rut_Activo & "'"
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
          '  For i = 1 To LvMateriales.ListItems.Count
                '6 - 3 - 7 - 8
                Printer.CurrentY = Printer.CurrentY + 0.11
                'Printer.CurrentX = Cx - 1.5
                
                Printer.CurrentX = Cx - 1
                
                
                'Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & Space(7) & _
                        Left(LvMateriales.ListItems(i).SubItems(3) & Space(44), 44) & Space(3) & _
                        Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(7)), 11) & Space(2) & _
                        Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(8)), 11)
                RSet Sp_Cantidad = RsTmp!Unidades
                LSet Sp_Detalle = RsTmp!Descripcion
                LSet Sp_Codigo = "" & RsTmp!ved_codigo_interno
                RSet Sp_Unitario = NumFormat(RsTmp!precio_final)
                RSet Sp_Total_linea = NumFormat(Round(RsTmp!precio_final * RsTmp!Unidades, 0))
                
                Printer.FontName = "Arial"
                Cy = Printer.CurrentY
                Printer.CurrentX = Cx - 0.3
                Printer.FontBold = True
                pos = Printer.CurrentY
                Printer.Print Sp_Cantidad & Space(2) & " x codigo:" & Sp_Codigo
                
                
                
                Printer.CurrentY = pos
                Printer.CurrentX = Cx + 4.1
                Printer.Print Sp_Unitario
                Printer.CurrentY = pos
                Printer.CurrentX = Cx + 5.4
                Printer.Print Sp_Total_linea
                
               
                Printer.FontBold = False
                Printer.CurrentX = Cx - 0.3
                Printer.CurrentY = Cy + 0.35
                pos = Printer.CurrentY
                Printer.Print Sp_Detalle
                
                
                
               ' Printer.Print Sp_Cantidad & Space(1) & Sp_Detalle & Space(1) & Sp_Codigo & Space(1) & Sp_Unitario & Space(1) & Sp_Total_linea
                 
                        
           ' Next
                RsTmp.MoveNext
         Loop
    End If
     Printer.FontSize = 8
    Printer.FontBold = True
    
    Printer.CurrentY = Printer.CurrentY + 0.7
     Printer.FontName = "Courier New"
     
     
     'Descunto
  '  If Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)) > 0 Then
  '
  '
  '          Printer.CurrentX = Cx + 2
  '          Printer.Print "Descuento $:" & " " & NumFormat(Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)))
  '          Printer.CurrentY = Printer.CurrentY + 0.3
  '
  '
  '  End If
     
    
      'Totales
    Dp_Total = CDbl(TxtTotalNC)
    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
    Dp_Iva = Dp_Total - Dp_Neto
    
    Printer.FontBold = True
    
    RSet Sp_Neto = NumFormat(Dp_Neto)
    RSet Sp_IVA = NumFormat(Dp_Iva)
    RSet Sp_Total = NumFormat(Dp_Total)
    Printer.CurrentX = Cx + 1
    linini = Printer.CurrentY - 0.1
    Printer.Print Sp_Neto_P & " " & Sp_Neto   'SI ES GUIA NO IMPRIME NETO IVA TOTAL, SOLO NETO

        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.Print Sp_IVA_P & " " & Sp_IVA
    Printer.CurrentX = Cx + 1
    
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.Print Sp_Total_P & " " & Sp_Total
    linfin = Printer.CurrentY + 0.1
    
    Printer.Line (Cx + 0.5, linini)-(6, linfin), , B 'Rectangulo Encabezado y N� Nota de Venta

    
    Printer.FontSize = 8
  
    Printer.CurrentY = Printer.CurrentY + 0.3
    pos = Printer.CurrentY
    Sp_Letras = UCase(BrutoAletras(CDbl(Dp_Total), True))
    mitad = Len(Sp_Letras) \ 2
    buscaespacio = InStr(mitad, Sp_Letras, " ")
    Printer.CurrentX = Cx
    If buscaespacio > 0 Then
            Printer.Print "Son: " & Mid(Sp_Letras, 1, buscaespacio)
            Printer.CurrentX = Cx + 1
            Printer.CurrentY = Printer.CurrentY + 0.1
            
            Printer.Print Mid(Sp_Letras, buscaespacio)
    Else
                
              Printer.Print "Son: " & Sp_Letras
    End If
    
    
    Printer.CurrentY = Printer.CurrentY + 0.3
     Printer.CurrentX = Cx
    
    Printer.PaintPicture LoadPicture(App.Path & "\timbre_pdf417.jpg"), Cx + 0.2, Printer.CurrentY
    Printer.CurrentY = Printer.CurrentY + 3.2
    Printer.CurrentX = Cx
    Printer.Print "Timbre Electronico S.I.I."
    Printer.CurrentX = Cx
    Printer.Print "Resolucion 80 del 22/08/2014"
    Printer.CurrentX = Cx
    Printer.Print "Verifique Documento: http://www.sii.cl"
    Printer.CurrentY = Printer.CurrentY + 0.3
    
    If Cedible Then
        Printer.FontSize = 10
        Printer.FontBold = True
        Printer.CurrentX = Cx
        Printer.Print "CEDIBLE"
        Printer.CurrentY = Printer.CurrentY + 0.2
        Printer.FontBold = False
    End If
    pos = Printer.CurrentY
    
    If Firma Then
        Printer.FontName = "Arial"
        Coloca Cx, pos, "Nombre:", True, 9
        Coloca Cx, pos + 0.4, "RUT:", True, 9
        Coloca Cx, pos + 0.8, "Fecha:", True, 9
        Coloca Cx, pos + 1.2, "Recinto:", True, 9
        Coloca Cx, pos + 1.6, "Firma:", True, 9
        Coloca Cx, pos + 2, "El acuse de recibo que se declara en este acto, de  ", False, 8
        Coloca Cx, pos + 2.3, "acuerdo a lo dispuesto en la letra b) del Art 4�, y la", False, 8
        Coloca Cx, pos + 2.6, "letra c) del Art. 5� de la ley 19.983, acredita que la", False, 8
        Coloca Cx, pos + 2.9, "entrega de mercaderias o servicio(s) prestado(s) ", False, 8
        Coloca Cx, pos + 3.2, "ha(n) sido recibido(s)", False, 8
        
        Printer.Line (Cx - 0.3, pos - 0.2)-(7, pos + 3.6), , B 'Rectangulo Encabezado y N� Nota de Venta
        Printer.CurrentY = pos + 3.8
    
    End If
    
    
    
    Printer.FontName = "Arial"
    Printer.FontSize = "7"
    Printer.CurrentX = Cx + 0.5
    Printer.Print "Desarrollado por ventas@alvamar.cl"
    
    Printer.NewPage
    Printer.EndDoc
End Sub

Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub

Public Sub ActualizaNC()
    Dim lp_Promedio As Double
    Dim Sp_Producto As String
    Dim Rp_Producto As Recordset
    
    Consulta RsTmp, Sm_Sql_Productos_NC
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
                Sp_Producto = "SELECT pro_inventariable " & _
                                "FROM maestro_productos " & _
                                "WHERE codigo='" & RsTmp!Codigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
                Consulta Rp_Producto, Sp_Producto
                If Rp_Producto.RecordCount > 0 Then
                    If Rp_Producto!pro_inventariable = "SI" Then
                    
                        Sql = "UPDATE maestro_productos " & _
                          "SET stock_Actual = stock_actual -" & CxP(RsTmp!Unidades) & _
                          " WHERE  rut_emp='" & SP_Rut_Activo & "' AND  codigo='" & RsTmp!Codigo & "'"
                        cn.Execute Sql
                        
                       '  = 0
                        lp_Promedio = CostoAVG(RsTmp!Codigo, IG_id_Bodega_Ventas)
                        
                        
                      ' MsgBox Val(CxP(Val(.ListItems(i).SubItems(6))))
                        Kardex Fql(Date), "ENTRADA", Me.CboNC.ItemData(CboNC.ListIndex), _
                                        Val(Lp_Folio), IG_id_Bodega_Ventas, RsTmp!Codigo, Val(CxP(RsTmp!Unidades)), _
                                      "ANULA DOCUMENTO " & LVDetalle.ListItems(1).SubItems(1) & " " & LVDetalle.ListItems(1).SubItems(2), lp_Promedio, lp_Promedio * Val(CxP(RsTmp!Unidades)), Me.TxtRut.Text, Me.TxtRazonSocial, "NO", 0, CboNC.ItemData(CboNC.ListIndex), , , , 0
                    Else
                        'Al no ser inventariable, no pasa nada
                    End If
                End If
                RsTmp.MoveNext
        Loop
    End If
            '
End Sub

