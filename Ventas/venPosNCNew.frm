VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form venPosNC 
   Caption         =   "Emision Nota de Credito Electronica"
   ClientHeight    =   11130
   ClientLeft      =   3945
   ClientTop       =   465
   ClientWidth     =   18015
   LinkTopic       =   "Form1"
   ScaleHeight     =   11130
   ScaleWidth      =   18015
   Begin VB.Frame FraProcesandoDTE 
      Height          =   1605
      Left            =   2250
      TabIndex        =   65
      Top             =   6570
      Visible         =   0   'False
      Width           =   13020
      Begin VB.Timer Timer2 
         Enabled         =   0   'False
         Interval        =   20
         Left            =   2940
         Top             =   930
      End
      Begin Proyecto2.XP_ProgressBar XP_ProgressBar1 
         Height          =   255
         Left            =   4050
         TabIndex        =   67
         Top             =   975
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   450
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16750899
         Scrolling       =   2
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   405
         Left            =   1170
         OleObjectBlob   =   "venPosNCNew.frx":0000
         TabIndex        =   66
         Top             =   495
         Width           =   10275
      End
   End
   Begin VB.CommandButton CmdAceptar 
      Caption         =   "&Generar Nota de Credito"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   705
      Left            =   5685
      TabIndex        =   39
      Top             =   9000
      Width           =   4755
   End
   Begin VB.CommandButton CmdSalir 
      Caption         =   "&Retornar"
      Height          =   405
      Left            =   840
      TabIndex        =   38
      Top             =   9240
      Width           =   1680
   End
   Begin VB.Frame FrmNV 
      Caption         =   "Datos para nota de credito"
      Height          =   10335
      Left            =   465
      TabIndex        =   7
      Top             =   105
      Width           =   17475
      Begin ACTIVESKINLibCtl.SkinLabel SknComentario 
         Height          =   255
         Left            =   3465
         OleObjectBlob   =   "venPosNCNew.frx":00C8
         TabIndex        =   80
         Top             =   9930
         Width           =   1605
      End
      Begin VB.TextBox TxtComentario 
         Height          =   390
         Left            =   5205
         TabIndex        =   79
         Top             =   9780
         Width           =   4740
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   1
         Left            =   7545
         OleObjectBlob   =   "venPosNCNew.frx":014C
         TabIndex        =   42
         Top             =   4425
         Width           =   2055
      End
      Begin VB.Frame FrameSelecciona 
         Caption         =   "Caja/Abono CtaCte"
         Height          =   825
         Left            =   7395
         TabIndex        =   63
         Top             =   3210
         Width           =   9825
         Begin VB.TextBox TxtNumCheque 
            Height          =   330
            Left            =   7365
            TabIndex        =   78
            Top             =   300
            Visible         =   0   'False
            Width           =   2010
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
            Height          =   210
            Left            =   120
            OleObjectBlob   =   "venPosNCNew.frx":01E2
            TabIndex        =   77
            Top             =   345
            Width           =   1995
         End
         Begin VB.ComboBox CboCajaAbono 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "venPosNCNew.frx":0270
            Left            =   2340
            List            =   "venPosNCNew.frx":0283
            Style           =   2  'Dropdown List
            TabIndex        =   64
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   255
            Width           =   4800
         End
      End
      Begin VB.Frame FraProductos 
         Caption         =   "Productos"
         Enabled         =   0   'False
         Height          =   3915
         Left            =   390
         TabIndex        =   43
         Top             =   4845
         Width           =   16815
         Begin VB.CheckBox ChkProductos 
            Caption         =   "Check2"
            Height          =   285
            Left            =   420
            TabIndex        =   57
            Top             =   1260
            Width           =   270
         End
         Begin VB.TextBox TxtDescripcion 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   2790
            MultiLine       =   -1  'True
            TabIndex        =   50
            TabStop         =   0   'False
            Top             =   720
            Width           =   6855
         End
         Begin VB.TextBox TxtCantidad 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   9690
            TabIndex        =   49
            Text            =   "1"
            Top             =   720
            Width           =   1380
         End
         Begin VB.TextBox TxtPrecio 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   11085
            TabIndex        =   48
            Text            =   "0"
            Top             =   720
            Width           =   1815
         End
         Begin VB.TextBox TxtTotalLinea 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   12930
            Locked          =   -1  'True
            TabIndex        =   47
            TabStop         =   0   'False
            Text            =   "0"
            Top             =   720
            Width           =   1650
         End
         Begin VB.CommandButton CmdAceptaLinea 
            Appearance      =   0  'Flat
            BackColor       =   &H8000000A&
            Caption         =   "Ok"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   14610
            TabIndex        =   46
            Top             =   720
            Width           =   720
         End
         Begin VB.TextBox TxtCodigo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   390
            TabIndex        =   45
            Top             =   720
            Width           =   2370
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkUbicacion 
            Height          =   300
            Left            =   3855
            OleObjectBlob   =   "venPosNCNew.frx":02F5
            TabIndex        =   44
            Top             =   585
            Width           =   5760
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   8
            Left            =   390
            OleObjectBlob   =   "venPosNCNew.frx":0352
            TabIndex        =   51
            Top             =   435
            Width           =   660
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   225
            Index           =   9
            Left            =   2805
            OleObjectBlob   =   "venPosNCNew.frx":03B4
            TabIndex        =   52
            Top             =   495
            Width           =   1530
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   3
            Left            =   9660
            OleObjectBlob   =   "venPosNCNew.frx":0420
            TabIndex        =   53
            Top             =   480
            Width           =   1395
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   10
            Left            =   11220
            OleObjectBlob   =   "venPosNCNew.frx":0486
            TabIndex        =   54
            Top             =   480
            Width           =   1635
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   12
            Left            =   13155
            OleObjectBlob   =   "venPosNCNew.frx":04E8
            TabIndex        =   55
            Top             =   480
            Width           =   1440
         End
         Begin MSComctlLib.ListView LvVenta 
            Height          =   2430
            Left            =   375
            TabIndex        =   56
            Top             =   1260
            Width           =   14955
            _ExtentX        =   26379
            _ExtentY        =   4286
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483646
            BackColor       =   -2147483643
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Object.Width           =   529
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Codigo"
               Object.Width           =   3660
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "T3000"
               Text            =   "Descripci�n"
               Object.Width           =   12153
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Object.Tag             =   "N102"
               Text            =   "Cantidad"
               Object.Width           =   2443
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Object.Tag             =   "N100"
               Text            =   "Precio"
               Object.Width           =   3201
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   3201
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "Stock"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Tag             =   "T1000"
               Text            =   "Inventario"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Precio Costo"
               Object.Width           =   0
            EndProperty
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Total Nota de Credito"
         Height          =   1425
         Left            =   10035
         TabIndex        =   40
         Top             =   8790
         Width           =   7200
         Begin VB.TextBox TxtOtrosImpuestos 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1560
            TabIndex        =   68
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   975
            Width           =   1470
         End
         Begin VB.TextBox TxtSubTotal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4905
            TabIndex        =   62
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   405
            Width           =   2085
         End
         Begin VB.TextBox TxtDescuentoNC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4905
            TabIndex        =   60
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   720
            Width           =   2085
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
            Height          =   195
            Left            =   3225
            OleObjectBlob   =   "venPosNCNew.frx":0548
            TabIndex        =   58
            Top             =   810
            Width           =   1530
         End
         Begin VB.TextBox TxtTotalNC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H0080FF80&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4905
            TabIndex        =   41
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   1035
            Width           =   2085
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
            Height          =   195
            Left            =   3195
            OleObjectBlob   =   "venPosNCNew.frx":05BA
            TabIndex        =   59
            Top             =   1110
            Width           =   1500
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
            Height          =   195
            Left            =   3195
            OleObjectBlob   =   "venPosNCNew.frx":0622
            TabIndex        =   61
            Top             =   495
            Width           =   1530
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
            Height          =   195
            Left            =   180
            OleObjectBlob   =   "venPosNCNew.frx":0690
            TabIndex        =   69
            Top             =   1065
            Width           =   1290
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Documento Referencia"
         Height          =   2775
         Left            =   7380
         TabIndex        =   32
         Top             =   375
         Width           =   9885
         Begin VB.CommandButton CmdModificaValor 
            Caption         =   "Modificar Valor"
            Height          =   285
            Left            =   7995
            TabIndex        =   70
            Top             =   3240
            Visible         =   0   'False
            Width           =   1410
         End
         Begin MSComCtl2.DTPicker DtFechaBoleta 
            Height          =   315
            Left            =   5985
            TabIndex        =   4
            Top             =   480
            Width           =   1200
            _ExtentX        =   2117
            _ExtentY        =   556
            _Version        =   393216
            Format          =   104660993
            CurrentDate     =   42279
         End
         Begin VB.ComboBox CboDocVenta 
            Height          =   315
            ItemData        =   "venPosNCNew.frx":070C
            Left            =   195
            List            =   "venPosNCNew.frx":070E
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   495
            Width           =   4170
         End
         Begin VB.TextBox txtNroDocumentoBuscar 
            BackColor       =   &H8000000E&
            Height          =   315
            Left            =   4335
            TabIndex        =   3
            Top             =   495
            Width           =   1650
         End
         Begin VB.TextBox txtTotalDocBuscado 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7170
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   495
            Width           =   1305
         End
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "Buscar"
            Height          =   315
            Left            =   8475
            TabIndex        =   6
            Top             =   480
            Width           =   1125
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   9
            Left            =   180
            OleObjectBlob   =   "venPosNCNew.frx":0710
            TabIndex        =   33
            Top             =   315
            Width           =   1980
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   10
            Left            =   4365
            OleObjectBlob   =   "venPosNCNew.frx":0780
            TabIndex        =   34
            Top             =   300
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   195
            Index           =   11
            Left            =   6090
            OleObjectBlob   =   "venPosNCNew.frx":07E4
            TabIndex        =   35
            Top             =   285
            Width           =   870
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
            Height          =   210
            Index           =   12
            Left            =   7215
            OleObjectBlob   =   "venPosNCNew.frx":084C
            TabIndex        =   36
            Top             =   315
            Width           =   1185
         End
         Begin MSComctlLib.ListView LvDetalle 
            Height          =   1605
            Left            =   195
            TabIndex        =   37
            Top             =   855
            Width           =   9480
            _ExtentX        =   16722
            _ExtentY        =   2831
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Tag             =   "N109"
               Text            =   "Id Unico"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Tag             =   "T1000"
               Text            =   "Documento"
               Object.Width           =   7355
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N109"
               Text            =   "Nro"
               Object.Width           =   2910
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Tag             =   "F1000"
               Text            =   "Fecha"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Key             =   "total"
               Object.Tag             =   "N100"
               Text            =   "Total"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Tag             =   "N109"
               Text            =   "doc_id"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   6
               Object.Tag             =   "N109"
               Text            =   "doc_sii"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Text            =   "Factura Por Guias"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Neto-Bruto"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Cliente"
         Height          =   3135
         Left            =   405
         TabIndex        =   9
         Top             =   1680
         Width           =   6840
         Begin ACTIVESKINLibCtl.SkinLabel ChekRut 
            Height          =   195
            Left            =   4890
            OleObjectBlob   =   "venPosNCNew.frx":08BE
            TabIndex        =   73
            Top             =   495
            Width           =   1380
         End
         Begin VB.CheckBox CheckRut 
            Caption         =   "Check2"
            Height          =   225
            Left            =   4530
            TabIndex        =   72
            Top             =   480
            Width           =   255
         End
         Begin VB.TextBox TxtRazonSocial 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   19
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   795
            Width           =   5730
         End
         Begin VB.CommandButton CmdBuscaCliente 
            Caption         =   "F1 - Buscar"
            Height          =   255
            Left            =   2985
            TabIndex        =   18
            Top             =   480
            Width           =   1395
         End
         Begin VB.TextBox TxtRut 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   17
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   465
            Width           =   1395
         End
         Begin VB.TextBox TxtGiro 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   16
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1125
            Width           =   5715
         End
         Begin VB.TextBox TxtDireccion 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   15
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1455
            Width           =   5670
         End
         Begin VB.TextBox txtComuna 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   14
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   1785
            Width           =   2160
         End
         Begin VB.TextBox txtFono 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1545
            TabIndex        =   13
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   2115
            Width           =   2175
         End
         Begin VB.TextBox TxtEmail 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   4305
            TabIndex        =   12
            ToolTipText     =   "Ingrese el RUT sin puntos ni guion."
            Top             =   2115
            Width           =   2370
         End
         Begin VB.TextBox TxtMontoCredito 
            Height          =   255
            Left            =   6735
            TabIndex        =   11
            Text            =   "Text1"
            Top             =   2040
            Visible         =   0   'False
            Width           =   1995
         End
         Begin VB.TextBox TxtCupoUtilizado 
            Height          =   285
            Left            =   6750
            TabIndex        =   10
            Text            =   "Text1"
            Top             =   2370
            Visible         =   0   'False
            Width           =   2010
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkCupoCredito 
            Height          =   270
            Left            =   4890
            OleObjectBlob   =   "venPosNCNew.frx":093A
            TabIndex        =   20
            Top             =   1800
            Width           =   1020
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   240
            Index           =   0
            Left            =   1005
            OleObjectBlob   =   "venPosNCNew.frx":099A
            TabIndex        =   21
            Top             =   495
            Width           =   495
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
            Height          =   210
            Index           =   2
            Left            =   840
            OleObjectBlob   =   "venPosNCNew.frx":0A04
            TabIndex        =   22
            Top             =   840
            Width           =   660
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   255
            Index           =   1
            Left            =   435
            OleObjectBlob   =   "venPosNCNew.frx":0A6E
            TabIndex        =   23
            Top             =   1470
            Width           =   1065
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   0
            Left            =   645
            OleObjectBlob   =   "venPosNCNew.frx":0ADE
            TabIndex        =   24
            Top             =   1815
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel18 
            Height          =   255
            Left            =   1080
            OleObjectBlob   =   "venPosNCNew.frx":0B48
            TabIndex        =   25
            Top             =   1155
            Width           =   420
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   1
            Left            =   615
            OleObjectBlob   =   "venPosNCNew.frx":0BAE
            TabIndex        =   26
            Top             =   2145
            Width           =   855
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   2
            Left            =   3765
            OleObjectBlob   =   "venPosNCNew.frx":0C14
            TabIndex        =   27
            Top             =   2130
            Width           =   480
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel19 
            Height          =   255
            Index           =   3
            Left            =   3810
            OleObjectBlob   =   "venPosNCNew.frx":0C7E
            TabIndex        =   28
            Top             =   1800
            Width           =   1035
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Documento Actual"
         Height          =   1290
         Left            =   420
         TabIndex        =   8
         Top             =   375
         Width           =   6750
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
            Height          =   390
            Left            =   150
            OleObjectBlob   =   "venPosNCNew.frx":0CF6
            TabIndex        =   74
            Top             =   855
            Width           =   2100
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   300
            Index           =   2
            Left            =   390
            OleObjectBlob   =   "venPosNCNew.frx":0D8C
            TabIndex        =   30
            Top             =   450
            Width           =   1560
         End
         Begin MSComCtl2.DTPicker DtFecha 
            Height          =   285
            Left            =   5295
            TabIndex        =   1
            Top             =   435
            Width           =   1395
            _ExtentX        =   2461
            _ExtentY        =   503
            _Version        =   393216
            Format          =   104660993
            CurrentDate     =   42269
         End
         Begin VB.TextBox TxtDocActual 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   2265
            TabIndex        =   29
            Text            =   "(auto)"
            ToolTipText     =   "Nro NC"
            Top             =   450
            Width           =   1305
         End
         Begin VB.ComboBox CboNC 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            ItemData        =   "venPosNCNew.frx":0E07
            Left            =   2280
            List            =   "venPosNCNew.frx":0E09
            Style           =   2  'Dropdown List
            TabIndex        =   0
            ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
            Top             =   825
            Width           =   4425
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
            Height          =   300
            Index           =   3
            Left            =   3525
            OleObjectBlob   =   "venPosNCNew.frx":0E0B
            TabIndex        =   31
            Top             =   450
            Width           =   1680
         End
         Begin ACTIVESKINLibCtl.SkinLabel SkFoliosDisponibles 
            Height          =   195
            Left            =   2445
            OleObjectBlob   =   "venPosNCNew.frx":0E7D
            TabIndex        =   71
            Top             =   180
            Width           =   3660
         End
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   0
      Top             =   0
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   60
      OleObjectBlob   =   "venPosNCNew.frx":0EDF
      Top             =   5220
   End
   Begin VB.Frame Frame3 
      Caption         =   "Caja/Abono CtaCte"
      Height          =   660
      Left            =   7740
      TabIndex        =   75
      Top             =   4110
      Width           =   5550
      Begin VB.ComboBox Combo1 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         ItemData        =   "venPosNCNew.frx":1113
         Left            =   1905
         List            =   "venPosNCNew.frx":1120
         Style           =   2  'Dropdown List
         TabIndex        =   76
         ToolTipText     =   "Seleccione Documento de  venta. Ventas con Retenci�n, cuando el contribuyente recibe factura de terceros "
         Top             =   195
         Width           =   3585
      End
   End
End
Attribute VB_Name = "venPosNC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public lP_Documento_Referenciado As Long
Public iP_Tipo_NC As Integer
Dim Sm_Nc_Boleta As String * 2
Dim Lp_Id_Venta As Long
Dim Bm_AfectarCaja As Boolean
Dim Sm_Sql_Productos_NC As String
Dim Lp_Folio As Long
Dim Im_TipoNC As Integer
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Sub CboCajaAbono_Click()
    If CboCajaAbono.ListIndex = 3 Then
        FraProductos.Enabled = False
        TxtNumCheque.Visible = True
    Else
        TxtNumCheque.Visible = False
    End If
End Sub
Private Sub CboModalidadNC_Click()
'Parcial, Total, Descuento
    Im_TipoNC = 1
    TxtSubTotal = "0"
    TxtDescuentoNC = "0"
  If LvDetalle.ListItems.Count = 0 Then Exit Sub
    If CboModalidadNC.ListIndex = 0 Then
        Im_TipoNC = 1
        FraProductos.Enabled = False
        LvDetalle_Click
        SumaSeleccion
        BuscaDescuentoNC
        TxtMontoXDescuento.Visible = False
    ElseIf CboModalidadNC.ListIndex = 1 Then
        Im_TipoNC = 3
        FraProductos.Enabled = True
        SumaSeleccion
        TxtMontoXDescuento.Visible = False
    ElseIf CboModalidadNC.ListIndex = 2 Then
    'Por Descuento
        Im_TipoNC = 4
        FraProductos.Visible = True
        SumaSeleccion
        TxtMontoXDescuento.Visible = True
    Else
        'Ingreso manual
        LvVenta.ListItems.Clear
        FraProductos.Enabled = True
        TxtSubTotal = 0
        TxtDescuentoNC = 0
        TxtTotalNC = 0
        TxtMontoXDescuento.Visible = False
    End If
    BuscaDescuentoNC
End Sub
Private Sub BuscaDescuentoNC()
        'busca descuentos del documento que no estaba considerando
        If Sm_Nc_Boleta = "NO" Then
                Sql = ""
                Sql = "call sp_sel_DocVentaRutEmp(" & LvDetalle.SelectedItem.SubItems(2) & ", " & LvDetalle.ListItems(1).SubItems(5) & ", '" & SP_Rut_Activo & "')"
                
         Else
                Sql = ""
                Sql = "call sp_sel_DocVentaRutEmp(" & LvDetalle.SelectedItem.SubItems(2) & ", " & LvDetalle.ListItems(1).SubItems(5) & ", '" & SP_Rut_Activo & "')"
                
         End If
         
         Consulta RsTmp, Sql
        If RsTmp!descuento > 0 Then
                TxtSubTotal = NumFormat(RsTmp!bruto + RsTmp!descuento)
                TxtDescuentoNC = NumFormat(RsTmp!descuento)
                TxtTotalNC = NumFormat(CDbl(TxtSubTotal) - RsTmp!descuento)
                Else
                TxtSubTotal = "0"
                TxtDescuentoNC = "0"
                'TxtTotalNC = "0"
        End If
End Sub
Private Sub CboNC_Click()
''Combo Tipo Documento Factura o Boleta
    Dim Rp_DocVenta As Recordset
    If CboNC.ListIndex = -1 Then Exit Sub

        Sql = ""
        Sql = " call sp_sel_CmbCboNC(" & CboNC.ItemData(CboNC.ListIndex) & ");"
        
        Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        If RsTmp!nc = "SI" Then
            LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_boleta_venta='SI'"
             Sm_Nc_Boleta = "SI"
        Else
            LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_boleta_venta='NO' AND doc_documento='VENTA' AND doc_nota_de_credito='NO' AND doc_contable='SI' AND doc_nota_de_venta='NO'"
            Sm_Nc_Boleta = "NO"
        End If
    End If
    
    If SP_Rut_Activo = "78.967.170-2" Then
           LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_activo='SI' AND doc_documento='VENTA'  AND doc_nota_de_credito='NO' AND doc_contable='SI' "
    End If

    If CboNC.ListIndex = -1 Then Exit Sub
        Ip_DocIdDocumento = CboNC.ItemData(CboNC.ListIndex)
  
            Sql = ""
            Sql = " call sp_sel_DocIdDocumento(" & Ip_DocIdDocumento & ");"

                
        Consulta Rp_DocVenta, Sql
        If Rp_DocVenta.RecordCount > 0 Then
            If Rp_DocVenta!doc_dte = "SI" Then

                Sql = ""
                Sql = " call sp_sel_DocRutEmp('" & SP_Rut_Activo & "', " & Rp_DocVenta!doc_cod_sii & ");"
                                
                Consulta Rp_DocVenta, Sql
                TxtNroDocumento = 0
                If Rp_DocVenta.RecordCount > 0 Then
                    TxtNroDocumento = Rp_DocVenta!Numero
                Else
                    Me.SkFoliosDisponibles = "No quedan folios disponibles.."
                End If
            End If
        End If
End Sub
Private Sub ChkProductos_Click()
    If ChkProductos.Value = 1 Then
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        Next
    Else
    
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = False
        Next
    
    End If
    SumaSeleccion
End Sub
Private Sub SumaSeleccion()
    Dim Lp_descuento As String
    BuscaDescuentoNC
    TxtTotalNC = 0
    If Val("" & RsTmp!descuento) = 0 Then
    Lp_descuento = 0
    Else
    Lp_descuento = NumFormat("" & RsTmp!descuento)
    End If
    
    For i = 1 To LvVenta.ListItems.Count
        
        If LvVenta.ListItems(i).Checked Then TxtTotalNC = NumFormat(CDbl(TxtTotalNC) + CDbl(LvVenta.ListItems(i).SubItems(5)))
    Next
        TxtSubTotal = TxtTotalNC
        If Lp_descuento = 0 Then
        TxtDescuentoNC = NumFormat(CDbl(Lp_descuento))
        Else
        TxtDescuentoNC = NumFormat(Round(NumFormat(CDbl(Lp_descuento)) / LvVenta.ListItems.Count, 0))
        End If
        TxtTotalNC = NumFormat(CDbl(TxtTotalNC - TxtDescuentoNC))
End Sub
Private Sub CmdAceptaLinea_Click()
    Dim p As Integer
    If Len(TxtCodigo) = 0 Then
        MsgBox "Ingrese c�digo...", vbInformation
        On Error Resume Next
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Len(TxtDescripcion) = 0 Then
        MsgBox "Faltan datos para agregar linea...", vbInformation
        TxtCodigo.SetFocus
        Exit Sub
    End If
    If Val(CxP(TxtCantidad)) = 0 Then
        MsgBox "Falta cantidad...", vbInformation
        TxtCantidad.SetFocus
        Exit Sub
    End If
    If Val(TxtPrecio) = 0 Then
        MsgBox "Falta precio...", vbInformation
        TxtPrecio.SetFocus
        Exit Sub
    End If
    
    LvVenta.ListItems.Add , , TxtCodigo.Tag
    p = LvVenta.ListItems.Count
    LvVenta.ListItems(p).SubItems(1) = TxtCodigo
    LvVenta.ListItems(p).SubItems(2) = TxtDescripcion
    LvVenta.ListItems(p).SubItems(3) = TxtCantidad
    LvVenta.ListItems(p).SubItems(4) = TxtPrecio
    
    If Sm_VentaRapida = "SI" Then
        LvVenta.ListItems(p).SubItems(5) = Round(CDbl(TxtPrecio) * Val(TxtCantidad), 0)
    Else
        LvVenta.ListItems(p).SubItems(5) = TxtTotalLinea
    End If
    LvVenta.ListItems(p).SubItems(6) = TxtStockLinea
    LvVenta.ListItems(p).SubItems(7) = SkInventariable
    
    LvVenta.ListItems(LvVenta.ListItems.Count).Selected = True
    LvVenta.ListItems(LvVenta.ListItems.Count).EnsureVisible
    TxtTotalNC = NumFormat(TotalizaColumna(LvVenta, "total"))
    SumaSeleccion
    
    TxtCodigo.SetFocus
    
End Sub

Private Sub CmdAceptar_Click()
    Dim Lp_SaldoDoc As Long
    Dim Sp_MontoACtaCte As String
    Dim Sp_MontoACheque As String
    Dim Sp_MontoACredito As String
    If LvVenta.ListItems.Count = 0 And Sm_Nc_Boleta = "NO" Then Exit Sub
   
    If CboNC.ListIndex = -1 Then
        MsgBox "Seleccione NC...", vbInformation
        CboNC.SetFocus
        Exit Sub
    End If
    
    If LvDetalle.ListItems.Count = 0 Then
        MsgBox "No hay documento referenciado...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    
    If Len(TxtRut) = 0 Then
        MsgBox "Debe seleccionar/ingresar cliente...", vbInformation
        TxtRut.SetFocus
        Exit Sub
    End If
    
    If CDbl(TxtTotalNC) = 0 Then
    
        MsgBox "Haga clic en el documento de la grilla..."
        LvDetalle.SetFocus
        Exit Sub
    End If
    
    Sp_MontoACtaCte = "NO"
    Sp_MontoACheque = "NO"
    Sp_MontoACredito = "NO"
    
    If CboCajaAbono.ListIndex = 0 Then
        MsgBox "Seleccione Accion de NC en caja...", vbInformation
        CboCajaAbono.SetFocus
        Exit Sub
    Else
        Lp_SaldoDoc = 0
        If Sm_Nc_Boleta = "NO" Then Lp_SaldoDoc = SaldoDocumento(LvDetalle.ListItems(1), "CLI")
        
        If Lp_SaldoDoc > 0 Then
            MsgBox "Se abonar� el monto de la NC al documento..." & vbNewLine & "    (documento Ref fue al credito)   ", vbInformation
            
        Else
            If CboCajaAbono.ListIndex = 2 Then
                Sp_MontoACtaCte = "SI"
            End If
            If CboCajaAbono.ListIndex = 3 Then
                Sp_MontoACheque = "SI"
                ''Aca Asignaremos valor a una variable para que se descuenten los cheques
            End If
            If CboCajaAbono.ListIndex = 4 Then
                Sp_MontoACredito = "SI"
                ''Aca Asignaremos valor a una variable para que se descuente el monto de Transbank Credito
            End If
        End If
    End If
 
    DoEvents
    Me.Timer2.Enabled = True
    FraProcesandoDTE.Visible = True
 
    Dim Dp_Total As Double
    Dim Dp_Neto As Double
    Dim Dp_Iva As Double
    Dim Sp_Documento_Referencia As String
    Dim sqlUpd As String
    
    CmdAceptar.Enabled = False
    sqlUpd = ""
                ''Verifica el numero de folio disponible
            Sql = ""
            Sql = " call sp_sel_DocRutEmp('" & SP_Rut_Activo & "', 61);"

            Consulta RsTmp, Sql
            
            If RsTmp.RecordCount = 0 Then
                MsgBox "No cuenta con folios disponibles para emitir NC Electronica..."
                 CmdAceptar.Enabled = True
                Exit Sub
            
            End If
                      
            dte_id = RsTmp!dte_id
            Lp_Folio = RsTmp!Numero
            
            Sql = ""
            Sql = " call sp_sel_DocVtaRutEmp('" & SP_Rut_Activo & "', " & Lp_Folio & ", 61);"

            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                MsgBox "Folio ya ha sido utilizado... consulte para liberar este Nro de NC...", vbInformation
                Exit Sub
            End If
            

    Lp_Id_Venta = UltimoNro("ven_doc_venta", "id")
    'Se debe generar la NC
    'con los mismos datos de la venta
    
    'TAMBIEN HAY QUE VERIFICAR COMO SE PAGA LA NC
    
    If Sm_Nc_Boleta = "NO" Then
        'Nc por factura
        Bm_AfectarCaja = True
        'verificamos si el doc es a credito,
        AbonaNCCredito LvDetalle.ListItems(1)
        
''        lacajita = LG_id_Caja
''        If Not Bm_AfectarCaja Then lacajita = 0
''
''        If Sp_MontoACtaCte = "SI" Then
''            lacajita = 0
''            ' Caso si se abona el monto a su Cta. Cte.
''            'Guardaremos estos valores en la tabla cta_pozo
''        Sql = ""
''        Sql = "call sp_ins_CtaPozo(" & lacajita & ", '" & Fql(DtFecha) & "', ''" & SP_Rut_Activo & "', 'CLI', '" & TxtRut & "', " & CDbl(TxtTotalNC) & ", 1);"
''
''        cn.Execute Sql
''        End If
''
''        If Sp_MontoACheque = "SI" Then
''            lacajita = 0
''            ' Caso si se devuelve cheque y se modifica su estado a "Devuelto"
''            'UPDATE abo_cheques SET che_estado='DEVUELTO' WHERE che_numero=_che_numero AND rut_emp=_rut_emp AND dte_id=_dte_id;
''        Sql = ""
''        Sql = "call sp_upd_dteAnulaCheque('" & TxtNumCheque & "', '" & TxtRut & "');"
''
''        cn.Execute Sql
''        End If
''
''        If Sp_MontoACredito = "SI" Then
''            lacajita = 0
''            ' Caso si se elimina el monto de Transbank Credito
''        Sql = ""
''        Sql = "call sp_sel_Upd_abo_tipos_de_pagos(" & LvDetalle.SelectedItem.SubItems(2) & ", " & LvDetalle.SelectedItem.SubItems(1) & ");"
''
''        cn.Execute Sql
''
''        End If
             lacajita = LG_id_Caja
        If Im_TipoNC = 1 Then
'                    Nota de Credito por anulacion de documento *****************************
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "',rut_cliente,nombre_cliente,'VD',neto,bruto,iva," & _
                                    "'ANULA DOCUMENTO','CONTADO',ven_id,ven_nombre,0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",0,'" & Fql(DtFecha) & "','" & LogUsuario & "'," & LvDetalle.ListItems(1) & "," & _
                                    "rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo," & _
                                    "ven_comentario,ven_ordendecompra,ven_tipo_calculo,ven_venta_arriendo," & _
                                    "'SI'," & lacajita & ",sue_id,ven_plazo_id,'SI',gir_id,rso_id,1,1,curtime(),doc_genero " & _
                            "FROM ven_doc_venta " & _
                            "WHERE id=" & LvDetalle.ListItems(1) & " AND rut_emp='" & SP_Rut_Activo & "'"
                                        ' Modificado: Marco Sobarzo
                                        ' Motivo: Reemplaza codigo embebido por procedimiento
                                        ' Fecha: 15-12-2016
               ''     Sql = ""
               ''     Sql = "call sp_ins_DocVenta(" & Lp_Id_Venta & "," & Lp_Folio & ", '" & Fql(DtFecha) & "', " & LogUsuario & ", " & LvDetalle.ListItems(1) & ", " & lacajita & ", '" & SP_Rut_Activo & "');"
                    
                    cn.Execute Sql
                        
                    Sql = ""
                    Sql = "call sp_ins_DocVentaDetalle('" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ", '" & SP_Rut_Activo & "', " & LvDetalle.ListItems(1).SubItems(2) & ", " & LvDetalle.ListItems(1).SubItems(5) & ", '" & LvDetalle.SelectedItem.SubItems(7) & "');"
                                        
                    cn.Execute Sql
                    
                    
                    
                    'Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                    '            "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                    '            "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                    '        "FROM ven_detalle " & _
                    '        "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LvDetalle.ListItems(1).SubItems(2) & "  AND doc_id=" & LvDetalle.ListItems(1).SubItems(5)
                                        ' Modificado: Marco Sobarzo
                                        ' Motivo: Reemplaza codigo embebido por procedimiento
                                        ' Fecha: 17-12-2016
                    Sm_Sql_Productos_NC = ""
                    Sm_Sql_Productos_NC = "call sp_sel_DocVentaDetalle('" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ", '" & SP_Rut_Activo & "', " & LvDetalle.ListItems(1).SubItems(2) & ", " & LvDetalle.ListItems(1).SubItems(5) & ");"
        
        Else     ' Nota de Credito Parcial, solo los productos de la grilla por Factura
                 'Nota de Credito por anulacion de documento *****************************
                                 'Totales
                    If LvDetalle.ListItems(1).SubItems(8) = "NETO" Then
                        Dp_Total = CDbl(TxtTotalNC)
                        Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                        Dp_Iva = Dp_Total - Dp_Neto
                    
                    Else
                                     
                        Dp_Total = CDbl(TxtTotalNC)
                        Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                        Dp_Iva = Dp_Total - Dp_Neto
                    End If
                    
                    Sql = ""
                    Sql = "call sp_ins_DocVentaNC(" & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "'," & Dp_Neto & "," & Dp_Total & "," & Dp_Iva & "," & CboNC.ItemData(CboNC.ListIndex) & ", '" & LogUsuario & "'," & LvDetalle.ListItems(1) & ", " & lacajita & ",'" & SP_Rut_Activo & "');"
                    
                    cn.Execute Sql
                    
                    '***** Detalle va solo con los productos de la grilla
                    
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) VALUES "
                    For i = 1 To LvVenta.ListItems.Count
                        If LvVenta.ListItems(i).Checked Then
                        
                             If LvDetalle.ListItems(1).SubItems(6) = "NETO" Then
                             
                                 Dp_Neto = CDbl(LvVenta.ListItems(i).SubItems(5))
                                 Dp_Total = Round(Dp_Total * Val("1." & DG_IVA), 0)
                             Else
                                
                                 Dp_Total = CDbl(LvVenta.ListItems(i).SubItems(5))
                                 Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                             End If

                            sql2 = ""
                            sql2 = "call sp_sel_VenDetalleRowCount(" & LvVenta.ListItems(i) & ");"
                                                        
                            Consulta RsTmp, sql2
                            If RsTmp.RecordCount > 0 Then
                                Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CxP(CDbl(LvVenta.ListItems(i).SubItems(3))) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                CxP(RsTmp!precio_costo) & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & RsTmp!pla_id & "," & _
                                RsTmp!are_id & "," & RsTmp!cen_id & "," & _
                                RsTmp!aim_id & "," & Dp_Total & "," & Dp_Neto & "),"
                            End If
                            
                        End If
'''                    Next
'''
'''                                        Sql = Mid(Sql, 1, Len(Sql) - 1)
'''                                        ' Modificado: Marco Sobarzo
'''                                        ' Motivo: Reemplaza codigo embebido por procedimiento
'''                                        ' Fecha: 18-12-2016
'''                                       ' Sql = ""
'''                                        Sql = "call sp_ins_VenDetalle" & Sql & ";"
'''
'''                    cn.Execute Sql
'''
'''
'''
'''                    '   Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
'''                    '            "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
'''                    '            "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
'''                    '        "FROM ven_detalle " & _
'''                    '        "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_Folio & "  AND doc_id=" & CboNC.ItemData(CboNC.ListIndex)
'''                    ' Modificado: Marco Sobarzo
'''                                        ' Motivo: Reemplaza codigo embebido por procedimiento
'''                                        ' Fecha: 18-12-2016
'''                                        Sm_Sql_Productos_NC = ""
'''                                        Sm_Sql_Productos_NC = "call sp_sel_VenDetalle('" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "');"
'''
'''
'''
'''        End If
                    Next
                    Sql = Mid(Sql, 1, Len(Sql) - 1)
                    cn.Execute Sql
                                
                       Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                            "FROM ven_detalle " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_Folio & "  AND doc_id=" & CboNC.ItemData(CboNC.ListIndex)
                    
        End If
            
        NotaCreditoElectronica 61, Lp_Folio, CboNC.ItemData(CboNC.ListIndex), False
                
        'Folio ya no esta disponible
        sqlUpd = ""
        sqlUpd = "call sp_upd_VenDetalle(" & dte_id & ");"
        cn.Execute sqlUpd
                
        ActualizaNC
'*************************************************************BOLETA*******************************************************************************'
    Else 'Nota de Credito Por Boleta
        Dim SqlVenDocVta As String
        SqlVenDocVta = ""
        'POR BOLETA
        If Im_TipoNC = 1 Then
                    Dp_Total = CDbl(Me.TxtTotalNC)
                    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                    Dp_Iva = Dp_Total - Dp_Neto
            
                    SqlVenDocVta = "(" & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "','" & TxtRut & "','" & TxtRazonSocial & "','VD'," & Dp_Neto & "," & Dp_Total & "," & Dp_Iva & "," & _
                                    "'ANULA DOCUMENTO','CONTADO',1,'EMPRESA',0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",0,'" & Fql(DtFecha) & "','" & LogUsuario & "',0" & LvDetalle.ListItems(1) & ",'" & _
                                    SP_Rut_Activo & "',99999,99999,99999,0,0,1,1," & _
                                    "'NC',0,1,'VTA'," & _
                                    "'SI'," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & ",1,'SI',0,0,1," & Lp_Folio & ",curtime(),'F');"
                                        
                                        Sql = ""
                                        Sql = "call sp_ins_VenDocVenta" & SqlVenDocVta
                                        
                                        cn.Execute Sql
                    
                   ' Dim Sp_Detalle_Boletas
                    
                    Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                            "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                            "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                                        "FROM ven_detalle " & _
                                        "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LvDetalle.ListItems(1).SubItems(2) & "  AND doc_id=" & LvDetalle.ListItems(1).SubItems(5)
                    
                                        If LvDetalle.ListItems.Count > 1 Then
                        For i = 2 To LvDetalle.ListItems.Count
                        
                            Sm_Sql_Productos_NC = Sm_Sql_Productos_NC & " UNION SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                                            "FROM ven_detalle " & _
                                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & LvDetalle.ListItems(i).SubItems(2) & "  AND doc_id=" & LvDetalle.ListItems(i).SubItems(5)
                        Next
                    
                    End If
                    
                    
                    
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) VALUES "
                    sql2 = ""
                    
                    
                    If LvVenta.ListItems.Count > 0 Then
                    
                        For i = 1 To LvVenta.ListItems.Count
                            If LvVenta.ListItems(i).Checked Then
                                 Dp_Total = CDbl(LvVenta.ListItems(i).SubItems(5))
                                 Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                                
                                sql2 = "SELECT precio_costo,pla_id,are_id,cen_id,aim_id " & _
                                        "FROM ven_detalle " & _
                                        "WHERE id=" & LvVenta.ListItems(i)
                                Consulta RsTmp, sql2
                                If RsTmp.RecordCount > 0 Then
                                    Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CxP(CDbl(LvVenta.ListItems(i).SubItems(3))) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                    CxP(RsTmp!precio_costo) & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & RsTmp!pla_id & "," & _
                                    RsTmp!are_id & "," & RsTmp!cen_id & "," & _
                                    RsTmp!aim_id & "," & Dp_Total & "," & Dp_Neto & "),"
                                End If
                                
                            End If
                        Next
                        
                        cn.Execute Mid(Sql, 1, Len(Sql) - 1)
                    Else
                        
                        For i = 1 To LvDetalle.ListItems.Count
                                
                            dp_netito = Round(CDbl(TxtTotalNC) / 1.19, 0)
                        
                            sql2 = sql2 & "(0,'','" & LvDetalle.ListItems(i).SubItems(1) & " " & LvDetalle.ListItems(i).SubItems(2) & "'," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & ",0,1," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & "," & CDbl(LvDetalle.ListItems(i).SubItems(4)) & "," & _
                                                "0,'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "',99999,99999,99999," & _
                                    "0 ," & LvDetalle.ListItems(i).SubItems(4) & "," & dp_netito & "),"
                
                        Next
                        cn.Execute Sql & Mid(sql2, 1, Len(sql2) - 1)
                    End If
                    
                
        Else ' NOTA DE CREDITO PARCIAL POR BOLETA
            
                    Dp_Total = CDbl(Me.TxtTotalNC)
                    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                    Dp_Iva = Dp_Total - Dp_Neto
                    Sql = "INSERT INTO ven_doc_venta (" & _
                                "id,no_documento,fecha,rut_cliente,nombre_cliente,tipo_movimiento,neto,bruto,iva,comentario,CondicionPago," & _
                                "ven_id,ven_nombre,ven_comision,forma_pago,tipo_doc,doc_id,suc_id,ven_fecha_vencimiento,usu_nombre," & _
                                "id_ref,rut_emp,pla_id,are_id,cen_id,exento,ven_iva_retenido,bod_id,ven_plazo,ven_comentario,ven_ordendecompra," & _
                                "ven_tipo_calculo,ven_venta_arriendo,ven_nc_utilizada,caj_id,sue_id,ven_plazo_id,ven_entrega_inmediata," & _
                                "gir_id,rso_id,tnc_id,ven_boleta_hasta,doc_time,doc_genero) " & _
                            "SELECT " & Lp_Id_Venta & "," & Lp_Folio & ",'" & Fql(DtFecha) & "','" & TxtRut & "','" & TxtRazonSocial & "','VD'," & Dp_Neto & "," & Dp_Total & "," & Dp_Iva & "," & _
                                    "'DEVOLUCION DE PRODUCTOS','CONTADO',1,'EMPRESA',0,'PENDIENTE','NOTA CREDITO'," & _
                                    CboNC.ItemData(CboNC.ListIndex) & ",0,'" & Fql(DtFecha) & "','" & LogUsuario & "',0" & LvDetalle.ListItems(1) & ",'" & _
                                    SP_Rut_Activo & "',99999,99999,99999,0,0,1,1," & _
                                    "'NC',0,1,'VTA'," & _
                                    "'SI'," & LG_id_Caja & "," & IG_id_Sucursal_Empresa & ",1,'SI',0,0,1," & Lp_Folio & ",curtime(),'F'"
            
                    cn.Execute Sql
                                                   
                    Sql = "INSERT INTO ven_detalle (" & _
                                "codigo,marca,descripcion,precio_real,descuento,unidades,precio_final," & _
                                "subtotal,precio_costo,btn_especial,comision,fecha,no_documento,doc_id," & _
                                "rut_emp,pla_id,are_id,cen_id,aim_id,ved_precio_venta_bruto,ved_precio_venta_neto) VALUES "
                    For i = 1 To LvVenta.ListItems.Count
                        If LvVenta.ListItems(i).Checked Then
                             Dp_Total = CDbl(LvVenta.ListItems(i).SubItems(5))
                             Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                            
                            sql2 = "SELECT precio_costo,pla_id,are_id,cen_id,aim_id " & _
                                    "FROM ven_detalle " & _
                                    "WHERE id=" & LvVenta.ListItems(i)
                            Consulta RsTmp, sql2
                            If RsTmp.RecordCount > 0 Then
                                Sql = Sql & "(" & LvVenta.ListItems(i).SubItems(1) & ",'','" & LvVenta.ListItems(i).SubItems(2) & "'," & CDbl(LvVenta.ListItems(i).SubItems(4)) & ",0," & CDbl(LvVenta.ListItems(i).SubItems(3)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & CDbl(LvVenta.ListItems(i).SubItems(5)) & "," & _
                                CxP(RsTmp!precio_costo) & ",'NO',0,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",'" & SP_Rut_Activo & "'," & RsTmp!pla_id & "," & _
                                RsTmp!are_id & "," & RsTmp!cen_id & "," & _
                                RsTmp!aim_id & "," & Dp_Total & "," & Dp_Neto & "),"
                            End If
                            
                        End If
                    Next
                    Sql = Mid(Sql, 1, Len(Sql) - 1)
                    cn.Execute Sql
                                
                       Sm_Sql_Productos_NC = "SELECT codigo,marca,descripcion,precio_real,descuento,unidades,precio_final,subtotal," & _
                                "precio_costo,btn_especial,comision,'" & Fql(DtFecha) & "'," & Lp_Folio & "," & CboNC.ItemData(CboNC.ListIndex) & ",rut_emp,pla_id,are_id,cen_id," & _
                                "aim_id , ved_precio_venta_bruto, ved_precio_venta_neto " & _
                            "FROM ven_detalle " & _
                            "WHERE rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & Lp_Folio & "  AND doc_id=" & CboNC.ItemData(CboNC.ListIndex)
                          
        End If
          NotaCreditoElectronica 61, Lp_Folio, CboNC.ItemData(CboNC.ListIndex), True
          'Folio ya no esta disponible
                cn.Execute "UPDATE dte_folios SET dte_disponible='NO' " & _
                                    "WHERE dte_id=" & dte_id
        ActualizaNC
        
        LvDetalle.ListItems.Clear
    
    End If
    
    
    '16 Enero 2016 _
    Solo para briones
    If SP_Rut_Activo = "3.630.608-4" Or SP_Rut_Activo = "76.178.895-7" Then
            If Principal.LvImpresorasDefecto.ListItems.Count > 0 Then
                If UCase(Printer.DeviceName) = UCase(Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)) Then
                        'SIGUE CON LA MISMA
                Else
                        EstableImpresora Principal.LvImpresorasDefecto.ListItems(1).SubItems(2)
                End If
                'Copia empresa
                ImprimeFacturaTermica Str(Lp_Folio), CboNC.ItemData(CboNC.ListIndex), False, False, "NOTA DE CREDITO"
                ImprimeFacturaTermica Str(Lp_Folio), CboNC.ItemData(CboNC.ListIndex), False, True, "NOTA DE CREDITO"
            End If
    End If
    
    ''MSG
            
        If Not Bm_AfectarCaja Then lacajita = 0
              
        If Sp_MontoACtaCte = "SI" Then
            lacajita = 0
            ' Caso si se abona el monto a su Cta. Cte.
            'Guardaremos estos valores en la tabla cta_pozo
        Sql = ""
        Sql = "call sp_ins_CtaPozo(" & lacajita & ", '" & Fql(DtFecha) & "', ''" & SP_Rut_Activo & "', 'CLI', '" & TxtRut & "', " & CDbl(TxtTotalNC) & ", 1);"
        
        cn.Execute Sql
        End If
        
        If Sp_MontoACheque = "SI" Then
            lacajita = 0
            ' Caso si se devuelve cheque y se modifica su estado a "Devuelto"
            'UPDATE abo_cheques SET che_estado='DEVUELTO' WHERE che_numero=_che_numero AND rut_emp=_rut_emp AND dte_id=_dte_id;
        Sql = ""
        Sql = "call sp_upd_dteAnulaCheque('" & TxtNumCheque & "', '" & TxtRut & "');"
                        
        cn.Execute Sql
        End If
        
        If Sp_MontoACredito = "SI" Then
            lacajita = 0
            ' Caso si se elimina el monto de Transbank Credito
        Sql = ""
        Sql = "call sp_sel_Upd_abo_tipos_de_pagos(" & LvDetalle.SelectedItem.SubItems(2) & ", " & LvDetalle.SelectedItem.SubItems(1) & ");"

        cn.Execute Sql

        End If

    ''MSG
    
    

    SkFoliosDisponibles = ""

        Sql = "SELECT count(dte_id) dte " & _
                "FROM dte_folios " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & 61 & " AND dte_disponible='SI' " & _
                "GROUP BY doc_id"
        Consulta RsTmp3, Sql
        If RsTmp3.RecordCount > 0 Then
            SkFoliosDisponibles = "Folios disponibles:" & RsTmp3!Dte
        End If
                          
        MsgBox "Documento emitido... " & vbNewLine & "Nro : " & Lp_Folio & vbNewLine & SkFoliosDisponibles, vbInformation
    
    Sql = ""
    LvDetalle.ListItems.Clear
    LvVenta.ListItems.Clear
    
    TxtTotalNC = 0
    TxtRut = ""
    TxtDireccion = ""
    TxtGiro = ""
    txtFono = ""
    txtComuna = ""
    TxtCiudad = ""
    Me.TxtRazonSocial = ""
    Me.txtNroDocumentoBuscar = ""
    Me.txtTotalDocBuscado = ""
    Me.TxtComentario = ""
    Me.TxtDocActual = ""
    
    
    LvVenta.Enabled = False
   
    Me.CboNC.ListIndex = 1
    TxtSubTotal = 0
    Me.TxtDescuentoNC = 0
    CmdAceptar.Enabled = True
    
    Me.Timer2.Enabled = False
    sql2 = ""
    FraProcesandoDTE.Visible = False
    Me.TxtDescuentoNC = ""
    Me.TxtMontoXDescuento = ""
    Me.TxtNumCheque = ""
    SkFoliosDisponibles = ""
    
End Sub
Private Sub CmdBuscaCliente_Click()
    LlamaClienteDe = "VD"
    
    ClienteEncontrado = False
    BuscaCliente.Show 1
    TxtRut = SG_codigo
    TxtRut.SetFocus
End Sub
Private Sub cmdBuscar_Click()
    If Sm_Nc_Boleta = "NO" Then
        Sql = ""
        Sql = "call sp_sel_VenDetalleFactura(" & txtNroDocumentoBuscar & ", " & CboDocVenta.ItemData(CboDocVenta.ListIndex) & ",'" & SP_Rut_Activo & "', '" & Sm_Nc_Boleta & "');"
        
        Consulta RsTmp, Sql
        
        LLenar_Grilla RsTmp, Me, LvVenta, True, True, True, False
        Me.ChkProductos.Value = 1
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        
        Next
        BuscaDescuentoNC
    Else
        'Por boleta
            Sql = "SELECT id,doc_nombre,no_documento,fecha,bruto,doc_cod_sii,v.doc_id " & _
                                "FROM ven_doc_venta v " & _
                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                "WHERE doc_nota_de_credito='NO' AND " & Filtro & "  v.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & txtNroDocumentoBuscar
 '   GoTo sigue
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        LvDetalle.ListItems.Add , , RsTmp!ID
        p = LvDetalle.ListItems.Count
        LvDetalle.ListItems(p).SubItems(1) = RsTmp!doc_nombre
        LvDetalle.ListItems(p).SubItems(2) = RsTmp!no_documento
        LvDetalle.ListItems(p).SubItems(3) = RsTmp!Fecha
        LvDetalle.ListItems(p).SubItems(4) = NumFormat(RsTmp!bruto)
        LvDetalle.ListItems(p).SubItems(5) = RsTmp!doc_id
        LvDetalle.ListItems(p).SubItems(6) = RsTmp!doc_cod_sii
        
End If
        
        
        Sql = ""
        Sql = "SELECT d.id,d.codigo,d.descripcion,unidades,precio_final,subtotal,pro_inventariable " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.no_documento =v.no_documento AND v.doc_id=d.doc_id " & _
                    "JOIN maestro_productos m ON d.codigo=m.codigo " & _
                    "WHERE d.no_documento=" & txtNroDocumentoBuscar.Text & " AND d.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND " & _
                    "d.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "'"
        Consulta RsTmp, Sql
        
        LLenar_Grilla RsTmp, Me, LvVenta, True, True, True, False
        Me.ChkProductos.Value = 1
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        
        Next
'        habilitar luego
'        BuscaDescuentoNC
'BuscaDescuentoNCxBoleta
  End If
  '        habilitar luego
'   TxtTotalNC = LvDetalle.ListItems(1).SubItems(4)
    
    For i = 1 To LvDetalle.ListItems.Count
        If LvDetalle.ListItems(i).SubItems(6) = 33 Then
            MsgBox "Es posible hacer notas de credito por una sola Factura Electronica...", vbInformation
            Exit Sub
        End If
    Next
    
    If CboDocVenta.ListIndex = -1 Then
        MsgBox "Seleccione documento...", vbInformation
        CboDocVenta.SetFocus
        Exit Sub
    End If
    
    If Val(txtNroDocumentoBuscar) = 0 Then
        MsgBox "Debe ingresar Nro para de " & CboDocVenta.Text & " ...", vbInformation
        txtNroDocumentoBuscar.SetFocus
        Exit Sub
    End If
    
    Filtro = ""
    If Sm_Nc_Boleta = "NO" Then
        Filtro = "rut_cliente='" & TxtRut & "' AND "
    
    
    End If
    
    If Sm_Nc_Boleta = "NO" Then
    
    Sql = "SELECT id,doc_nombre,no_documento,fecha,bruto,doc_cod_sii,v.doc_id " & _
                                "FROM ven_doc_venta v " & _
                                "JOIN sis_documentos d ON v.doc_id=d.doc_id " & _
                                "WHERE doc_nota_de_credito='NO' AND " & Filtro & "  v.doc_id=" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & " AND no_documento=" & txtNroDocumentoBuscar
 '   GoTo sigue
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        LvDetalle.ListItems.Add , , RsTmp!ID
        p = LvDetalle.ListItems.Count
        LvDetalle.ListItems(p).SubItems(1) = RsTmp!doc_nombre
        LvDetalle.ListItems(p).SubItems(2) = RsTmp!no_documento
        LvDetalle.ListItems(p).SubItems(3) = RsTmp!Fecha
        LvDetalle.ListItems(p).SubItems(4) = NumFormat(RsTmp!bruto)
        LvDetalle.ListItems(p).SubItems(5) = RsTmp!doc_id
        LvDetalle.ListItems(p).SubItems(6) = RsTmp!doc_cod_sii
   
        
    Else
sigue:
        If Sm_Nc_Boleta = "NO" Then
            MsgBox "Documento no encontrado para este cliente...", vbInformation
            CboDocVenta.SetFocus
            Exit Sub
        End If
    
        
        If MsgBox("Documento no se encontr� en los registros del sistema..." & vbNewLine & " �Agregar de todas formas...?", vbQuestion + vbOKCancel) = vbCancel Then Exit Sub

        Sql = ""
        Sql = "call sp_sel_SisDocTip(" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & ");"
                
        Consulta RsTmp, Sql
        LvDetalle.ListItems.Add , , ""
        p = LvDetalle.ListItems.Count
        LvDetalle.ListItems(p).SubItems(1) = CboDocVenta.Text
        LvDetalle.ListItems(p).SubItems(2) = txtNroDocumentoBuscar
        LvDetalle.ListItems(p).SubItems(3) = DtFechaBoleta
        LvDetalle.ListItems(p).SubItems(4) = NumFormat(txtTotalDocBuscado)
        LvDetalle.ListItems(p).SubItems(5) = CboDocVenta.ItemData(CboDocVenta.ListIndex)
        LvDetalle.ListItems(p).SubItems(6) = RsTmp!doc_cod_sii
    End If
    Else
    End If
    SumaGrilla
   '' BuscaDescuentoNC
    txtTotalDocBuscado = ""
    txtFechaDocBuscado = ""
    txtTotalDocBuscado = ""
    CboDocVenta.SetFocus
    
End Sub

Private Sub CmdModificaValor_Click()
    Dim Lp_NuevoValor As Variant
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
        Lp_NuevoValor = InputBox("Ingrese Valor", "Cambiar monto", LvDetalle.SelectedItem.SubItems(4))
        If Val(Lp_NuevoValor) > 0 Then
            LvDetalle.SelectedItem.SubItems(4) = NumFormat(Val(Lp_NuevoValor))
        End If
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    Skin2 Me, , 5
    Centrar Me
    CboCajaAbono.ListIndex = 0
    TxtMontoXDescuento.Visible = False
    
    ''Indica la cantidad de Folios Disponibles
        SkFoliosDisponibles = ""

        Sql = "SELECT count(dte_id) dte " & _
                "FROM dte_folios " & _
                "WHERE rut_emp='" & SP_Rut_Activo & "' AND doc_id=" & 61 & " AND dte_disponible='SI' " & _
                "GROUP BY doc_id"
        Consulta RsTmp3, Sql
        If RsTmp3.RecordCount > 0 Then
            SkFoliosDisponibles = "Folios disponibles:" & RsTmp3!Dte
        End If
    ''Fin Indica la cantidad de Folios Disponibles
    
    Sql = ""
    Sql = "call sp_sel_ParBodegas('" & SP_Rut_Activo & "', " & IG_id_Sucursal_Empresa & ");"
         
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        IG_id_Bodega_Ventas = RsTmp!bod_id
    End If
    If SP_Rut_Activo = "77.774.580-8" Or SP_Rut_Activo = "78.967.170-2" Or SP_Rut_Activo = "76.039.757-1" Then
        LLenarCombo CboNC, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='SI'", "doc_orden"
    Else
        LLenarCombo CboNC, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='SI' AND doc_dte='SI'", "doc_orden"
    End If
    CboNC.ListIndex = 0 'Selecciona por defecto nota de credito electronica por factura
  '  LLenarCombo CboDocVenta, "doc_nombre", "doc_id", "sis_documentos", "doc_documento='VENTA' AND doc_nota_de_credito='SI'", "doc_orden"
  '  CboDocVenta.ListIndex = 0
    DtFecha = Date
    DtFechaBoleta = Date
    CboModalidadNC.ListIndex = 0
      
   ''Carga Los Folios Disponibles
    Sql = ""
    Sql = " call sp_sel_DocRutEmp('" & SP_Rut_Activo & "', 61);"
    Consulta RsTmp, Sql
    TxtDocActual = RsTmp!Numero
    
    If RsTmp.RecordCount = 0 Then
        MsgBox "No cuenta con folios disponibles para emitir NC Electronica..."
        CmdAceptar.Enabled = True
        Exit Sub
    End If
End Sub

Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    If Sm_Nc_Boleta = "NO" Then
        Sql = ""
        Sql = "call sp_sel_VenDetalleLvVenta(" & LvDetalle.SelectedItem.SubItems(2) & ", " & LvDetalle.SelectedItem.SubItems(5) & ", '" & SP_Rut_Activo & "', '" & LvDetalle.SelectedItem.SubItems(7) & "');"
                
        Consulta RsTmp, Sql
        
        LLenar_Grilla RsTmp, Me, LvVenta, True, True, True, False
        Me.ChkProductos.Value = 1
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        
        Next
        BuscaDescuentoNC
        
    Else 'Por boleta
    
        Sql = "SELECT d.id,d.codigo,d.descripcion,unidades,precio_final,subtotal,pro_inventariable " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON d.no_documento =v.no_documento AND v.doc_id=d.doc_id " & _
                    "JOIN maestro_productos m ON d.codigo=m.codigo " & _
                    "WHERE d.no_documento=" & LvDetalle.SelectedItem.SubItems(2) & " AND d.doc_id=" & LvDetalle.SelectedItem.SubItems(5) & " AND " & _
                    "d.rut_emp='" & SP_Rut_Activo & "' AND v.rut_emp='" & SP_Rut_Activo & "' AND m.rut_emp='" & SP_Rut_Activo & "'"
'
'                Sql = ""
'                Sql = "call sp_sel_VenDetalleBoleta(" & LvDetalle.SelectedItem.SubItems(2) & ", " & LvDetalle.SelectedItem.SubItems(5) & ", '" & SP_Rut_Activo & "');"
'
        Consulta RsTmp, Sql
        
        LLenar_Grilla RsTmp, Me, LvVenta, True, True, True, False
        Me.ChkProductos.Value = 1
        For i = 1 To LvVenta.ListItems.Count
            LvVenta.ListItems(i).Checked = True
        
        Next
        BuscaDescuentoNC
    End If
    ''Luego Habilitar
'    TxtSubTotal = "0"
'    TxtDescuentoNC = "0"
'    TxtTotalNC = "0"
    
  '     TxtTotalNC = LvDetalle.ListItems(LvDetalle.SelectedItem.Checked).SubItems(4)
   TxtTotalNC = LvDetalle.SelectedItem.SubItems(4)
End Sub
Private Sub LvDetalle_DblClick()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    
    Busca_Id_Combo CboDocVenta, Val(LvDetalle.SelectedItem.SubItems(5))
    txtNroDocumentoBuscar = LvDetalle.SelectedItem.SubItems(2)
    txtFechaDocBuscado = LvDetalle.SelectedItem.SubItems(3)
    txtTotalDocBuscado = LvDetalle.SelectedItem.SubItems(4)
    
    LvDetalle.ListItems.Remove LvDetalle.SelectedItem.Index
    
    CboDocVenta.SetFocus
End Sub
Private Sub LvVenta_DblClick()
        If LvVenta.SelectedItem Is Nothing Then Exit Sub
        With LvVenta
            TxtCodigo.Tag = LvVenta.SelectedItem
            TxtCodigo = .SelectedItem.SubItems(1)
            Me.TxtDescripcion = .SelectedItem.SubItems(2)
            TxtCantidad = .SelectedItem.SubItems(3)
            TxtPrecio = .SelectedItem.SubItems(4)
            TxtTotalLinea = .SelectedItem.SubItems(5)
            TxtStock = .SelectedItem.SubItems(6)
            TxtCodigo.SetFocus
            LvVenta.ListItems.Remove .SelectedItem.Index
        End With
End Sub
Private Sub LvVenta_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    SumaSeleccion
End Sub

Private Sub Timer2_Timer()
    Me.XP_ProgressBar1.Value = Me.XP_ProgressBar1.Value + 10
    If XP_ProgressBar1.Value = 100 Then XP_ProgressBar1.Value = 1
End Sub
Private Sub txtCantidad_GotFocus()
    En_Foco TxtCantidad
End Sub
Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
    If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub txtCantidad_Validate(Cancel As Boolean)
 '   Dim Dp_Cantidad As decim
    If Val(CxP(TxtCantidad)) > 0 Then
        CalculaCantPrecio
    Else
       TxtCantidad = "0"
    End If
End Sub
Private Sub CalculaCantPrecio()
    If Val(CxP(TxtCantidad)) > 0 Then
        TxtCantidad = Format(TxtCantidad, "#0.000")
        'Dp_Cantidad = TxtCantidad
        TxtTotalLinea = NumFormat(TxtCantidad * CDbl(TxtPrecio))
    End If
End Sub

Private Sub txtNroDocumentoBuscar_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub txtComuna_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtDireccion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtGiro_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

'Private Sub TxtNumCheque_Click()
'    If CboModalidadNC.ListIndex = 0 Then
'        FraProductos.Enabled = False
'        LvDetalle_Click
'        SumaSeleccion
'        BuscaDescuentoNC
'        TxtMontoXDescuento.Visible = False
'    Else
'    End If
'End Sub

Private Sub TxtPrecio_GotFocus()
    En_Foco TxtPrecio
End Sub
Private Sub TxtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtPrecio_Validate(Cancel As Boolean)
    If Val(TxtPrecio) = 0 Then
        TxtPrecio = 0
    Else
    
        TxtPrecio = NumFormat(TxtPrecio)
        CalculaCantPrecio
    End If
End Sub

Private Sub TxtRazonSocial_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_GotFocus()
    En_Foco TxtRut
End Sub

Private Sub TxtRut_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    On Error Resume Next
    If KeyAscii = 13 Then SendKeys ("{TAB}")
    If KeyAscii = 39 Then KeyAscii = 0
End Sub

Private Sub TxtRut_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdBuscaCliente_Click
End Sub

Private Sub TxtRut_LostFocus()
    If Len(TxtRut.Text) = 0 Then
        Me.TxtRazonSocial.Text = ""
        Exit Sub
    End If
  
    
    If ClienteEncontrado Then
       
    Else
       ' TxtRut.SetFocus
    End If
End Sub

Private Sub TxtRut_Validate(Cancel As Boolean)
    TxtRazonSocial.Tag = ""
    If Len(TxtRut.Text) = 0 Then Exit Sub
    
    If CboNC.ListIndex = -1 Then
        MsgBox "Selecione Nota de Credito...", vbOKCancel
        CboNC.SetFocus
        Exit Sub
    End If
    
    LvDetalle.ListItems.Clear
    
    TxtRut.Text = Replace(TxtRut.Text, ".", "")
    TxtRut.Text = Replace(TxtRut.Text, "-", "")
    Respuesta = VerificaRut(TxtRut.Text, NuevoRut)
   
    If Respuesta = True Then
        Me.TxtRut.Text = NuevoRut
        'Buscar el cliente
                Sql = ""
                Sql = "call sp_sel_MaestroClteAsoc('" & SP_Rut_Activo & "', '" & Me.TxtRut.Text & "');"
                
        Consulta RsTmp, Sql
        If RsTmp.RecordCount > 0 Then
                'Cliente encontrado
            With RsTmp
                TxtMontoCredito = !cli_monto_credito
         
                TxtRut.Text = !rut_cliente
                TxtGiro = !giro
                TxtDireccion = !direccion
                txtComuna = !comuna
                txtFono = !fono
                TxtEmail = !email
                TxtRazonSocial.Text = !nombre_rsocial
               
                ClienteEncontrado = True
     
                If Val(TxtMontoCredito) > 0 Then
                    'Debemos consultar el saldo de credito disponible del cliente.
                    TxtCupoUtilizado = ConsultaSaldoCliente(TxtRut)
                    SkCupoCredito = NumFormat(Val(TxtMontoCredito) - Val(TxtCupoUtilizado))
                End If
                
               If CheckRut.Value = 0 Then
               
                
                'buscaremos documentos emitidos a este cliente
                Sql = ""
                Sql = "call sp_sel_DocVentaClte('" & Sm_Nc_Boleta & "', '" & SP_Rut_Activo & "', '" & TxtRut & "');"
                                
                Consulta RsTmp, Sql
                If RsTmp.RecordCount > 0 Then
                    iP_Tipo_NC = 0
                    SG_codigo2 = CboNC.ItemData(CboNC.ListIndex)
                    vta_BuscaDocumentos.SM_SoloSelecciona = "SI"
                    vta_BuscaDocumentos.CmdContinuar.Visible = False
                    vta_BuscaDocumentos.CmdNC.Visible = True
                    vta_BuscaDocumentos.B_Seleccion = True
                    'ver documentos del cliente
                    
                    vta_BuscaDocumentos.Show 1
                    If venPosNC.iP_Tipo_NC > 0 Then
                        Sql = ""
                        Sql = "call sp_sel_DocVtaEmp(" & venPosNC.lP_Documento_Referenciado & ");"
                                                
                         Consulta RsTmp, Sql
                         If RsTmp.RecordCount > 0 Then
                            TxtTotalNC = NumFormat(RsTmp!bruto)
                            LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
                            SumaGrilla
                         
                         End If
                         
                    End If
                    
                    'if DG_ID_Unico = Me.LP_Documento_Referenciado
                Else
                    If Sm_Nc_Boleta = "NO" Then
                        MsgBox "El cliente no tiene facturas registradas en el sistema...", vbInformation
                        TxtRut.SetFocus
                        Exit Sub
                    End If
                    
                End If
                
            End If
                
                
            End With
                
            If bm_SoloVistaDoc Then Exit Sub
    
        Else
                'Cliente no existe aun �crearlo??
                Respuesta = MsgBox("Cliente no encontrado..." & Chr(13) & _
                "     �Desea Crear?    ", vbYesNo + vbQuestion)
                If Respuesta = 6 Then
                    'crear
                    SG_codigo = ""
                    AccionCliente = 4
                    AgregoCliente.TxtRut.Text = Me.TxtRut.Text
                    
                    AgregoCliente.TxtRut.Locked = True
                    ClienteEncontrado = False
                    AgregoCliente.Timer1.Enabled = True
            
                    AgregoCliente.Show 1
                    If ClienteEncontrado = True Then
                        TxtRut_Validate False
                       
                        TxtCodigo.SetFocus
                    Else
                        TxtRut_Validate True
                    End If
                Else
                    'volver al rut
                    ClienteEncontrado = False
                    Me.TxtRut.Text = ""
                    On Error Resume Next
                    SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
                End If
        End If
    Else
        Me.TxtRut.Text = ""
        On Error Resume Next
        SendKeys "+{TAB}" ' Shift TAB retrocede al control anterior segun el orden del tabindex
    End If
    Exit Sub
CancelaImpesionFacturacion:
    'No imprime
    Unload Me
End Sub
Private Sub txtTotalDocBuscado_GotFocus()
    En_Foco txtTotalDocBuscado
End Sub

Private Sub txtTotalDocBuscado_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtMontoXDescuento_KeyPress(KeyAscii As Integer)
    KeyAscii = SoloNumeros(KeyAscii)
    If KeyAscii = 39 Then KeyAscii = 0
End Sub
Private Sub TxtMontoXDescuento_GotFocus()
    En_Foco TxtMontoXDescuento
End Sub
Private Sub txtTotalDocBuscado_Validate(Cancel As Boolean)
    If Val(txtTotalDocBuscado) = 0 Then txtTotalDocBuscado = "0"
End Sub
Private Sub SumaGrilla()
    TxtTotalNC = NumFormat(TotalizaColumna(LvDetalle, "total"))
End Sub
Private Sub NotaCreditoElectronica(Tipo As String, NroDocumento As Long, Docid As Integer, PorBoletas As Boolean)
    Dim Sp_Xml As String, Sp_XmlAdicional As String
    Dim obj As DTECloud.Integracion
    Dim Lp_Sangria As Long
    Dim Sp_Sql As String
    Dim Bp_Adicional As Boolean
    Dim Dp_Neto As Long
    Dim Dp_Iva As Long
    Dim Dp_Total As Long

    Bp_Adicional = False
    
    '1ro Crear xml
    Lp_Sangria = 4
    X = FreeFile
    
   ' Sp_Xml = App.Path & "\dte\" & CboDocVenta.ItemData(CboDocVenta.ListIndex) & "_" & TxtNroDocumento & ".xml"
    Sp_Xml = "C:\FACTURAELECTRONICA\nueva.xml"
      'Open Sp_Archivo For Output As X
    Open Sp_Xml For Output As X
        '<?xml version="1.0" encoding="ISO-8859-1"?>
        Print #X, "<DTE version=" & Chr(34) & "1.0" & Chr(34) & ">"
        Print #X, "" & Space(Lp_Sangria) & "<Documento ID=" & Chr(34) & "ID" & Lp_Id_Venta & Chr(34) & ">"
        'Encabezado
        Print #X, "" & Space(Lp_Sangria) & "<Encabezado>"
            'id documento
            Print #X, "" & Space(Lp_Sangria * 2) & "<IdDoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<TipoDTE>" & Tipo & "</TipoDTE>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Folio>" & NroDocumento & "</Folio>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<FchEmis>" & Fql(DtFecha) & "</FchEmis>"
           '     Print #X, "" & Space(Lp_Sangria * 3) & "<FchVenc>" & Format(DtFecha.Value + CboPlazos.ItemData(CboPlazos.ListIndex), "; YYYY - MM - DD; ") & "</FchVenc>"
                If Sm_Con_Precios_Brutos = "SI" Then
                    '30 Mayo 2015, cuando las lineas de detalle son precios brutos se debe identificar aqui.
                 '   Print #X, "" & Space(Lp_Sangria * 2) & "<MntBruto>1</MntBruto>"
                End If
            Print #X, "" & Space(Lp_Sangria * 2) & "</IdDoc>"
            'Emisor
            'Select empresa para obtener los datos del emisor
                Sp_Sql = ""
                Sp_Sql = "call sp_sel_SisEmpresas('" & SP_Rut_Activo & "');"
                Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Emisor>"
                'Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTEmisor>" & Replace(SP_Rut_Activo, ".", "") & "</RUTEmisor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSoc>" & Replace(RsTmp!nombre_empresa, "�", "N") & "</RznSoc>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroEmis>" & Replace(RsTmp!giro, "�", "N") & "</GiroEmis>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Acteco>" & Replace(RsTmp!actividad, "�", "N") & "</Acteco>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirOrigen>" & Replace(RsTmp!direccion, "�", "N") & "</DirOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaOrigen>" & Replace(RsTmp!comuna, "�", "N") & "</CmnaOrigen>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadOrigen>" & Replace(RsTmp!ciudad, "�", "N") & "</CiudadOrigen>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Emisor>"
            'Receptor
            'Select datos cliente
                Sp_Sql = ""
                Sp_Sql = "call sp_sel_MaestroClte('" & TxtRut & "');"
                        
            Consulta RsTmp, Sp_Sql
            
            Print #X, "" & Space(Lp_Sangria * 2) & " <Receptor>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RUTRecep>" & Replace(TxtRut, ".", "") & "</RUTRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<RznSocRecep>" & QuitarAcentos(Mid(Replace(RsTmp!nombre_rsocial, "�", "N"), 1, 100)) & "</RznSocRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<GiroRecep>" & QuitarAcentos(Mid(Replace(RsTmp!giro, "�", "N"), 1, 40)) & "</GiroRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<Contacto></Contacto>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<DirRecep>" & QuitarAcentos(Mid(Replace(RsTmp!direccion, "�", "N"), 1, 60)) & "</DirRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CmnaRecep>" & QuitarAcentos(Mid(Replace(RsTmp!comuna, "�", "N"), 1, 20)) & "</CmnaRecep>"
                Print #X, "" & Space(Lp_Sangria * 3) & "<CiudadRecep>" & QuitarAcentos(Mid(Replace(RsTmp!ciudad, "�", "N"), 1, 20)) & "</CiudadRecep>"
            Print #X, "" & Space(Lp_Sangria * 2) & "</Receptor>"
            
             'Totales
            Dp_Total = CDbl(TxtTotalNC)
            Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
            Dp_Iva = Dp_Total - Dp_Neto
             
            'Inicio Exento
            If LvDetalle.SelectedItem.SubItems(8) = "NETO" Then
                 Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                     Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & Dp_Total & "</MntExe>"
                     Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & Dp_Total & "</MntTotal>"
                 Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
             Else
'               fin Exento
               If Im_TipoNC = 4 Then ''Si es por descuento
                             'Totales
                    Dp_Total = CDbl(TxtMontoXDescuento)
                    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
                    Dp_Iva = Dp_Total - Dp_Neto
            
                 Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                     Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & Dp_Neto & "</MntNeto>"
                     Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & 0 & "</MntExe>"
                     Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>" & DG_IVA & "</TasaIVA>"
                     Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>" & Dp_Iva & "</IVA>"
                     Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & Dp_Total & "</MntTotal>"
                 Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
               Else
                 Print #X, "" & Space(Lp_Sangria * 2) & " <Totales>"
                     Print #X, "" & Space(Lp_Sangria * 3) & "<MntNeto>" & Dp_Neto & "</MntNeto>"
                     Print #X, "" & Space(Lp_Sangria * 3) & "<MntExe>" & 0 & "</MntExe>"
                     Print #X, "" & Space(Lp_Sangria * 3) & " <TasaIVA>" & DG_IVA & "</TasaIVA>"
                     Print #X, "" & Space(Lp_Sangria * 3) & " <IVA>" & Dp_Iva & "</IVA>"
                     Print #X, "" & Space(Lp_Sangria * 3) & "<MntTotal>" & Dp_Total & "</MntTotal>"
                 Print #X, "" & Space(Lp_Sangria * 2) & "</Totales>"
               End If
            End If
            'Cerramos encabezado
            Print #X, "" & Space(Lp_Sangria) & "</Encabezado>"
                        
           'Comenzamos el detalle
           If SP_Control_Inventario = "SI" Then
           'Recorremos la grilla con productos
           
                If Im_TipoNC = 1 Then
                        'SI ES NC POR ANULACION,
                        'LEEMOS LOS MISMO DATOS DE LA VENTA
                        Sql = ""
                        Sql = "call sp_sel_VenDetalleDocVenta(" & Docid & ", " & NroDocumento & ",'" & SP_Rut_Activo & "');"
                        
                        Consulta RsTmp, Sql
                        i = 1
                        RsTmp.MoveFirst
                        Do While Not RsTmp.EOF
                            'For i = 1 To LvMateriales.ListItems.Count
                              Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & i & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & "" & RsTmp!Codigo & "</VlrCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & QuitarAcentos(Replace(Mid(RsTmp!Descripcion, 1, 80), "�", "N")) & "</NmbItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & Replace(RsTmp!Unidades, ",", ".") & "</QtyItem>"
                       '       Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & Replace(RsTmp!unitario, ",", ".") & "</PrcItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & Replace(Round(RsTmp!unitario, 0), ",", ".") & "</PrcItem>"
              '                    <DescuentoPct>15</DescuentoPct>
              '                    <DescuentoMonto>4212</DescuentoMonto>
              '                             <!- Solo se indica el c�digo del impuesto adicional '
              '                    <CodImpAdic>17</CodImpAdic>
                              Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & Replace(Round(RsTmp!ved_precio_venta_neto, 0), ",", ".") & "</MontoItem>"
                      
                            Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                             'Next
                            RsTmp.MoveNext
                            i = i + 1
                        Loop
                        Dim dsctoGeneral As Long
                        dsctoGeneral = 0
                        If TxtDescuentoNC = "" Then
                          TxtDescuentoNC = 0
                          End If
                         dsctoGeneral = CDbl(TxtDescuentoNC)
                        If dsctoGeneral > 0 Then
                            dsctoGeneral = Round(dsctoGeneral / (1.19), 0)
                            Print #X, "" & Space(Lp_Sangria) & "<DscRcgGlobal>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDR>1</NroLinDR>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<TpoMov>D</TpoMov>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<GlosaDR>Dscto. Cliente</GlosaDR>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<TpoValor>$</TpoValor>"
                                   Print #X, "" & Space(Lp_Sangria * 2) & "<ValorDR>" & dsctoGeneral & "</ValorDR>"
                            Print #X, "" & Space(Lp_Sangria) & "</DscRcgGlobal>"
                        
                                '<DscRcgGlobal>
                                '    <NroLinDR>1</NroLinDR>
                                '    <TpoMov>D</TpoMov>
                                '    <GlosaDR>Descuento global al documento</GlosaDR>
                                ''    <TpoValor>%</TpoValor>
                                 '   <ValorDR>12</ValorDR>
                                '</DscRcgGlobal>
                        End If
           '     If Im_TipoNC = 3 Then
           '     End If
                Else
                    'CUANDO ES PARCIAL, SOLO SE HARA POR LOS PRODUCTOS QUE ESTA SACANDO
                    'LOS QUE ESTAN EN LA GRILLA MARCADOS
                    n = 1
                    For i = 1 To LvVenta.ListItems.Count
                        If LvVenta.ListItems(i).Checked Then
                            Print #X, "" & Space(Lp_Sangria) & "<Detalle>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinDet>" & n & "</NroLinDet>"
                              Print #X, "" & Space(Lp_Sangria * 2) & " <CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<TpoCodigo>PLU</TpoCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 3) & "<VlrCodigo>" & LvVenta.ListItems(i).SubItems(1) & "</VlrCodigo>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "</CdgItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<NmbItem>" & QuitarAcentos(LvVenta.ListItems(i).SubItems(2)) & "</NmbItem>"
                              Print #X, "" & Space(Lp_Sangria * 2) & "<QtyItem>" & CxP(LvVenta.ListItems(i).SubItems(3)) & "</QtyItem>"
                              If LvDetalle.ListItems(1).SubItems(8) = "BRUTO" Then
                                
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & Round(CDbl(LvVenta.ListItems(i).SubItems(4)) / (1.19), 0) & "</PrcItem>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & Round(CDbl(LvVenta.ListItems(i).SubItems(5)) / (1.19), 0) & "</MontoItem>"
                              Else
                                  
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<PrcItem>" & CDbl(LvVenta.ListItems(i).SubItems(4)) & "</PrcItem>"
                                    Print #X, "" & Space(Lp_Sangria * 2) & "<MontoItem>" & CDbl(LvVenta.ListItems(i).SubItems(5)) & "</MontoItem>"
                              End If
                              
                            Print #X, "" & Space(Lp_Sangria) & "</Detalle>"
                            n = n + 1
                        
                        End If
                        
                    Next
    
                End If
            Else
                'CUANDO ES SIN INVENTARIO
            End If
            'Cerramos detalle
          
'        If FrmReferencia.Visible Then
'                'CREAR REFERENCIA
                Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
                    If Not PorBoletas Then
                        'NC por factura
                            Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>33</TpoDocRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & LvDetalle.ListItems(1).SubItems(2) & "</FolioRef>"
        
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(LvDetalle.ListItems(1).SubItems(3)) & "</FchRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & Im_TipoNC & "</CodRef>"
                            If Im_TipoNC = 1 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>ANULA DOCUMENTO " & "   " & TxtComentario & "</RazonRef>"
                            ElseIf Im_TipoNC = 3 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>DEVOLUCION DE MERCADERIAS " & "   " & TxtComentario & "</RazonRef>"
                            ElseIf Im_TipoNC = 4 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>POR DESCUENTO " & "   " & TxtComentario & "</RazonRef>"
                            End If
                    Else
                        'Por Boleta
                            Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>35</TpoDocRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & LvDetalle.ListItems(1).SubItems(2) & "</FolioRef>"
        
                            Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & Fql(LvDetalle.ListItems(1).SubItems(3)) & "</FchRef>"
                            Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & Im_TipoNC & "</CodRef>"
                            If Im_TipoNC = 1 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>ANULA DOCUMENTO " & "   " & TxtComentario & "</RazonRef>"
                            ElseIf Im_TipoNC = 3 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>DEVOLUCION DE MERCADERIAS " & "   " & TxtComentario & "</RazonRef>"
                            ElseIf Im_TipoNC = 4 Then
                                Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>POR DESCUENTO " & "   " & TxtComentario & "</RazonRef>"
                            End If
                        'Nc por boletas
                    End If
                    
                 Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
                        
'                    If SkCodigoReferencia = 4 Then SkCodigoReferencia = 3
'                    SkFechaReferencia = Format(Me.SkFechaReferencia, "YYYY-MM-DD")
'                          '1  SkDocIdReferencia = 33
'
'                    Print #X, "" & Space(Lp_Sangria) & "<Referencia>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<NroLinRef>1</NroLinRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<TpoDocRef>" & SkDocIdReferencia & "</TpoDocRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<FolioRef>" & TxtNroReferencia & "</FolioRef>"
'
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<FchRef>" & SkFechaReferencia & "</FchRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<CodRef>" & SkCodigoReferencia & "</CodRef>"
'                    Print #X, "" & Space(Lp_Sangria * 2) & "<RazonRef>" & TxtMotivoRef & "</RazonRef>"
'
'                Print #X, "" & Space(Lp_Sangria) & "</Referencia>"
'
'
'            'CERRAR REFERNCIA
'        End If

        Print #X, "" & Space(Lp_Sangria) & "</Documento>"
        Print #X, "" & Space(Lp_Sangria) & "</DTE>"
    Close #X
        Bp_Adicional = False
'        End
'    If Len(TxtComentario) > 0 Or Len(TxtOrdenesCompra) > 0 Then
'        Bp_Adicional = True
'        X = FreeFile
'        Sp_XmlAdicional = "C:\nueva_adicional.xml"
'        Open Sp_XmlAdicional For Output As X
'            Print #X, "<Adicional>"
'            If Len(TxtComentario) > 0 Then
'                Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtComentario & "</Uno>"
'                If Len(TxtOrdenesCompra) > 0 Then
'                    Print #X, "" & Space(Lp_Sangria) & "<Dos>" & TxtOrdenesCompra & "</Dos>"
'                End If
'            Else
'                If Len(TxtOrdenesCompra) > 0 Then
'                    Print #X, "" & Space(Lp_Sangria) & "<Uno>" & TxtOrdenesCompra & "</Uno>"
'                End If
'            End If
'            Print #X, "" & Space(Lp_Sangria) & "<Tres></Tres>"
'            Print #X, "" & Space(Lp_Sangria) & "<Cuatro></Cuatro>"
'            Print #X, "" & Space(Lp_Sangria) & "<Cinco></Cinco>"
'            Print #X, "" & Space(Lp_Sangria) & "<Seis></Seis>"
'            Print #X, "" & Space(Lp_Sangria) & "<Siete></Siete>"
'            Print #X, "" & Space(Lp_Sangria) & "<Ocho></Ocho>"
'            Print #X, "" & Space(Lp_Sangria) & "<Nueve></Nueve>"
'            Print #X, "" & Space(Lp_Sangria) & "<Diez></Diez>"
'            Print #X, "</Adicional>"
'        Close #X
'
'
'    End If
        
        Set obj = New DTECloud.Integracion
   
        obj.UrlServicioFacturacion = SG_Url_Factura_Electronica '"http://wsdtedesa.dyndns.biz/WSDTE/Service.asmx"
        obj.HabilitarDescarga = True
        obj.FormatoNombrePDF = "0"
        obj.CarpetaDestino = "C:\FACTURAELECTRONICA\PDF"
        obj.Password = "1234"
        
        obj.AsignarFolio = False
        obj.IncluirCopiaCesion = True
       ' obj.FolioAsignado
        
        
        'MSXML.DOMDocument
        Dim Documento As MSXML2.DOMDocument30
        Set Documento = New DOMDocument30
        
        Documento.preserveWhiteSpace = True
        Documento.Load (Sp_Xml)
        '  Dim oXMLAdicional As New xml.XMLDocument
        Dim documento2 As MSXML2.DOMDocument30
        Set documento2 = New DOMDocument30
        
        documento2.preserveWhiteSpace = True
        If Bp_Adicional Then
            documento2.Load (Sp_XmlAdicional)
        Else
            documento2.Load ("")
        End If
        
        '******************************* modo productivo
        
        obj.Productivo = BG_ModoProductivoDTE
        
        '***********************************
        If obj.ProcesarXMLBasico(Documento.XML, documento2.XML) Then
             'MsgBox obj.URLPDF
             Archivo = "C:\FACTURAELECTRONICA\PDF\T" & Tipo & "F" & NroDocumento & ".PDF"
            ' dte_MostrarPDF.Dte "33", Str(NroDocumento)
         '   If SP_Rut_Activo = "76.178.895-7" Then 'Solo total gomas
         '       DescargaForzadaDTE Trim(Str(NroDocumento))
            
         '   Else
                '16-09-2015
                'Se supone que esto es lo normal, sin embargo, totalgomas no funcionan
               ShellExecute Me.hWnd, "open", Archivo, "", "", 4
          '  End If
            
            
            ' MsgBox obj.URLPDF
        Else
            MsgBox (CStr(obj.IDResultado) + " " + obj.DescripcionResultado)
            Form_Load
        End If
End Sub
Private Sub AbonaNCCredito(IdDocumento As String)
    Dim Lp_SaldoRef As Long
    Dim Lp_Id As Long
    Dim Lp_IdAbo As Long
    Dim Lp_Nro_Comprobante As Long
    
  'Buscar el documento referenciado    /// 20-10-2012
            'y ver si su saldo.
            'si el saldo es mayor o igual a la NC abonar el monto de la nota de credito
            
            Lp_SaldoRef = SaldoDocumento(IdDocumento, "CLI")
            
            Dim Lp_Abono_a_Realizar As Long
            Dim Lp_Abono_a_Pozo As Long
            Lp_Id = Val(IdDocumento)
            'If Lp_SaldoRef >= CDbl(TxtTotal) Then
            If Lp_SaldoRef > 0 Then
                    Bm_AfectarCaja = False
                    'hacer abono
                    lp_abono_a_plazo = 0
                    
                    If Lp_SaldoRef >= CDbl(Me.TxtTotalNC) Then
                        Lp_Abono_a_Realizar = CDbl(TxtTotalNC)
                    Else
                        Lp_Abono_a_Realizar = Lp_SaldoRef
                        Lp_Abono_a_Pozo = CDbl(TxtTotalNC) - Lp_SaldoRef
                    End If
            
                    MsgBox "El monto " & NumFormat(Lp_Abono_a_Realizar) & " de esta NC se abono al documento original...", vbInformation
                    Lp_Nro_Comprobante = AutoIncremento("lee", 200, , IG_id_Sucursal_Empresa)
                
                    'Dim lp_IDAbo As Long
                    Bp_Comprobante_Pago = True
                    Lp_IdAbo = UltimoNro("cta_abonos", "abo_id")
                    PagoDocumento Lp_IdAbo, "CLI", TxtRut, DtFecha, CDbl(Lp_Abono_a_Realizar), 8888, "NC", "ABONO CON NC", LogUsuario, Lp_Nro_Comprobante, Lp_Id, "NCVENTA"

                    Sql = ""
                    Sql = "call sp_ins_CtaAbonoDoc(" & Lp_IdAbo & "," & IdDocumento & "," & CDbl(Lp_Abono_a_Realizar) & ",'" & SP_Rut_Activo & "');"
                                        
                    cn.Execute Sql
                    
                    AutoIncremento "GUARDA", 200, Lp_Nro_Comprobante, IG_id_Sucursal_Empresa
                    
                    Sp_NC_Utilizada = "SI"
                    If Lp_Abono_a_Pozo > 0 Then

                    Sql = ""
                    Sql = "call sp_ins_CtaPozo(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & Me.TxtRut & "'," & Lp_Abono_a_Pozo & ", 2);"
                                                
                    cn.Execute Sql
                    
                    End If
                    
                    
            Else
                    Sql = ""
                    Sql = "call sp_ins_CtaPozo(" & Lp_Id & ",'" & Fql(DtFecha) & "','" & SP_Rut_Activo & "','CLI','" & Me.TxtRut & "'," & Lp_Abono_a_Pozo & ", 2);"
                    
                    cn.Execute Sql
                    Sp_NC_Utilizada = "SI"
            End If
End Sub

Private Sub ImprimeFacturaTermica(Numero As String, Doc As String, Cedible As Boolean, Firma As Boolean, NombreDocumento As String)
    Dim Cx As Double, Cy As Double, Sp_Fecha As String, Ip_Mes As Integer, Dp_S As Double, Sp_Letras As String
    Dim Sp_Neto As String * 12, Sp_IVA As String * 12, Sp_Total As String * 12
    Dim Sp_Cantidad As String * 7
    Dim Sp_Detalle As String * 40
    Dim Sp_Codigo As String * 10
    Dim Sp_Unitario As String * 11
    Dim Sp_Total_linea As String * 11
    Dim Sp_GuiasFacturadas As String
    Dim Sp_Segun_Guias As String
    Dim Sp_Sql As String
    Dim Sp_Neto_P As String * 10
    Dim Sp_IVA_P As String * 10
    Dim Sp_Total_P As String * 10
    
    
    RSet Sp_Neto_P = "NETO    $:"
    RSet Sp_IVA_P = "IVA " & DG_IVA & "% $:"
    RSet Sp_Total_P = "TOTAL   $:"
    Printer.DrawWidth = 2
    Printer.FontName = "Courier New"
    Printer.FontBold = False
    Printer.ScaleMode = vbCentimeters
    Printer.FontSize = 10
    Printer.ScaleMode = 7
    Printer.FontBold = False
    Cx = 0.5
    'Cy = 5.9
    Cy = 1
    Dp_S = 0.17 'interlinea
    Printer.FontName = "Arial"
    Printer.FontSize = 11
    Printer.FontBold = True
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print "                  REPUESTOS BRIONES"
    Else
    
        Printer.Print "                     TOTALGOMAS"
    End If
    linini = Printer.CurrentY + 0.1
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.CurrentX = Cx
    Printer.FontSize = 10
    Printer.FontName = "Courier"
    
    Printer.Print "                R.U.T.:" & SP_Rut_Activo
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    Printer.Print "       NOTA DE CREDITO"
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    
    Printer.Print "                    N� " & Numero
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    linfinm = Printer.CurrentY
    
    Printer.Print "                      S.I.I. - TEMUCO"
    Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    
    Printer.Line (Cy, linini)-(6.7, linfinm), , B  'Rectangulo Encabezado y N� Nota de Venta
     
    
    Printer.FontBold = False
    Printer.FontSize = 9
     
    Printer.CurrentX = Cx
    Printer.CurrentY = linfinm + 0.7
    
    
    Printer.FontBold = True
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print " SERGIO ADOLFO BRIONES VALDERRAMA"
    Else
        Printer.Print " SOC. COMERCIAL TOTALGOMAS LTDA."
    End If
 '   Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    Printer.FontBold = False
    
        
    Printer.Print " VENTA DE PARTES, PIEZAS Y ACCESORIOS"
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
        
    Printer.Print " DE VEHICULOS AUTOMOTORES."
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print " MANUEL MONTT 1117 - TEMUCO - TEMUCO"
    Else
        Printer.Print " MANUEL MONTT 1299 - TEMUCO - TEMUCO"
    End If
  '  Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    If SP_Rut_Activo = "3.630.608-4" Then
    
    Else
        Printer.Print " FONO 0452730022"
    End If
   ' Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    If SP_Rut_Activo = "3.630.608-4" Then
        Printer.Print "repuestosbrionestemuco@gmail.com"
    Else
        Printer.Print " ventastotalgomas@gmail.com"
    End If
    'Printer.CurrentY = Printer.CurrentY + 0.1
    Printer.CurrentX = Cx
    
    
    Printer.FontName = "Arial"
    Printer.FontSize = "8"
    
    md = 1.3
    'pos = Printer.CurrentY
    Sp_Fecha = Date
    Printer.CurrentX = Cx
    Printer.CurrentY = Printer.CurrentY + 0.6
    Cy = Printer.CurrentY
    Printer.FontBold = True
    Printer.Print "FECHA:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    
    Printer.Print Day(Date)
    Printer.CurrentX = Cx + md + 0.5
    Printer.CurrentY = Cy
    Printer.Print MonthName(Month(Date))
    Printer.CurrentX = Cx + md + 2.5
    Printer.CurrentY = Cy
    Printer.Print Year(Date)
    
    'Printer.Print Format(Sp_Fecha, "dd mmmm yyyy")
   ' Ip_Mes = Val(Mid(Sp_Fecha, 4, 2))
   ' Printer.Print Space(43) & Mid(Sp_Fecha, 1, 2) & Space(7) & UCase(MonthName(Ip_Mes))
   ' Printer.CurrentX = Cx + 6.7
   ' Printer.CurrentY = pos
   ' Printer.Print Space(49) & Mid(Sp_Fecha, 9, 2)  'A�O DE LA FECHA
   ' Printer.CurrentX = Cx + 0.6
   ' 'Printer.CurrentY = Printer.CurrentY + Dp_S
   ' Printer.CurrentY = Printer.CurrentY + Dp_S + 0.1
    
    
    
    
    
    
    'pos = Printer.CurrentY
    
    
     'Select datos cliente
    'Sp_Sql = "SELECT nombre_rsocial,giro,direccion,comuna,ciudad " & _
    '                    "FROM maestro_clientes " & _
    '                    "WHERE rut_cliente='" & TxtRut & "'"
    ' Modificado: Marco Sobarzo
        ' Motivo: Reemplaza codigo embebido por procedimiento
        ' Fecha: 15-12-2016
        Sp_Sql = ""
        Sp_Sql = "call sp_sel_InfMaestroClte('" & TxtRut & "');"
        Consulta RsTmp, Sp_Sql
    
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "RUT:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print TxtRut
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "R. Social:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!nombre_rsocial
    
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Giro.:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!giro
        
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Direcc.:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!direccion
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Comuna:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!comuna
    
    

    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Ciudad:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print RsTmp!ciudad
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "C. Pago:"
    Printer.FontBold = False
    Printer.CurrentX = Cx + md
    Printer.CurrentY = Cy
    Printer.Print Sm_Fpago
    
    
    Printer.CurrentY = Printer.CurrentY + 0.6
    
    
    Cy = Printer.CurrentY
    Printer.CurrentX = Cx
    Printer.FontBold = True
    Printer.Print "Cant   / Codigo / Descripcion        / Valor"
    Printer.FontBold = False
    
    
    Cy = Printer.CurrentY + 2
    
    ' Sql = "SELECT unidades,descripcion,precio_final,ved_codigo_interno " & _
    '        "FROM ven_detalle " & _
    '        "WHERE doc_id=" & Doc & " AND no_documento=" & Numero & " AND rut_emp='" & SP_Rut_Activo & "'"
    ' Modificado: Marco Sobarzo
        ' Motivo: Reemplaza codigo embebido por procedimiento
        ' Fecha: 15-12-2016
        Sql = ""
        Sql = "call sp_sel_InfVenDetalle(" & Doc & ", " & Numero & ", '" & SP_Rut_Activo & "');"
        
        Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF
          '  For i = 1 To LvMateriales.ListItems.Count
                '6 - 3 - 7 - 8
                Printer.CurrentY = Printer.CurrentY + 0.11
                'Printer.CurrentX = Cx - 1.5
                
                Printer.CurrentX = Cx - 1
                
                
                'Printer.Print Right(Space(8) & LvMateriales.ListItems(i).SubItems(6), 8) & Space(7) & _
                        Left(LvMateriales.ListItems(i).SubItems(3) & Space(44), 44) & Space(3) & _
                        Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(7)), 11) & Space(2) & _
                        Right(Space(11) & NumFormat(LvMateriales.ListItems(i).SubItems(8)), 11)
                RSet Sp_Cantidad = RsTmp!Unidades
                LSet Sp_Detalle = RsTmp!Descripcion
                LSet Sp_Codigo = "" & RsTmp!ved_codigo_interno
                RSet Sp_Unitario = NumFormat(RsTmp!precio_final)
                RSet Sp_Total_linea = NumFormat(Round(RsTmp!precio_final * RsTmp!Unidades, 0))
                
                Printer.FontName = "Arial"
                Cy = Printer.CurrentY
                Printer.CurrentX = Cx - 0.3
                Printer.FontBold = True
                pos = Printer.CurrentY
                Printer.Print Sp_Cantidad & Space(2) & " x codigo:" & Sp_Codigo
                
                
                
                Printer.CurrentY = pos
                Printer.CurrentX = Cx + 4.1
                Printer.Print Sp_Unitario
                Printer.CurrentY = pos
                Printer.CurrentX = Cx + 5.4
                Printer.Print Sp_Total_linea
                
               
                Printer.FontBold = False
                Printer.CurrentX = Cx - 0.3
                Printer.CurrentY = Cy + 0.35
                pos = Printer.CurrentY
                Printer.Print Sp_Detalle
                
                
                
               ' Printer.Print Sp_Cantidad & Space(1) & Sp_Detalle & Space(1) & Sp_Codigo & Space(1) & Sp_Unitario & Space(1) & Sp_Total_linea
                 
                        
           ' Next
                RsTmp.MoveNext
         Loop
    End If
     Printer.FontSize = 8
    Printer.FontBold = True
    
    Printer.CurrentY = Printer.CurrentY + 0.7
     Printer.FontName = "Courier New"
     
     
     'Descunto
  '  If Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)) > 0 Then
  '
  '
  '          Printer.CurrentX = Cx + 2
  '          Printer.Print "Descuento $:" & " " & NumFormat(Val(LvDetalle.SelectedItem.SubItems(11)) + Val(LvDetalle.SelectedItem.SubItems(12)) - Val(LvDetalle.SelectedItem.SubItems(13)))
  '          Printer.CurrentY = Printer.CurrentY + 0.3
  '
  '
  '  End If
     
    
      'Totales
    Dp_Total = CDbl(TxtTotalNC)
    Dp_Neto = Round(Dp_Total / Val("1." & DG_IVA), 0)
    Dp_Iva = Dp_Total - Dp_Neto
    
    Printer.FontBold = True
    
    RSet Sp_Neto = NumFormat(Dp_Neto)
    RSet Sp_IVA = NumFormat(Dp_Iva)
    RSet Sp_Total = NumFormat(Dp_Total)
    Printer.CurrentX = Cx + 1
    linini = Printer.CurrentY - 0.1
    Printer.Print Sp_Neto_P & " " & Sp_Neto   'SI ES GUIA NO IMPRIME NETO IVA TOTAL, SOLO NETO

        
    Printer.CurrentX = Cx + 1
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.Print Sp_IVA_P & " " & Sp_IVA
    Printer.CurrentX = Cx + 1
    
    Printer.CurrentY = Printer.CurrentY + 0.2
    Printer.Print Sp_Total_P & " " & Sp_Total
    linfin = Printer.CurrentY + 0.1
    
    Printer.Line (Cx + 0.5, linini)-(6, linfin), , B 'Rectangulo Encabezado y N� Nota de Venta

    
    Printer.FontSize = 8
  
    Printer.CurrentY = Printer.CurrentY + 0.3
    pos = Printer.CurrentY
    Sp_Letras = UCase(BrutoAletras(CDbl(Dp_Total), True))
    mitad = Len(Sp_Letras) \ 2
    buscaespacio = InStr(mitad, Sp_Letras, " ")
    Printer.CurrentX = Cx
    If buscaespacio > 0 Then
            Printer.Print "Son: " & Mid(Sp_Letras, 1, buscaespacio)
            Printer.CurrentX = Cx + 1
            Printer.CurrentY = Printer.CurrentY + 0.1
            
            Printer.Print Mid(Sp_Letras, buscaespacio)
    Else
                
              Printer.Print "Son: " & Sp_Letras
    End If
    
    
    Printer.CurrentY = Printer.CurrentY + 0.3
     Printer.CurrentX = Cx
    
    Printer.PaintPicture LoadPicture(App.Path & "\timbre_pdf417.jpg"), Cx + 0.2, Printer.CurrentY
    Printer.CurrentY = Printer.CurrentY + 3.2
    Printer.CurrentX = Cx
    Printer.Print "Timbre Electronico S.I.I."
    Printer.CurrentX = Cx
    Printer.Print "Resolucion 80 del 22/08/2014"
    Printer.CurrentX = Cx
    Printer.Print "Verifique Documento: http://www.sii.cl"
    Printer.CurrentY = Printer.CurrentY + 0.3
    
    If Cedible Then
        Printer.FontSize = 10
        Printer.FontBold = True
        Printer.CurrentX = Cx
        Printer.Print "CEDIBLE"
        Printer.CurrentY = Printer.CurrentY + 0.2
        Printer.FontBold = False
    End If
    pos = Printer.CurrentY
    
    If Firma Then
        Printer.FontName = "Arial"
        Coloca Cx, pos, "Nombre:", True, 9
        Coloca Cx, pos + 0.4, "RUT:", True, 9
        Coloca Cx, pos + 0.8, "Fecha:", True, 9
        Coloca Cx, pos + 1.2, "Recinto:", True, 9
        Coloca Cx, pos + 1.6, "Firma:", True, 9
        Coloca Cx, pos + 2, "El acuse de recibo que se declara en este acto, de  ", False, 8
        Coloca Cx, pos + 2.3, "acuerdo a lo dispuesto en la letra b) del Art 4�, y la", False, 8
        Coloca Cx, pos + 2.6, "letra c) del Art. 5� de la ley 19.983, acredita que la", False, 8
        Coloca Cx, pos + 2.9, "entrega de mercaderias o servicio(s) prestado(s) ", False, 8
        Coloca Cx, pos + 3.2, "ha(n) sido recibido(s)", False, 8
        
        Printer.Line (Cx - 0.3, pos - 0.2)-(7, pos + 3.6), , B 'Rectangulo Encabezado y N� Nota de Venta
        Printer.CurrentY = pos + 3.8
    
    End If
    
    
    
    Printer.FontName = "Arial"
    Printer.FontSize = "7"
    Printer.CurrentX = Cx + 0.5
    Printer.Print "Desarrollado por ventas@alvamar.cl"
    
    Printer.NewPage
    Printer.EndDoc
End Sub

Private Sub EstableImpresora(NombreImpresora As String)
   'Seteamos al impresora.
    Dim impresora As Printer
    ' Establece la impresora que se utilizar� para imprimir
    'Recorremos el objeto Printers
    For Each impresora In Printers
        'Si es igual al contenido se�alado en el combobox
        If UCase(impresora.DeviceName) = UCase(NombreImpresora) Then
            'Lo seteamos as�.
            Set Printer = impresora
            Exit For
        End If
    Next

End Sub
Public Sub ActualizaNC()
    Dim lp_Promedio As Double
    Dim Sp_Producto As String
    Dim Rp_Producto As Recordset
    
    Consulta RsTmp, Sm_Sql_Productos_NC
    If RsTmp.RecordCount > 0 Then
        RsTmp.MoveFirst
        Do While Not RsTmp.EOF

                Sp_Producto = ""
                Sp_Producto = "call sp_sel_MaestroProd('" & RsTmp!Codigo & "', '" & SP_Rut_Activo & "');"
                                
                Consulta Rp_Producto, Sp_Producto
                If Rp_Producto.RecordCount > 0 Then
                    If Rp_Producto!pro_inventariable = "SI" Then
                    
                Sql = "sp_upd_MaestroProd(" & CxP(RsTmp!Unidades) & ", '" & RsTmp!Codigo & "', '" & SP_Rut_Activo & "');"
                cn.Execute Sql
                        
                       '  = 0
                lp_Promedio = CostoAVG(RsTmp!Codigo, IG_id_Bodega_Ventas)
                        
                        
                      ' MsgBox Val(CxP(Val(.ListItems(i).SubItems(6))))
                        Kardex Fql(Date), "ENTRADA", Me.CboNC.ItemData(CboNC.ListIndex), _
                                        Val(Lp_Folio), IG_id_Bodega_Ventas, RsTmp!Codigo, Val(CxP(RsTmp!Unidades)), _
                                      "ANULA DOCUMENTO " & LvDetalle.ListItems(1).SubItems(1) & " " & LvDetalle.ListItems(1).SubItems(2), lp_Promedio, lp_Promedio * Val(CxP(RsTmp!Unidades)), Me.TxtRut.Text, Me.TxtRazonSocial, "NO", 0, CboNC.ItemData(CboNC.ListIndex), , , , 0
                    Else
                        'Al no ser inventariable, no pasa nada
                    End If
                End If
                RsTmp.MoveNext
        Loop
    End If
            '
End Sub
