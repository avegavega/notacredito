VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form AgregarProducto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nuevo Producto"
   ClientHeight    =   5370
   ClientLeft      =   1290
   ClientTop       =   1500
   ClientWidth     =   7620
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   7620
   Begin VB.TextBox TxtComentario 
      Height          =   735
      Left            =   9480
      MultiLine       =   -1  'True
      TabIndex        =   41
      Top             =   4890
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.TextBox TxtStockCritico 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   9480
      TabIndex        =   40
      Text            =   "0"
      Top             =   3690
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox TxtStockActual 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   9480
      Locked          =   -1  'True
      TabIndex        =   39
      TabStop         =   0   'False
      Text            =   "0"
      Top             =   3390
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox TxtPrecioCompra 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   9495
      Locked          =   -1  'True
      TabIndex        =   38
      TabStop         =   0   'False
      Text            =   "0"
      Top             =   2250
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.ComboBox CboMarca 
      Height          =   315
      ItemData        =   "AgregarProducto.frx":0000
      Left            =   9495
      List            =   "AgregarProducto.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   37
      Top             =   1065
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.ComboBox CboBodega 
      Height          =   315
      ItemData        =   "AgregarProducto.frx":0004
      Left            =   9480
      List            =   "AgregarProducto.frx":0006
      Style           =   2  'Dropdown List
      TabIndex        =   35
      Top             =   3975
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.TextBox txtUbicacionBodega 
      Height          =   285
      Left            =   9480
      TabIndex        =   34
      Top             =   4290
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.CommandButton CmdMarcas 
      Caption         =   "Marcas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   11895
      TabIndex        =   33
      ToolTipText     =   "Ingrese al Mantenedor de Marcas de Art�culos"
      Top             =   1065
      Visible         =   0   'False
      Width           =   810
   End
   Begin VB.CommandButton CmdBodega 
      Caption         =   "Bodega"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   11880
      TabIndex        =   32
      Top             =   3975
      Visible         =   0   'False
      Width           =   810
   End
   Begin VB.TextBox TxtFlete 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   9495
      TabIndex        =   31
      Text            =   "0"
      Top             =   1965
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Frame FrameAyuda 
      Caption         =   "Ayuda"
      Height          =   645
      Left            =   525
      TabIndex        =   24
      Top             =   4500
      Width           =   6825
      Begin ACTIVESKINLibCtl.SkinLabel skAyuda 
         Height          =   300
         Left            =   180
         OleObjectBlob   =   "AgregarProducto.frx":0008
         TabIndex        =   25
         Top             =   255
         Width           =   6480
      End
   End
   Begin VB.Timer Timer2 
      Interval        =   100
      Left            =   105
      Top             =   6900
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   30
      Top             =   6135
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ingreso de Productos"
      Height          =   4200
      Left            =   630
      TabIndex        =   11
      Top             =   315
      Width           =   6870
      Begin VB.ComboBox CboHabilitado 
         Height          =   315
         ItemData        =   "AgregarProducto.frx":005F
         Left            =   1905
         List            =   "AgregarProducto.frx":0069
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   2850
         Width           =   765
      End
      Begin VB.TextBox TxtPrecioCostoSFlete 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1905
         TabIndex        =   4
         Text            =   "0"
         Top             =   1740
         Width           =   975
      End
      Begin VB.CommandButton CmdUme 
         Caption         =   "UM"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6240
         TabIndex        =   28
         ToolTipText     =   "Unidades de medida"
         Top             =   1440
         Width           =   450
      End
      Begin VB.ComboBox CboUme 
         Height          =   315
         ItemData        =   "AgregarProducto.frx":0075
         Left            =   4815
         List            =   "AgregarProducto.frx":0077
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1440
         Width           =   1440
      End
      Begin VB.CommandButton CmdFamilias 
         Caption         =   "Tipos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5160
         TabIndex        =   27
         ToolTipText     =   "Ingrese al Mantenedor de Tipos de Art�culos"
         Top             =   810
         Width           =   1095
      End
      Begin VB.ComboBox CboInventariable 
         Height          =   315
         ItemData        =   "AgregarProducto.frx":0079
         Left            =   1905
         List            =   "AgregarProducto.frx":0083
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   3165
         Width           =   765
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel11 
         Height          =   225
         Left            =   2910
         OleObjectBlob   =   "AgregarProducto.frx":008F
         TabIndex        =   23
         Top             =   2670
         Width           =   300
      End
      Begin VB.Frame Frame2 
         Caption         =   "Impuestos del Producto"
         Enabled         =   0   'False
         Height          =   3210
         Left            =   7095
         TabIndex        =   21
         Top             =   1035
         Width           =   4545
         Begin MSComctlLib.ListView LVDetalle 
            Height          =   2670
            Left            =   255
            TabIndex        =   22
            Top             =   390
            Width           =   4110
            _ExtentX        =   7250
            _ExtentY        =   4710
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Id"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Descripcion"
               Object.Width           =   4260
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Tag             =   "N102"
               Text            =   "Factor"
               Object.Width           =   1720
            EndProperty
         End
      End
      Begin VB.TextBox TxtCodigo 
         Height          =   285
         Left            =   1905
         MaxLength       =   50
         TabIndex        =   0
         ToolTipText     =   "Aqui se ingresa el codigo unico"
         Top             =   510
         Width           =   2055
      End
      Begin VB.TextBox TxtDescripcion 
         Height          =   285
         Left            =   1905
         TabIndex        =   2
         Top             =   1455
         Width           =   2895
      End
      Begin VB.TextBox TxtPorcentaje 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00%"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   13322
            SubFormatType   =   5
         EndProperty
         Height          =   285
         Left            =   1905
         TabIndex        =   5
         Text            =   "0"
         Top             =   2010
         Width           =   975
      End
      Begin VB.TextBox TxtPrecioVta 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1905
         TabIndex        =   6
         Text            =   "0"
         Top             =   2310
         Width           =   975
      End
      Begin VB.CommandButton CmdGuardar 
         Caption         =   "Guardar Registro"
         Height          =   375
         Left            =   45
         TabIndex        =   9
         Top             =   3765
         Width           =   1800
      End
      Begin VB.CommandButton CmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "&Retornar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5025
         TabIndex        =   10
         ToolTipText     =   "Salir sin hacer cambios"
         Top             =   3615
         Width           =   1695
      End
      Begin VB.ComboBox CboTipoProducto 
         Height          =   315
         Left            =   1905
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   795
         Width           =   3255
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel7 
         Height          =   255
         Left            =   345
         OleObjectBlob   =   "AgregarProducto.frx":00ED
         TabIndex        =   12
         Top             =   2595
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   480
         OleObjectBlob   =   "AgregarProducto.frx":0169
         TabIndex        =   13
         Top             =   2355
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   600
         OleObjectBlob   =   "AgregarProducto.frx":01E5
         TabIndex        =   14
         Top             =   2055
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   315
         Left            =   495
         OleObjectBlob   =   "AgregarProducto.frx":0257
         TabIndex        =   15
         Top             =   840
         Width           =   1335
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   375
         OleObjectBlob   =   "AgregarProducto.frx":02D5
         TabIndex        =   16
         Top             =   510
         Width           =   1455
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Index           =   0
         Left            =   615
         OleObjectBlob   =   "AgregarProducto.frx":033F
         TabIndex        =   17
         Top             =   1455
         Width           =   1215
      End
      Begin ACTIVESKINLibCtl.SkinLabel LbMargen 
         Height          =   255
         Left            =   1905
         OleObjectBlob   =   "AgregarProducto.frx":03B3
         TabIndex        =   18
         Top             =   2610
         Width           =   975
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel12 
         Height          =   195
         Left            =   675
         OleObjectBlob   =   "AgregarProducto.frx":0413
         TabIndex        =   26
         Top             =   3180
         Width           =   1095
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel13 
         Height          =   255
         Left            =   60
         OleObjectBlob   =   "AgregarProducto.frx":048B
         TabIndex        =   29
         Top             =   1785
         Width           =   1770
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel15 
         Height          =   195
         Left            =   690
         OleObjectBlob   =   "AgregarProducto.frx":0501
         TabIndex        =   30
         Top             =   2880
         Width           =   1095
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Para poder grabar el registro debe completar todos los campos"
         Height          =   780
         Left            =   3870
         TabIndex        =   20
         Top             =   2085
         Width           =   1935
      End
      Begin VB.Label LbCodigoActual 
         Caption         =   "Label5"
         Height          =   255
         Left            =   4800
         TabIndex        =   19
         Top             =   60
         Width           =   1335
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   0
      OleObjectBlob   =   "AgregarProducto.frx":0573
      Top             =   5520
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkiUbicaci�n 
      Height          =   255
      Left            =   8190
      OleObjectBlob   =   "AgregarProducto.frx":07A7
      TabIndex        =   36
      Top             =   4005
      Visible         =   0   'False
      Width           =   1215
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel10 
      Height          =   375
      Left            =   8310
      OleObjectBlob   =   "AgregarProducto.frx":0811
      TabIndex        =   42
      Top             =   4860
      Visible         =   0   'False
      Width           =   1095
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel9 
      Height          =   255
      Left            =   7950
      OleObjectBlob   =   "AgregarProducto.frx":0883
      TabIndex        =   43
      Top             =   3435
      Visible         =   0   'False
      Width           =   1455
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel8 
      Height          =   255
      Left            =   7950
      OleObjectBlob   =   "AgregarProducto.frx":08F9
      TabIndex        =   44
      Top             =   3720
      Visible         =   0   'False
      Width           =   1455
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
      Height          =   255
      Left            =   7650
      OleObjectBlob   =   "AgregarProducto.frx":096F
      TabIndex        =   45
      Top             =   2295
      Visible         =   0   'False
      Width           =   1770
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   255
      Index           =   1
      Left            =   8205
      OleObjectBlob   =   "AgregarProducto.frx":09F9
      TabIndex        =   46
      Top             =   1080
      Visible         =   0   'False
      Width           =   1215
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
      Height          =   255
      Index           =   2
      Left            =   8190
      OleObjectBlob   =   "AgregarProducto.frx":0A61
      TabIndex        =   47
      Top             =   4290
      Visible         =   0   'False
      Width           =   1215
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkinLabel14 
      Height          =   255
      Left            =   7635
      OleObjectBlob   =   "AgregarProducto.frx":0AD1
      TabIndex        =   48
      Top             =   2010
      Visible         =   0   'False
      Width           =   1770
   End
End
Attribute VB_Name = "AgregarProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Bm_Nuevo As Boolean
Private Sub CboInventariable_GotFocus()
    FrameAyuda = "INVENTARIABLE"
    skAyuda = "Si no afecta inventario seleccione NO "
End Sub
Private Sub CboInventariable_LostFocus()
    FrameAyuda = ""
    skAyuda = Empty
End Sub
Private Sub CboMarca_GotFocus()
    FrameAyuda = "Marca o Proveedor(overlock,singer,etc)"
    skAyuda = "Aqui se ingresa la marca o el proveedor del producto "
End Sub
Private Sub CboMarca_LostFocus()
    skAyuda = Empty
    FrameAyuda = ""
End Sub
Private Sub CboTipoProducto_GotFocus()
    FrameAyuda = "Tipo (sacos,g�neros,hilos,etc)"
    skAyuda = "Aqui se ingresa el tipo de producto "
End Sub
Private Sub CboTipoProducto_LostFocus()
    skAyuda = Empty
    FrameAyuda = ""
End Sub
Private Sub CmdBodega_Click()
     With Mantenedor_Simple
        .S_Id = "bod_id"
        .S_Nombre = "bod_nombre"
        .S_Activo = "bod_activo"
        .S_tabla = "par_bodegas"
        .S_Consulta = "SELECT bod_id,bod_nombre,bod_activo FROM par_bodegas WHERE rut_emp='" & SP_Rut_Activo & "' ORDER BY bod_nombre"
        .S_RutEmpresa = "SI"
        .Caption = "Mantenedor de bodegas"
        .FrmMantenedor.Caption = "Bodegas"
        .B_Editable = True
        .Show 1
    End With
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
End Sub

Private Sub CmdCancelar_Click()
    If AccionProducto = 1 Then
        MasterProductos.CmdNuevo.Enabled = True
        MasterProductos.CmdEditar.Enabled = True
    End If
    Unload Me
End Sub

Private Sub CmdFamilias_Click()
      'Aqui llamamos a un mantenedor
    Sql = "SELECT mav_id,mav_nombre,mav_activo,mav_tabla,mav_consulta,mav_caption,mav_caption_frm,man_editable, " & _
                        "man_dep_id,man_dep_nombre,man_dep_tabla,man_dep_activo,man_dep_titulo " & _
                  "FROM sis_mantenedor_simple " & _
                  "WHERE man_activo='SI' AND man_id=8"
            Consulta RsTmp2, Sql
            If RsTmp2.RecordCount > 0 Then
                With Mantenedor_Dependencia
                    .S_Id = RsTmp2!mav_id
                    .S_Nombre = RsTmp2!mav_nombre
                    .S_Activo = RsTmp2!mav_activo
                    .S_tabla = RsTmp2!mav_tabla
                    .S_Consulta = RsTmp2!mav_consulta
                    .B_Editable = IIf(RsTmp2!man_editable = "SI", True, False)
                    .S_id_dep = RsTmp2!man_dep_id
                    .S_nombre_dep = RsTmp2!man_dep_nombre
                    .S_Activo_dep = RsTmp2!man_dep_activo
                    .S_tabla_dep = RsTmp2!man_dep_tabla
                    .S_titulo_dep = RsTmp2!man_dep_titulo
                    .CmdBusca.Visible = True
'                    .Caption = RsTmp2!mav_caption
'                    .FrmMantenedor.Caption = RsTmp2!mav_caption_frm

                    .Show 1
                End With
            End If
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
End Sub

Private Sub CmdGuardar_Click()
    Dim Filtro As String
    Dim sp_Actualiza As String
    If Len(TxtComentario) = 0 Then TxtComentario = "-"
    If Len(txtUbicacion) = 0 Then txtUbicacion = "-"
    'accion producto 7 = "Venta directa"'
    'accion producto 6 = "Maestro Productos"'
    'accion producto 8 = "OT
    'accion producto 4 = "busca producto
    Filtro = "Codigo = '" & Me.TxtCodigo.Text & "'"
    If Val(Me.TxtPorcentaje) = 0 Then TxtPorcentaje = 0
    
    If CboUme.ListIndex = -1 Then
        MsgBox "Seleccione Unidad de Medida", vbInformation
        CboUme.SetFocus
        Exit Sub
    End If
    If CboMarca.ListIndex = -1 Then
        MsgBox "Seleccione Marca", vbInformation
        CboMarca.SetFocus
        Exit Sub
    End If
    If CboBodega.ListIndex = -1 Then
        MsgBox "Seleccione Bodega...", vbInformation
        CboBodega.SetFocus
        Exit Sub
    End If
    On Error GoTo ErrorGrabando
    cn.BeginTrans
    If Bm_Nuevo Then
            Sql = "SELECT codigo " & _
                  "FROM maestro_productos " & _
                  "WHERE  rut_emp='" & SP_Rut_Activo & "' AND  " & Filtro
            Call Consulta(RsTmp, Sql)
            If RsTmp.RecordCount > 0 Then
                MsgBox "El codigo ingresado no esta disponible" & Chr(13) & "El sistema no permite la duplicacion de codigos"
                Me.TxtCodigo.SetFocus
                Exit Sub
            End If
            Sql = "INSERT INTO maestro_productos (" & _
                  "rut_empresa,marca,codigo,descripcion,precio_compra,porciento_utilidad,precio_venta,margen," & _
                  "stock_actual,stock_critico,ubicacion_bodega,comentario,bod_id,mar_id,tip_id,pro_inventariable," & _
                  "rut_emp,ume_id,pro_precio_sin_flete,pro_precio_flete,pro_activo) " & _
                  "VALUES ('" & RutEmpresa & "','" & CboMarca.Text & "','" & TxtCodigo & "','" & _
                  TxtDescripcion & "'," & TxtPrecioCompra & "," & Replace(TxtPorcentaje, ",", ".") & "," & _
                  TxtPrecioVta & "," & LbMargen & "," & TxtStockActual & "," & TxtStockCritico & ",'" & _
                   txtUbicacion & "','" & "" & TxtComentario & "'," & CboBodega.ItemData(CboBodega.ListIndex) & _
                   "," & CboMarca.ItemData(CboMarca.ListIndex) & "," & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & ",'" & Me.CboInventariable.Text & _
                   "','" & SP_Rut_Activo & "'," & CboUme.ItemData(CboUme.ListIndex) & "," & CDbl(TxtPrecioCostoSFlete) & "," & _
                   CDbl(TxtFlete) & ",'" & CboHabilitado.Text & "')"
                   cn.Execute Sql
            
            'Aqui creamos el Kardex inicial del producto
            '2 Abril 2011
            Kardex Format(Date, "YYYY-MM-DD"), "ENTRADA", 0, 0, CboBodega.ItemData(CboBodega.ListIndex), _
            TxtCodigo, TxtStockActual, "KARDEX INICIAL", TxtPrecioCompra, TxtPrecioCompra * TxtStockActual
            
            
         Else 'ACTUALIZAR PRODUCTO
            sp_Actualiza = "UPDATE maestro_productos SET marca='" & CboMarca.Text & "'," & _
                                                "descripcion='" & TxtDescripcion & "'," & _
                                                "precio_compra=" & TxtPrecioCompra & "," & _
                                                "porciento_utilidad=" & Replace(TxtPorcentaje, ",", ".") & "," & _
                                                "precio_venta=" & TxtPrecioVta & "," & _
                                                "margen=" & LbMargen & "," & _
                                                "stock_actual=" & TxtStockActual & "," & _
                                                "stock_critico=" & TxtStockCritico & "," & _
                                                "ubicacion_bodega='" & txtUbicacion & "'," & _
                                                "comentario='" & TxtComentario & "', " & _
                                                "bod_id=" & CboBodega.ItemData(CboBodega.ListIndex) & "," & _
                                                "mar_id=" & CboMarca.ItemData(CboMarca.ListIndex) & "," & _
                                                "pro_inventariable='" & Me.CboInventariable.Text & "'," & _
                                                "rut_emp='" & SP_Rut_Activo & "'," & _
                                                "tip_id=" & CboTipoProducto.ItemData(CboTipoProducto.ListIndex) & ", " & _
                                                "ume_id=" & CboUme.ItemData(CboUme.ListIndex) & "," & _
                                                "pro_precio_sin_flete=" & CDbl(Me.TxtPrecioCostoSFlete) & "," & _
                                                "pro_precio_flete=" & CDbl(TxtFlete) & "," & _
                                                "pro_activo='" & CboHabilitado.Text & "' " & _
                    "WHERE codigo='" & TxtCodigo & "' AND rut_emp='" & SP_Rut_Activo & "'"
            Debug.Print sp_Actualiza
            
            
            cn.Execute sp_Actualiza
            
            sp_Actualiza = "UPDATE pro_stock SET pro_ultimo_precio_compra=" & CDbl(TxtPrecioCompra) & " " & _
                           "WHERE rut_emp='" & SP_Rut_Activo & "' AND pro_codigo='" & TxtCodigo & "'"
            cn.Execute sp_Actualiza
        End If
        cn.CommitTrans
        Unload Me
        Exit Sub
ErrorGrabando:
    MsgBox "Ocurrio un error al intentar grabar el registro...", vbExclamation
    cn.RollbackTrans
End Sub


Private Sub CmdMarcas_Click()
    With Mantenedor_Simple
        .S_Id = "mar_id"
        .S_Nombre = "mar_nombre"
        .S_Activo = "mar_activo"
        .S_tabla = "par_marcas"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Marcas de Articulos"
        .FrmMantenedor.Caption = "Marcas "
        .B_Editable = True
        .Show 1
    End With
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"

End Sub

Private Sub CmdUme_Click()
    With Mantenedor_Simple
        .S_Id = "ume_id"
        .S_Nombre = "ume_nombre"
        .S_Activo = "ume_activo"
        .S_tabla = "sis_unidad_medida"
        .S_RutEmpresa = "SI"
        .S_Consulta = "SELECT " & .S_Id & "," & .S_Nombre & "," & .S_Activo & " " & _
                      "FROM " & .S_tabla & " " & _
                      "WHERE  rut_emp='" & SP_Rut_Activo & "' "
        .Caption = "Mantenedor Unidades de medida"
        .FrmMantenedor.Caption = "Unidades de medida "
        .B_Editable = True
        .Show 1
    End With
    LLenarCombo CboUme, "ume_nombre", "ume_id", "sis_unidad_medida", "ume_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ume_nombre"

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{Tab}"
End Sub

Private Sub Form_Load()
    CboHabilitado.ListIndex = 0
    LLenarCombo CboTipoProducto, "tip_nombre", "tip_id", "par_tipos_productos", "tip_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "tip_nombre"
    LLenarCombo CboMarca, "mar_nombre", "mar_id", "par_marcas", "mar_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "mar_nombre"
    LLenarCombo CboBodega, "bod_nombre", "bod_id", "par_bodegas", "bod_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "bod_nombre"
    LLenarCombo CboUme, "ume_nombre", "ume_id", "sis_unidad_medida", "ume_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ume_nombre"
    CboBodega.ListIndex = 0
    CboMarca.ListIndex = 0
    CboInventariable.ListIndex = 0
    'Aplicar_skin Me
    Skin2 Me, , 5
   ' CargaImpuestos
   
    If SG_codigo = Empty Then
        'Asumimos que es nuevo producto
        Bm_Nuevo = True
    Else
        'Aqui editamos el producto
        
        Bm_Nuevo = False
        Sql = "SELECT codigo,tip_id,mar_id,descripcion," & _
                    "IFNULL((SELECT pro_ultimo_precio_compra FROM pro_stock s WHERE s.rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=maestro_productos.codigo LIMIT 1),0) precio_compra," & _
                    "porciento_utilidad,precio_venta,margen," & _
                    "IFNULL((SELECT SUM(sto_stock) FROM pro_stock s WHERE rut_emp='" & SP_Rut_Activo & "' AND s.pro_codigo=maestro_productos.codigo),0) stock," & _
                     "stock_critico,bod_id,ubicacion_bodega,comentario,pro_inventariable,ume_id,pro_precio_sin_flete,pro_precio_flete,pro_activo " & _
              "FROM maestro_productos  " & _
              "WHERE  rut_emp='" & SP_Rut_Activo & "' AND codigo='" & SG_codigo & "'"
        Consulta RsTmp, Sql
        If RsTmp.RecordCount = 0 Then Exit Sub
        
        With RsTmp
            TxtCodigo = !Codigo
            Busca_Id_Combo CboTipoProducto, !tip_id
            Busca_Id_Combo CboMarca, !mar_id
            TxtDescripcion = !Descripcion
            
            Me.TxtPorcentaje = !porciento_utilidad
            Me.TxtPrecioVta = !precio_venta
            LbMargen = !Margen
            TxtStockActual = !stock
            TxtStockCritico = !stock_critico
            Busca_Id_Combo CboBodega, !bod_id
            txtUbicacionBodega = !ubicacion_bodega
            TxtComentario = !comentario
            TxtPrecioCostoSFlete = !pro_precio_sin_flete
            TxtFlete = !pro_precio_flete
            TxtPrecioCompra = !precio_compra
            Me.CboInventariable.ListIndex = IIf(!pro_inventariable = "SI", 0, 1)
            CboHabilitado.ListIndex = IIf(!pro_activo = "SI", 0, 1)
            Busca_Id_Combo CboUme, !ume_id
        End With
        TxtCodigo.Locked = True
   End If
   
End Sub
Private Sub CargaImpuestos()
    Sql = "SELECT imp_id,imp_nombre,imp_adicional " & _
          "FROM par_impuestos " & _
          "WHERE imp_activo='SI'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, True, True, True, False
    
    
    
End Sub
Private Sub Timer1_Timer()
    On Error GoTo Herror
    If Len(Me.TxtCodigo.Text) > 0 And _
        Me.CboTipoProducto.ListIndex > -1 And _
        Len(Me.TxtDescripcion.Text) > 0 And _
        Val(Me.TxtPrecioCompra.Text) > 0 And _
        Val(Me.TxtPrecioVta.Text) > 0 And _
        Me.LbMargen.Caption <> "" And _
        Me.TxtStockActual.Text <> "" And _
        CboInventariable.ListIndex > -1 And _
        Me.TxtStockCritico.Text <> "" Then
        Me.CmdGuardar.Enabled = True
    Else
        Me.CmdGuardar.ToolTipText = "Para poder grabar debe completar todos los campos"
        Me.CmdGuardar.Enabled = False
    End If
    For Each TXTsx In Controls
        If (TypeOf TXTsx Is TextBox) Then
            If Me.ActiveControl.Name = TXTsx.Name Then 'Foco activo
                TXTsx.BackColor = IIf(TXTsx.Locked, ClrDesha, ClrCfoco)
            Else
                TXTsx.BackColor = IIf(TXTsx.Locked, ClrDesha, ClrSfoco)
            End If
        End If
    Next
    Exit Sub
    
Herror:
'error
End Sub

Private Sub Timer2_Timer()
    If Bm_Nuevo Then
        TxtCodigo.SetFocus
    Else
        CboTipoProducto.SetFocus
    End If
    Timer2.Enabled = False
    Timer1.Enabled = True
End Sub

Private Sub TxtCodigo_GotFocus()
    FrameAyuda = "Codigo"
    skAyuda = "Aqui se ingresa codigo unico, puede ser alfanumerico "
End Sub

Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
    Dim letra As String    'Aqui controlamos las teclas
    letra = UCase(Chr(KeyAscii)) 'a mayuscula el caractere ingresado
    KeyAscii = Asc(letra) 'recupero el codigo ascci del caractar ya transofrmado
End Sub

Private Sub TxtCodigo_LostFocus()
    skAyuda = Empty
    FrameAyuda = ""
End Sub

Private Sub TxtCodigo_Validate(Cancel As Boolean)
    If AccionProducto <> 2 Then
        If Len(TxtCodigo) = 0 Then Exit Sub
        Sql = "SELECT codigo FROM maestro_productos WHERE  rut_emp='" & SP_Rut_Activo & "' AND codigo='" & TxtCodigo & "'"
        Call Consulta(RsTmp, Sql)
        If RsTmp.RecordCount > 0 Then
            MsgBox "Codigo ya existe ", vbOKOnly + vbInformation
            Exit Sub
        End If
    End If
End Sub

Private Sub TxtDescripcion_GotFocus()
    En_Foco TxtDescripcion
    FrameAyuda = "Descripci�n"
    skAyuda = "Aqui se ingresa la descripci�n del producto "
End Sub

Private Sub txtDescripcion_KeyPress(KeyAscii As Integer)
    Dim letra As String    'Aqui controlamos las teclas
    letra = UCase(Chr(KeyAscii)) 'a mayuscula el caractere ingresado
    KeyAscii = Asc(letra) 'recupero el codigo ascci del caractar ya transofrmado
End Sub



Private Sub TxtDescripcion_LostFocus()
    skAyuda = Empty
    FrameAyuda = ""
End Sub

Private Sub TxtFlete_Change()
    TxtPrecioCompra = Val(TxtPrecioCostoSFlete) + Val(TxtFlete)
End Sub

Private Sub TxtFlete_GotFocus()
    En_Foco TxtFlete
End Sub

Private Sub TxtFlete_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtFlete_Validate(Cancel As Boolean)
    If Val(TxtFlete) = 0 Then TxtFlete = "0"
End Sub

Private Sub TxtPorcentaje_GotFocus()
    En_Foco TxtPorcentaje
    
End Sub

Private Sub TxtPorcentaje_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPorcentaje_Validate(Cancel As Boolean)
    If Val(Me.TxtPorcentaje.Text) > 0 Then
        If Val(TxtPrecioCompra.Text) = 0 Then Me.TxtPrecioCompra.Text = 1
        Me.TxtPrecioVta.Text = Round(Val(Me.TxtPrecioCompra) + (Val(Me.TxtPrecioCompra.Text) / 100 * Val(Me.TxtPorcentaje.Text)), 0)
        Me.LbMargen.Caption = Val(Me.TxtPrecioVta) - Val(Me.TxtPrecioCompra)
    Else
        TxtPorcentaje = 0
    End If
End Sub

Private Sub TxtPrecioCompra_GotFocus()
    En_Foco TxtPrecioCompra
End Sub

Private Sub TxtPrecioCompra_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPrecioCompra_Validate(Cancel As Boolean)
    If Val(TxtPrecioCompra.Text) = 0 Then Me.TxtPrecioCompra.Text = 1
    
    
    
    If Val(Me.TxtPrecioVta.Text) > 1 And _
        Val(Me.TxtPrecioVta) > Val(Me.TxtPrecioCompra) Then
            
        Me.LbMargen.Caption = Val(Me.TxtPrecioVta) - Val(Me.TxtPrecioCompra)
        Me.TxtPorcentaje.Text = Format(Val(Me.LbMargen.Caption) * 100 / Val(Me.TxtPrecioCompra.Text), "###.##")
        Me.TxtPorcentaje.Text = Replace(Me.TxtPorcentaje.Text, ",", ".")
    End If
End Sub

Private Sub TxtPrecioCostoSFlete_Change()
    TxtPrecioCompra = Val(TxtPrecioCostoSFlete) + Val(TxtFlete)
End Sub

Private Sub TxtPrecioCostoSFlete_GotFocus()
    En_Foco TxtPrecioCostoSFlete
End Sub

Private Sub TxtPrecioCostoSFlete_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPrecioCostoSFlete_Validate(Cancel As Boolean)
    If Val(TxtPrecioCostoSFlete) = 0 Then Me.TxtPrecioCostoSFlete = "0"
End Sub

Private Sub TxtPrecioVta_GotFocus()
    En_Foco TxtPrecioVta
End Sub

Private Sub TxtPrecioVta_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtPrecioVta_Validate(Cancel As Boolean)
    If Val(TxtPrecioCompra.Text) = 0 Then Me.TxtPrecioCompra.Text = 1
    Me.LbMargen.Caption = Val(Me.TxtPrecioVta) - Val(Me.TxtPrecioCompra)
    Me.TxtPorcentaje.Text = Format(Val(Me.LbMargen.Caption) * 100 / Val(Me.TxtPrecioCompra.Text), "###.##")
    Me.TxtPorcentaje.Text = Replace(Me.TxtPorcentaje.Text, ",", ".")
End Sub

Private Sub TxtStockActual_GotFocus()
    En_Foco TxtStockActual
End Sub

Private Sub TxtStockActual_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub TxtStockCritico_GotFocus()
    En_Foco TxtStockCritico
End Sub

Private Sub TxtStockCritico_KeyPress(KeyAscii As Integer)
    KeyAscii = AceptaSoloNumeros(KeyAscii)
End Sub

Private Sub txtUbicacion_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase$(Chr(KeyAscii)))
End Sub



Private Sub txtUbicacionBodega_GotFocus()
    En_Foco txtUbicacionBodega
    FrameAyuda = "UBICACION BODEGA :"
    skAyuda = " Se refiere al lugar o estante f�sico donde se encuentra el producto"
End Sub

Private Sub txtUbicacionBodega_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtUbicacionBodega_LostFocus()
    skAyuda = Empty
    FrameAyuda = ""
End Sub
