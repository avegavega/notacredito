VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form AccesoFlor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ACCESO"
   ClientHeight    =   5595
   ClientLeft      =   3585
   ClientTop       =   2625
   ClientWidth     =   6840
   Icon            =   "AccesoFlor.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3305.713
   ScaleMode       =   0  'User
   ScaleWidth      =   6422.388
   ShowInTaskbar   =   0   'False
   Begin ACTIVESKINLibCtl.SkinLabel SkVersion 
      Height          =   195
      Left            =   3720
      OleObjectBlob   =   "AccesoFlor.frx":0442
      TabIndex        =   15
      Top             =   5280
      Width           =   3045
   End
   Begin VB.PictureBox PicLogo 
      Height          =   1215
      Left            =   0
      ScaleHeight     =   1155
      ScaleWidth      =   6810
      TabIndex        =   14
      Top             =   45
      Width           =   6870
   End
   Begin VB.Frame LogoEmpresa 
      Caption         =   "INGRESE SUS DATOS"
      Height          =   3915
      Left            =   -135
      TabIndex        =   10
      Top             =   1230
      Width           =   6825
      Begin VB.Frame Frame3 
         Caption         =   "Tipo de Conexion"
         Height          =   1485
         Left            =   1470
         TabIndex        =   13
         Top             =   1935
         Width           =   4080
         Begin VB.TextBox TxtRed 
            Height          =   285
            Left            =   1500
            TabIndex        =   6
            Text            =   "192.168.1.100"
            Top             =   690
            Width           =   2025
         End
         Begin VB.OptionButton Opt_Red 
            Caption         =   "Red Local"
            Height          =   195
            Left            =   360
            TabIndex        =   5
            Top             =   705
            Width           =   1230
         End
         Begin VB.OptionButton Opt_Local 
            Caption         =   "Local (localhost)"
            Height          =   255
            Left            =   345
            TabIndex        =   4
            Top             =   375
            Value           =   -1  'True
            Width           =   1935
         End
         Begin VB.OptionButton Opt_Remota 
            Caption         =   "Remota"
            Height          =   255
            Left            =   360
            TabIndex        =   7
            Top             =   1020
            Width           =   1080
         End
         Begin VB.TextBox TxtRemota 
            Height          =   285
            Left            =   1500
            TabIndex        =   8
            Text            =   "www.empresa.cl"
            Top             =   1005
            Width           =   2025
         End
      End
      Begin VB.TextBox txtUserName 
         Height          =   345
         Left            =   5700
         TabIndex        =   3
         Top             =   1590
         Visible         =   0   'False
         Width           =   2325
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel2 
         Height          =   255
         Left            =   495
         OleObjectBlob   =   "AccesoFlor.frx":04BC
         TabIndex        =   12
         Top             =   1635
         Width           =   2055
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   285
         OleObjectBlob   =   "AccesoFlor.frx":0526
         TabIndex        =   11
         Top             =   600
         Width           =   945
      End
      Begin VB.TextBox txtPassword 
         Height          =   345
         IMEMode         =   3  'DISABLE
         Left            =   2700
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   1575
         Width           =   2325
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   4095
         TabIndex        =   9
         Top             =   3480
         Width           =   1380
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   1440
         TabIndex        =   2
         Top             =   3480
         Width           =   1380
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   1410
         Left            =   1530
         TabIndex        =   0
         Top             =   150
         Width           =   4035
         _ExtentX        =   7117
         _ExtentY        =   2487
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "T1000"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1500"
            Text            =   "Nombre Usuario"
            Object.Width           =   5292
         EndProperty
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   555
      OleObjectBlob   =   "AccesoFlor.frx":058A
      Top             =   1365
   End
End
Attribute VB_Name = "AccesoFlor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmdCancel_Click()
    'establecer la variable global a false
    'para indicar un inicio de sesi�n fallido
    LoginSucceeded = False
    Me.Hide
    End
End Sub

Private Sub CmdOk_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    txtUserName = LvDetalle.SelectedItem
    If Len(txtPassword) = 0 Then txtPassword.SetFocus
    'comprobar si la contrase�a es correcta
    Dim Ip_Version_Actual As Integer
    Ip_Version_Actual = App.Major & App.Minor & App.Revision
    SkVersion = "Version " & Ip_Version_Actual
    If Len(Me.txtUserName) = 0 Or Len(Me.txtPassword) = 0 Then Exit Sub
     
    If Me.Opt_Red.Value Or Me.Opt_Remota Then
        If Opt_Red.Value Then
            If Len(TxtRed) = 0 Then
                MsgBox "No ha ingresado una IP valida...", vbInformation
                TxtRed.SetFocus
            End If
        Else
            If Len(TxtRemota) = 0 Then
                MsgBox "No ha ingresado una IP valida...", vbInformation
                TxtRed.SetFocus
            End If
        End If
    End If
    
    If Me.Opt_Local.Value Then Sp_Servidor = "localhost"
    If Me.Opt_Red.Value Then Sp_Servidor = TxtRed
    If Me.Opt_Remota.Value Then Sp_Servidor = TxtRemota
    'Cargamos los parametros
    Main
    
    
    If BG_FalloConexion Then
        BG_FalloConexion = False
        Exit Sub
    End If
    
    If Ip_Version_Actual < IG_Version Then
        'MsgBox "Esta utilizando una version anterior del sistema"
        Sis_NecesarioActualizar.Show 1
        End
    End If
    If Len(Me.txtUserName) > 0 And Len(Me.txtPassword) > 0 Then
        
        Sql = "SELECT usu_id,usu_login,usu_nombre,u.per_id,p.per_nombre " & _
              "FROM sis_usuarios u INNER JOIN sis_perfiles p USING(per_id) " & _
              "WHERE usu_activo='SI' AND usu_login = '" & Me.txtUserName & "' and usu_pwd = MD5('" & Me.txtPassword & "')"
        Consulta RSUsuarios, Sql
        If RSUsuarios.RecordCount > 0 Then
            'Usuario aceptado
            LoginSucceeded = True
            LogIdUsuario = RSUsuarios!usu_id
            LogUsuario = Me.txtUserName
            LogPass = Me.txtPassword
            LogPerfil = RSUsuarios!per_id
            Me.Hide
            
            
            
           Principal.TxtNombre = RSUsuarios!usu_nombre
           Principal.TxtPerfil = RSUsuarios!per_nombre
            
            Sp_Archivo = App.Path & "\conecta.ini"
            X = FreeFile
            Open Sp_Archivo For Output As X
                If Opt_Local.Value Then Print #X, "1"
                If Opt_Red.Value Then Print #X, "2"
                If Opt_Remota.Value Then Print #X, "3"
                Print #X, TxtRed
                Print #X, TxtRemota
                
            Close X
            
            Principal.Show
        Else
            MsgBox "La contrase�a no es v�lida. Vuelva a intentarlo", , "Inicio de sesi�n"
            txtPassword.SetFocus
            SendKeys "{Home}+{End}"
        End If
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys vbKeyTab
    End If
End Sub

Private Sub Form_Load()
   
    Dim Sp_Archivo As String
    
    PicLogo = LoadPicture(App.Path & "\REDMAROK.jpg")
    
    Ip_Version_Actual = App.Major & "." & App.Minor & "." & App.Revision
    SkVersion = "Versi�n " & Ip_Version_Actual
    
    
    SG_Funcion_Equipo = "SI"
    Sp_Archivo = Dir(App.Path & "\funcion.ini")
    If Sp_Archivo = "" Then
        MsgBox "Falta archivo de funcion, imposible continuar...", vbExclamation
        End
    Else
        X = FreeFile
        On Error GoTo Sigue
        Open App.Path & "\funcion.ini" For Input As X
            Line Input #X, Sp_Archivo
            If Len(Sp_Archivo) = 0 Then
                MsgBox "Fallo en archivo de funcion..., imposible continuar", vbExclamation
                End
            Else
                SG_Funcion_Equipo = Sp_Archivo
            End If
            Sp_Archivo = ""
        Close X
    End If
    
    
    Sp_Archivo = Dir(App.Path & "\conecta.ini")
    If Sp_Archivo = "" Then
        Opt_Local.Value = True
        'No existe
    Else
        Sp_Archivo = App.Path & "\conecta.ini"
        X = FreeFile
        On Error GoTo Sigue
        Open Sp_Archivo For Input As X
            Line Input #X, Sp_Archivo
            If Val(Sp_Archivo) = 1 Then Opt_Local.Value = True
            If Val(Sp_Archivo) = 2 Then Opt_Red.Value = True
            If Val(Sp_Archivo) = 3 Then Opt_Remota.Value = True
            Line Input #X, Sp_Archivo
            TxtRed = Sp_Archivo
            Line Input #X, Sp_Archivo
            TxtRemota = Sp_Archivo
        Close X
    End If
    Close X
    Aplicar_skin Me
    Centrar Me
    
    If Me.Opt_Local.Value Then Sp_Servidor = "localhost"
    If Me.Opt_Red.Value Then Sp_Servidor = TxtRed
    If Me.Opt_Remota.Value Then Sp_Servidor = TxtRemota
    'Cargamos los parametros
    Main
    
    Sql = "SELECT usu_login,usu_nombre " & _
          "FROM sis_usuarios " & _
          "WHERE usu_activo='SI'"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    Exit Sub
Sigue:
    MsgBox Err.Description & vbNewLine & Err.Number
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub LvDetalle_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    txtUserName = LvDetalle.SelectedItem
End Sub

Private Sub LvDetalle_GotFocus()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    txtUserName = LvDetalle.SelectedItem
End Sub

Private Sub LvDetalle_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        txtUserName = LvDetalle.SelectedItem
        txtPassword.SetFocus
    End If
End Sub

Private Sub txtPassword_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub TxtPassword_KeyPress(KeyAscii As Integer)
     KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
Private Sub TxtRemota_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub txtUserName_GotFocus()
    En_Foco Me.ActiveControl
End Sub

Private Sub txtUserName_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys vbKeyTab
    End If
    KeyAscii = Asc(UCase(Chr(KeyAscii)))

End Sub

Private Sub txtUserName_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        SendKeys vbKeyTab
    End If
End Sub
