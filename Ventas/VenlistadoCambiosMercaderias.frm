VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form VenlistadoCambiosMercaderias 
   Caption         =   "Cambios de mercader�a"
   ClientHeight    =   9495
   ClientLeft      =   -1785
   ClientTop       =   2460
   ClientWidth     =   13710
   LinkTopic       =   "Form1"
   ScaleHeight     =   9495
   ScaleWidth      =   13710
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   360
      OleObjectBlob   =   "VenlistadoCambiosMercaderias.frx":0000
      Top             =   5970
   End
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   780
      Top             =   6750
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1395
      Left            =   360
      TabIndex        =   10
      Top             =   300
      Width           =   4995
      Begin VB.TextBox TxtBusqueda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   150
         TabIndex        =   11
         Top             =   525
         Width           =   4710
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   165
         OleObjectBlob   =   "VenlistadoCambiosMercaderias.frx":0234
         TabIndex        =   12
         Top             =   285
         Width           =   2295
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1395
      Left            =   5325
      TabIndex        =   2
      Top             =   300
      Width           =   2970
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "VenlistadoCambiosMercaderias.frx":0299
         Left            =   135
         List            =   "VenlistadoCambiosMercaderias.frx":029B
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   450
         Width           =   1575
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1710
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   435
         Width           =   1215
      End
      Begin VB.CheckBox ChkFecha 
         Height          =   225
         Left            =   210
         TabIndex        =   3
         Top             =   1035
         Width           =   225
      End
      Begin MSComCtl2.DTPicker DtDesde 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   480
         TabIndex        =   4
         Top             =   1005
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   66256897
         CurrentDate     =   41001
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "VenlistadoCambiosMercaderias.frx":029D
         TabIndex        =   7
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   1725
         OleObjectBlob   =   "VenlistadoCambiosMercaderias.frx":0301
         TabIndex        =   8
         Top             =   255
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   1680
         TabIndex        =   9
         Top             =   1005
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   66256897
         CurrentDate     =   41001
      End
   End
   Begin VB.CommandButton CmdTodos 
      Caption         =   "Todos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   13470
      TabIndex        =   1
      ToolTipText     =   "Muestra todas las compras"
      Top             =   1365
      Width           =   1095
   End
   Begin VB.CommandButton CmdBusca 
      Caption         =   "Buscar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   0
      ToolTipText     =   "Busca texto ingresado"
      Top             =   1290
      Width           =   1095
   End
   Begin MSComctlLib.ListView LvCambios 
      Height          =   3480
      Left            =   375
      TabIndex        =   13
      Top             =   1785
      Width           =   9195
      _ExtentX        =   16219
      _ExtentY        =   6138
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Tag             =   "T3000"
         Text            =   "Id"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Object.Tag             =   "F1000"
         Text            =   "Fecha"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Object.Tag             =   "T3000"
         Text            =   "Usuario"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Object.Tag             =   "T2000"
         Text            =   "Documeneto"
         Object.Width           =   3175
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Tag             =   "N109"
         Text            =   "Nro Documento"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "VenlistadoCambiosMercaderias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdBusca_Click()
    CargaCambios
End Sub

Private Sub CargaCambios()
    Sql = "SELECT cam_id,cam_fecha, cam_usuario, doc_nombre,no_documento " & _
            "FROM ven_cambio_mercaderia m " & _
            "JOIN ven_doc_venta v ON m.id=v.id " & _
            "JOIN sis_documentos d ON v.doc_id=d.doc_id"
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvCambios, False, True, True, False

End Sub

Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
    CargaCambios
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    LvCambios.SetFocus
    Timer1.Enabled = False
End Sub
