VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{DA729E34-689F-49EA-A856-B57046630B73}#1.0#0"; "progressbar-xp.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form VtaListadoFlor 
   Caption         =   "Listado Ventas"
   ClientHeight    =   8985
   ClientLeft      =   60
   ClientTop       =   1830
   ClientWidth     =   15120
   LinkTopic       =   "Form1"
   ScaleHeight     =   8985
   ScaleWidth      =   15120
   Begin VB.Frame frmOts 
      Caption         =   "Listado historico"
      Height          =   6630
      Left            =   150
      TabIndex        =   7
      Top             =   1440
      Width           =   14790
      Begin VB.TextBox TxtCredito 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12270
         Locked          =   -1  'True
         TabIndex        =   33
         Top             =   5685
         Width           =   1650
      End
      Begin VB.TextBox TxtLocal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   10605
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   5685
         Width           =   1650
      End
      Begin VB.TextBox TxtClientes 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8940
         Locked          =   -1  'True
         TabIndex        =   31
         Top             =   5685
         Width           =   1650
      End
      Begin VB.TextBox TxtTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   10455
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   6180
         Width           =   3420
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   5295
         Left            =   240
         TabIndex        =   8
         Top             =   315
         Width           =   14430
         _ExtentX        =   25453
         _ExtentY        =   9340
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   13
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N100"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Fecha"
            Object.Width           =   1852
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "T1000"
            Text            =   "Nombre Cliente"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "T2500"
            Text            =   "Tipo Documento"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Object.Tag             =   "N109"
            Text            =   "Nro Doc"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "T1500"
            Text            =   "Movimiento"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Tag             =   "T2500"
            Text            =   "Vendedor"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Tag             =   "N100"
            Text            =   "Abonos"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Key             =   "clientes"
            Object.Tag             =   "N100"
            Text            =   "Contado Clientes"
            Object.Width           =   2910
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Key             =   "local"
            Object.Tag             =   "N100"
            Text            =   "Contado Local"
            Object.Width           =   2910
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Key             =   "credito"
            Object.Tag             =   "N100"
            Text            =   "Credito"
            Object.Width           =   2910
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Object.Tag             =   "N109"
            Text            =   "Boleta Final"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel3 
         Height          =   255
         Left            =   9030
         OleObjectBlob   =   "VtaListadoFlor.frx":0000
         TabIndex        =   27
         Top             =   6255
         Width           =   1290
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel6 
         Height          =   255
         Left            =   90
         OleObjectBlob   =   "VtaListadoFlor.frx":006D
         TabIndex        =   30
         Top             =   6090
         Width           =   8055
      End
   End
   Begin VB.CommandButton CmdVtaConBoletas 
      Caption         =   "F4 - Ventas con Boletas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2745
      TabIndex        =   29
      ToolTipText     =   "Nueva compra"
      Top             =   8355
      Width           =   2595
   End
   Begin VB.Frame Frame2 
      Caption         =   "Estado documento"
      Height          =   750
      Left            =   8025
      TabIndex        =   23
      Top             =   -30
      Width           =   3285
      Begin VB.ComboBox CboPago 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   225
         Style           =   2  'Dropdown List
         TabIndex        =   26
         Top             =   315
         Width           =   2880
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Vendedor"
      Height          =   600
      Left            =   8040
      TabIndex        =   24
      Top             =   765
      Width           =   3285
      Begin VB.ComboBox CboVendedores 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   210
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   210
         Width           =   2880
      End
   End
   Begin VB.CommandButton CmdBusca 
      Caption         =   "Buscar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11745
      TabIndex        =   19
      ToolTipText     =   "Busca texto ingresado"
      Top             =   855
      Width           =   1095
   End
   Begin VB.CommandButton CmdTodos 
      Caption         =   "Todos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12870
      TabIndex        =   18
      ToolTipText     =   "Muestra todas las compras"
      Top             =   855
      Width           =   1095
   End
   Begin VB.CommandButton cmdExportar 
      Caption         =   "F3- Exportar a Excel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10260
      TabIndex        =   17
      ToolTipText     =   "Exportar"
      Top             =   8355
      Width           =   2055
   End
   Begin VB.Frame FraProgreso 
      Caption         =   "Progreso exportaci�n"
      Height          =   795
      Left            =   1170
      TabIndex        =   15
      Top             =   4260
      Visible         =   0   'False
      Width           =   11430
      Begin Proyecto2.XP_ProgressBar BarraProgreso 
         Height          =   330
         Left            =   150
         TabIndex        =   16
         Top             =   315
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   582
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BrushStyle      =   0
         Color           =   16777088
         Scrolling       =   1
         ShowText        =   -1  'True
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Mes y A�o"
      Height          =   1395
      Left            =   5100
      TabIndex        =   9
      Top             =   -30
      Width           =   2970
      Begin VB.CheckBox ChkFecha 
         Height          =   225
         Left            =   210
         TabIndex        =   22
         Top             =   1035
         Width           =   225
      End
      Begin MSComCtl2.DTPicker DtDesde 
         CausesValidation=   0   'False
         Height          =   285
         Left            =   480
         TabIndex        =   20
         Top             =   1005
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   20905985
         CurrentDate     =   41001
      End
      Begin VB.ComboBox ComAno 
         Height          =   315
         Left            =   1695
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   450
         Width           =   1215
      End
      Begin VB.ComboBox comMes 
         Height          =   315
         ItemData        =   "VtaListadoFlor.frx":0157
         Left            =   135
         List            =   "VtaListadoFlor.frx":0159
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   450
         Width           =   1575
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   255
         Left            =   120
         OleObjectBlob   =   "VtaListadoFlor.frx":015B
         TabIndex        =   12
         Top             =   255
         Width           =   375
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel5 
         Height          =   255
         Left            =   1725
         OleObjectBlob   =   "VtaListadoFlor.frx":01BF
         TabIndex        =   13
         Top             =   255
         Width           =   375
      End
      Begin MSComCtl2.DTPicker DtHasta 
         Height          =   285
         Left            =   1680
         TabIndex        =   21
         Top             =   1005
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   20905985
         CurrentDate     =   41001
      End
   End
   Begin VB.CommandButton CmdSalir 
      Cancel          =   -1  'True
      Caption         =   "Esc  -Retornar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   12480
      TabIndex        =   6
      ToolTipText     =   "Salir"
      Top             =   8355
      Width           =   2055
   End
   Begin VB.CommandButton CmdSeleccionar 
      Caption         =   "F2 - Ver"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7875
      TabIndex        =   5
      ToolTipText     =   "Visualizar compra"
      Top             =   8355
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      Height          =   1395
      Left            =   135
      TabIndex        =   2
      Top             =   -30
      Width           =   4995
      Begin VB.TextBox TxtBusqueda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   150
         TabIndex        =   3
         Top             =   525
         Width           =   4710
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel1 
         Height          =   255
         Left            =   165
         OleObjectBlob   =   "VtaListadoFlor.frx":0223
         TabIndex        =   4
         Top             =   285
         Width           =   2295
      End
   End
   Begin VB.CommandButton CmdNueva 
      Caption         =   "F1 - Nueva"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5535
      TabIndex        =   1
      ToolTipText     =   "Nueva compra"
      Top             =   8355
      Width           =   2175
   End
   Begin VB.CommandButton CmdEliminaDocumento 
      Caption         =   "Eliminar Documento "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   495
      TabIndex        =   0
      ToolTipText     =   "Exportar"
      Top             =   8355
      Width           =   2055
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   -15
      OleObjectBlob   =   "VtaListadoFlor.frx":0288
      Top             =   8070
   End
   Begin ACTIVESKINLibCtl.SkinLabel SkEmpresaActiva 
      Height          =   255
      Left            =   7440
      OleObjectBlob   =   "VtaListadoFlor.frx":04BC
      TabIndex        =   14
      Top             =   30
      Width           =   6780
   End
   Begin VB.Menu mn 
      Caption         =   "Tareas"
      Visible         =   0   'False
      Begin VB.Menu mn_n 
         Caption         =   "Nuevo"
      End
      Begin VB.Menu mn_v 
         Caption         =   "Ver seleccionada"
      End
   End
End
Attribute VB_Name = "VtaListadoFlor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sp_Condicion As String
Private Sub cmdMesAno_Click()
    CargaDatos
End Sub
Private Sub ChkFecha_Click()
    On Error Resume Next
    If ChkFecha.Value = 1 Then
        comMes.Enabled = False
        ComAno.Enabled = False
        DtDesde.Enabled = True
        DtHasta.Enabled = True
        DtDesde.SetFocus
    Else
        comMes.SetFocus
        DtDesde.Enabled = False
        DtHasta.Enabled = False
        comMes.Enabled = True
        ComAno.Enabled = True
    End If
End Sub

Private Sub CmdBusca_Click()
    Dim Ip_IdPago As Integer
    If Len(TxtBusqueda) = 0 Then
        sp_Condicion = " AND 1=1 "
    Else
        sp_Condicion = " AND nombre_cliente LIKE '" & TxtBusqueda & "%' "
    End If
    If ChkFecha.Value = 1 Then
        sp_Condicion = sp_Condicion & " AND fecha BETWEEN '" & Fql(DtDesde.Value) & "' AND '" & Fql(DtHasta.Value) & "' "
    Else
        sp_Condicion = sp_Condicion & " AND MONTH(fecha)=" & comMes.ItemData(comMes.ListIndex) & " AND YEAR(fecha)=" & IIf(ComAno.ListCount = 0, Year(Date), ComAno.Text)
    End If
    
    
     If CboVendedores.ListIndex > -1 Then
        If CboVendedores.Text = "TODOS" Then
            sp_Condicion = sp_Condicion
        Else
            sp_Condicion = sp_Condicion & " AND ven_id=" & CboVendedores.ItemData(CboVendedores.ListIndex)
        End If
    End If
    
    
    Ip_IdPago = CboPago.ItemData(CboPago.ListIndex)
    Select Case Ip_IdPago
        Case Is = 1
            sp_Condicion = sp_Condicion
        Case Is = 2
            sp_Condicion = sp_Condicion & " HAVING abonos  < bruto"
        Case Is = 3
            sp_Condicion = sp_Condicion & " HAVING abonos = bruto "
    End Select
    
   
    CargaDatos
End Sub

Private Sub CmdEliminaDocumento_Click()
 Dim s_Inventario As String, L_Unico As Long, s_mov As String
 Dim rs_Detalle As Recordset, Sp_Llave As String, Sp_Bodega As Integer, Lp_IdAbono As Long
 Dim s_FacturaGuias As String * 2
    Dim Sp_Codigos As String
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    L_Unico = LvDetalle.SelectedItem.Text
    SG_codigo2 = Empty
    sis_InputBox.FramBox = "Ingrese llave para anulacion"
    sis_InputBox.Show 1
    Sp_Llave = UCase(SG_codigo2)
    If Len(Sp_Llave) = 0 Then Exit Sub
    Sql = "SELECT usu_id,usu_nombre,usu_login " & _
          "FROM sis_usuarios " & _
          "WHERE usu_pwd = MD5('" & Sp_Llave & "')"
    Consulta RsTmp3, Sql
    
    
    '1ro Extraeremos el abo_id en el caso que exista 21-01-2012
    Sql = "SELECT abo_id " & _
          "FROM cta_abono_documentos d " & _
          "INNER JOIN cta_abonos a USING(abo_id) " & _
          "WHERE abo_cli_pro='CLI' AND a.rut_emp ='" & SP_Rut_Activo & "' AND d.rut_emp ='" & SP_Rut_Activo & "' AND id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
                '2DO SABREMOS QUE TIPO DE ABONO FUE,
            Lp_IdAbono = RsTmp!abo_id
            Sql = "SELECT abo_id,abo_fecha,mpa_nombre,usu_nombre,a.mpa_id,abo_fecha_pago " & _
                  "FROM cta_abonos a " & _
                  "INNER JOIN par_medios_de_pago m USING(mpa_id) " & _
                  "INNER JOIN cta_abono_documentos d USING(abo_id) " & _
                  "WHERE  abo_cli_pro='PRO' AND a.rut_emp='" & SP_Rut_Activo & "' AND d.rut_emp='" & SP_Rut_Activo & "' AND a.abo_id=" & Lp_IdAbono
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                If RsTmp.RecordCount > 1 Then
                    'Ojo que el abono de este documento fue junto con varios documento
                    'por lo tanto no debemos permitir eliminar este documento
                    MsgBox "No es posible eliminar este documento..." & vbNewLine & "Es parte de un abono de multiples documentos " & _
                    vbNewLine & "Cant. Documentos " & RsTmp.RecordCount & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                    Exit Sub
                End If
                If RsTmp.RecordCount = 1 Then
                    If RsTmp!mpa_id <> 1 Then
                        MsgBox "No es posible eliminar este documento..." & vbNewLine & "Ya se realiz� abono ... " & _
                        vbNewLine & " Fecha Abono " & Format(RsTmp!abo_fecha_pago, "DD-MM-YYYY") & " Usuario:" & RsTmp!usu_nombre & " Medio de pago:" & RsTmp!mpa_nombre, vbExclamation
                        Exit Sub
                    Else
                        'El abono es solo a un documento y en contado(efectivo) por lo que no es problema eliminarlo
                    End If
                End If
            
            
            End If
            
            If MsgBox("El documento tiene un abono registrado ..." & vbNewLine & "Nro Comprobante XX " & vbNewLine & "�Continuar?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
            
    Else
        'El documento es al credito pero no tiene abonos, es posible continuar, no afectara nada
    
    End If
    
    
    
    If RsTmp3.RecordCount > 0 Then
        X = InputBox("Usuario:" & RsTmp3!usu_nombre & vbNewLine & "Ingrese motivo", "Motivo de eliminaci�n")
        If Len(X) = 0 Then
            MsgBox "No ingreso motivo, no fue eliminado el documento"
            Exit Sub
        End If
        
        With LvDetalle.SelectedItem
             Sql = "INSERT INTO sis_eliminacion_documentos (id,eli_fecha,eli_motivo,usu_login,eli_ven_com) VALUES (" & _
                   LvDetalle.SelectedItem & ",'" & Fql(Date) & "','" & UCase(X) & "','" & RsTmp3!usu_login & "','VEN')"
            
            'Sql = "INSERT INTO com_historial_eliminaciones (doc_id,no_documento,rut_proveedor,eli_fecha,eli_motivo,usu_id) VALUES (" & _
                        .SubItems(7) & "," & .SubItems(4) & ",'" & .SubItems(8) & "','" & Format(Now, "YYYY-DD-MM HH:MM:SS") & "','" & X & "'," & RsTmp3!usu_id & ")"
            
        End With
            
    Else
        MsgBox "Contrase�a ingresada no fue encontrada...", vbInformation
        Exit Sub
    End If
    On Error GoTo ErrorElimina
    cn.BeginTrans 'COMENZAMOS LA TRANSACCION
    cn.Execute Sql
    
    Sql = "DELETE FROM cta_abonos " & _
          "WHERE abo_cli_pro='CLI' AND rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    cn.Execute Sql
    Sql = "DELETE FROM cta_abono_documentos " & _
          "WHERE rut_emp='" & SP_Rut_Activo & "' AND abo_id=" & Lp_IdAbono
    cn.Execute Sql
    
    Sql = "SELECT rut_cliente rut,nombre_cliente nombre_proveedor,doc_movimiento inventario," & _
                 "no_documento,c.doc_id,doc_nombre,bod_id,doc_factura_guias " & _
          "FROM ven_doc_venta c,sis_documentos d " & _
          "WHERE d.doc_id=c.doc_id AND id=" & L_Unico
    Consulta RsTmp, Sql
    If RsTmp.RecordCount > 0 Then
        Sp_Bodega = RsTmp!bod_id
        s_FacturaGuias = RsTmp!doc_factura_guias
        If RsTmp!Inventario = "ENTRADA" Then s_mov = "SALIDA"
        If RsTmp!Inventario = "SALIDA" Then s_mov = "ENTRADA"
        
        Sql = "SELECT d.codigo pro_codigo,unidades cantidad," & _
              "(SELECT pro_precio_neto*d.unidades " & _
               "FROM inv_kardex k " & _
               "WHERE k.rut_emp='" & SP_Rut_Activo & "' AND k.pro_codigo=d.codigo ORDER BY kar_id DESC LIMIT 1) total_linea " & _
              "FROM ven_detalle d " & _
              "INNER JOIN maestro_productos m ON d.codigo=m.codigo " & _
              "WHERE m.rut_emp='" & SP_Rut_Activo & "' AND  pro_inventariable='SI' AND d.rut_emp='" & SP_Rut_Activo & "' AND no_documento=" & RsTmp!no_documento & " AND doc_id=" & RsTmp!doc_id
        Consulta rs_Detalle, Sql
        If rs_Detalle.RecordCount > 0 Then
            rs_Detalle.MoveFirst
            Do While Not rs_Detalle.EOF
                Kardex Format(Date, "YYYY-MM-DD"), s_mov, RsTmp!doc_id, RsTmp!no_documento, Sp_Bodega, _
                    rs_Detalle!pro_codigo, rs_Detalle!cantidad, "ELIMINA " & RsTmp!doc_nombre & " Nro:" & RsTmp!no_documento, Round(rs_Detalle!total_linea / rs_Detalle!cantidad), _
                    rs_Detalle!total_linea, RsTmp!Rut, RsTmp!nombre_proveedor, "NO", Round(rs_Detalle!total_linea / rs_Detalle!cantidad)
                    
                rs_Detalle.MoveNext
            Loop
        End If
        
        'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS QUE HACEN REFERENCIA AL QUE SE ESTA INGRESANDO
        Sql = "UPDATE ven_doc_venta " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
              "WHERE id_ref=" & L_Unico
        cn.Execute Sql
                
        'DEBEMOS CONSULTAR SI EXISTEN DOCUMENTOS FACTURADOS CON ESTE DOCUMENTO PARA LIBERARLOS Abril 2012
        If s_FacturaGuias = "SI" Then
            Sql = "UPDATE ven_doc_venta " & _
                     "SET id_ref=0,doc_id_factura=0,nro_factura=0 " & _
                  "WHERE nro_factura=" & RsTmp!no_documento & " AND doc_id_factura=" & RsTmp!doc_id & " " & _
                       "AND rut_emp='" & SP_Rut_Activo & "'"
            cn.Execute Sql
        End If
                
                
        If RsTmp!Inventario = "NN" Then
            '28 Abril 2011
            'No modifica inventario
            'por lo tanto podriamos eliminar el documeneto sin tocar el
            'kardex ni el stock
        End If
        'multidelete
        
        If SP_Control_Inventario = "SI" Then
            'Antes de eliminar los registros
            'Debemos saber si hay algun activo inmovilizado
            Sql = "SELECT aim_id " & _
                    "FROM ven_detalle d " & _
                    "JOIN ven_doc_venta v ON (v.no_documento=d.no_documento AND v.doc_id=d.doc_id) " & _
                    "WHERE aim_id>0 AND v.rut_emp='" & SP_Rut_Activo & "' AND  v.id=" & L_Unico
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_fecha_egreso=Null,aim_habilitado='SI' " & _
                                    "WHERE aim_id=" & RsTmp!aim_id
                    RsTmp.MoveNext
                Loop
            End If
        
        
            Sql = "DELETE FROM " & _
                  "ven_doc_venta, " & _
                  "ven_detalle " & _
                  "USING ven_doc_venta " & _
                  "LEFT JOIN ven_detalle ON (ven_doc_venta.no_documento=ven_detalle.no_documento AND ven_doc_venta.doc_id=ven_detalle.doc_id) " & _
                  "WHERE ven_doc_venta.rut_emp='" & SP_Rut_Activo & "' AND  ven_doc_venta.id=" & L_Unico
            'Consulta RsTmp, Sql
        Else
            'Antes de eliminar los registros
            'Debemos saber si hay algun activo inmovilizado
            Sql = "SELECT aim_id " & _
                    "FROM ven_sin_detalle " & _
                    "WHERE aim_id>0 AND id=" & L_Unico
            Consulta RsTmp, Sql
            If RsTmp.RecordCount > 0 Then
                RsTmp.MoveFirst
                Do While Not RsTmp.EOF
                    cn.Execute "UPDATE con_activo_inmovilizado " & _
                                    "SET aim_fecha_egreso=Null,aim_habilitado='SI' " & _
                                    "WHERE aim_id=" & RsTmp!aim_id
                    RsTmp.MoveNext
                Loop
            End If
            
        
        
            Sql = "DELETE FROM " & _
                  "ven_doc_venta, " & _
                  "ven_sin_detalle " & _
                  "USING ven_doc_venta " & _
                  "LEFT JOIN ven_sin_detalle USING(id) " & _
                  "WHERE ven_doc_venta.id=" & L_Unico
            'Consulta RsTmp, Sql
        End If
        cn.Execute Sql
    End If
    cn.CommitTrans
    CargaDatos
    Exit Sub
ErrorElimina:
    MsgBox "Ocurrio un error al eliminar documento"
    cn.RollbackTrans
End Sub

Private Sub cmdExportar_Click()
    Dim tit(2) As String
    If LvDetalle.ListItems.Count = 0 Then Exit Sub
    FraProgreso.Visible = True
    tit(0) = Me.Caption & " " & DtFecha & " - " & Time
    tit(1) = ""
    tit(2) = ""
    ExportarNuevo LvDetalle, tit, Me, BarraProgreso
    FraProgreso.Visible = False
End Sub

Private Sub CmdNueva_Click()
    On Error GoTo fina
    DG_ID_Unico = 0
    
    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("2,10", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    
    VtaDirecta.Tag = "NUEVA"
    VtaDirecta.Show 1
    CargaDatos
    Exit Sub
fina:
End Sub

Private Sub CmdSalir_Click()
    On Error Resume Next
    Unload Me
End Sub

Private Sub CmdSeleccionar_Click()
    If LvDetalle.SelectedItem Is Nothing Then Exit Sub
    If LvDetalle.SelectedItem.SubItems(5) = "NULO" Then
        MsgBox "Documento NULO, no se puede editar...", vbInformation
        Exit Sub
    End If
    DG_ID_Unico = LvDetalle.SelectedItem.Text
    
    If Val(LvDetalle.SelectedItem.SubItems(9)) > 0 Then
       'Se trata de ingreso de boletas
        VtaBoletas.Show 1
        
    Else
        VtaDirecta.Tag = "VIEJA"
        VtaDirecta.Show 1
    End If
    CargaDatos
End Sub

Private Sub CmdTodos_Click()
    sp_Condicion = ""
    CargaDatos
End Sub

Private Sub CmdVtaConBoletas_Click()


    If Principal.CmdMenu(8).Visible = True Then
        If ConsultaCentralizado("2,10", IG_Mes_Contable, IG_Ano_Contable) Then
            MsgBox "periodo contable ya ha sido centralizado, " & vbNewLine & "No puede modificar"
            Exit Sub
        End If
    End If
    
    
    DG_ID_Unico = 0
    VtaBoletas.Show 1
    CargaDatos
End Sub

Private Sub ComAno_Click()
'CargaDatos
End Sub
Private Sub comMes_Click()
'    CargaDatos
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then CmdNueva_Click
    If KeyCode = vbKeyF2 Then CmdSeleccionar_Click
    If KeyCode = vbKeyF3 Then cmdExportar_Click
    If KeyCode = vbKeyF4 Then CmdVtaConBoletas_Click
End Sub

Private Sub Form_Load()
   'Skin2 Me, , 3
    'Skin2 Me, , 5
    Aplicar_skin Me
    'FILTRO ESTADO DE PAGOS DE DOCUMENTOS
    CboPago.AddItem "TODOS"
    CboPago.ItemData(CboPago.ListCount - 1) = 1
    CboPago.AddItem "CON DEUDA"
    CboPago.ItemData(CboPago.ListCount - 1) = 2
    CboPago.AddItem "SIN DEUDA"
    CboPago.ItemData(CboPago.ListCount - 1) = 3
    CboPago.ListIndex = 0
        
    If SP_Control_Inventario = "SI" Then
        CmdVtaConBoletas.Visible = False
    End If
        
        
        
    DtDesde.Value = Date
    DtHasta.Value = Date
    Centrar Me
    SkEmpresaActiva = SP_Empresa_Activa
    sp_Condicion = ""
    For i = 1 To 12
        comMes.AddItem UCase(MonthName(i, False))
        comMes.ItemData(comMes.ListCount - 1) = i
    Next
    Busca_Id_Combo comMes, Val(IG_Mes_Contable)
    LLenaYears ComAno, 2010
   ' For i = Year(Date) To Year(Date) - 1 Step -1
   '     ComAno.AddItem i
   '     ComAno.ItemData(ComAno.ListCount - 1) = i
   ' Next
   ' Busca_Id_Combo ComAno, Val(IG_Ano_Contable)
    
    'VENDEDORES PARA FILTRO
    LLenarCombo CboVendedores, "ven_nombre", "ven_id", "par_vendedores", "ven_activo='SI' AND rut_emp='" & SP_Rut_Activo & "'", "ven_nombre"
    CboVendedores.AddItem "TODOS"
    CboVendedores.ListIndex = CboVendedores.ListCount - 1
    CmdBusca_Click
End Sub
Private Sub CargaDatos()
'IF( doc_factura_guias='SI','{ Por Guia(s) }',''),IF(nro_factura>0,' {FACTURADA} ',''))
    Sql = "SELECT id,fecha,nombre_cliente,CONCAT(tipo_doc,' ',IF(doc_factura_guias='SI','POR GUIA(S)','')  ," & _
                "IF(nro_factura>0,' {FACTURADA} ','')) tipo_d    ,no_documento," & _
                " IF(ven_boleta_hasta>0,CONCAT(' AL ',CAST(ven_boleta_hasta AS CHAR)), tipo_movimiento)," & _
                "bruto * IF(doc_signo_libro='-',-1,1)  ,ven_nombre,IFNULL((SELECT SUM(d.ctd_monto) " & _
                                            "FROM cta_abono_documentos d " & _
                                            "INNER JOIN cta_abonos a USING(abo_id) " & _
                                            "WHERE a.abo_cli_pro='CLI' AND d.id=ven_doc_venta.id " & _
                                            "GROUP BY d.id),0) abonos, " & _
                    "IF(pla_grupo=1,bruto *IF(doc_signo_libro = '-' ,- 1,1),0) CONTADO_CLIENTES, " & _
                    "IF(pla_grupo=2,bruto *IF(doc_signo_libro = '-' ,- 1,1),0) CONTADO_LOCAL, " & _
                    "IF(pla_grupo=3,bruto *IF(doc_signo_libro = '-' ,- 1,1),0) CREDITO, " & _
                    "ven_boleta_hasta " & _
          "FROM ven_doc_venta " & _
          "JOIN sis_documentos d USING(doc_id) " & _
          "JOIN par_plazos_vencimiento p ON ven_doc_venta.ven_plazo_id=p.pla_id " & _
          "WHERE ven_estado_nota_venta='PROCESO' AND rut_emp='" & SP_Rut_Activo & "' " & sp_Condicion
    Consulta RsTmp, Sql
    LLenar_Grilla RsTmp, Me, LvDetalle, False, True, True, False
    '6 y 9
    For i = 1 To LvDetalle.ListItems.Count
        If CDbl(LvDetalle.ListItems(i).SubItems(6)) = CDbl(LvDetalle.ListItems(i).SubItems(8)) Then
            For X = 1 To LvDetalle.ColumnHeaders.Count - 1
                LvDetalle.ListItems(i).ListSubItems(X).ForeColor = vbBlue
            Next
        Else
            For X = 1 To LvDetalle.ColumnHeaders.Count - 1
                LvDetalle.ListItems(i).ListSubItems(X).ForeColor = vbRed
            Next
        End If
    Next
   ' TxtTotal = NumFormat(TotalizaColumna(LvDetalle, "total"))
    TxtClientes = NumFormat(TotalizaColumna(LvDetalle, "clientes"))
    TxtLocal = NumFormat(TotalizaColumna(LvDetalle, "local"))
    TxtCredito = NumFormat(TotalizaColumna(LvDetalle, "credito"))
    txtTotal = NumFormat(CDbl(TxtClientes) + CDbl(TxtLocal) + CDbl(TxtCredito))
End Sub





Private Sub LvDetalle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ordListView ColumnHeader, Me, LvDetalle
End Sub

Private Sub LvDetalle_DblClick()
    CmdSeleccionar_Click
End Sub

Private Sub TxtBusqueda_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


