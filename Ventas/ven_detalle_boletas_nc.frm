VERSION 5.00
Object = "{74848F95-A02A-4286-AF0C-A3C755E4A5B3}#1.0#0"; "actskn43.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form ven_detalle_boletas_nc 
   Caption         =   "Boletas para Emitir NC"
   ClientHeight    =   5415
   ClientLeft      =   6540
   ClientTop       =   2610
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   5415
   ScaleWidth      =   11880
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   0
      Top             =   0
   End
   Begin VB.Frame Frame6 
      Caption         =   "Seleccione Boletas para Nota de Credito"
      Height          =   5055
      Left            =   150
      TabIndex        =   0
      Top             =   195
      Width           =   11535
      Begin VB.ComboBox CboStock 
         Height          =   315
         Left            =   8475
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   510
         Width           =   1890
      End
      Begin VB.CommandButton CmdAceptar 
         Caption         =   "&Generar Nota de Credito"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   8460
         TabIndex        =   13
         Top             =   4395
         Width           =   2850
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         Height          =   315
         Left            =   10380
         TabIndex        =   7
         Top             =   495
         Width           =   930
      End
      Begin VB.TextBox txtTotalDocBuscado 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7170
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   510
         Width           =   1290
      End
      Begin VB.TextBox txtFechaDocBuscado 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   6090
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   510
         Width           =   1065
      End
      Begin VB.TextBox txtNroDocumentoBuscar 
         BackColor       =   &H8000000E&
         Height          =   315
         Left            =   4335
         TabIndex        =   4
         Top             =   510
         Width           =   1740
      End
      Begin VB.ComboBox CboDocVenta 
         Height          =   315
         Left            =   165
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   510
         Width           =   4170
      End
      Begin VB.Frame Frame2 
         Caption         =   "Total Nota de Credito"
         Height          =   630
         Left            =   4125
         TabIndex        =   1
         Top             =   4245
         Width           =   4125
         Begin VB.TextBox TxtTotalNC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H0080FF80&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   990
            TabIndex        =   2
            Text            =   "0"
            ToolTipText     =   "Total NC"
            Top             =   285
            Width           =   2085
         End
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Index           =   9
         Left            =   180
         OleObjectBlob   =   "ven_detalle_boletas_nc.frx":0000
         TabIndex        =   8
         Top             =   315
         Width           =   1980
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Index           =   10
         Left            =   4365
         OleObjectBlob   =   "ven_detalle_boletas_nc.frx":0070
         TabIndex        =   9
         Top             =   300
         Width           =   1020
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   195
         Index           =   11
         Left            =   6090
         OleObjectBlob   =   "ven_detalle_boletas_nc.frx":00D4
         TabIndex        =   10
         Top             =   285
         Width           =   870
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Index           =   12
         Left            =   7215
         OleObjectBlob   =   "ven_detalle_boletas_nc.frx":013C
         TabIndex        =   11
         Top             =   315
         Width           =   1185
      End
      Begin MSComctlLib.ListView LvDetalle 
         Height          =   3270
         Left            =   150
         TabIndex        =   12
         Top             =   855
         Width           =   11190
         _ExtentX        =   19738
         _ExtentY        =   5768
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "N109"
            Text            =   "Id Unico"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Tag             =   "T1000"
            Text            =   "Documento"
            Object.Width           =   7355
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Tag             =   "N109"
            Text            =   "Nro"
            Object.Width           =   3140
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Tag             =   "F1000"
            Text            =   "Fecha"
            Object.Width           =   1896
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Key             =   "total"
            Object.Tag             =   "N100"
            Text            =   "Total"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Tag             =   "N109"
            Text            =   "doc_id"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Object.Tag             =   "N109"
            Text            =   "doc_sii"
            Object.Width           =   0
         EndProperty
      End
      Begin ACTIVESKINLibCtl.SkinLabel SkinLabel4 
         Height          =   210
         Index           =   0
         Left            =   8490
         OleObjectBlob   =   "ven_detalle_boletas_nc.frx":01AE
         TabIndex        =   15
         Top             =   300
         Width           =   1980
      End
   End
   Begin ACTIVESKINLibCtl.Skin Skin1 
      Left            =   60
      OleObjectBlob   =   "ven_detalle_boletas_nc.frx":0234
      Top             =   5220
   End
End
Attribute VB_Name = "ven_detalle_boletas_nc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    Aplicar_skin Me
    Centrar Me
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    CboDocVenta.SetFocus
    Timer1.Enabled = False
End Sub


