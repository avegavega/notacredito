VERSION 5.00
Begin VB.Form Sis_Previsualizar 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFFFF&
   Caption         =   "Form1"
   ClientHeight    =   10020
   ClientLeft      =   2970
   ClientTop       =   1200
   ClientWidth     =   11370
   FillColor       =   &H00C0C0C0&
   ForeColor       =   &H00000000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   10020
   ScaleWidth      =   11370
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   0
      ScaleHeight     =   585
      ScaleWidth      =   11340
      TabIndex        =   0
      Top             =   9405
      Width           =   11370
      Begin VB.ComboBox Combo1 
         Height          =   315
         Index           =   0
         ItemData        =   "Sis_Previsualizar.frx":0000
         Left            =   960
         List            =   "Sis_Previsualizar.frx":0019
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   120
         Width           =   855
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Index           =   1
         ItemData        =   "Sis_Previsualizar.frx":003B
         Left            =   3000
         List            =   "Sis_Previsualizar.frx":0048
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   120
         Width           =   1095
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Index           =   2
         ItemData        =   "Sis_Previsualizar.frx":0061
         Left            =   5520
         List            =   "Sis_Previsualizar.frx":007A
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   120
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Margen"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   6
         Top             =   120
         Width           =   735
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Alineaci�n"
         Height          =   255
         Index           =   1
         Left            =   2040
         TabIndex        =   5
         Top             =   120
         Width           =   735
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Espacio vertical"
         Height          =   195
         Index           =   2
         Left            =   4200
         TabIndex        =   4
         Top             =   120
         Width           =   1125
      End
   End
End
Attribute VB_Name = "Sis_Previsualizar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private WithEvents cText As clstextformat
Attribute cText.VB_VarHelpID = -1

Private Sub Combo1_Click(Index As Integer)
    If Me.Visible Then
        Form_Resize
    End If
End Sub

Private Sub Form_Load()
    Set cText = New clstextformat
    Combo1(0).ListIndex = 4
    Combo1(1).ListIndex = 0
    Combo1(2).ListIndex = 4
End Sub

Private Sub Form_Resize()
    With cText
        
        Me.Cls
        .Clear ' borrar todo
        
        .Font.Name = "Verdana"
        .Font.Size = 8
        .Margin = Combo1(0).Text
        .SpacingLine = CLng(Combo1(2).Text)
        
        
        ''''''''''''''''''''''''''
        
        .AddParagraph Combo1(1).ListIndex
        .AddText "Microsoft Visual Basic .... ", , 24
        .AddText " VB ", , 40, vbWhite, vbBlack
        
        .DrawLine vbBlack, vbDot
        
        .AddParagraph Combo1(1).ListIndex
        .AddText "Visual Basic es un lenguaje de programaci�n desarrollado por Alan Cooper para Microsoft. El lenguaje de programaci�n es un dialecto de BASIC, con importantes a�adidos. Su primera versi�n fue presentada en 1991 con la intenci�n de simplificar la programaci�n utilizando un ambiente de desarrollo completamente gr�fico que facilitara la creaci�n de interfaces gr�ficas y en cierta medida tambi�n la programaci�n misma. En 2001 Microsoft propone abandonar el desarrollo basado en la API Win32 y pasar a trabajar sobre un framework o marco com�n de librer�as independiente de la version del sistema operativo, .NET Framework, a trav�s de Visual Basic .NET (y otros lenguajes como C-Sharp (C#) de f�cil transici�n de c�digo entre ellos) que presenta serias incompatibilidades con el c�digo Visual Basic existente."

        .DrawLine &HC0C0C0, vbSolid
        
        Call cText.Draw(Me)
        
    End With
End Sub
